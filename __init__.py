"""
Package providing Model-Centric Debugging capabilities to GDB.
"""

import sys, traceback
import time, os, sys
import inspect

PY3 = sys.version[0] == '3'

from . import my_logging; my_logging.setup_log()
import logging

log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

log_user.info("Loading mcgdb Python environment...")

try:
    import gdb
    assert "built-in" in str(gdb.breakpoints) # make sure we have the right gdb package
except Exception as e:
    import importlib
    if (not ("No module named" in str(e) and "gdb" in str(e)) and 
        not ("module 'gdb' has no attribute 'breakpoints'" in str(e))):
        log.error("Cannot import gdb: {}".format(e))
        
    log.critical("Importing DUMMY gdb module")
    dummy_gdb_path = "{}/documentation/dummy_modules".format(__path__[0])
    sys.path.insert(0, dummy_gdb_path)

    try:
        importlib.reload(gdb)
    except NameError: # name 'gdb' is not defined
        import gdb

my_logging.info_log()

log.info("Loading mcgdb interaction/representation/capture ...")
from . import interaction, representation, capture
log.info("Loading mcgdb toolbox ...")
from .toolbox import target, python_utils
from .toolbox.python_utils import internal, info
from .toolbox import register_model, toggle_activate_submodules

log.info("Loading mcgdb models ...")
try:
    from . import model
except Exception as e:
    log_user.fatal("###                              ###")
    log_user.fatal("### Loading mcgdb models failed. ###")
    log_user.fatal("###                              ###")
    log_user.exception(e)
log_user.info("Loading of mcgdb environment completed.")
    
###################
    
def initialize_by_name():
    with open("/proc/self/cmdline") as cmdline:
        line = cmdline.readline()
        gdb_binname = line.split("\x00")[0]
        
    if "mcgdb" not in gdb_binname:
        log_user.warn("*Not* loading mcgdb debugging extention.")
        log_user.warn("Run `py import mcgdb; mcgdb.initialize()` to load it manually.")
        return
    
    initialize()
    
    load_by_name(gdb_binname)
    
def load_by_name(name):
    log.info("Enabling mcgdb models by name ... ({})".format(name))
    toolbox.load_models_by_name(name)
    log.info("Finished enabling mcgdb models by name.")
    
@internal
def initialize():
    """
    Initialization bootstrap.
    """
    log.info("Initializing mcgdb environment ...")
    
    sys.excepthook = info

    import mcgdb
    python_utils.initializePackage(mcgdb)
    
    log.info("Initialized of mcgdb environment completed.")
    
    
