"""
Module implementing communication representationing.
Purpose somehow unclear ...
"""

from mcgdb.toolbox import my_gdb
from . import CommEntity

@my_gdb.Switchable
@my_gdb.Dicted
class CommInferior(CommEntity):
    """
    A communication entity based on GDB's inferior (aka process)

    :var inferior: GDB's inferior object associated with this entity.
    :var pid: the PID of the inferior.
    """
    
    def __init__(self, inferior):
        CommEntity.__init__(self)
        my_gdb.Switchable.__init__(self)
        CommInferior.init_dict(inferior.pid, self)
        
        self.inferior = inferior
        self.pid = inferior.pid                            
    
    def assert_self_consistency(self):
        """
        :raises gdb.error: if `self.pid` is -1.
        :raises gdb.error: if `self.inferior` is `None`
        """
        if self.pid == -1:
            assert self.inferior is None
            raise gdb.error("Empty comm_inf") 
        
        if self.inferior is None:
            raise gdb.error("Not attached")
        
        if not self.inferior.is_valid():
            raise gdb.error("Invalid inferior")
    
    def exit_or_detach(self):
        """
        Disconnects the interior linked with this entity.
        Detach it if it is live in the system,
        Exit it if it is dead.
        """
        
        self.assert_self_consistency()
        
        if my_system.is_alive(self.pid):
            self.detach()
        else:
            self.exit()
            
    def check_useless(self):
        """
        :returns: True if the list of endpoints is empty.
        """
        if self.pid == None and len(self.endpoints) == 0:
            try:
                del CommInferior.dict_[self.pid]
            except KeyError:
                pass
            
    def detach(self):
        """
        Cuts the binding with the inferior.
        """
        self.inferior = None
    
    def do_exit(self):
        """
        Removes itself from the list of known inferiors
        and exits the associated endpoints.
        """
        
        del CommInferior.dict_[self.pid]
        
        self.inferior = None
        
        self.pid = None
        
        for endpoint in self.endpoints:
            endpoint.comm_ent_exit()
        self.endpoints = {}
        
    def __str__(self):
        if self.inferior is None:
            num = None
        elif not self.inferior.is_valid():
            self.exit_or_detach()
            return "CommInferior[exited]"
        else:
            num = self.inferior.num
        return "CommInferior[%s, %s]" % (self.pid, num)
    
    def is_alive(self):
        """
        :returns: True if the entity still knows its PID.
        """
        return self.pid != None
    
    @staticmethod
    def get_current_CommInferior():
        """
        :returns: the entity associated with GDB's selected inferior.
        :rtype: CommInferior
        """
        return CommInferior.pid_to_CommInferior(gdb.selected_inferior().pid)
    
    @staticmethod
    def pid_to_CommInferior(pid):
        """
        :returns: the entity associated with `pid`.
        :rtype: CommInferior
        :raises ValueError: if `pid` is not a process #module-communication.connection_mon
        """
        pid = int(pid)
        if pid in CommInferior.dict_.keys():
            return CommInferior.dict_[pid]
        else:
            inf = my_gdb.pid_to_inf_obj(pid)
            if inf is None:
                raise ValueError("'%d' doesn't match any inferior PID" % pid)
            else:
                return CommInferior(inf)
        

def ConnectionMonitor_handle_exit(event):
    """
    To be connected to `gdb.events.exited`.
    Triggers entities' exit method with they are no nore alive in the system.
    """
    
    to_remove = []
    for pid in CommInferior.dict_.keys():
        if not my_system.is_alive(pid):
            to_remove.append(pid)
            
    while to_remove:
        CommInferior.dict_[to_remove.pop()].exit()


@my_gdb.virtual
class CommunicationPrimitive:
    """
    A communication operation primitive.

    :var IS_BLOCKING: True if the communication is blocking.
    :var IS_CONTINOUS: True if the communication is continuous.
    """
    
    primitives = []
    
    def __init__(self, IS_BLOCKING=True, IS_CONTINOUS=True):
        self.IS_BLOCKING = IS_BLOCKING
        self.IS_CONTINOUS = IS_CONTINOUS
        
        self.__class__.primitives.append(self)

class IndependentCommunicatingEndpoint():
    """
    A communication endpoint not connected to an entity.
    """
    comm_endpoints = []
    
    def __init__(self):
        self.comm_primitives = {}
        self.__class__.comm_endpoints.append(self)
    
    def begin_comm(self, comm_primitive):
        """
        Begins a communication over `comm_primitive` operation.

        :param comm_primitive: the primitive on which the communication is done.
        :type comm_primitive: CommunicationPrimitive
        """
        
        print ("begin %s in thr %d" % (comm_primitive.location, my_gdb.current_infthr()[1].num))
        self.comm_primitives[comm_primitive] = my_gdb.current_infthr()
    
    def finish_comm(self, comm_primitive):
        """
        Closes the communication over `comm_primitive` operation.

        :param comm_primitive: the primitive on which the communication is done.
        :type comm_primitive: CommunicationPrimitive
        """
        
        print ("finish %s in thr %d" % (comm_primitive.location, )
                                       my_gdb.current_infthr()[1].num)
        assert self.comm_primitives[comm_primitive] == my_gdb.current_infthr()
        del self.comm_primitives[comm_primitive]

    def __del__(self):
        """
        Removes itself from the list of active independent endpoints.
        Log warnings if some communication were still active.
        """
        
        self.__class__.comm_endpoints.pop(self)
        if len(self.comm_primitives):
            my_gdb.log.warning("%s:" % self)
        while len(self.comm_primitives):
            my_gdb.log.warning("%s was still in communication",
                               self.comm_primitives.keys().pop())

    def get_comm_primitives(self):
        """
        :returns: the list of active communication primitives.
        :rtype: list(CommunicationPrimitive)
        """
        return self.comm_primitives.keys()
    
    def get_remote_endpoints(self):
        """
        :returns: None if the endpoint is not connected to a link.
        :returns: the list of the other endpoints connected to this endpoint's link.
        :rtype: list(Endpoint)
        """
        if self.link is None: 
            return None

        return [endpoint for endpoint in self.link.endpoints if endpoint is not self]
        
    @staticmethod
    def get_selected_inf_communicating_endpoints():
        """
        :returns: the list of the endpoints with communication primitives associated with the current infthr.
        :rtype: list(Endpoints)
        """
        endpoints = []
        current_infthr = my_gdb.current_infthr()
        for comm_endpoint in IndependentCommunicatingEndpoint.comm_endpoints:
            for primitive, infthr in comm_endpoint.comm_primitives.items():
                if infthr == current_infthr:
                    endpoints.append((primitive, comm_endpoint))
        return endpoints


################
@my_gdb.internal
def initialize():
    gdb.events.exited.connect(ConnectionMonitor_handle_exit)
