"""
Module proving GDB user commands for connection representationing.
"""
import gdb

# import for initialization
from .. import representation
from . import my_gdb as repre_my_gdb
from ..toolbox import my_gdb
from . import aspect

class param_verbose(gdb.Parameter):
    __self = None

    @classmethod
    def get_verbose(clazz, topic=None):
        try:
            return clazz.__self.value
        except:
            # not initialized
            return 1
    
    def __init__ (self):
        gdb.Parameter.__init__(self, "mcgdb-verbose",
                               gdb.COMMAND_OBSCURE,
                               gdb.PARAM_INTEGER)
        self.value = 1
        assert param_verbose.__self is None
        param_verbose.__self = self
        
    def get_set_string(self):
        return "mcGDB verbose level set to {}".format(self.value)
        
    def get_show_string (self, svalue):
        return "mcGDB verbose level is {}".format(svalue)

#####################
### Stop Requests ###
#####################

def push_stop_requests(stop_next, msgs):
    """
    :param stop_next: if True, pushes a stop request with no message.
    :param msgs: messages to push in the stop requests.
    :type msgs: list(str)
    """
    if stop_next:
        push_stop_request()
    for msg in msgs:
        push_stop_request(msg)

stop_requests = []
def push_stop_request(msg=None):
    """
    Register a stop request for the next stop-point.

    :param msg: Reason for the execution stop, or None.
    """
    stop_requests.append(msg)

def cancel_stop_request(msg):
    stop_requests.remove(msg)
    
def clean_stop_requests(evt=None):
    del stop_requests[:]
    
@my_gdb.internal
def proceed_stop_requests():
    """
    Tells if stop stop request were registered since the previous call.
    Logs the reasons, if any.
    Cleans the list afterwards.

    :returns: True if GDB should stop the execution.
    """
    
    stop = len(stop_requests) != 0
    for msg in stop_requests:
        if msg != None:
            print(msg)
    del stop_requests[:]

    return stop



########################
## GDB CLI extensions ##
########################

class cmd_info_connection (gdb.Command):
    """Displays the links in place between the inferiors"""

    def __init__ (self):
        gdb.Command.__init__ (self, "info connections", gdb.COMMAND_NONE)

    def invoke (self, arg, from_tty):
        
        selected = []
        rest = str(arg)
        while rest != "":
            first, part, rest = rest.partition(" ")
            if first == '.':
                current_compo = \
                   CommEntity.get_selected_component(silent=True)
                if current_compo:
                    selected.append(current_compo.numbers[CommEntity])
            elif first.isdigit():
                selected.append(int(first))
            else:
                my_gdb.log.warning("argument '%s' not recognized", first) 

        if not CommEntity.list_:
            print ("No active connection.")
            
        is_first = True
        print ("Inf.\tEndpoint\tLink\t\tRemote side")
        for i, comm_ent in enumerated(CommEntity.list_):
            number = comm_ent.numbers[CommEntity]
            if not selected and number not in selected:
                continue
            if not is_first:
                print ("")
                is_first = False
                
            print ("#%d %s" % (number, comm_ent))
            for endpoint in comm_ent.endpoints:
                comm = ""
                if (isinstance(endpoint, IndependentCommunicatingEndpoint) 
                    and endpoint.get_comm_primitives()):
                    comm = " --> %s" \
                           % ", ".join([str(x) for x in \
                                              endpoint.get_comm_primitives()])
                    
                print ("\t%s%s" % (endpoint, comm))
                if endpoint.siblings:
                    for sibling in endpoint.siblings:
                        if sibling is not endpoint:
                            print ("\t(%s)" % sibling     )
                    
                if endpoint.link is None:
                    continue
                print ("\t\t\t%s" % endpoint.link)
                for rmt_endpoint in endpoint.link.endpoints:
                    if rmt_endpoint is not endpoint:
                        print ("\t\t\t\t\t%s\t%s #%d" 
                              % (rmt_endpoint, rmt_endpoint.comm_entity, 
                                 rmt_endpoint.comm_entity.numbers[CommEntity]))
cmd_info_conn = None

class cmd_info_links (gdb.Command):
    """Displays the links in place between the inferiors"""

    def __init__ (self):
        gdb.Command.__init__ (self, "info links", gdb.COMMAND_NONE)

    def invoke (self, arg, from_tty):    
        selected = []
        rest = str(arg)
        while rest != "":
            first, part, rest = rest.partition(" ")
            if first.isdigit():
                selected.append(int(first))
            else:
                my_gdb.log.warning("argument '%s' not recognized", first) 
                    
        print ("Link\tEndpoint\t\tCommInf")
        for i, link in enumerated(Link.list_):
            number = link.numbers[Link]
            if not selected and number not in selected:
                continue

            link_printed = False
            for endpoint in link.endpoints:
                if not link_printed:
                    print ("#%d %s" % (number, link))
                    link_printed = True
                    
                print ("\t%s"     % endpoint)
                print ("\t\t\t%s" % endpoint.comm_entity)
                for sibling in endpoint.siblings:
                    if sibling is not endpoint:
                        print ("\t(%s\t%s)" % (sibling, sibling.comm_entity))
            if not link_printed:
                print ("%s" % link)

class cmd_mcgdb(gdb.Command):
    def __init__(self):
        gdb.Command.__init__ (self, "mcgdb", gdb.COMMAND_NONE, prefix=True)
    def invoke (self, arg, from_tty):
        import pdb;pdb.set_trace()
        pass
    
def preInitialize():
    gdb.Command("mcgdb", gdb.COMMAND_NONE, prefix=1)
    gdb.Command("communication", gdb.COMMAND_NONE, prefix=1)
    gdb.Command("communication deprecated", gdb.COMMAND_NONE, prefix=1)
    gdb.Command("communication switch", gdb.COMMAND_NONE, prefix=1)
    gdb.Command("message", gdb.COMMAND_NONE, prefix=1)
    
@my_gdb.internal
def initialize():
    global cmd_info_conn
    cmd_info_conn = cmd_info_connection()
    cmd_info_links()
    param_verbose()
    #gdb.events.cont.connect(clean_stop_requests)
    
