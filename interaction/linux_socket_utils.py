import re
import gdb

from mcgdb.toolbox.target import my_access

PROC_NETTCP_LINE = "\s+(\d+):\s+(\S+):(\S+)\s+(\S+):" \
                   "(\S+)\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+\S+\s+(\S+)"
(PROC_NETTCP_LINE_ID, PROC_NETTCP_LOCAL_ADDR, PROC_NETTCP_LOCAL_PORT, 
 PROC_NETTCP_REMOT_ADDR, PROC_NETTCP_REMOT_PORT, 
 PROC_NETTCP_INODE) = range (1, 7)

class cmd_info_nettcp (gdb.Command):
    """Communication Socket prefix"""

    def __init__ (self):
        gdb.Command.__init__ (self, "info nettcp", gdb.COMMAND_NONE)

    def invoke(self, args, from_tty):
        print_net_tcp(args)

def print_net_tcp(tcpfile=None):
    if tcpfile is None or len(tcpfile) == 0:
        tcpfile = "/proc/net/tcp"
    tcplines = my_access.Fopen(tcpfile, "r").readlines()
    serv_sock = []
    serv_sock_conn = []
    local_sock = []
    other = []
    for tcpline in tcplines:
        line = re.match (PROC_NETTCP_LINE, tcpline)
        if line is not None:
            lineid = line.group(PROC_NETTCP_LINE_ID)
            
            inode = line.group(PROC_NETTCP_INODE)
            cur_addr = utils.int_to_dotted_ip(
                         utils.hex_to_dec(line.group(PROC_NETTCP_LOCAL_ADDR)))
            cur_port = utils.hex_to_dec(line.group(PROC_NETTCP_LOCAL_PORT))
            
            rmt_addr = utils.int_to_dotted_ip(
                         utils.hex_to_dec(line.group(PROC_NETTCP_REMOT_ADDR)))
            rmt_port = utils.hex_to_dec(line.group(PROC_NETTCP_REMOT_PORT))
            to_print = "%s-%s)\t\t%s:%s\t\t-->\t\t%s:%s" % \
                       (lineid, inode, cur_addr, cur_port, rmt_addr, rmt_port)
            
            if rmt_addr == "0.0.0.0":
                serv_sock.append(to_print)
            elif inode == "0":
                serv_sock_conn.append(to_print)
            elif cur_addr == "127.0.0.1":
                local_sock.append(to_print)
            else:
                other.append(to_print)
                
    print ("Server sockets listening:")
    for line in serv_sock:
        print (line)
    print ("Server sockets connected:")
    for line in serv_sock_conn:
        print (line)
    print ("Local sockets:")
    for line in local_sock:
        print (line        )
    print ("Other sockets:")
    for line in other:
        print (line)

def initialize():
    cmd_info_nettcp()
    print ("[SocketSystem functions enabled]")

    
