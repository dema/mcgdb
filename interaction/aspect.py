import gdb

from ..toolbox import aspect

class cmd_aspect_list(gdb.Command):    
    def __init__(self, limited=False):
        gdb.Command.__init__ (self, "mcgdb aspects", gdb.COMMAND_NONE, prefix=True)
        
    def invoke (self, args, from_tty):
        max_len = max(map(len, aspect.registered_trackers.keys()))
        for name, aspects in aspect.registered_trackers.items():
            mod = aspects[1].values()[0].__module__
            print("\t{name:{max_len}s}\t{mod}".format(max_len=max_len, name=name, mod=mod))

class cmd_aspect_remove(gdb.Command):
    
    def __init__(self, limited=False):
        gdb.Command.__init__ (self, "mcgdb aspects remove", gdb.COMMAND_NONE)
        
    def invoke (self, argv, from_tty):
        if argv.startswith("all"):
            aspect.registered_trackers.clear()
            print("All aspects removed.")
            return
        
        args = gdb.string_to_argv(argv)
        for arg in args:
            try:
                del aspect.registered_trackers[arg]
                print("Aspect '{}' removed.".format(arg))
            except KeyError:
                print("No aspect with name '{}'".format(arg))
            
def initialize():
    cmd_aspect_list()
    cmd_aspect_remove()
