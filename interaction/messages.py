"""
Module extending GDB's CLI with commands related to messages.
"""

import gdb

import mcgdb

###########################################################"

class cmd_info_messages (gdb.Command):
    """
    Lists the messages stored in the different entities and links.
    Options:
    +|full  Prints all checkpoints crossed by the message.
    #id*    Prints only messages with given IDs
    """
    
    def __init__ (self):
        gdb.Command.__init__(self, "info messages", gdb.COMMAND_OBSCURE)
        
    def invoke (self, arg, from_tty):
        full = False
        boxe_filters = []
        msg_filters = []
        rest = arg
        while rest != "":
            first, part, rest = rest.partition(" ")
            if first in ("full", "+"):
                full = True
            elif first.isdigit():
                msg_filters.append(int(first))
            elif len(first) == 1:
                boxe_filters.append(first)
            else:
                my_gdb.log.warning("argument '%s' not recognized", first) 
        no_msg_filter = len(msg_filters) == 0
        
        def print_messages(box_id, msgs, head):
            is_first = True
            if  no_msg_filter:
                print (head)
                is_first = False
                
            if msgs is None:
                if no_msg_filter:
                    print ("\t Cannot hold messages.")
                return

            for msg in msgs:
                if msg is None:
                    continue
                idx = msg.numbers[mcgdb.representation.Message]
                
                if not no_msg_filter and idx not in msg_filters:
                    continue
                
                if is_first:
                    is_first = False
                    print (head)
                bpk = ""
                if msg.breakpoint:
                    if msg.breakpoint_cnt == 0:
                        bpk = " BPK"
                    elif msg.breakpoint_cnt == 1 :
                        bpk = " BPK (1 hit)"
                    else:
                        bpk = " BPK (%d hits)" % msg.breakpoint_cnt
                        
                print ("\t #%d %s%s" % (idx, msg, bpk),)
                if full:
                    print ("{")
                    print ("\t\t"+",\n\t\t".join(["%s%s" %
                            (id_, " --> %s" % payload if payload else "") for id_, payload in msg.checkpoints]))
                    print ("\t}")
                else:
                    print
        i = "A"
        for ent in mcgdb.representation.CommEntity.list_:
            idx = i
            i = chr(ord(i) + 1)

            if len(boxe_filters) != 0 and idx not in boxe_filters:
                continue
            
            print_messages(idx, ent.get_messages(), "#%s %s" % (idx, ent))
            
        for link in mcgdb.representation.Link.list_:
            idx = i
            i = chr(ord(i) + 1)

            if len(boxe_filters) != 0 and idx not in boxe_filters:
                continue
            
            print_messages(idx, link.get_messages(), 
                           "#%s %s (%s)" % (idx, link, 
                                           link.numbers[mcgdb.representation.Link]))
            
            
    def complete(self, text, word):
        complete = []
        if word.startswith("full"):
            complete.append("full")
        return complete


class cmd_message (gdb.Command):
    """
    Usage: message IDs+  {break|unbreak}
    Set a breakpoint on the operations handling this message.
    """
    def __init__ (self):
        gdb.Command.__init__(self, "message", gdb.COMMAND_OBSCURE)
    
    def invoke (self, arg, from_tty):
        set_break = None
        requested_msgs = []
        rest = arg
        while rest != "":
            first, part, rest = rest.partition(" ")
            if first in ["break"]:
                set_break = True
            elif first in ["unbreak"]:
                set_break = False
            elif first.isdigit():
                requested_msgs.append(int(first))
            else:
                print ("Argument '%s' ignored." % first)
                
        if len(requested_msgs) == 0:
            print ("No message id specified.")
            return
        if set_break is None:
            print ("No action specified.")
            return
        
        for msg in mcgdb.representation.Message.all_messages():
            if msg is None:
                continue
            idx = msg.numbers[mcgdb.representation.Message]
            if idx not in requested_msgs:
                continue
            requested_msgs.remove(idx)
            if set_break is not None:
                msg.breakpoint = set_break
                print ("Breakpoint %s on message #%d" % \
                    (set_break and "set" or "unset", idx))
        if len(requested_msgs) != 0:
            print ("No message with id %s" % ", ".join(requested_msgs))

def initialize():
    cmd_message()
    cmd_info_messages()
