"""
Module extending GDB's CLI with commands related to our target stack.
"""

import gdb

class cmd_comm_target (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "communication target", gdb.COMMAND_NONE,
                             prefix=1)
     
    def invoke (self, args, from_tty):
        print ("nothing so far")
        #for ttype, impls in impls_list.items():
        #    print ("%s:" % ttype.__name__)
        #    for (name, impl) in impls.items():
        #        selected = current_impls[ttype] == impl and "*" or " "
        #        print ("\t%s%s\t->\t%s" % (selected, name, impl))
                
class cmd_comm_target_x (gdb.Command):
    def __init__ (self, name, ttype):
        gdb.Command.__init__ (self, "communication target %s" % name, 
                              gdb.COMMAND_NONE, prefix=1)
        self.type = ttype
        self.name = name
        
    def invoke (self, args, from_tty):
        print ("nothing so far")
        #global current_impls, impls_list
        #for (name, impl) in impls_list[self.type].items():
        #    print ((impl == current_impls[self.type]) and "*" or " ", )
        #    print ("%s\t%s" % (name, impl))

class cmd_comm_target_switch (gdb.Command):
    def __init__ (self, type_name, ttype, name, impl):
        gdb.Command.__init__ (self, 
                              "communication target %s %s" % (type_name, name),
                               gdb.COMMAND_NONE)
        self.type = ttype
        self.name = name
        self.impl = impl
        
    def invoke (self, args, from_tty):
        print ("nothing so far")
        #global current_impls
        #current_impls[self.type] = self.impl
        #print ("Target implementation correctly switched to '%s'" % self.name)

def initialize():
    cmd_comm_target()
