import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")
import mcgdb

from .capture import cudaMalloc
from .capture import cudaMemcpy
from .capture import cudaFree
from .capture import cudaKernel

has_events = False

def initialize():
    mcgdb.register_model("Cuda", "cudaMalloc", mcgdb.toggle_activate_submodules(__name__))
    log_user.debug("Cuda GPU model registered")
