import re

import gdb

import mcgdb
from mcgdb.toolbox import my_gdb
from mcgdb.toolbox.target import my_archi
from mcgdb.model.gpu import representation

name_resolver = []
def get_cudaMallocName(line):
    try:
        var = re.match(r".*cudaMalloc\((.*)\)", line).group(1).split(',')[0].strip()
        if re.match(r"\(.*\)(.*)", var):
            return re.match(r"\(.*\)(.*)", var).group(1).strip()
    except:
        return None

def get_new_buffer_name_line():
    for a_try, a_depth in name_resolver:
        current = gdb.newest_frame()
        for i in range(a_depth):
            current = current.older()
        sal = current.find_sal()
        
        line_no = sal.line
        line = ""
        line = gdb.execute("list %s:%d,%d" % (sal.symtab.filename, line_no, line_no), to_string=True)
        
        a_name = a_try(line)
        if a_name:
            return a_name, "%s (%s:%d)" % (line, sal.symtab.filename, line_no)
    else:
        return None, None

class cudaMallocBP(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func
    
    def __init__(self):
        mcgdb.capture.FunctionBreakpoint.__init__(self, "cudaMalloc")
        
    def prepare_before (self):
        data = {}
        data["name"],  data["line"] = get_new_buffer_name_line()
        data["pointer"] = my_archi.nth_arg(1, my_archi.VOID_PP)
        data["size"] = int(my_archi.nth_arg(2, my_archi.INT))
        return (False, True, data)

    def prepare_after(self, data):
        handle = data["pointer"].dereference()
        representation.Buffer(handle, size=data["size"], line=data["line"], name=data["name"])

def initialize():
    name_resolver.append([get_cudaMallocName, 1])
    
def activate():
    cudaMallocBP()
