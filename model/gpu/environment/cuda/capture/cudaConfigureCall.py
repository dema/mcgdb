import gdb

import mcgdb
from mcgdb.toolbox import my_gdb
from mcgdb.toolbox.target import my_archi

from mcgdb.model.gpu import representation, capture

class cudaConfigureCallBP(capture.OpenCLFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func
    
    def __init__(self):
        representation.OpenCLFunctionBreakpoint.__init__(self, "cudaConfigureCall")
        
    def prepare_before (self):
        data = {}
        data["dimGrid"] = my_archi.first_arg(my_archi.VOID_P)
        data["dimBlock"] = my_archi.second_arg(my_archi.VOID_P)
        
        return (False, True, data)
    
    def prepare_after(self, data):
        data["retcode"] =  my_archi.return_value(my_archi.INT)
        
        if int(data["retcode"]) != 0:
            print ("%s failed" % self.location)
            return
        
def activate():
    cudaConfigureCallBP()
