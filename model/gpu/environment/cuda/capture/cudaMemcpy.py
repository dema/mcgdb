import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.model.gpu import representation

""" cudaMemcpy(void *dst, void *src, size_t nbytes, enum cudaMemcpyKind direction); """
class cudaMemcpyBP(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func
    
    def __init__(self):
        mcgdb.capture.FunctionBreakpoint.__init__(self, "cudaMemcpy")
        
    def prepare_before (self):
        data = {}
        data["dst"] = my_archi.first_arg(my_archi.VOID_P)
        data["src"] = my_archi.second_arg(my_archi.VOID_P)
        data["size"] = my_archi.third_arg(my_archi.INT)
        data["direction"] = my_archi.nth_arg(4, my_archi.INT)
        
        print ("Copy from %s to %s %db, direction is %d" % 
            (data["src"], data["dst"], data["size"], data["direction"]))
        
        is_read = data["direction"] == 2
        buffer = data["src"] if is_read else data["dst"]
        
        representation.enqueue_transfer_buffer(buffer, True, 
                                (None, None, None),
                                is_read)
        
        return (False, False, None)

def activate():
    cudaMemcpyBP()
