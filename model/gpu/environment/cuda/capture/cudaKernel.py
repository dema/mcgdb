import re

import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.model.gpu import representation

class cudaKernelInitBP(mcgdb.capture.FunctionBreakpoint):
    
    def __init__(self):
        mcgdb.capture.FunctionBreakpoint.__init__ (self, "main")

    def prepare_before (self):
        PREFIX = "__device_stub__"
        t = gdb.execute("info functions %s" % PREFIX, to_string=True)
        kern_list = [e.split("Pi")[0]\
                      .split("_implii")[0] for e in
                     re.findall(r'Z\d+(\S+(?!(?:Pi)))(?:Pf|Pi)', t)]
        for kern in kern_list:
            try:
                cudaKernelBP(kern)
            except gdb.error as e:
                print ("FAILURE for kernel %s" % kern)
                print ("---> %s" % e)
         
class cudaKernelBP(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func
    
    def __init__(self, name):
        mcgdb.capture.FunctionBreakpoint.__init__(self, name)
        representation.Kernel(None, name, self, None)
        self.function = gdb.parse_and_eval(name)
        fstre = str(self.function).split(name)[1]
        self.nb_args = str(fstre).count(",") + 1
        
    def prepare_before (self):
        data = {}
        for idx in range(0, self.nb_args):
            try:
                arg = my_archi.nth_arg(idx + 1, my_archi.VOID_P)
            except e:
                print (e)
                break
            representation.set_kernel_arg(self, idx, 0, arg, arg, None)
        representation.enqueueKernel(self, None, None)
        return (False, True, data)
    
    def prepare_after(self, data):
        data["retcode"] =  my_archi.return_value(my_archi.INT)
        
        if int(data["retcode"]) != representation.CL_SUCCESS:
            print ("%s failed" % self.location)
            return
        
def activate():
    cudaKernelInitBP()
