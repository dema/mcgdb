import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.model.gpu import representation

class cudaFreeBP(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func
    
    def __init__(self):
        mcgdb.capture.FunctionBreakpoint.__init__(self, "cudaFree")
        
    def prepare_before (self):
        data = {}
        data["buffer"] = my_archi.first_arg(my_archi.VOID_P)
        
        representation.releaseBuffer(data["buffer"])
        
        return (False, False, data)

def activate():
    cudaFreeBP()
