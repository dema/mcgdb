import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")
import mcgdb

from . import capture

has_events = False

def initialize():
    mcgdb.register_model("OpenCL", "clCreateKernel", mcgdb.toggle_activate_submodules(__name__))
    log_user.debug("OpenCL GPU model registered")
