import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.model.gpu import representation, capture
from .. import user
from .. import representation as ocl_representation

class clEnqueueReadWriteBufferBP(capture.OpenCLFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func
    
    def __init__(self, spec, is_read, param_retcode=False):
        capture.OpenCLFunctionBreakpoint.__init__(self, spec)
        self.is_read = is_read
        self.param_retcode = param_retcode
        
    def prepare_before (self):
        data = {}
        
        data["buffer"] = my_archi.second_arg(my_archi.VOID_P)
        data["blocking"] = my_archi.third_arg(my_archi.INT)
        
        if self.is_read is not None:
            data["is_read"] = self.is_read
        else:
            data["is_read"] = int(my_archi.nth_arg(4, my_archi.INT)) != 2
        
        data["num_events_in_wait_list"] = my_archi.nth_arg(7, my_archi.INT)
        data["event_wait_list"] = my_archi.nth_arg(8, my_archi.VOID_PP)
        data["event"] = my_archi.nth_arg(9, my_archi.VOID_PP)
        
        data["callsite"] = user.get_read_write_buffer_callsite()
        
        return (False, True, data)
    
    def prepare_after(self, data):
        #if self.param_retcode:
        #    data["retcode"] =  int(my_archi.nth_arg(5, my_archi.INT_P).dereference())
        #else:
        #    data["retcode"] =  my_archi.return_value(my_archi.INT)
        
        #if int(data["retcode"]) != ocl_representation.CL_SUCCESS:
        #    print "%s failed (retcode=%d)" % (self.location, data["retcode"])
        #    return
        
        representation.enqueue_transfer_buffer(data["buffer"], data["blocking"] == ocl_representation.CL_TRUE, 
                                        (data["num_events_in_wait_list"], data["event_wait_list"], data["event"]),
                                        data["is_read"], data["callsite"])
def activate():
    clEnqueueReadWriteBufferBP("clEnqueueReadBuffer", is_read=True)
    clEnqueueReadWriteBufferBP("clEnqueueWriteBuffer", is_read=False)
    clEnqueueReadWriteBufferBP("clEnqueueMapBuffer", is_read=None, param_retcode=True)
