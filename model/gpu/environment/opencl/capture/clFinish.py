import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.model.gpu import representation, capture
from .. import user

class clFinishBP(capture.OpenCLFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func
    
    def __init__(self):
        capture.OpenCLFunctionBreakpoint.__init__(self, "clFinish")
        
    def prepare_before (self):
        data = {}
        data["kernel"] = my_archi.first_arg(my_archi.VOID_P)
        
        representation.finishQueue()
        
        return (False, False, data)
        
def activate():
    clFinishBP()
