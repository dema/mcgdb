import gdb

import mcgdb.capture
from mcgdb.toolbox.target import my_archi
from mcgdb.model.gpu import representation, capture
from .. import user

from .. import representation as ocl_representation

class clCreateKernelBP(capture.OpenCLFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func
    
    def __init__(self):
        capture.OpenCLFunctionBreakpoint.__init__(self, "clCreateKernel")
        
    def prepare_before (self):
        data = {}
        data["program_p"] = my_archi.first_arg(my_archi.VOID_P)
        data["name"] = my_archi.second_arg(my_archi.CHAR_P)
        data["retcode_p"] = my_archi.third_arg(my_archi.INT_P)
        
        return (False, True, data)
    
    def prepare_after(self, data):
        if int(data["retcode_p"].dereference()) != ocl_representation.CL_SUCCESS:
            print ("%s failed" % self.location)
            return
        
        data["stack"] = gdb.execute("where 3", to_string=True)
        data["ret"] =  my_archi.return_value(my_archi.VOID_P)
        data["name"] = str(data["name"].string())
        representation.Kernel(data["program_p"], data["name"], data["ret"], data["stack"])
        
def activate():
    clCreateKernelBP()
