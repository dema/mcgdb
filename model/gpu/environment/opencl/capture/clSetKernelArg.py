import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.model.gpu import capture, representation
from .. import user
from .. import representation as ocl_representation

class clSetKernelArgBP(capture.OpenCLFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func
    
    def __init__(self):
        capture.OpenCLFunctionBreakpoint.__init__(self, "clSetKernelArg")
        
    def prepare_before (self):
        data = {}
        data["kernel"] = my_archi.first_arg(my_archi.VOID_P)
        data["index"] = my_archi.second_arg(my_archi.INT)
        data["size"] = my_archi.third_arg(my_archi.INT)
        data["value_pp"] = my_archi.nth_arg(4, my_archi.VOID_PP)
        
        data["callsite"] = user.get_set_kernel_arg_callsite()
        
        try:
            data["value_p"] = data["value_pp"].dereference()
            str(data["value_p"]) # throws the exception if impossible
        except gdb.MemoryError:
            data["value_p"] = None
        
        return (False, True, data)
    
    def prepare_after(self, data):
        data["retcode"] =  my_archi.return_value(my_archi.INT)
        
        if int(data["retcode"]) != ocl_representation.CL_SUCCESS:
            print ("%s failed" % self.location)
            return
        
        representation.set_kernel_arg(data["kernel"], data["index"], data["size"], data["value_p"], data["value_pp"], data["callsite"])

def activate():
    clSetKernelArgBP()
