import mcgdb

from . import clCreateKernel
from . import clCreateBuffer
from . import clEnqueueNDRangeKernel
from . import clSetKernelArg
from . import clEnqueueReadWriteBuffer
from . import clReleaseMemObject
from . import clReleaseKernel
from . import clFinish

def activate():
    mcgdb.toggle_activate_submodules(__name__)
