import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.model.gpu import representation, capture
from .. import user
from .. import representation as ocl_representation

class clEnqueueNDRangeKernelBP(capture.OpenCLFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func
    
    def __init__(self):
        capture.OpenCLFunctionBreakpoint.__init__(self, "clEnqueueNDRangeKernel")
        
    def prepare_before (self):
        data = {}
        data["kernel"] = my_archi.second_arg(my_archi.VOID_P)
        
        data["num_events_in_wait_list"] = my_archi.nth_arg(7, my_archi.INT)
        data["event_wait_list"] = my_archi.nth_arg(8, my_archi.VOID_PP)
        data["event"] = my_archi.nth_arg(9, my_archi.VOID_PP)
         
        data["callsite"] = user.get_enqueue_kernel_callsite()
         
        return (False, True, data)
    
    def prepare_after(self, data):
        data["retcode"] =  my_archi.return_value(my_archi.INT)
        
        if int(data["retcode"]) != ocl_representation.CL_SUCCESS:
            print ("%s failed" % self.location)
            return

        representation.enqueueKernel(data["kernel"], (data["num_events_in_wait_list"], data["event_wait_list"], data["event"]), data["callsite"])

def activate():        
    clEnqueueNDRangeKernelBP()
