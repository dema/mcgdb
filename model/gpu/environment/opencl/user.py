functools = None

import gdb

from mcgdb.toolbox import my_gdb
from .. import user_helper

def get_callsite(depth):
    return "NA"
    try:
        current = gdb.newest_frame()
        for d in range(depth):
            current = current.older()
        callsite = current.function().name
        return callsite
    except Exception as e:
        my_gdb.log.warning("Couldn't find callsite: %s" % e)
        return "NA"

def initialize():
    global functools
    import functools # standard lib package

    global get_set_kernel_arg_callsite, get_enqueue_kernel_callsite, get_read_write_buffer_callsite
    get_set_kernel_arg_callsite = functools.partial(get_callsite, depth=2)
    get_enqueue_kernel_callsite = functools.partial(get_callsite, depth=2)
    get_read_write_buffer_callsite = functools.partial(get_callsite, depth=2)
    
    #user_helper.event_on_breakpoint("local_partial_density_ocl", "local_partial_density_ocl", with_finish=True)
