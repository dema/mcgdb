import gdb

import mcgdb
from mcgdb.toolbox import my_gdb
from ..representation import Event

def push_user_event(txt, after=False):
    Event("#!|USER|%s|%s|#" % ("A" if after else "B", txt))

    
class EventBreakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func

    def __init__(self, location, txt, with_finish):
        mcgdb.capture.FunctionBreakpoint.__init__(self, location)
        self.txt = txt
        self.with_finish = with_finish
        
    def prepare_before (self):
        push_user_event(self.txt)
        print ("[Beginning of event '%s']" % self.txt)
        return (True, self.with_finish, None)
    
    def prepare_after(self, data):
        push_user_event(self.txt, after=True)
        print ("[End of event '%s']" % self.txt)
        return True
        
def event_on_breakpoint(location, txt, with_finish=False):
    EventBreakpoint(location, txt, with_finish)
    
    
class cmd_UserEvent(gdb.Command):
    def __init__(self):
        gdb.Command.__init__ (self, "user_event", gdb.COMMAND_NONE)
    
    def invoke (self, args, from_tty):
        push_user_event(args)

def initialize():
    cmd_UserEvent()
