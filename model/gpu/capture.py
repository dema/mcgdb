import gdb

import mcgdb
from mcgdb.toolbox import my_gdb

DO_CHECK_BEFORE=True
class OpenCLFunctionBreakpoint(mcgdb.capture.FunctionBreakpoint):
    def do_check_prepare_before(self):
        
        right_location = "/lib64/libOpenCL.so" in gdb.execute("where 1", to_string=True)
        if not right_location:
            return
        #print ("[[%s]]" % self.location)
        return self.actual_prepare_before()
            
    def __init__(self, spec):
        mcgdb.capture.FunctionBreakpoint.__init__(self, spec)
        
        if not DO_CHECK_BEFORE:
            return
        
        self.actual_prepare_before = self.prepare_before
        
        self.prepare_before = self.do_check_prepare_before

def initialize():
    pass
