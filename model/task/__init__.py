"""
Modules implementing Model-Centric Debugging for Platform 2012 environments.
"""

from . import interaction, representation
from .environment import p2012, openmp
