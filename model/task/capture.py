import gdb

import mcgdb
from mcgdb.toolbox import my_gdb

class UserComponentBreakpoint(gdb.Breakpoint):
    """
    Very close to ComponentBreakpoint.
    Displays a message if an execution stop is requested.
    """
    def __init__(self, component, spec):
        gdb.Breakpoint.__init__(self, spec)
        self.component = component
        self.depth = 0
        self.silent = True
        
    def stop(self):
        if self.component == CommComponent.read_selected_component(self.depth):
            print ("Stopped on Component breakpoint #%d at %s" % 
                  (self.number, self.location))
            return True
        return False

class ComponentBreakpoint(mcgdb.capture.FunctionBreakpoint):
    """
    FunctionBreakpoint set on a specific component.

    :var component: the component on which this breakpoint is active.
    :var depth: the number of frame we need to undig to lookup the component currently active.
    """
    def __init__(self, component, spec):
        mcgdb.capture.FunctionBreakpoint.__init__(self, spec)
        self.component = component
        self.depth = 1
        
    def stop(self):
        """
        :returns: None if the component currently active (`CommComponent.read_selected_component(self.depth)`) doesn't match the component associated with this breakpoint.
        :returns: `model.capture.FunctionBreakpoint.stop(self)` otherwise.
        """
        
        if self.component is not CommComponent.read_selected_component(self.depth):
            return
        
        return model.capture.FunctionBreakpoint.stop(self)
        
class InterfaceBreakpoint(ComponentBreakpoint):
    """
    ComponentBreakpoint set on a specific interface.
    """
    def __init__(self, component, spec):
        ComponentBreakpoint.__init__(self, component, spec)
        self.depth = 1
