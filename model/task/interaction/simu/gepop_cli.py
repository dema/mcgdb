import gdb

from mcgdb.toolbox import my_gdb
from mcgdb.model.task.environment.p2012.toolbox import gepop


class cmd_gepop_info_procs (gdb.Command):     
    def __init__ (self):
        gdb.Command.__init__ (self, "gepop info processors", 
                              gdb.COMMAND_OBSCURE)
        
    def invoke (self, arg, from_tty):
        all_ = False
        regs = False
        selected = []
        rest = arg
        while rest != "":
            first, part, rest = rest.partition(" ")
            if first in ("all"):
                all_ = True
            elif first in ("regs", "registers"):
                regs = True
            elif first.isdigit():
                selected.append(int(first)) 
            else:
                my_gdb.log.warning("argument '%s' not recognized.", first)
        
        masterPool = gdb.parse_and_eval("masterPool")
        for i in range(0, int(gdb.parse_and_eval("nbMaster"))):
            if len(selected) != 0 and i not in selected:
                continue
            
            current = masterPool[i]
            if not all_ and str(current["state"]) == "INACTIVE":
                continue
            print ("#%d %s" % (i, current["debug_name"].string()))
            print ("    %s (%s)" % (current["state"], current["entry"]))
            if regs:
                print ("    %s" % (current["context"][0]["__jmpbuf"]))
            print ()


class cmd_gepop_procs_id (gdb.Command):     
    def __init__ (self):
        gdb.Command.__init__ (self, "gepop processor", gdb.COMMAND_OBSCURE,
                              prefix=1)
    def invoke (self, arg, from_tty):    
        next_ = gdb.parse_and_eval("masterPool[%d]" % int(arg))
        gepop.switch_inactive_thread(next_, master=False, force=False)

class cmd_gepop_procs_master (gdb.Command):     
    def __init__ (self):
        gdb.Command.__init__ (self, "gepop processor master", 
                              gdb.COMMAND_OBSCURE)
    def invoke (self, arg, from_tty):    
        gepop.switch_inactive_thread(
                       gdb.parse_and_eval("getCurrentThread()")["mainMaster"], 
                       master=True)


def initialize():
    gdb.Command("gepop", gdb.COMMAND_NONE, prefix=1)
    gdb.Command("gepop info", gdb.COMMAND_NONE, prefix=1)
    
    cmd_gepop_info_procs()
    cmd_gepop_procs_id()
    cmd_gepop_procs_master()
