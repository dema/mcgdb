import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.model.task import representation

from . import newWorker
from . import workMeth

class NewFilterBreakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func
    
    def __init__(self, spec):
        mcgdb.capture.FunctionBreakpoint.__init__(self, spec)
        
    def prepare_before (self):
        is_module = False
        gdb.newest_frame().older().select()
        this = gdb.parse_and_eval("this")
        _type = str(this.type.target())
        
        if "PedfBaseModule" in _type:
            gdb.selected_frame().older().select()
            this = gdb.parse_and_eval("this")
            _type = str(this.type.target())
            is_module = True
        
        if "TopModuleWrapper" in _type:
            return
        
        if "DchanApexModel" in _type:
            return
        
        clazz = representation.Module if is_module else representation.Filter
        filter_ = clazz(None, this, type_=_type)
        
        params = self.get_parameters()
        
        try:
            assert params["parent"] is not None and not params["parent"].is_optimized_out
        except:
            return
        
        
        parent = representation.Filter.key_to_value(params["parent"])
        if parent is not None:
            representation.connect_owner_interface(parent, filter_)
        else:
            filter_.is_top = True
        
        return (False, False, None)
    
    def get_parameters(self):
        data = {}
        try:
            gdb.selected_frame().older().select()
            data["parent"] = gdb.parse_and_eval("parent")
        except gdb.error:
            data["parent"] = None
            #FirmwareApex.PedfBaseClass does not have a parent
            pass
        
        return data

def enable():
    NewFilterBreakpoint("PedfBaseFilter::PedfBaseFilter")
