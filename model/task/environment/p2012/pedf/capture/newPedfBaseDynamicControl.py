import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.model.task import representation

class PedfBaseDynamicControlBreakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.comm_func
    
    def __init__(self, spec):
        mcgdb.capture.FunctionBreakpoint.__init__(self, spec)
        
    def prepare_before (self):
        gdb.newest_frame().older().select()
        stop = True 
        
        this_type = gdb.parse_and_eval("this").type.target()
        this_type_str = str(this_type)
        
        try:
            if this_type_str.split("<")[0].endswith("In"):
                template = this_type.template_argument(0)
                # otherwise, ::pop(<type> &) is also hit
                PopDSBreakpoint("%s::pop(%s*)" % (this_type_str, template))
            elif this_type_str.split("<")[0].endswith("Out"):
                PushDSBreakpoint( "%s::push" % this_type_str)
            else:
                my_gdb.log.warning("PedfBaseDynamicControl '%s' is neither In or Out ...", this_type)
                return None
            
        except my_gdb.AlreadyInstalledException:
            pass
        
        return (False, False, None)

class PushDSBreakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.comm_func
    
    def __init__(self, spec):
        mcgdb.capture.FunctionBreakpoint.__init__(self, spec)
        
    def prepare_before (self):
        itf = representation.DynamicControlInterface.key_to_value(gdb.parse_and_eval("this"))
        
        if itf is None:
            return
        
        msg = itf.comm_entity.produce_message(itf)
        if msg is None:
            msg = itf.produce_message()
        
        if msg is None:
            return
        
        msg.check_breakpoint("%s/%s" % (itf.comm_entity,
                                        itf.name), "LNK")
        itf.link.message_queue.insert(0, msg)
            
        stop_next, stop_next_msgs = itf.get_stop_next(msg)
        my_gdb.push_stop_requests(stop_next, stop_next_msgs)
        
        #assert itf.comm_entity.executing
        #assert not itf.comm_entity.asleep
        #assert not itf.comm_entity.waiting_io
        itf.link.waiting_io = True
        #gdb.execute("graph-it +work +suffix=")
        itf.comm_entity.waiting_io = True
        return (False, True, itf)
    
    def prepare_after(self, itf):
        #assert itf.comm_entity.executing
        #assert not itf.comm_entity.asleep
        #assert itf.comm_entity.waiting_io
        itf.comm_entity.waiting_io = False
        itf.link.waiting_io = False
        #gdb.execute("graph-it +work +suffix=")
        
class PopDSBreakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func
    
    def __init__(self, spec):
        mcgdb.capture.FunctionBreakpoint.__init__(self, spec)
        
    def prepare_before (self):
        itf = representation.DynamicControlInterface.key_to_value(gdb.parse_and_eval("this"))
        
        #assert itf.comm_entity.executing
        #assert not itf.comm_entity.asleep
        #assert not itf.comm_entity.waiting_io
        itf.comm_entity.waiting_io = True
        itf.link.waiting_io = True
        #gdb.execute("graph-it +work +suffix=")
        return (False, True, itf)
    
    def prepare_after (self, itf):
        #assert itf.comm_entity.executing
        #assert not itf.comm_entity.asleep
        #assert itf.comm_entity.waiting_io
        itf.comm_entity.waiting_io = False
        itf.link.waiting_io = False
        itf.comm_entity.consume_message(itf.link.message_queue.pop())
        #gdb.execute("graph-it +work +suffix=")
        return False
        
class ConnectToDCBreakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func
    
    def __init__(self, spec):
        mcgdb.capture.FunctionBreakpoint.__init__(self, spec)
        
    def prepare_before (self):
        this = gdb.parse_and_eval("this")
        res = getNameAndModule(this)
        if res is None:
            return None
        this_name, this_module = res
        
        that = gdb.parse_and_eval("ctlPort")
        res = getNameAndModule(that)
        if res is None:
            return None
        that_name, that_module = res
        
        representation.connect_dynamic_control_interface(this, this_module, this_name, 
                                                   that, that_module, that_name)
        
        
        return (False, False, None)
    

def getNameAndModule(this):
    this_name = str(this["myName"].string())
    this_owner = this["myController"]
    this_owner = this_owner.cast(this_owner.dynamic_type)
    
    this_module = representation.Filter.key_to_value(this_owner)
    if this_module is None:
        return None
    
    return this_name, this_module


def initialize():
    #PedfBaseDynamicControlBreakpoint("PedfBaseDynamicControl::PedfBaseDynamicControl")
    #ConnectToDCBreakpoint("PedfBaseDynamicControl::connectTo")
    pass
