import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.model.task import representation

class WaitEndStepBreakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func
    
    def __init__(self, spec):
        mcgdb.capture.FunctionBreakpoint.__init__(self, spec)
        
    def prepare_before (self):
        this = gdb.parse_and_eval("this")
        filter_ = representation.Filter.key_to_value(this)
        if filter_ is None:
            return
        
        if filter_.is_top:
            return
        
        if isinstance(filter_, representation.Module):
            filter_ = filter_.controller_itf.comm_entity
        
        #assert filter.executing
        #assert not filter.asleep
        #assert not filter.waiting_io
        
        filter_.asleep = True
        return (False, False, filter)
    
    def prepare_after(self, filter_):        
        #assert filter.executing
        #assert filter.asleep
        #assert not filter.waiting_io
        
        filter.asleep = False

def initialize():
    WaitEndStepBreakpoint("PedfBaseModule::RDF_WAIT_FOR_END_STEP")
