import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.model.task import representation

class ConnectToDSBreakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func
    
    def __init__(self, spec):
        mcgdb.capture.FunctionBreakpoint.__init__(self, spec)
        
    def prepare_before (self):
        this = gdb.parse_and_eval("this")
        res = getNameAndModule(this)
        
        if res is None:
            return None
        
        this_name, this_module = res
        
        that = gdb.parse_and_eval("boundStream")
        res = getNameAndModule(that)
        
        if res is None:
            return None
        
        that_name, that_module = res
        
        representation.connect_datastream_interface(this, this_module, this_name, 
                                                    that, that_module, that_name)
        
        return (False, False, None)

def getNameAndModule(this):
    try:
        this_name = str(this["streamName"].string())
    except gdb.MemoryError: #Address 0x0 out of bounds
        this_name = "No name"
        
    this_owner = this["streamOwner"]
    #this_owner = this["myController"]
    this_owner = this_owner.cast(this_owner.dynamic_type)
    
    this_module = representation.Filter.key_to_value(this_owner)
    
    if this_module is None:
        my_gdb.log.warning("couldn't find module %s." % this_owner)
        return None
    
    return this_name, this_module

def initialize():
    #ConnectToDSBreakpoint("PedfBaseDataStream::connectTo")
    pass
