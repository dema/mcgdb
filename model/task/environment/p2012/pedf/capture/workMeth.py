import gdb

import mcgdb
from mcgdb import toolbox
from mcgdb.toolbox.target import my_archi
from mcgdb.model.task import representation

class WorkMethBreakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.exec_func
    
    def __init__(self, spec):
        mcgdb.capture.FunctionBreakpoint.__init__(self, spec)
        
    def prepare_before (self):
        gdb.newest_frame().older().select()
        this = gdb.parse_and_eval("this")
        gdb.newest_frame().select()
        
        _filter = representation.Filter.key_to_value(this)
        if _filter is None:
            return
        
        if isinstance(_filter, representation.Module):
            _filter = _filter.controller_itf.comm_entity
        
        if toolbox.catchable.state("work", entity=_filter):
            my_gdb.push_stop_request("[Stopped on 'work' method of %s]"
                                      % _filter)
        
        if isinstance(_filter, representation.Worker) and _filter.owner_itf.comm_entity.is_top:
            if representation.catchable_state("main-work"):
                my_gdb.push_stop_request("[Stopped on 'main-work' event of %s]"
                                          % _filter.owner_itf.comm_entity)

        
        for itf in _filter.endpoints:
            if not isinstance(itf, representation.DataStreamInterface):
                continue
            if itf.is_source:
                continue
            _filter.consume_message(itf.produce_message())
        
        _filter.run()
        
        return (False, False, _filter)
    
class FinishWorkMethBreakpoint(mcgdb.capture.FunctionBreakpoint):
    """ Required because GDB is not SysC-thread aware. FinishBreakpoints
    are thread-specific, so all the "finish" are triggered as the same 
    time, because it's always the same GDB, from GDB POV"""
     
    func_type = mcgdb.capture.FunctionTypes.exec_func
    
    def __init__(self):
        mcgdb.capture.FunctionBreakpoint.__init__(self, "src/pedfBaseFilter.cc:449")
        
    def prepare_before(self):
        this = gdb.parse_and_eval("this")
        _filter = representation.Filter.key_to_value(this)
        
        if _filter is None:
            return
        
        for itf in _filter.endpoints:
            if not isinstance(itf, representation.DataStreamInterface):
                continue
            if not itf.is_source:
                continue
            itf.consume_message(_filter.produce_message(itf))
        
        _filter.finish_run()
        
        return (False, False, None)
    

def enable():
    FinishWorkMethBreakpoint()
    
    toolbox.catchable.register("work")
    toolbox.catchable.register("main-work")
