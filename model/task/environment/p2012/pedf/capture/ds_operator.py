import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.model.task import representation

class OperatorBefDSBreakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.comm_func
    
    def __init__(self, spec):
        mcgdb.capture.FunctionBreakpoint.__init__(self, spec)
        
    def prepare_before (self):
        this = gdb.parse_and_eval("(void *)$ebx")
        itf = representation.DataStreamInterface.key_to_value(this)
        
        if itf is None:
            return 
        itf.fetch_data() 
        
        return (False, False, None)


class OperatorAftDSBreakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.comm_func
    
    def __init__(self, spec):
        model.capture.FunctionBreakpoint.__init__(self, spec)
    
    def prepare_before(self):
        this = gdb.parse_and_eval("(void *) $ebx")
        itf = representation.DataStreamInterface.key_to_value(this)
        
        if itf is None:
            return
        itf.finish_fetch_data()
        
        return (False, False, None)

class DSPopBreakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.comm_func
    
    def __init__(self, spec):
        mcgdb.capture.FunctionBreakpoint.__init__(self, spec)
    
    def prepare_before(self):
        this = gdb.selected_frame().older().read_var("this")
        itf = representation.DataStreamInterface.key_to_value(this)
        
        if itf is None:
            return
        
        itf.pop_data()
        
        return (False, True, itf)
    
    def prepare_after(self, itf):
        itf.finish_pop_data()


class DSPushBreakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.comm_func
    
    def __init__(self, spec):
        mcgdb.capture.FunctionBreakpoint.__init__(self, spec)
    
    def prepare_before(self):
        this = gdb.selected_frame().older().read_var("this")
        itf = representation.DataStreamInterface.key_to_value(this)
        
        if itf is None:
            return
        
        itf.flush_data()
        
        return (False, True, itf)
    
    def prepare_after(self, itf):
        itf.finish_flush_data()
    
class OperatorEqBreakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.comm_func
    
    def __init__(self, spec):
        mcgdb.capture.FunctionBreakpoint.__init__(self, spec)
    
    def prepare_before(self):
        thisItf = gdb.parse_and_eval("strm")
        itf = representation.DataStreamInterface.key_to_value(thisItf)
        
        if itf is None:
            return
        
        itf.push_data()
        
        return (False, True, itf)
    
    def prepare_after(self, itf):
        itf.finish_push_data()
        
def init():
    """
    inited from representation.complete_interface_mapping
    DSPushBreakpoint("PedfFunctional... requires the 
    symfile to be loaded.
    """
    #pedf-v2/modules/pedf/platform-api/
    #class PedfArrayStreamIn::selectToken (begining and end)
    #OperatorBefDSBreakpoint("apex/includes/pedfDataStream.h:524")
    #OperatorAftDSBreakpoint("apex/includes/pedfDataStream.h:565")
        
    #DSPopBreakpoint("PedfFunctionalFifo<int>::pop(int*)")
    
    #DSPushBreakpoint("*PedfFunctionalFifo<int>::push(int*, int)")
    
    # class PedfTokenSelector::operator=
    OperatorEqBreakpoint("apex/includes/pedfDataStream.h:114")
