import gdb

import mcgdb
from mcgdb import toolbox
from mcgdb.toolbox.target import my_archi
from mcgdb.model.task import representation

class PedfDeploy(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func
    
    def __init__(self, spec):
        mcgdb.capture.FunctionBreakpoint.__init__(self, spec)

    def prepare_before (self):
        return (False, True, None)

    def prepare_after (self, data):
        representation.complete_interface_mapping()
        
        if toolbox.catchable.state("initialized"):
            my_gdb.push_stop_request("[Stopped after PEDF initialization]")
            
        return True

def enable():    
    toolbox.catchable.register("initialized")
    PedfDeploy("pedf_deploy")
