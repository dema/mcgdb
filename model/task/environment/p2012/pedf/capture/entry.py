import gdb

import mcgdb
from mcgdb import toolbox
from mcgdb.toolbox.target import my_archi
from mcgdb.model.task import representation

class EntryPointBreakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.exec_func
    
    def __init__(self, spec):
        mcgdb.capture.FunctionBreakpoint.__init__(self, spec)
        
    def prepare_before (self):
        if toolbox.catchable.state("entry-point"):
           my_gdb.push_stop_request("[Stopped on pedf entry point]")
           
        return (False, False, None)

def enable():        
    EntryPointBreakpoint("entry")
    toolbox.catchable.register("entry-point")
