import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.model.task import representation

from . import workMeth

class TmpBreakpoint(gdb.Breakpoint):
    def __init__(self, spec, worker):
        gdb.Breakpoint.__init__(self, spec, internal = True)
        self.silent = True
        self.worker = worker
        
    def stop(self):
        self.enabled = False
        gdb.post_event(self.delete)
        
        parent_this = self.worker.instance["parent"]
        parent = representation.Filter.key_to_value(parent_this)
        
        if parent is None:
            return 
        
        representation.connect_control_interface(parent, self.worker)
        
        return False

class NewWorkerBreakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func
    
    def __init__(self, spec):
        mcgdb.capture.FunctionBreakpoint.__init__(self, spec)
        
    def prepare_before (self):
        gdb.newest_frame().older().select()
        this = gdb.parse_and_eval("this")
        parent = gdb.parse_and_eval("parent")
        _type = str(this.type.target())
        
        worker = representation.Worker(this, parent)
        
        for attr in this.type.target().fields():
            if not "PedfDynamicControl" in str(attr.type):
                continue

        workMeth_name = " __component_%s_work" % _type[0:_type.index("_PedfControllerBaseClass")]
        symb, found = gdb.lookup_symbol(workMeth_name)
        worker.work_breakpoint = handle_workMeth.WorkMethBreakpoint(workMeth_name)
        worker.work_method = workMeth_name
        
        return (False, True, worker)
    
    
    def prepare_after(self, worker):
        #continue until the end of the frame above
        bkp = TmpBreakpoint("*%d" % gdb.newest_frame().older().pc(), worker)

        return False


def enable():
    NewWorkerBreakpoint("PedfWorker::PedfWorker")
