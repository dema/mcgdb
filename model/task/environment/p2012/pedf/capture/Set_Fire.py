import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.model.task import representation


class SetFireBreakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func
    
    def __init__(self, spec):
        mcgdb.capture.FunctionBreakpoint.__init__(self, spec)
        
    def prepare_before (self):
        fired = gdb.parse_and_eval("e")
        fired_module = representation.Filter.key_to_value(fired)
        
        print ("SetFire to", fired_module)
        
        if isinstance(fired_module, representation.Module):
            fired_module = fired_module.controller_itf.comm_entity
        
        fired_module.fire_tokens += 1
        if fired_module.max_tokens < fired_module.fire_tokens:
            fired_module.max_tokens = fired_module.fire_tokens
        return (False, False, None)

def initialize():
    SetFireBreakpoint("PedfBaseModule::RDF_SET_FIRE")
