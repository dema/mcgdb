"""
Module extending GDB's CLI with commands related to graph visualization.
"""

from collections import defaultdict
import subprocess

import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb
from mcgdb.model.task import representation

###########################################################"

HEAD = """
digraph G {
node [style=filled];
\trankdir=LR;
"""

BOTTOM = """
}% 
"""

#%(uid)s [label = self];
SUBGRAPH = """
subgraph cluster_%(uid)s {
\tlabel = %(label)s
\t%(filters)s
}
"""

CONTROLLER = """
%(name)s_controller [label="%(label)s_controller"] [style = filled, shape= rect, color = green] ;
%(name)s_controller;
"""

FILTER = """
%(name)s [label="%(label)s" color="%(color)s"]
"""

LOOP_LINK = """
%(from)s->%(to)s [color="%(color)s"];
"""

CONTROLLER_LINK = """
%(from)s->%(to)s [shape="box", style="filled", color="red"];
"""

DATASTREAM_LINK = """
%(from)s->%(to)s [color="%(color)s" label="%(label)s" arrowtail="%(tail)s" arrowhead="%(head)s" dir=both];
"""

CTRL_DATASTREAM_LINK = """
%(from)s->%(to)s [color="blue"];
"""

DATASTREAM_BRIDGE_LINK = """
%(from)s->%(to)s [color="black"];
"""

DATASTREAM_BRIDGED_LINK = """
%(from)s->%(to)s [color="grey"];
"""

CONTROL_FLOW_LINK = """%(from)s->%(to)s [style="dashed" color="0.%(weight)d 0.5 0.5"];"""
CONTROL_FLOW_LINK_LAST = """%(from)s->%(to)s [style="dashed" color="red"];"""
CONTROL_FLOW_NODE_DST = """%(to)s [color="red"];"""
CONTROL_FLOW_NODE_SRC = """%(from)s [color="orange"];"""
CONTROL_FLOW_NODE_RAINBOW = """%(from)s [color="0.%(weight)d 0.5 0.5"];"""


CONTROL_FLOW_NODE_ASLEEP = """%(node)s [color="orange"];"""
CONTROL_FLOW_NODE_RUNNING = """%(node)s [color="red"];"""
CONTROL_FLOW_NODE_IO = """%(node)s [color="chartreuse3"];"""
CONTROL_FLOW_LINK_IO = """%(from)s->%(to)s [color="chartreuse3"];"""

def join_filters(filters):
    return "\n\t".join(filters)

def get_uid_parent(filter, top=True):
    filter_name_prefix = ""
    filter_name_suffix = ""
    if isinstance(filter, pedf_mon.Worker):
        filter_name_suffix = "_controller"
        filter = filter.owner_itf.comm_entity
        
    if isinstance(filter.owner_itf, pedf_mon.OwnerInterface):
        filter_name_prefix = get_uid_parent(filter.owner_itf.comm_entity) + "_"
        
    
    uid = filter_name_prefix + str(filter) + filter_name_suffix
    
    return uid.replace(".", "_")

def get_filter_uid(filter):
    return get_uid_parent(filter)

def module_to_graph(filter, print_status=False, parent_name=None, print_cycles=False):
    uniq_name = ""
    if parent_name is not None:
        uniq_name = "%s_" % parent_name
    uniq_name += str(filter).replace(".", "_")
    
    if isinstance(filter, pedf_mon.Module):
        module = filter
        filters = []
        if module.controller_itf is not None:
            
            filters.append(CONTROLLER % {"name":uniq_name, "label":str(module)})
        
        for itf in module.endpoints:
            if not isinstance(itf, pedf_mon.OwnerInterface):
                continue
            if isinstance(itf, pedf_mon.ControllerInterface):
                continue
            filters.append(module_to_graph(itf.get_other_side().comm_entity, print_status, uniq_name, print_cycles=print_cycles))
        
        return SUBGRAPH % {"uid":uniq_name, "label": str(module), "filters":join_filters(filters)}
    else:
        do_print_cycle = print_cycles or (isinstance(print_cycles, int) and not isinstance(print_cycles, bool))
        color = "" if not filter.running or do_print_cycle \
                   else ("green" if filter.executing else "red")
            
        label = str(filter) + ("\\n"+filter.state if print_status else "")
            
        return FILTER % {"name":uniq_name, "label":label, "color":color}
        

def loop_to_graph():
    colors = ("red", "blue", "green", "pink")
    edges = []

    for l, loop in enumerate(pedf_mon.get_loops()):
        print (", ".join([str(n) for n in loop]))
        for f, filter in enumerate(loop[:-1]):
            edges.append(LOOP_LINK % {"from":get_filter_uid(filter), 
                                      "to":get_filter_uid(loop[f+1]),
                                      "color":colors[l % len(colors)]
                                      })
    return "\n".join(edges)

def interface_to_graph(print_count=False, print_cycles=False):
    edges = []
    if print_cycles or (isinstance(print_cycles, int) and not isinstance(print_cycles, bool)):
        print_count = False
        loop_pairs = [zip(lp, lp[1:]) for lp in pedf_mon.get_loops()]
        #loop_pairs = list of list of loop nodes (a-b, b-c, c-a)*
        
    for itf in pedf_mon.DynamicControlInterface.list_:
        if not itf.is_source: 
            continue
        
        edges.append(CONTROLLER_LINK % {"from":get_filter_uid(itf.comm_entity), 
                                        "to":get_filter_uid(itf.get_other_side().comm_entity)})
    
    printed_bridges = []
    for itf in pedf_mon.DataStreamInterface.list_:
        if not itf.is_source: 
            continue
        if isinstance(itf, pedf_mon.DataStreamBridgeInterface):
            template = DATASTREAM_BRIDGE_LINK
        elif itf.link.bridge is not None:
            if itf.link in printed_bridges:
                continue
            printed_bridges.append(itf.link)
            template = DATASTREAM_BRIDGED_LINK
            continue
        elif isinstance(itf.comm_entity, pedf_mon.Worker) or isinstance(itf.get_other_side().comm_entity, pedf_mon.Worker):
            template = CTRL_DATASTREAM_LINK
        else:
            head = "open" if not itf.get_other_side().busy else "normal"
            tail = "none" if not itf.busy                  else "inv"
            
            color = "black" if not (itf.blocking or itf.get_other_side().blocking) else "red"
            
            label = "%d/%d/%d" % (itf.link.count, itf.link.in_count, -itf.link.out_count) if print_count else ""
            if print_cycles or (isinstance(print_cycles, int) and not isinstance(print_cycles, bool)):
                loops = []
                for idx, loop_pair in enumerate(loop_pairs):
                    if isinstance(print_cycles, int) and idx != print_cycles:
                        continue 
                    if (itf.comm_entity, itf.get_other_side().comm_entity) in loop_pair:
                        loops.append("l%d" % idx)
                if not isinstance(print_cycles, int):
                    label = "\\n" + ", ".join(loops)
                else:
                    color = "black" if len(loops) == 0 else "red"
            
            template = DATASTREAM_LINK % {"color":color, "label":label, "head":head, "tail":tail,
                                          "from" : "%(from)s", "to" : "%(to)s"}
        
        edges.append(template % {"from":get_filter_uid(itf.comm_entity), 
                                 "to":get_filter_uid(itf.get_other_side().comm_entity)})
    
    return "\n".join(edges)
       
WEIGHTED = False 
def control_flow_to_graph():
    flows = []
    for filter in pedf_mon.Filter.list_:
        if filter.executing:
            if filter.asleep:
                flows.append(CONTROL_FLOW_NODE_ASLEEP % {"node":get_filter_uid(filter)})
            elif filter.waiting_io:
                flows.append(CONTROL_FLOW_NODE_IO % {"node":get_filter_uid(filter)})
            else: 
                flows.append(CONTROL_FLOW_NODE_RUNNING % {"node":get_filter_uid(filter)})
            
    for link in connection_mon.Link.list_:
        if hasattr(link, "waiting_io"):
            if link.waiting_io:
                from_ep, to_ep = link.endpoints
                if not from_ep.is_source:
                    from_ep, to_ep = link.endpoints[::-1]
                flows.append(CONTROL_FLOW_LINK_IO % {"from":get_filter_uid(from_ep.comm_entity), "to":get_filter_uid(to_ep.comm_entity)})
    return "\n".join(flows)

DOT_FILENAME = "/tmp/my_graph%s.dot"
OUT_FILENAME = "/tmp/my_graph%s.png"
counter = 1
class cmd_graph_it (gdb.Command):
    """
    Generates graph (graphviz to PNG) of the current entity interconnections.
    Options:
    +open  Opens the generated image with `eog`.
    +work
    +cycles_only Draws all the possibilities of deadlocks (cycles) of the graph.
    +cycles[=idx] Draws in red all the cycles or only `idx`.
    +status Writes the status of entities in the graph.
    +count Writes the execution counter of the entities in the graph.
    +suffix=keyword Add a keyword in the graph filename.
    """
    def __init__ (self):
        gdb.Command.__init__(self, "graph-it", gdb.COMMAND_OBSCURE)
        
    def invoke (self, arg, from_tty):
        print_work = False
        print_count = False
        print_cycles = False
        print_only_cycles = False
        print_status = False
        do_open = False
        suffix = ""
        rest = arg
        while rest != "":
            first, part, rest = rest.partition(" ")
            if "+open" in first:
                do_open = True
            elif "+work" in first:
                print_work = True
            elif "+cycles_only" in first:
                print_only_cycles = True
            elif "+cycles" in first:
                print_cycles = True
            elif "+cycle=" in first:
                print_cycles = int(first[first.index("=")+1:])
            elif "+status" in first:
                print_status = True
            elif "+count" in first:
                print_count = True
            elif "+suffix=" in first:
                global counter
                suffix = first.partition("=")[2]
                if not suffix:
                    suffix = "%02d" % counter
                    counter += 1
                suffix = "." + suffix
            else:
                my_gdb.log.warning("argument '%s' not recognized", first) 
        
        edges = ""
        control_flow = ""
        nodes = module_to_graph(p2012_mon.CommComponent.list_[0], print_status, print_cycles=print_cycles)
        
        if print_only_cycles:
            edges = loop_to_graph()
        else:
            edges = interface_to_graph(print_count, print_cycles)
        
        if print_work:
            control_flow = control_flow_to_graph()
        
        graph = HEAD + nodes + edges + control_flow + BOTTOM
        
        with open(DOT_FILENAME  % suffix, "w") as dot: 
            print(graph, file=dot)
        
        process = subprocess.Popen(['dot', '-Tpng'], shell=False, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        stdout, stderr = process.communicate(input=graph)
        file = OUT_FILENAME % suffix
        if do_open:
            subprocess.Popen(['eog', file], shell=False)
        else:
            print (file)
        f = open(file, "w")
        f.write(stdout)
        f.close()
                      
        
class cmd_info_cycles (gdb.Command):
    """
    Lists the cycles found in graph of interconnected entities.
    """
    def __init__ (self):
        gdb.Command.__init__(self, "info cycles", gdb.COMMAND_OBSCURE)
        
    def invoke (self, arg, from_tty):
        deads = pedf_mon.check_deadlock()
        print ("Id.\tLoop")
        for l, loop in enumerate(pedf_mon.get_loops()):
            print ("#%d\t%s" % (l, " -> ".join([str(n) for n in loop])))
            if l in deads:
                print ("\tDeadlocked !")

@my_gdb.internal
def initialize():
    #cmd_graph_it()
    #cmd_info_cycles()
    pass
    
