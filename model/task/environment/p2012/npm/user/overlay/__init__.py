inspect = None
definitions = []

def import_overlay(module):
    """Import overlay definitions.
    --> define module.definitions[key] = overlay
    if 'key' is a class then 'overlay' is applied to it
    if 'key' is a string, then 'overlay will be applied to component matching this key
    (not possible yet)"""
    
    definitions.append(module.definitions)
    
def get_overlay(cls, name=None):
    for def_ in definitions:
        overlay_def = def_.get(cls, cls)
        if inspect.isclass(overlay_def):
            return overlay_def
        
        assert name is not None
        for key in overlay_def.keys():
            if key in name:
                return overlay_def[key]
################

#break cyclic imports
def import_overlays():
    from . import matrix
    from . import pklt
    
    import_overlay(pklt)
    import_overlay(matrix)

def initialize():
    global inspect
    import inspect
