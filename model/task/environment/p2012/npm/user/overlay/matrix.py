import gdb

import mcgdb
from mcgdb.model.task import representation, capture
from mcgdb.model.task.environment.p2012 import npm

class HostDMAPush(npm.representation.dma_link.P2012HostDMAPushEndpoint):
    count = 0
    def do_produce_message(self):
        
        # payload = str(gdb.parse_and_eval("*((int *)%d)@%d" % 
        #                                  (self.baseAddr, self.increment /
        #                                   gdb.lookup_type("int").sizeof)))
        
        payload = "Push>%s" % self.id
        HostDMAPush.count += 1
        return model.representation.Message(self, payload, 
                                      "Message #%d" % HostDMAPush.count)

class HostDMAPull(npm.representation.dma_link.P2012HostDMAPullEndpoint):
    def get_message(self, msg):
        
        # payload = str(gdb.parse_and_eval("*((int *)%d)@%d" % 
        #                                  (self.baseAddr, self.increment /
        #                                   gdb.lookup_type("int").sizeof)))
        
        
        payload = "Pull<%s" % self.id
        msg.checkpoint(self, payload)
        return msg

class MyComponent(representation.CommComponent):
    def init(self):
        self.mailbox = []
		
    def do_consume_message(self, endpoint, msg):
        self.mailbox.insert(0, msg)
    
    def do_produce_message(self, endpoint):
        try:
            msg = self.mailbox.pop()
            msg.checkpoint("MatrixProcessor/%d" % self.id, 
                           "%sx%s" % (msg.last_payload(), self.id))
            return msg
        except IndexError:
            return model.representation.Message(self, "EMPTY")
        
    def get_messages(self):
        return self.mailbox
    
class MyHost: #(representation.CommHost):
    def init(self):
        self.mailbox = []

    def do_consume_message(self, endpoint, msg):
        self.mailbox.append(msg)
        
    def get_messages(self):
        return self.mailbox
        
definitions = {}
def initialize():
    definitions[model.representation.process.CommHost] = MyHost
    definitions[representation.CommComponent] = MyComponent
    definitions[npm.representation.dma_link.P2012HostDMAPushEndpoint] = HostDMAPush
    definitions[npm.representation.dma_link.P2012HostDMAPullEndpoint] = HostDMAPull
