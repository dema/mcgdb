import gdb

import mcgdb
from mcgdb.model.task import representation

class HostDMAPush: #(dma_link_mon.P2012HostDMAPushEndpoint):
    counts = {}
    def do_produce_message(self):
        if self.name not in HostDMAPush.counts:
            HostDMAPush.counts[self.name] = 1
            
        other_side = self.get_other_side()
        other_side_name = other_side.name if other_side is not None else None
        msg_name = "%s #%d" % (other_side_name, HostDMAPush.counts[self.name])
        HostDMAPush.counts[self.name] += 1
        
        checkpoint = "%s>%s" % (self.comm_entity, self.name)
        return connection_mon.Message(checkpoint, name=msg_name)

class HostDMAPull: #(dma_link_mon.P2012HostDMAPullEndpoint):
    def get_message(self, msg):
        """anything useful here"""
        msg.checkpoint("%s>%s" % (self.comm_entity, self.name))
        
        return msg

class MySmoothAndSampleComponent(representation.CommComponent):
    def init(self):
        self.coeff = None
        self.src = None
		
    def do_consume_message(self, endpoint, msg):
        if endpoint.name == "coeffPullBuffer":
            self.coeff = msg
        if endpoint.name == "srcPullBuffer":
            self.src = msg
        if endpoint.name == "srcTmpPullBuffer":
            self.src = msg
            
        msg.checkpoint("%s>%s" % (self, endpoint.name))
            
    def do_produce_message(self, endpoint):
        if endpoint.name == "dstTmpPushBuffer":
            msg = self.src
        elif endpoint.name == "dstPushBuffer":
            msg = self.src
        else:
            return None
        self.src = None
        msg.checkpoint("%s>%s" % (self, endpoint.name))
        return msg
    
    def get_messages(self):
        return (self.coeff, self.src)

class MyOpticalFlowCalcComponent(representation.CommComponent):
    def init(self):
        self.p_feat = None
        self.t_error = None
        self.i_compute = {}
        self.j_compute = {}
        
    def do_consume_message(self, endpoint, msg):
        if endpoint.name == "prevFeaturesPullBuffer":
            self.p_feat = msg
        if endpoint.name == "trackErrorPullPushBuffer":
            self.t_error = msg
        if "prevImgPullBuffer" in endpoint.name:
            self.i_compute[endpoint.name] = msg
        if "nextImgPullBuffer" in endpoint.name:
            self.j_compute[endpoint.name] = msg
        
        msg.checkpoint("%s<%s" % (self, endpoint.name))
        
    def do_produce_message(self, endpoint):
        if endpoint.name == "featureStatusPushBuffer":
            msg = connection_mon.Message(self, 
                                        "OpticalFlowCalc #%d status" % self.id)
        elif endpoint.name == "nextFeaturesPushBuffer":
            msg = self.p_feat
        elif endpoint.name == "trackErrorPullPushBuffer":
            msg = self.t_error
        else:
            return None
        
        msg.checkpoint("%s>%s" % (self, endpoint.name))
        
        return msg
        
    def get_messages(self):
        return [self.p_feat, self.t_error] + self.i_compute.values() \
                + self.j_compute.values()
    
class MyHost: #(representation.CommHost):
    def init(self):
        self.mailbox = []

    def do_consume_message(self, endpoint, msg):
        msg.checkpoint("%s<%s" % (self, endpoint.name))
        self.mailbox.append(msg)
        if len(self.mailbox) > 10:
            self.mailbox.pop()
    def get_messages(self):
        return self.mailbox
        
definitions = {}
def initialize():
    definitions[model.representation.process.CommHost] = MyHost
    definitions[representation.CommComponent] = \
        {"SmoothAndSample": MySmoothAndSampleComponent,
         "OpticalFlowCalc": MyOpticalFlowCalcComponent}
    
    definitions[dma_link_mon.P2012HostDMAPushEndpoint] = HostDMAPush
    definitions[dma_link_mon.P2012HostDMAPullEndpoint] = HostDMAPull
