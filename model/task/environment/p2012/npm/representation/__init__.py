from . import dma_link
class P2012ComponentBufferInterface:
    # where did this class ended up ??
    pass

class P2012ComponentPullBufferInterface(P2012ComponentBufferInterface):
    installed = False
    
    def __init__(self, component, name):
        P2012ComponentBufferInterface.__init__(self, component, name,
                                               "PullBuffer")

    def consume_message(self, msg):
        return self.comm_entity.consume_message(self, msg)
        
    def do_pull(self):
        self.link.do_pull()
        
    def finish_pull(self):
        self.link.finish_pull()
        
        
    def install_breakpoints(self, vTable):
        this_compo = self.comm_entity

        pull = my_gdb.value_to_methodNameAddr(vTable["pull"])[0]
        try:
            PullBuffer_Pull_Breakpoint(pull)
        except my_gdb.AlreadyInstalledException:
            pass

class P2012ComponentBufferInterface:
    # where did this class ended up ?
    pass

class P2012ComponentPushBufferInterface(P2012ComponentBufferInterface):
    def __init__(self, component, name):
        p2012_mon.P2012ComponentBufferInterface.__init__(self, component, name,
                                                         "PushBuffer")
        
    def produce_message(self):
        return self.comm_entity.produce_message(self)
        
    def do_push(self):
        self.link.do_push()
        
    def do_waitTransfers(self):
        self.link.do_waitTransfers()
        
    def finish_waitTransfers(self):
        self.link.finish_waitTransfers()

    def install_breakpoints(self, vTable):        
        #type = 
        #   gdb.lookup_type("struct __component_npm_buffer_PushBuffer_vTable")
        #      .pointer()
        #assert type == vTable.type
        
        #use ADDR instead of function name
        #my_gdb.log.warning("push/pushAndGet/send breakpoint crash GDB, must "\
        #                   "find out why...")
        
        #str_push = "*%s" % str(my_gdb.value_to_methodNameAddr(vTable["push"])[1])
        
        str_push = my_gdb.value_to_methodNameAddr(vTable["push"])[0]
        try:
            PushBuffer_Push_Breakpoint(str_push)
        except my_gdb.AlreadyInstalledException:
            pass
        
        str_waitTransfers = \
                    my_gdb.value_to_methodNameAddr(vTable["waitTransfers"])[0]
        try:
            PushBuffer_WaitTransfers_Breakpoint(str_waitTransfers)
        except my_gdb.AlreadyInstalledException:
            pass
