from mcgdb.toolbox import my_gdb
from mcgdb.model import task
from mcgdb.model.task.environment.p2012.npm.capture.iface import pull_buffer, push_buffer
        
class FIFOBuffer: #(task.representation.P2012Link):
    def __init__(self, prod_ep, cons_ep, size, nb):
        p2012.representation.P2012Link.__init__(self)
        
        self.size = size
        self.nb = nb
        
        self.producer = prod_ep
        self.consumer = cons_ep
        self.add_endpoint(prod_ep)
        self.add_endpoint(cons_ep)
        
        self.message_queue = []
        
    def details(self):
        return "..." 
        
    def do_pull(self):
        pass
        
    def finish_pull(self):
        msg = self.message_queue.pop()
        if msg is not None:
            msg.check_breakpoint("FIFO", "%s/%s" % (self.producer.comm_entity,
                                                    self.producer.name))
            self.consumer.consume_message(msg)
        
        stop_next, stop_next_msgs = self.consumer.get_stop_next(msg)
        my_gdb.push_stop_requests(stop_next, stop_next_msgs)
    
    def get_messages(self):
        return self.message_queue
            
#########################
        
    def do_push(self):
        msg = self.producer.produce_message()
        if msg is not None:
            msg.check_breakpoint("%s/%s" % (self.producer.comm_entity,
                                            self.producer.name), "FIFO")
            self.message_queue.insert(0, msg)
            
        stop_next, stop_next_msgs = self.producer.get_stop_next(msg)
        my_gdb.push_stop_requests(stop_next, stop_next_msgs)
        
    def do_waitTransfers(self):
        pass
    def finish_waitTransfers(self):
        pass
        
def connect_fifo_link(consumerComp, consumerInputItf, producerComp, 
                      producerOutputItf, size, nb):
    consCompEP = pull_buffer.\
             P2012ComponentPullBufferInterface(consumerComp, consumerInputItf)
    prodCompEP = push_buffer.\
            P2012ComponentPushBufferInterface(producerComp, producerOutputItf)
    
    FIFOBuffer(prodCompEP, consCompEP, size, nb)
