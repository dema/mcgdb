import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb
from mcgdb.model.task import representation

from ..representation import dma_link

class NPM_configureDMABufferBreakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    
    def __init__(self, spec, is_push):
        mcgdb.capture.FunctionBreakpoint.__init__(self, spec)
        self.is_push = is_push
        
    def prepare_before (self):
        params = self.get_parameters()
        host_endpoint = representation.P2012HostEndpoint.\
                                        id_to_HostEndpoint(params["bufferId"])
        str_baseAddr = str(params["baseAddr"])
        int_increment = int(params["increment"])
        
        dma_link.configure_dma_buffer(host_endpoint, str_baseAddr, 
                                          int_increment)
        return (False, False, None)
    
    def get_parameters(self):
        "LOC(bufferId, baseAddr, increment)"
        
        data = {}
        data["bufferId"] = my_archi.first_arg(my_archi.VOID_P)
        data["baseAddr"] =  my_archi.nth_arg(2, my_archi.VOID_P)
        data["increment"] =  my_archi.nth_arg(3, my_archi.INT)
        return data
    
    def add_return_values(self, data):
        "ret = LOC(bufferId, baseAddr, increment)"
        data["ret"] =  my_archi.return_value(my_archi.INT)

def initialize():
    NPM_configureDMABufferBreakpoint("NPM_configureDMAPullBuffer", is_push=False)
    NPM_configureDMABufferBreakpoint("NPM_configureDMAPushBuffer", is_push=True)
