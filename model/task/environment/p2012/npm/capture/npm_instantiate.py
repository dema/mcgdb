import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb
from mcgdb.model.task import representation


class NPMInstantiateBreakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func
    
    def __init__(self, spec):
        mcgdb.capture.FunctionBreakpoint.__init__(self, spec)
        
    def prepare_before (self):
        params = self.get_parameters()
        params["str_name"] = str(params["name"].string())
        params["int_target"] = int(params["target"])
        try:
            my_gdb.log.warning(
                    "BEGINNING OF HUGLYNESS (newest_frame.older.read_var(id)")
            params["obsc_int_id"] = int(gdb.newest_frame().older()
                                                          .read_var("id"))
        except (ValueError, AttributeError):
            raise RuntimeError("Could prepare the ID for the current component")
        
        if p2012_mon.is_component_interesting(params["str_name"],
                                              params["int_target"]):
            return (False, True, params)
        else:
            return None
    @my_gdb.hugly
    def prepare_after (self, params):
        #read first value of instance ...
        instance = params["instance"].dereference()
        str_name = params["str_name"]
        int_target = params["int_target"]
        
        compo_this = p2012_mon.instantiate_component(str_name, int_target, 
                                                     instance, 
                                                     params["obsc_int_id"])
        
        if p2012_mon.catchable_state("instantiate"):
            my_gdb.push_stop_request("[Stopped on 'instantiate' method of %s]"
                                      % compo_this)
            
        return False
    
    def get_parameters(self):
        "CM_instantiate(name, target, instance)"
        data = {}
        data["instance"] =  my_archi.first_arg(my_archi.VOID_PP)
        data["name"] = my_archi.second_arg(my_archi.CHAR_P)
        data["target"] = my_archi.third_arg(my_archi.INT)
        
        return data
    
    def add_return_values(self, data):
        "ret = CM_instantiate(name, target, instance)"
        data["ret"] =  my_archi.return_value(my_archi.INT)


def initialize():
    p2012_mon.catchable_register("instantiate")

    NPMInstantiateBreakpoint("NPM_instantiateRTMComponent")
