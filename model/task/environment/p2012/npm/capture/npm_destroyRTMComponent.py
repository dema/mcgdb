import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb
from mcgdb.model.task import representation

class NPMDestroyBreakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func
    
    def __init__(self, spec):
        mcgdb.capture.FunctionBreakpoint.__init__(self, spec)
        
    def prepare_before (self):
        params = self.get_parameters()
        
        instance = params["instance_ptr"].dereference()
        comp = representation.CommComponent.instance_to_CommComponent(instance)
        if comp is None:
            return
        
        if representation.catchable_state("destroy"):
            my_gdb.push_stop_request("[Stopped on 'destroy' event of %s]"
                                      % comp)
        #comp.exit()
        
        return (False, False, None)
    
    def get_parameters(self):
        data = {}
        data["instance_ptr"] =  my_archi.first_arg(my_archi.VOID_PP)
        
        return data

def initialize():
    representation.catchable_register("destroy")

    NPMDestroyBreakpoint("NPM_destroyRTMComponent")
    NPMDestroyBreakpoint("NPM_destroyRTMComponent_v01")
