import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb
from mcgdb.model.task import representation

class NPM_instantiateFifoBufferBreakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func
    
    def __init__(self):
        mcgdb.capture.FunctionBreakpoint.__init__(self, "NPM_instantiateFIFOBuffer")
        
    def prepare_before (self):
        params = self.get_parameters()
        
        comp_consumerComp = representation.CommComponent.\
                             instance_to_CommComponent(params["consumerComp"])
        assert comp_consumerComp is not None
        
        str_consumerInputItf = str(params["consumerInputItf"].string())
        
        comp_producerComp = representation.CommComponent.\
                             instance_to_CommComponent(params["producerComp"])
        assert comp_producerComp is not None
        
        str_producerOutputItf = str(params["producerOutputItf"].string())
        
        int_bufferSize = int(params["bufferSize"])
        int_nbBuffer = int(params["nbBuffer"])
        fifo_link_mon.connect_fifo_link(comp_consumerComp, 
                                        str_consumerInputItf,
                                        comp_producerComp, 
                                        str_producerOutputItf,
                                        int_bufferSize, int_nbBuffer)
        return (False, False, None)

    
    def get_parameters(self):
        """
        NPM_instantiateFIFOBuffer(bufferId, consumComp, consumerInputItf, 
                                  producerComp, consumerOutputItf, bufferSize,
                                  nbBuffer)
        """
        data = {}
        data["bufferId"] = my_archi.first_arg(my_archi.VOID_P)
        data["producerComp"] = my_archi.second_arg(my_archi.VOID_P)
        data["producerOutputItf"] =  my_archi.third_arg(my_archi.CHAR_P)
        data["consumerComp"] = my_archi.nth_arg(4, my_archi.VOID_P)
        data["consumerInputItf"] =  my_archi.nth_arg(5, my_archi.CHAR_P)
        data["bufferSize"] =  my_archi.nth_arg(6, my_archi.INT)
        data["nbBuffer"] =  my_archi.nth_arg(7, my_archi.INT)
        return data
    
    def add_return_values(self, data):
        """
        ret = NPM_instantiateFIFOBuffer(bufferId, consumComp, 
                                        consumerInputItf, bufferSize, 
                                        nbBuffer)
        """
        data["ret"] =  my_archi.return_value(my_archi.INT)


def initialize():
    NPM_instantiateFifoBufferBreakpoint()
