import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb
from mcgdb.model.task import representation
from mcgdb.model.task.environment.p2012.npm.representation import dma_link

class NPM_instantiateDMABufferBreakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func
    
    def __init__(self, spec, is_push):
        mcgdb.capture.FunctionBreakpoint.__init__(self, spec)
        self.is_push = is_push
        
    def prepare_before (self):
        params = self.get_parameters()
        
        comp_consumerComp = representation.CommComponent.\
                               instance_to_CommComponent(params["consumComp"])
        str_consumerInputItf = str(params["consumerInputItf"].string())
        int_bufferSize = int(params["bufferSize"])
        int_nbBuffer = int(params["nbBuffer"])

        dma_link.connect_dma_buffer(self.is_push, str(params["bufferId"]),
                                    comp_consumerComp, 
                                    str_consumerInputItf, 
                                    int_bufferSize, int_nbBuffer)
        return (False, False, None)
    
    def get_parameters(self):
        "LOC(bufferId, consumComp, consumerInputItf, bufferSize, nbBuffer)"
        
        data = {}
        data["bufferId"] = my_archi.first_arg(my_archi.VOID_P)
        data["consumComp"] = my_archi.second_arg(my_archi.VOID_P)
        data["consumerInputItf"] =  my_archi.third_arg(my_archi.CHAR_P)
        data["bufferSize"] =  my_archi.nth_arg(4, my_archi.INT)
        data["nbBuffer"] =  my_archi.nth_arg(5, my_archi.INT)
        return data
    
    def add_return_values(self, data):
        """
        ret = LOC(bufferId, consumComp, consumerInputItf, bufferSize, nbBuffer)
        """
        data["ret"] =  my_archi.return_value(my_archi.INT)

def initialize():
    NPM_instantiateDMABufferBreakpoint("NPM_instantiateDMAPullBuffer",
                                       is_push=False)
    NPM_instantiateDMABufferBreakpoint("NPM_instantiateDMAPushBuffer", 
                                       is_push=True)
