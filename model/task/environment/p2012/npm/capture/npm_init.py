import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb
from mcgdb.model.task import representation


class NPMInitBreakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func
    
    def __init__(self, spec):
        mcgdb.capture.FunctionBreakpoint.__init__(self, spec)
        
    def prepare_before (self):
        p2012_mon.init_comete()
        
        if p2012_mon.catch["instantiate"]:
            my_gdb.push_stop_request("[Stopped on 'instantiate' "\
                                     "method of Host]")
            
        return (False, False, None)
        
def initialize():
    NPMInitBreakpoint("NPM_init")
