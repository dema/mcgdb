import gdb
import re

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb
from mcgdb.model.task import representation

class sdk_version:
    def __init__(self, id_, thread_method, state_type, run_method):
        self.id = id_
        self.thread_method = thread_method
        self.state_type = state_type
        self.run_method = run_method

sdk_v2011_3 = sdk_version("2001.3", "__component_thread_main_rtm", 
                          "__component_RTM_engine_state_t", "master_method")
sdk_current = sdk_v2011_3

class ThreadMainRTMBreakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func
    
    def __init__(self):
        model.capture.FunctionBreakpoint.__init__(self, sdk_current.thread_method)
        self.runMeth_bp = {}
        
    def prepare_before (self):
        engine_state_type = gdb.lookup_type(sdk_current.state_type).pointer()
        engine_state = my_archi.first_arg(engine_state_type)
        run_meth = engine_state[sdk_current.run_method]
        runMeth_name = my_gdb.value_to_methodNameAddr(run_meth)[0]
        
        if runMeth_name not in self.runMeth_bp:
            self.runMeth_bp[runMeth_name] = RunMethodBreakpoint(runMeth_name)
        #more thing to do here ...

class RunMethodBreakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.define_func
    
    def __init__(self, spec):
        mcgdb.capture.FunctionBreakpoint.__init__(self, spec)
    
    @my_gdb.hugly
    def prepare_before (self):
        compo_this = p2012_mon.CommComponent.get_selected_component()
        assert compo_this is not None
        
        assert compo_this.task is None
        compo_this.task = \
                      p2012_mon.CommComponent.TASK_MANAGER.get_selected_task()
        
        if compo_this.mind_this is None:
            # only if it's the fist time we see this component
            try:
                mind_this = gdb.newest_frame().read_var("_mind_this")
            except ValueError:
                raise gdb.error("Cannot initialize the component, " \
                                "no variable _mind_this available.")
            
            compo_this.mind_this = mind_this
            
            # Match dynamically the interfaces to their component type
            mind_this_type = p2012_mon.mind_get_type(mind_this)
            # need to register interface TYPE and lookup set.....
            #Breakpoints with it
            
            for itf in compo_this.endpoints:
                # can't test it this way, interfaces[itf.name].type is the 
                # implementation (DMA), not the interface
                #assert str(interfaces[itf.name].type) in iface.known_interfaces
                vTable = p2012_mon.mind_itf_desc_get_vTable(
                              p2012_mon.mind_type_get_itf_desc(mind_this_type,
                                                               itf.name))
                
                    
                iface.handle_interface(compo_this, itf, vTable)
        
            for field in mind_this_type.type.values():
                val = mind_this_type[field.name]
                if str(val.type) not in iface.known_interfaces:
                    continue
                if int(val.cast(my_archi.INT)) != 0:
                    continue
                
                class MyWP(gdb.Breakpoint):
                    def __init__(self, spec, name):
                        gdb.Breakpoint.__init__(self, spec, 
                                                type=gdb.BP_WATCHPOINT,
                                                wp_class=gdb.WP_READ, 
                                                internal=True)
                        self.name = name
                        self.silent = True
                        
                    def stop(self):
                        my_gdb.log.info("Trying to access an "
                                        "unbound interface: %s", self.name)
                        my_gdb.log.warning("execution almost crashed!")
                        return True
                watch = MyWP("*"+str(val.address), field.name)
                my_gdb.push_stop_request("[Unbound interface detected: %s::%s]" 
                                         % compo_this, field.name)
                
                
        if p2012_mon.catchable_state("run"):
            my_gdb.push_stop_request("[Stopped on 'run' method of %s]" 
                                     % compo_this)
            
        return (False, True, compo_this)
    
    def prepare_after(self, compo_this):
        compo_this.task = None
        
        return False

def initialize():
    mcgdb.catchable_register("run")
    ThreadMainRTMBreakpoint()   
