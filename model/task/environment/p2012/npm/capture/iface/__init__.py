known_interfaces = []

def handle_interface(compo, itf, vTable):
    itf.install_breakpoints(vTable)

def initialize():
    known_interfaces.append("npm_buffer_PushBuffer")
    known_interfaces.append("npm_buffer_PullBuffer")
