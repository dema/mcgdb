import mcgdb
from mcgdb.toolbox.target import my_archi

from mcgdb.model.task.environment.p2012.capture import mind

class PushBuffer_Push_Breakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.comm_func
    
    def __init__(self, location):
        mcgdb.capture.FunctionBreakpoint.__init__(self, location)
        
    def prepare_before (self):
        itf = mind.get_interface_from_inside()
        if itf is None:
            return  
        
        itf.do_push()
        return (False, False, None)
    
    def get_parameters(self):
        "Push (buffer)"
        data = {}
        return data
        
class PushBuffer_WaitTransfers_Breakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.comm_func
    
    def __init__(self, location):
        mcgdb.capture.FunctionBreakpoint.__init__(self, location)
        
    def prepare_before (self):
        itf = p2012_mon.get_interface_from_inside()
        if itf is None:
            return  
        
        itf.do_waitTransfers()
        return (False, True, itf)
    
    def prepare_after(self, itf):
        itf.finish_waitTransfers()
        
