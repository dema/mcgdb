import mcgdb
from mcgdb.toolbox.target import my_archi

from mcgdb.model.task.environment.p2012.capture import mind

        
class PullBuffer_Pull_Breakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.comm_func
    
    def __init__(self, location):
        mcgdb.capture.FunctionBreakpoint.__init__(self, location)
        
    def prepare_before (self):
        itf = mind.get_interface_from_inside()
        if itf is None:
            return  
        
        itf.do_pull()
        return (False, True, itf)
    
    def prepare_after (self, itf):
        itf.finish_pull()
    
    def get_parameters(self):
        "PullBuffer.pull()"
        data = {}
        return data
    
    def add_return_values(self, data):
        "buffer = PullBuffer.pull()"
        data["buffer"] =  my_archi.return_value(my_archi.INT_P)
