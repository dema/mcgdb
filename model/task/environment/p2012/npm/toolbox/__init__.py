import gdb

from mcgdb.model.task.environment.p2012.toolbox import gepop

current_simu = gepop

#################################
## Task Manager implementation ##
#################################

def get_selected_task():
    return current_simu.get_selected_task()

def get_mark(task):
    return current_simu.get_mark(task)

def switch_to(task, force=False):
    return current_simu.switch_to(task, force=force)

def is_selected_task(task):
    return current_simu.is_selected_task(task)

##                             ##
#################################

def get_selected_host_task():
    return current_simu.get_selected_host_task()
