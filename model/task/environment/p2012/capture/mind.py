"""
Toolset for accessing Mind datatype.
"""

import gdb
from mcgdb.toolbox import my_gdb

###########################################################"
@my_gdb.internal
def mind_get_type(mind_this):
    return mind_this["__component_internal_data"]["type"]

@my_gdb.internal
def mind_type_get_itf_desc(mind_this_type, itf_name):
    ARRAY_ITF = "(\S+)\[(\d+)\]" #interface[idx]
    ARRAY_NAME, ARRAY_IDX = (1, 2) 
    
    array_itf = re.match(ARRAY_ITF, itf_name)
    if array_itf is None:
        vTable = mind_this_type[itf_name]
    else:
        array = mind_this_type[array_itf.group(ARRAY_NAME)]
        assert array.type.code == gdb.TYPE_CODE_ARRAY
        vTable = array[int(array_itf.group(ARRAY_IDX))]
    return vTable

@my_gdb.internal
def mind_itf_desc_get_vTable(itf_desc):
    return itf_desc["meths"]

@my_gdb.internal
def mind_itf_desc_get_selfData(itf_desc):
    return itf_desc["selfData"]

@my_gdb.internal
def get_interface(this_compo, mind_this, mind_remote):
    for itf in this_compo.interfaces:
        type = mind_get_type(mind_this)
        self_data = \
                 mind_itf_desc_get_selfData(mind_type_get_itf_desc(type, itf))
        
        if self_data == mind_remote:
            return itf

@my_gdb.internal
def get_interface_from_inside():
    mind_callee = gdb.selected_frame().read_var("_mind_this")
    mind_caller = gdb.selected_frame().older().read_var("_mind_this")
    compo_caller = CommComponent.mind_this_to_CommComponent(mind_caller)
    if compo_caller is None:
        return None    
    itf_name = get_interface(compo_caller, mind_caller, mind_callee)
    return compo_caller.get_interface(itf_name)
