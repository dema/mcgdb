import gdb

from mcgdb.toolbox import my_gdb
from mcgdb.toolbox import aspect

@my_gdb.internal
def initialize():
    pass

representation = None
@my_gdb.internal
def activate():
    global representation; from .. import representation

    gdb.Command("omp barrier", gdb.COMMAND_OBSCURE, prefix=1)
    cmd_omp_barrier_pass()
    
class cmd_omp_barrier_pass (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp barrier pass", gdb.COMMAND_OBSCURE)
        
    def invoke (self, arg, from_tty):
        if not representation.RuntimeController.is_available():
            print("OpenMP runtime controller not available ...")
            return

        if not required_breakpoint_set(["barrier", "omp_preload"]):
            return
        
        if not mcgdb.toolbox.required_model("OpenMP preloaded helper library."):
            log_user.critical("Cannot run this command, OpenMP preloaded helper library not found.")
            return
        
        current = representation.Worker.get_current_worker()
        
        barrier = [job for worker in representation.Worker.list_ \
                       for job in worker.job_stack \
                       if isinstance(job, representation.Barrier)]

        if not barrier:
            print("Could not find any active barrier ...")
            return
        
        assert len(barrier) == 1
        barrier = barrier[0]

        def done():
            where = " ".join(set(barrier.loc.values()))
            print("{} ({}) passed.".format(barrier, where))

        representation.rt_ctrl.set_barrier(representation.Zone.Barrier, done)
        gdb.execute("continue")
        

