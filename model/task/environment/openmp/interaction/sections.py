import logging; log = logging.getLogger(__name__)

import gdb

from mcgdb.toolbox import my_gdb
from mcgdb.toolbox import aspect
from mcgdb.interaction import push_stop_request

representation = None
@my_gdb.internal
def activate():
    global representation; from .. import representation

    gdb.Command("omp sections", gdb.COMMAND_OBSCURE, prefix=1)
    cmd_omp_sections_new()
    cmd_omp_sections_next()
    cmd_omp_sections_finish()
    
    aspect.register("sections", aspects)
        
finish_sections_breakpoint = False
class cmd_omp_sections_finish (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp sections finish", gdb.COMMAND_OBSCURE)

    def invoke (self, args, from_tty):
        global finish_sections_breakpoint

        section = [section for section in representation.SectionJob.list_ if not section.has_completed]
        if not section:
            print("Cannot detect any active section ...")
            return
        
        finish_sections_breakpoint = True
        gdb.execute("continue")
        
new_sections_breakpoint = False
class cmd_omp_sections_new (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp sections new", gdb.COMMAND_OBSCURE)

    def invoke (self, args, from_tty):
        global new_sections_breakpoint
        on = not "off" in args

        if on:
            print("Setting a permanent breakpoint on section zones.")
        else:
            print("New section zone breakpoint disabled.")
        
        new_sections_breakpoint = on
        
next_sections_breakpoint = False
class cmd_omp_sections_next (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp sections step-by-step", gdb.COMMAND_OBSCURE)

    def invoke (self, args, from_tty):
        global next_sections_breakpoint
        on = not "off" in args
        if not on:
            print("Section zone breakpoint disabled.")
            next_sections_breakpoint = False
            return

        section = [section for section in representation.SectionJob.list_ if not section.has_completed]
        if not section:
            print("Cannot detect any active section ...")
            return

        if len(section) != 1:
            print("Several section zones seem to be active ({}), that's not handled yet. Defaulting to the first one.".format(", ".join(section)))
            
        section = section[0]
        
        if gdb.parameter("scheduler-locking") == "off":
            log.warn("Setting GDB scheduler-locking to 'on' for stepping between sections.")
            gdb.execute("set scheduler-locking on")

        next_sections_breakpoint = True
        
        log.info("{} has {} sections, {} already completed.".format(section,
                                                                    section.section_count,
                                                                    section.ended))
        print("Section zone breakpoint enabled on {}.".format(section))
        
    @staticmethod
    def section_finished(section):
        push_stop_request("[{} finished.]".format(section))

        log.warn("Zone stepping finished, disabling GDB scheduler-locking.")
        gdb.execute("set scheduler-locking off")
        global next_sections_breakpoint
        next_sections_breakpoint = False
    
#######################
# Aspect for sections #
#######################

def next():
    gdb.execute("next", to_string=True)
        
def aspects(Tracks):
    @Tracks(representation.SectionJob)
    class SectionJobTracker:
        def __init__(this): 
            this.args.parallel_job, this.args.worker, this.args.count = this.meth_args

            if new_sections_breakpoint:
                push_stop_request("[Beginning {}.]".format(this.self))
            
        def work_on_section(this):
            if this.args.section_id == 0:
                # this worker has completed its work, allow others to continue
                gdb.execute("set scheduler-locking off") 
                
            if next_sections_breakpoint:
                if this.args.section_id == 0:
                    cmd_omp_sections_next.section_finished(this.self)
                    return
                # we're inside a section, enforce single-thread execution
                gdb.execute("set scheduler-locking on")
                push_stop_request("[Starting execution of Section {}.]".format(this.args.section_id))
                my_gdb.set_prompt_hook(next)
            
        def ended(this):
            pass
        
        def completed(this):
            global finish_sections_breakpoint
            if finish_sections_breakpoint:
                gdb.execute("set scheduler-locking off")
                push_stop_request("[{} finished.]".format(this.self))
                finish_sections_breakpoint = False
            
            
