import gdb

from mcgdb.toolbox import my_gdb
from mcgdb.toolbox import aspect
from mcgdb.interaction import push_stop_request, cancel_stop_request

import logging; log = logging.getLogger("mcgdb.log.user.omp.task")

representation = None
@my_gdb.internal
def activate():
    global representation; from .. import representation

    gdb.Command("omp task", gdb.COMMAND_OBSCURE, prefix=1)
    cmd_omp_task_break()
    cmd_omp_task_break_next()
    cmd_omp_task_break_all()
    cmd_omp_task_set_prop()
    
    aspect.register("task", aspects)


task_breakpoints = set()
class cmd_omp_task_break (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp task break", gdb.COMMAND_OBSCURE, prefix=1)
    
    def invoke (self, arg, from_tty):
        args = gdb.string_to_argv(arg)
        args = list(map(int, args))

        for task in representation.TaskJob.list_:
            if task.number not in args:
                continue
            task_breakpoints.add(task)
            print("Catchpoint set on {}".format(task))
        
next_task_breakpoint = 0
class cmd_omp_task_break_next (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp task break next", gdb.COMMAND_OBSCURE)

    def invoke (self, args, from_tty):
        print("next")
        if args.isdigit():
            on = int(args)
        elif "off" in args:
            on = 0
        else:
            on = 1
            
        if on:
            print("Setting a temporary breakpoint on {} task execution{}.".format(
                    "the next" if on == 1 else on,
                    "" if on == 1 else "s"))
        else:
            print("Task execution breakpoint disabled.")

        cmd_omp_task_break_next.set_next_task_breakpoint(on)
        
    @staticmethod 
    def set_next_task_breakpoint(on_off):
        global next_task_breakpoint
        next_task_breakpoint = on_off

    @staticmethod 
    def inc_next_task_breakpoint():
        global next_task_breakpoint
        next_task_breakpoint += 1

    @staticmethod 
    def get_next_task_breakpoint():
        return next_task_breakpoint
        
class cmd_omp_task_set_prop (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp task set_property", gdb.COMMAND_OBSCURE)

    def invoke (self, args, from_tty):
        argv = gdb.string_to_argv(args)
        tid, key, value = argv
        try:
            task = representation.TaskJob.dict_[int(tid)]
        except KeyError:
            log.error("Cannot find task #{}".format(tid))
            return
        
        if task.set_property(key, value):
            print("Set {} = {} for {}".format(key, value, tid))
    
all_task_breakpoint = False
class cmd_omp_task_break_all (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp task break all", gdb.COMMAND_OBSCURE)

    def invoke (self, args, from_tty):
        global all_task_breakpoint
        global next_task_breakpoint
        
        on = not "off" in args

        if on:
            print("Setting a permanent breakpoint on the tasks execution.")
        else:
            print("All task breakpoint disabled.")
        
        all_task_breakpoint = on
        next_task_breakpoint = 0
        
new_task_breakpoint = False
class cmd_omp_task_new (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp task new", gdb.COMMAND_OBSCURE)

    def invoke (self, args, from_tty):
        global all_task_breakpoint
        on = not "off" in args

        if on:
            print("Setting a permanent breakpoint on the task creation.")
        else:
            print("New task breakpoint disabled.")
        
        new_task_breakpoint = on

deadlock_request_pushed = False
def reset_deadlock_notification(evt=None):
    global deadlock_request_pushed

    deadlock_request_pushed = False
    gdb.events.stop.disconnect(reset_deadlock_notification)
    
#######################
# Aspect for tasks    #
#######################

def aspects(Tracks):
    @Tracks(representation.TaskJob)
    class TaskJobTracker:
        def __init__(this):
            this.args.worker, = this.meth_args
            
            global new_task_breakpoint
            if not new_task_breakpoint:
                return
            
            push_stop_request("[Creation of {}.]".format(this.self))
        
        def start(this):
            global next_task_breakpoint
            do_break = False
            
            if next_task_breakpoint > 1:
                next_task_breakpoint -= 1

            if all_task_breakpoint:
                do_break = True
                
            if next_task_breakpoint == 1:
                next_task_breakpoint = 0
                do_break = True

            if this.self in task_breakpoints:
                task_breakpoints.remove(this.self)
                do_break = True
            
            if do_break:
                push_stop_request("[Beginning the execution of {}.]".format(this.self))

        def set_property(this, after):
            # stop on deadlock mechanism
            
            is_locked = representation.TaskJob.detect_lock()

            DEADLOCK_STOP_REQUEST = "[Deadlock detected, stopping the execution.]"
            
            global deadlock_request_pushed
            if deadlock_request_pushed:
                if not is_locked:
                    # we already pushed the request, but the graph is unlocked
                    cancel_stop_request(DEADLOCK_STOP_REQUEST)
                    reset_deadlock_notification()
                return

            if not is_locked:
                # request not pushed, graph not locked, bye
                return

            push_stop_request(DEADLOCK_STOP_REQUEST)
            gdb.events.stop.connect(reset_deadlock_notification)
            deadlock_request_pushed = True
