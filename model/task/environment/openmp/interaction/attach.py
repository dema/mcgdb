import gdb

from mcgdb.toolbox import my_gdb

representation = None

@my_gdb.internal
def activate():
    global representation; from .. import representation

    cmd_omp_attach()
    

class cmd_omp_attach (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp attach", gdb.COMMAND_OBSCURE)

    def invoke (self, args, from_tty):
        if representation._attach is None:
            print("ERROR: No `attach` mechanism currently installed.")
            return
        representation.is_attached = True
        representation._attach()
