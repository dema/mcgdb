from __future__ import print_function

import gdb

from mcgdb.toolbox import my_gdb

representation = None
zone_codes = None
def initialize_zone_codes():
    return {
        representation.ParallelJob:
            lambda self, worker : "Parallel Zone {}".format(self.number),
        representation.SectionJob :
            lambda self, worker : "Sections Zone {}.{}".format(self.number,
                                                           self.sections[worker]),
        representation.SingleJob:
            lambda self, worker : "Single Zone {}".format(self.number),
        representation.Barrier:
            lambda self, worker : "Barrier {}".format(self.number),
        representation.CriticalJob:
            lambda self, worker : "Critical Zone {}{}".format(self.number,
                                         "@" if self.visitor is worker else ""),
        representation.TaskJob:
            lambda self, worker : "Task {}".format(self.number)
    }

@my_gdb.internal
def activate():
    global representation
    from .. import representation

    global zone_codes
    zone_codes = initialize_zone_codes()
    
    cmd_omp_show()
    
class cmd_omp_show (gdb.Command):
    """ Shows the position of the workers"""
    
    def __init__ (self):
        gdb.Command.__init__(self, "omp show", gdb.COMMAND_OBSCURE)
        
    def invoke (self, arg, from_tty):
        current = representation.Worker.get_current_worker()
        
        for worker in representation.Worker.list_:
            print("*" if worker is current else " ", end="")
            print("#{} ".format(worker.number), end="")

            print(" >> ".join([zone_codes[job.__class__](job, worker) \
                                  for job in worker.job_stack]))
