import gdb


import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")


from mcgdb.toolbox import my_gdb
from mcgdb.toolbox import aspect
import mcgdb.interaction


representation = None
current_worker = None


@my_gdb.internal
def activate():
    global representation; from .. import representation
    global current_worker; current_worker = representation.Worker.get_current_worker

    cmd_omp_loop()
    cmd_omp_loop_profile()
    cmd_omp_loop_profile_iterations()
    cmd_omp_loop_last_range()
    cmd_omp_loop_break()
    cmd_omp_loop_break_after_next()
    fct_omp_loop_start()
    aspect.register("loop-profile", loop_aspects)

    
class cmd_omp_loop (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp loop", gdb.COMMAND_OBSCURE, prefix=True)

        
class cmd_omp_loop_last_range (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp loop last_range", gdb.COMMAND_OBSCURE, prefix=1)

    def invoke (self, args, from_tty):
        if not required_breakpoint_set(["loop static"]):
            return
        
        loop, iteration = representation.ForLoopJob.get_last_loop_of(current_worker())

        if not loop:
            log_user.warning("Couldn't find any loop for the current worker.")

        already = "(already terminated)" if iteration[-1] else ""
        log_user.info("Found loop {}@{} [{} --> {}]{}.".format(loop, hex(loop.loc),
                                                               iteration[0], iteration[1],
                                                               already))

        
class cmd_omp_loop_break (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp loop break", gdb.COMMAND_OBSCURE, prefix=True)

        
class cmd_omp_loop_break_after_next (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp loop break after_next", gdb.COMMAND_OBSCURE, prefix=1)
        
    def invoke (self, args, from_tty):
        if not required_breakpoint_set(["loop static"]):
            return
        
        last_loop = representation.ForLoopJob.get_last_loop()

        if not last_loop:
            log_user.warn("Could not find any last loop")
            return

        action = "Enable" if not last_loop.break_after_next else "Disable"

        log_user.info("{} break-after-next breakpoint on loop {}.".format(action, repr(last_loop)))
        last_loop.break_after_next = not last_loop.break_after_next

        
iteration_profiling_enabled = False

class cmd_omp_loop_profile (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp loop profile", gdb.COMMAND_OBSCURE, prefix=True)

    def invoke (self, arg, from_tty):
        log_user.warn("Please specify what kind of profiling you want to do.")

        
class cmd_omp_loop_profile_iterations (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp loop profile iterations", gdb.COMMAND_OBSCURE, prefix=1)

    def invoke (self, args, from_tty):
        global iteration_profiling_enabled

        if not required_breakpoint_set(["loop static"]):
            return
        
        iteration_profiling_enabled = not iteration_profiling_enabled
        action = "enabled" if iteration_profiling_enabled else "disabled"

        log_user.info("Loop iteration profiling {}.".format(action))
        
        if not iteration_profiling_enabled and gdb.parameter("scheduler-locking") == "on":
            gdb.execute("set scheduler-locking off")
            log_user.info("Scheduler locking disabled.")        

            
def loop_aspects(Tracks):
    from .. import representation
    
    @Tracks(representation.ForLoopJob)
    class ForLoopJob:
        def __init__(this):
            pass
                
        def start_work_on(this):
            if iteration_profiling_enabled:
                gdb.execute("set scheduler-locking on")
                #print("START profiling {} {}".format(this.args.lower, this.args.upper))
                gdb.execute("profile manual start")
                
        def stop_work_of(this):
            log.warn("stop work of {}".format(current_worker()))
            if iteration_profiling_enabled:
                gdb.execute("profile manual stop")
                gdb.execute("set scheduler-locking off")
                
        def done(this):
            global iteration_profiling_enabled
            
            if iteration_profiling_enabled:
                # set loop.break_after_next = True
                
                mcgdb.interaction.push_stop_request("Iteration profiling finished.")
                iteration_profiling_enabled = False


class fct_omp_loop_start(gdb.Function):
    def __init__ (self):
        gdb.Function.__init__(self, "omp_loop_start")
        
    def invoke (self):
        if not required_breakpoint_set(["loop static"]):
            return
        
        worker = current_worker()
        loop, iteration = representation.ForLoopJob.get_last_loop_of(worker)
        
        if not loop: return None
        
        lower, upper, done = iteration

        return lower
        
        
