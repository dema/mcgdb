import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

import gdb

representation = None
capture = None

def activate():
    global representation, capture
    from .. import representation
    from .. import capture

    cmd_omp_breakpoint_sets()
    cmd_omp_breakpoint_sets_status()
    cmd_omp_breakpoint_sets_enable()
    cmd_omp_breakpoint_sets_disable()
   

class cmd_omp_breakpoint_sets (gdb.Command):

    def __init__ (self):
        gdb.Command.__init__(self, "omp breakpoint_sets", gdb.COMMAND_OBSCURE, prefix=True)

    def invoke (self, args, from_tty):
        if not args:
            gdb.execute("omp breakpoint_sets status")
        else:
            log_user.warn("Command not recognized ...")
        
class cmd_omp_breakpoint_sets_status (gdb.Command):

    def __init__ (self):
        gdb.Command.__init__(self, "omp breakpoint_sets status", gdb.COMMAND_OBSCURE)
        
    def invoke (self, args, from_tty):
        status = capture.OmpFunctionBreakpoint.breakpoint_sets_status
        
        for setname  in sorted(capture.OmpFunctionBreakpoint.breakpoint_sets.keys()):
            bpt_set = capture.OmpFunctionBreakpoint.breakpoint_sets[setname]
            
            log_user.warn("OpenMP '{}' set: {}".format(setname, "enabled" if status[setname] else "disabled"))
            for bpt in bpt_set:
                log_user.info("\t {}".format(bpt.__class__.__name__))
            log_user.info("")
                
    
class cmd_omp_breakpoint_sets_enable (gdb.Command):

    def __init__ (self):
        gdb.Command.__init__(self, "omp breakpoint_sets enable", gdb.COMMAND_OBSCURE)
        
    def invoke (self, args, from_tty):
        status = capture.OmpFunctionBreakpoint.breakpoint_sets_status
        
        if args == "all":
            for setname, is_enabled in status.items():
                if is_enabled: continue
                log_user.info("=== {} ===".format(setname))
                self.invoke(setname, from_tty=from_tty)
                log_user.info("")
            return
                
        found = True
        if args is not "" and args not in status:
            log_user.info("Cannot find '{}' breakpoint sets (use autocompletion).".format(args))
            found = False
        elif args is not "" and status[args]:
            log_user.info("Breakpoint set '{}' already enabled.".format(args))
            return
            
        if not found or args is "":
            disabled = [str(setname) for setname, is_enabled in status.items() if not is_enabled]
            log_user.info("Sets that can be enabled: {}".format(", ".join(disabled)))
            return

        for bpt in capture.OmpFunctionBreakpoint.breakpoint_sets[args]:
            log_user.info("Enabling {} ...".format(bpt.__class__.__name__))
            bpt.enabled = True
            
        status[args] = True

    def complete(self, text, word):
        status = capture.OmpFunctionBreakpoint.breakpoint_sets_status
        disabled_prefixed = [str(setname) for setname, is_enabled in status.items() if not is_enabled and setname.startswith(word)]
        
        return disabled_prefixed

class cmd_omp_breakpoint_sets_disable (gdb.Command):

    def __init__ (self):
        gdb.Command.__init__(self, "omp breakpoint_sets disable", gdb.COMMAND_OBSCURE)
        
    def invoke (self, args, from_tty):
        status = capture.OmpFunctionBreakpoint.breakpoint_sets_status

        if args == "all":
            for setname, is_enabled in status.items():
                if not is_enabled: continue
                log_user.info("=== {} ===".format(setname))
                self.invoke(setname, from_tty=from_tty)
                log_user.info("")
            return
                
        found = True
        if args is not "" and args not in status:
            log_user.info("Cannot find '{}' breakpoint sets (use autocompletion).".format(args))
            found = False
        elif args is not "" and not status[args]:
            log_user.info("Breakpoint set '{}' already disabled.".format(args))
            return
            
        if not found or args is "":
            enabled = [str(setname) for setname, is_enabled in status.items() if is_enabled]
            log_user.info("Sets that can be disabled: {}".format(", ".join(enabled)))                          
            return

        for bpt in capture.OmpFunctionBreakpoint.breakpoint_sets[args]:
            log_user.info("Disabling {} ...".format(bpt.__class__.__name__))
            bpt.enabled = False
            
        status[args] = False
        
    def complete (self, text, word):
        status = capture.OmpFunctionBreakpoint.breakpoint_sets_status
        enabled_prefixed = [str(setname) for setname, is_enabled in status.items() if is_enabled and str(setname).startswith(text)]

        return enabled_prefixed

