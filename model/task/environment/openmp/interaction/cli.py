import gdb

from mcgdb.toolbox import my_gdb
from mcgdb.toolbox import aspect

@my_gdb.internal
def initialize():
    #aspect.register("prompt", aspects)
    pass

@my_gdb.internal
def activate():
    pass
    
def aspects(Tracks):    
    from .. import representation
    
    @Tracks(representation.Worker)
    class WorkerTracker:
        def __init__(this):
            pass

    @Tracks(representation.ParallelJob)
    class ParallelJobTracker:
        def __init__(this):
            this.args.num_workers, this.args.parent_worker = this.meth_args
            pass
            
        def start_working(this):
            pass

        def stop_working(this):
            pass
            
    @Tracks(representation.SingleJob)
    class SingleTracker:
        def __init__(this):
            this.args.worker, this.args.parallel_job = this.meth_args
            pass
            
        def enter(this):
            pass

        def finished(this):
            pass

        def completed(this):
            pass
            
    @Tracks(representation.TaskJob)
    class TaskJobTracker:
        def __init__(this):
            this.args.worker, = this.meth_args
            pass
            
        def start(this):
            pass
        
        def finish(this):
            pass

    @Tracks(representation.CriticalJob)
    class CriticalJobTracker:
        def __init__(this): 
            this.args.worker,this. args.parallel_job = this.meth_args
            pass
        
        def try_enter(this):
            pass
        
        def entered(this):
            pass
        
        def left(this):
            pass
            

    @Tracks(representation.Barrier)
    class BarrierTracker:
        def __init__(this): 
            this.args.parallel_job, this.args.worker = this.meth_args
            pass
        
        def reach_barrier(this):
            pass
        
        def leave_barrier(this):
            pass
        
    @Tracks(representation.SectionJob)
    class SectionJobTracker:
        def __init__(this): 
            this.args.parallel_job, this.args.worker, this.args.count = this.meth_args
            pass
            
        def work_on_section(this):
            pass
        
        def ended(this):
            pass
        
        def completed(this):
            pass
