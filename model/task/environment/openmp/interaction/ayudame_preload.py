import logging; log = logging.getLogger(__name__)
import os
import gdb

import mcgdb
from mcgdb.toolbox import my_gdb
from mcgdb.toolbox import aspect

def on_ayu_loaded():
    aspect.register("ayudame_preload", ayudame_preload_aspects)
    
def activate():
    my_gdb.exec_on_sharedlibrary(on_ayu_loaded, "libayudame.so")
    
def ayudame_preload_aspects(Tracks):
    from .. import representation
    
    @Tracks(representation.TaskJob)
    class TaskJobTracker:
        def __init__(this):
            pass
        
        def set_property(this):
            if this.args.name == "state" and this.args.value == "blocked_by_ayudame":
                print("[{} is blocked by Ayudame (ayu_id={}).]".format(this.self, this.self.properties["ayu_id"]))
