import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user.temanejo")
from threading import Thread
import fcntl, signal, sys, time, os
import atexit
import inspect

import gdb

import mcgdb
from mcgdb.toolbox import my_gdb
from mcgdb.toolbox import aspect
from mcgdb.toolbox import SimpleClass
from . import sequence

PY3 = sys.version_info >= (3, 0)

sys.path.append("/home/kevin/travail/git/ayudame/install/lib/python2.7/site-packages")

class Mocker:
    def __init__(self, ref_class):
        self.ref_class = ref_class
        self.calls = []
        
    def __getattr__(self, name):
        ref_attr = getattr(self.ref_class, name)
        
        def foo(*args, **kwargs):
            self.calls.append((ref_attr, args, kwargs))
            
        return foo

    def run(self, ref_obj):
        for ref_attr, args, kwargs in self.calls:
            ref_attr(ref_obj, *args, **kwargs)

ayu_socket = None
ayu = None
tem = None
representation = None

DO_IT_LATER = []

@my_gdb.internal
def activate():
    global representation; from .. import representation
        
    cmd_Ayudame()
    cmd_Temanejo()
    cmd_Temanejo_debug()
    cmd_Temanejo_nop()
    cmd_Temanejo_debug_userdata()
    
    global ayu
    ayu = Mocker(AyudameProxy)
    aspect.register("ayudame", ayudame_aspects)
    
def on_activated():
    cmd_Temanejo_init()
    cmd_Ayudame_preload()

def on_sequence_updated(sequence):
    ayu.user_data("MCGDB#SEQUENCE#{}".format(sequence))
    
mocker = None
def on_connected(args):
    host = "localhost"
    port = 8888
    
    for arg in gdb.string_to_argv(args):
        if arg.startswith("+host="):
            host = arg.partition("=")[-1]
        elif arg.startswith("+port="):
            port = arg.partition("=")[-1]
        else:
            print("Argument not recognized: {}".format(arg))
            
    global ayu, mocker

    if mocker is None:
        mocker = ayu if ayu and ayu.calls else None
    
    ayu = AyudameProxy(host, port)
    
    if mocker:
        mocker.run(ayu)
    
    cmd_Temanejo_finish()
    gdb.events.cont.connect(do_later_work)
    cmd_Temanejo_jobs()

    sequence.on_update.add(on_sequence_updated)
    
def do_later_work(not_used=None):
    if not DO_IT_LATER: return
    
    log.info("Execute Temanejo stacked commands.")
    for i, (descr, work) in enumerate(DO_IT_LATER):
        try:
            log.info(descr)
            work()
        except Exception as e:
            log.warning("{} failed: {}".format(descr, e))
    DO_IT_LATER[:] = []
    
########################
## Local interactiion ##
########################
            
class cmd_Temanejo(gdb.Command):
    def __init__(self, limited=False):
        gdb.Command.__init__ (self, "omp temanejo", gdb.COMMAND_NONE, prefix=True)

    def invoke (self, args, from_tty):
        global ayu_socket
        
        if ayu_socket is not None:
            log.info("Ayudame loaded already loaded.")
            return

        try:
            import ayudame
            import ayudame.ayu_socket
            ayu_socket = ayudame.ayu_socket

            assert ayu_socket is not None
        except ImportError as e:
            print("Cannot import Ayudame.ayu_socket, please check your PYTHONPATH.")
            raise e

        on_activated()

        if args:
            gdb.execute("omp temanejo {}".format(args))
            
class cmd_Temanejo_jobs(gdb.Command):
    def __init__(self, limited=False):
        gdb.Command.__init__ (self, "omp temanejo jobs", gdb.COMMAND_NONE, prefix=True)
        
        cmd_Temanejo_jobs_run()
        cmd_Temanejo_jobs_run_all()
        cmd_Temanejo_jobs_cancel()

    def invoke (self, args, from_tty):
        for i, (descr, work) in enumerate(DO_IT_LATER):
            print("#{} {}".format(i, descr))
        
class cmd_Temanejo_jobs_run_all(gdb.Command):
    def __init__(self, limited=False):
        gdb.Command.__init__ (self, "omp temanejo jobs run all", gdb.COMMAND_NONE)

    def invoke (self, args, from_tty):
        do_later_work()
    
class cmd_Temanejo_jobs_cancel(gdb.Command):
    def __init__(self, limited=False):
        gdb.Command.__init__ (self, "omp temanejo jobs cancel", gdb.COMMAND_NONE)

    def invoke (self, args, from_tty):
        if not args:
            print("Please provide a job id.")
            return
        
        idx = int(args)
        assert idx < len(DO_IT_LATER)

        print("Canceling Job #{}: {}".format(idx, DO_IT_LATER[idx][0]))
        del DO_IT_LATER[idx]

class cmd_Temanejo_jobs_run(gdb.Command):
    def __init__(self, limited=False):
        gdb.Command.__init__ (self, "omp temanejo jobs run", gdb.COMMAND_NONE, prefix=True)

    def invoke (self, args, from_tty):
        if not args:
            print("Please provide a job id.")
            return
        try:
            idx = int(args) # ValueError
            assert idx < len(DO_IT_LATER) # AssertionError
        except:
            print("Invalid job id: '{}'".format(args))
            return
        
        descr, work = DO_IT_LATER[idx]
        del DO_IT_LATER[idx]
        
        print("Running Job #{}: {}".format(idx, descr))
        work()
        
class cmd_Temanejo_debug(gdb.Command):
    def __init__(self, limited=False):
        gdb.Command.__init__ (self, "omp temanejo debug", gdb.COMMAND_NONE, prefix=True)

    def invoke (self, args, from_tty):
        if args:
            TemAyuProxy.debug = args.upper() == "ON"
            
        print("Ayudame debug is {}".format("on" if TemAyuProxy.debug else "off"))

class cmd_Temanejo_nop(gdb.Command):
    def __init__(self, limited=False):
        gdb.Command.__init__ (self, "omp temanejo debug nop", gdb.COMMAND_NONE)

    def invoke (self, args, from_tty):
        if args:
            TemAyuProxy.nop = args.upper() == "ON"
            
        print("Ayudame nop-mode is {}".format("on" if TemAyuProxy.nop else "off"))
        
class cmd_Temanejo_debug_userdata(gdb.Command):
    def __init__(self, limited=False):
        gdb.Command.__init__ (self, "omp temanejo debug userdata", gdb.COMMAND_NONE)

    def invoke (self, args, from_tty):
        ayu.user_data("MCGDB#{}".format(args))

class cmd_Temanejo_finish(gdb.Command):
    def __init__(self, limited=False):
        gdb.Command.__init__ (self, "omp temanejo debug finish", gdb.COMMAND_NONE)
        
    def invoke (self, args, from_tty):
        global ayu
        ayu.finish()
        gdb.execute("omp temanejo debug nop on")

class cmd_Temanejo_debug_disconnect(gdb.Command):
    ayu_proxy = None
    
    def __init__(self, limited=False):
        gdb.Command.__init__ (self, "omp temanejo debug disconnect", gdb.COMMAND_NONE, prefix=True)
        
        
    def invoke (self, args, from_tty):
        if not cmd_Temanejo_debug_reconnect.instanciated:
            cmd_Temanejo_debug_reconnect()
            
        global ayu
        
        assert cmd_Temanejo_debug_disconnect.ayu_proxy is None
        cmd_Temanejo_debug_disconnect.ayu_proxy = ayu

        # create a new mocker to record ayu events
        ayu = Mocker(AyudameProxy)
        
        print("Ayudame proxy disconnected. Event recorder installed.")
        
class cmd_Temanejo_debug_reconnect(gdb.Command):
    instanciated = False
    
    def __init__(self, limited=False):
        gdb.Command.__init__ (self, "omp temanejo debug reconnect", gdb.COMMAND_NONE, prefix=True)
        cmd_Temanejo_debug_reconnect.instanciated = True
        
    def invoke (self, args, from_tty):
        assert cmd_Temanejo_debug_disconnect.ayu_proxy is not None
        global ayu, mocker
        
        # replay saved events
        mocker = ayu
        assert isinstance(ayu, Mocker)
        mocker.run(ayu)
        # reinstall ayu proxy
        ayu = cmd_Temanejo_debug_disconnect.ayu_proxy
        # cleanup storage location
        cmd_Temanejo_debug_disconnect.ayu_proxy = None
        print("Ayudame proxy reconnected")
    
class cmd_Temanejo_init(gdb.Command):
    """Connects mcGDB to Temanejo. Options:
    +host=localhost
    +port=8888"""
    def __init__(self, limited=False):
        gdb.Command.__init__ (self, "omp temanejo init", gdb.COMMAND_NONE)

    def invoke (self, args, from_tty):
        if isinstance(ayu, AyudameProxy):
            print("Ayudame proxy is already initialized.")
            return
        
        args = args.partition("#")[0]
        
        on_connected(args)
        
class cmd_Ayudame(gdb.Command):
    def __init__(self, limited=False):
        gdb.Command.__init__ (self, "omp ayudame", gdb.COMMAND_NONE, prefix=True)

    def invoke (self, args, from_tty):
        print("Argument not recognized: {}".format(args))
        
class cmd_Ayudame_preload(gdb.Command):
    def __init__(self, limited=False):
        gdb.Command.__init__ (self, "omp ayudame preload", gdb.COMMAND_NONE)

    def invoke (self, args, from_tty):
        from mcgdb.model.task.environment.openmp.capture.ompss import ayudame_preload
        ayudame_preload.configure(args)      

########################
## Comm with Temanejo ##
########################

class TemAyuProxy:
    CLIENT_ID = 1000

    SLEEP_TIME_BETWEEN_RQS = 0.5
    
    debug = False
    nop  =  False

    def __init__(self):
        self._ts = 0
        
        def do_init():
            self.socket = ayu_socket.Ayu_Socket()
            self.conf_socket(self.socket)
            
            if not self.__class__.nop:
                self.socket.init()
            
            log_user.error("Please note that Temanejo cannot control *GDB*, only mcGDB.")
            log_user.error("Manually `continue` the execution when needed.")
                
            self.dead = False
            self.thread = Thread(target=self._thread_listener)
            self.thread.setDaemon(True)
            self.thread.start()
            atexit.register(self.finish)
            
        with my_gdb.may_start_threads():
            do_init()

    @property
    def ts(self):
        self._ts += 1
        return self._ts

    def finish(self):
        if TemAyuProxy.debug:
            print("AYU_PROXY: shutdown_socket()")
            
        if not TemAyuProxy.nop:
            if not self.dead:
                self.dead = True
                self.thread.join()
            self.socket.shutdown_socket()
    
    def _write(self, what):
        if TemAyuProxy.debug:
            self._print(what)

        if not TemAyuProxy.nop:
            self.socket.socket_write(what)

    def _print(self, event):
        if event is None:
            print("AYU_PROXY: No event.")
        elif event.get_event() == ayu_socket.AYU_ADDTASK:
            task = ayu_socket.Intern_event_castToEventTask(event)
            print("AYU_PROXY: task #{} '{}' (scope: {} / client: {})".format(
                    task.get_task_id(),
                    task.get_task_label(),
                    task.get_scope_id(),
                    task.get_client_id()
                    ))
            
        elif event.get_event() == ayu_socket.AYU_ADDDEPENDENCY:
            dep = ayu_socket.Intern_event_castToEventDependency(event)
            print("AYU_PROXY: dependency #{}: {} -> {} ({})".format(
                    dep.get_dependency_id(),
                    dep.get_from_id(),
                    dep.get_to_id(),
                    dep.get_dependency_label()))
            
        elif event.get_event() == ayu_socket.AYU_REQUEST:
            req = ayu_socket.Intern_event_castToRequest(event)
            print("AYU_PROXY: request: {}->{}".format(req.get_key(), req.get_value()))
            
        elif event.get_event() == ayu_socket.AYU_USERDATA:
            data = ayu_socket.Intern_event_castToEventUserdata(event)
            print("AYU_PROXY: userdata: {}->{}".format(data.get_data(), data.get_size()))            
        elif event.get_event() == ayu_socket.AYU_SETPROPERTY:
            prop = ayu_socket.Intern_event_castToEventProperty(event)
            print("AYU_PROXY: property for #{}, {}={}".format(prop.get_property_owner_id(),
                                              prop.get_key(),
                                              prop.get_value()))
            
        else:
            print("AYU_PROXY: unknown event type: {}".format(event.get_event()))
        
class AyudameProxy(TemAyuProxy):    
    def __init__(self, host="localhost", port=8888):
        self.host = host
        self.port = port
        self.inout_uid_cpt = 0
        self.inout_uids = {}
        
        TemAyuProxy.__init__(self)
        
    def conf_socket(self, socket):
        socket.set_client_data(self.port, self.host)
        
    def _thread_listener(self):
        from . import task as task_interaction
        
        assert not TemAyuProxy.nop

        need_reprint = False
        key = val = None
        while not self.dead:
            try:
                event = self.socket.socket_read()
                if not event:
                    if need_reprint:
                        if key == "step_client" and val == "1":
                            cnt = task_interaction.cmd_omp_task_next.get_next_task_breakpoint()
                            assert cnt != 0
                            if cnt == 1:
                                print("[Single-step request received from Temanejo.]")
                            else:
                                print("[FastForward*{} request received from Temanejo.]".format(cnt))
                        my_gdb.reprint_prompt()
                        need_reprint = False
                    time.sleep(AyudameProxy.SLEEP_TIME_BETWEEN_RQS)
                    continue
            
                need_reprint = True
                if TemAyuProxy.debug:
                    self._print(event)
        
                if not event.get_event() == ayu_socket.AYU_REQUEST:
                    log.warning("Event received from Temanejo is not a request ... (type={})".format(event.get_event()))
                    log.warning("Event ignored.")
                    continue

                request = ayu_socket.Intern_event_castToRequest(event)

                key = request.get_key()
                val = request.get_value()

                if key == "step_client" and val == "1":
                    task_interaction.cmd_omp_task_next.inc_next_task_breakpoint()
                elif key in ("break_task", "unbreak_task"):
                    try:
                        taskjob = representation.TaskJob.key_to_value(int(val))
                        assert taskjob is not None
                    except Exception as e:
                        log.error("Could set task breakpoint on Temanejo's request: {}".format(e))
                        continue

                    task_interaction.task_breakpoints.add(taskjob)
                    print("Breakpoint set on {}.".format(taskjob))
                elif key in ("block_task", "unblock_task"):
                    set_blocked = (key == "block_task") 
                    
                    try:
                        task = representation.TaskJob.key_to_value(int(val))
                        assert task is not None
                    except Exception as e:
                        log.error("Could not block task on Temanejo's request: {}".format(e))
                        continue

                    def later():
                        task.set_property("__blocked", set_blocked)
                        print("Task #{} {}.".format(val, "blocked" if set_blocked else "unblocked"))

                    action = "{} task #{}".format("Block" if set_blocked else "Unblock",
                                                  task.number)
                    DO_IT_LATER.append((action,
                                        later))
                    print("Action '{}' registered.".format(action))
                else:
                    log.warning("Unexpected request received fron Temanejo: {}->{}".format(key, val))
                    log.warning("Event ignored.")
                    
            except Exception as e:
                log.error("Unexpected exception in Temanejo thread listener: {}".format(e))
                need_reprint = True
                import traceback, sys
                traceback.print_exc(file=sys.stdout)
                
    def new_task(self, task_id):
        scope = 1
        label = "Task #{}".format(task_id)
        task = ayu_socket.Intern_event_create_intern_event_task(
            AyudameProxy.CLIENT_ID,
            ayu_socket.AYU_ADDTASK,
            self.ts, task_id, scope, label)
        self._write(task)

    @staticmethod
    def dep_uid(dep_id):
        MAX_UID_SPACE = 5000
        assert dep_id < MAX_UID_SPACE
        return MAX_UID_SPACE - dep_id # uids should not collide ...
        
    def add_dependency(self, dep_id, dep_uid, from_tid, to_tid, addr):
        dep = ayu_socket.Intern_event_create_intern_event_dependency(
            AyudameProxy.CLIENT_ID, ayu_socket.AYU_ADDDEPENDENCY, self.ts,
            AyudameProxy.dep_uid(dep_uid),
            from_tid,
            AyudameProxy.CLIENT_ID,
            to_tid,
            "#{} on {}".format(dep_id, addr))
        
        self._write(dep)

    # actually belongs to aspect DependenceTracker,
    # but there is no way to put it there
    def declare_dependency(self, dep, inout, writer, reader):
        if inout not in self.inout_uids:
            self.inout_uids[inout] = {}

        if reader in self.inout_uids[inout]:
            log.warn("Dependency {} {} already declared.".format(writer, reader))
            return

        self.inout_uids[inout][reader] = uid = self.inout_uid_cpt
        self.inout_uid_cpt += 1
        
        self.add_dependency(dep.number, uid,
                           writer.number, reader.number,
                           dep.address)
        self.dependency_property(inout, "uid", uid)
        self.dependency_property(inout, "ready", inout.ready)
        self.dependency_property(inout, "dep id", dep.number)
        
        if dep.name:
            self.dependency_property(inout, "varname", dep.name)
        
        if dep.is_taskwait_inbound is not None:
            self.dependency_property(inout, "taskwait",
                                     "inbound" if dep.is_taskwait_inbound else "outbound")
            
    def dependency_property(self, inout, label, value):
        try:
            inout_ids = self.inout_uids[inout]
        except KeyError:
            # dependency is not declared yet
            return

        for reader, ioid in inout_ids.items():
            self.set_property(AyudameProxy.dep_uid(ioid), label, value)
        
    def set_property(self, tid, label, value):
        if label.startswith("__"):
            return

        prop = ayu_socket.Intern_event_create_intern_event_property(
            AyudameProxy.CLIENT_ID,
            ayu_socket.AYU_SETPROPERTY,
            self.ts, tid,
            label, str(value))
        
        self._write(prop)

    def user_data(self, data):
        event = ayu_socket.Intern_event_create_intern_event_userdata(
            AyudameProxy.CLIENT_ID,
            ayu_socket.AYU_USERDATA,
            self.ts, len(data), data)
        self._write(event)
        
###################
## Local capture ##
###################
        
def ayudame_aspects(Tracks):
    from .. import representation
    
    @Tracks(representation.TaskJob)
    class TaskJobTracker:
        def __init__(this):
            if this.self.masterTask: return
            
            ayu.new_task(this.self.number)

            for name, value in this.self.properties.items():
                ayu.set_property(this.self.number, name, value)

        def set_property(this):
            if this.self.masterTask: return
            
            ayu.set_property(this.self.number, this.args.name, this.args.value)
            
        def start(this):
            if this.self.masterTask: return
            
            ayu.set_property(this.self.number, "state", "running")
            if this.self.properties.get("breakpoint", False):
                mcgdb.interaction.push_stop_request("[Breakpoint on {} execution.]".format(this.self))
                
        def finish(this):
            if this.self.masterTask: return
            
            ayu.set_property(this.self.number, "state", "finished")
            
    @Tracks(representation.Dependence)
    class DependenceTracker:
        def __init__(this):
            pass
                
        def set_name(this):
            dep = this.self
            name = str(this.args.symb)

            for inout in dep.inouts:
                ayu.dependency_property(inout, "varname", name)

        def add_reader(this, after, ret):
            inout = ret
            
            if inout.writer:
                ayu.declare_dependency(this.self, inout, inout.writer, this.args.task)
                
        def add_writer(this, after, ret):
            inout = ret
            for reader in inout.readers:
                ayu.declare_dependency(this.self, inout, this.args.task, reader)
                    
        def set_task_inout_ready(this, after, ret):
            inout = ret
            if not inout:
                return
            ayu.dependency_property(inout, "ready", True)
