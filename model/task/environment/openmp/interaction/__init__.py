import gdb

import mcgdb

import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user.openmp.cli.start")

representation = None

from .. import capture

from . import  step, show, \
     sequence, info, start, all_out, \
     task, critical, barrier, sections, \
     attach, stacktree

from . import breakpoint_sets

from . import info

from . import loop

from . import ayudame_wrapper, ayudame_preload
from . import graph

def activate():
    global representation
    from .. import representation

    global omp_bpt_set_default_enabled
    
    gdb.Command("omp", gdb.COMMAND_OBSCURE, prefix=1)
    gdb.Command("omp task", gdb.COMMAND_OBSCURE, prefix=1)
    gdb.Command("omp critical", gdb.COMMAND_OBSCURE, prefix=1)
    
    mcgdb.toggle_activate_submodules(__name__)(do_activate=True,
                                               include_self=False)

    cmd_omp_model_centric()
    
    cmd_omp_debug()
    cmd_omp_sched()
    cmd_omp_sched_single()
    cmd_omp_sched_all()
    
    cmd_omp_debug_block()
    cmd_omp_debug_unblock()

    
def required_breakpoint_set(bpt_list):
    all_good = True
    for setname, enabled in capture.omp_has_bpt_set_enabled(bpt_list).items():
        if enabled is False: # let's consider that None is correct as well
            log_user.critical("Cannot run this command because breakpoint set '{}' is not enabled.".format(setname))
            log_user.warn("Consider running `omp breakpoint_sets enable {}`".format(setname))
            all_good = False
    return all_good


class cmd_omp (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp", gdb.COMMAND_OBSCURE, prefix=1)

        
class cmd_omp_model_centric (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp model-centric-debugging", gdb.COMMAND_OBSCURE, prefix=1)

    def invoke (self, args, from_tty):
        if not representation.model_centric_debugging:
            gdb.execute("omp breakpoint_sets enable all")
            log_user.info("OpenMP model-centric debugging activated")
            representation.model_centric_debugging = True
            
            if (gdb.selected_inferior() and
                len(gdb.selected_inferior().threads()) > 1):
                log.warn("The current inferior already has thread, model-centric debugging might not work well ...")
        else:
            gdb.execute("omp breakpoint_sets disable all")
            log_user.info("OpenMP model-centric debugging deactivated")
            representation.model_centric_debugging = False
        
class cmd_omp_debug (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp debug", gdb.COMMAND_OBSCURE, prefix=1)

    def invoke (self, args, from_tty):
        activate = "on" in args

        print("OMP debugging activated: {}".format(activate))
        from .. import toolbox
        toolbox.NO_PRINT = not activate

class cmd_omp_sched (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp schedule", gdb.COMMAND_OBSCURE, prefix=1)

    def invoke (self, args, from_tty):
        if args:
            print("WARNING: argumment '{}' not parsed.".format(args))
        if gdb.parameter("scheduler-locking") == "on":
            print("single -- Only current worker can run -- scheduler-locking is active")
        else:
            print("all -- All workers can run -- scheduler-locking is off")

class cmd_omp_sched_single (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp schedule single", gdb.COMMAND_OBSCURE, prefix=1)

    def invoke (self, args, from_tty):
        gdb.execute("set scheduler-locking on")
        
class cmd_omp_sched_all (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp schedule all", gdb.COMMAND_OBSCURE, prefix=1)

    def invoke (self, args, from_tty):
        gdb.execute("set scheduler-locking off")

class cmd_omp_debug_block (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp debug thread_block", gdb.COMMAND_OBSCURE, prefix=1)

    def invoke (self, args, from_tty):
        blocker = representation.TaskJob.blockers[representation.TaskJob.preferred_blocked]
        blocker.block()

class cmd_omp_debug_unblock (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp debug thread_unblock", gdb.COMMAND_OBSCURE, prefix=1)

    def invoke (self, args, from_tty):
        blocker = representation.TaskJob.blockers[representation.TaskJob.preferred_blocked]
        blocker.unblock()
