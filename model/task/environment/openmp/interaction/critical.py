import gdb

from mcgdb.toolbox import my_gdb

representation = None
@my_gdb.internal
def activate():
    global representation; from .. import representation

    gdb.Command("omp critical", gdb.COMMAND_OBSCURE, prefix=1)
    cmd_omp_critical_next()
    cmd_omp_critical_finish()


def get_critical_zone():
    current_worker = representation.Worker.get_current_worker()
    if not current_worker:
        print("Couldn't find the current worker. Is OpenMP initialized?")
        return
        
    if not current_worker.job_stack:
        print("No job in the current worker stack. Is it actually started?")
        return
    
    current_zone = current_worker.job_stack[-1]
        
    if not isinstance(current_zone, representation.CriticalJob):
        print("This is not a CRITICAL zone, sorry.")
        return

    return current_zone

critical_step = None
class cmd_omp_critical_next (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp critical next", gdb.COMMAND_OBSCURE)

    def invoke (self, args, from_tty):
        current_zone = get_critical_zone()
        if current_zone is None:
            return
        
        global critical_step
        if critical_step is None:
            representation.rt_ctrl.set_barrier(representation.Zone.Critical, self.done)
            critical_step = current_zone
            
        gdb.execute("omp step")

    def done(self):
        global critical_step
        critical_step = None
        print("[Critical zone completed.]")

class cmd_omp_critical_finish (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp critical finish", gdb.COMMAND_OBSCURE)

    def invoke (self, args, from_tty):
        current_zone = get_critical_zone()
        if current_zone is None:
            return
        assert current_zone == critical_step

        gdb.execute("continue")
