from __future__ import print_function

import inspect
import types
import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user.openmp.graph")
from collections import defaultdict

"""
Todo:
- add implicite dependencies: barriers / task wait synchronize all the previously created task
- parent task implicite link ?
- add properties to tasks [executed_by]
- understand why there are loops in sparelu !
--> take range into accounts !
--> compute array index value?
--> arrays of array?
- link tasks processed by a given thread ?
"""

WITH_WEAK_DEP = False
WITH_TASK_THREAD_FLOW = False
TASK_ONLY = True

could_initialize = None # updated in cmd_graph_it()
nx = None
pgv = None
plt = None

import gdb

from mcgdb.toolbox import my_gdb
from mcgdb.toolbox import aspect
from mcgdb.toolbox import SimpleClass

def activate():
    log.warn("OpenMP graph module disabled (hardcoded)")
    return

    aspect.register("graph", graph_aspects, pre_aspect)
    
    cmd_graph_it()

    global nx, plt, pgv
    global could_initialize
    
    could_initialize = False

    aspect.register("graph", graph_aspects, pre_aspect)
    
    try:
        import networkx as nx # https://pypi.python.org/pypi/networkx/
        import matplotlib as mpl ; mpl.use('Agg') # Force matplotlib/nx to not use any Xwindows backend.
        #import pygraphviz as pgv # https://pygraphviz.github.io/
    except ImportError as e:
        log.warning("Could not initialize {} package: '{}'".format(__name__, e))
        return

    could_initialize = True

    if TASK_ONLY:
        log.warn("Graph aspect enabled only for tasks and dependencies.")
    
    cmd_graph_it()
    
    
DOT_FILENAME = "/tmp/my_graph.dot"

class to_string_graph:    
    @staticmethod
    def new():
        return to_string_graph("top")
    
    @staticmethod
    def draw(graph):
        print("DRAW({})".format(graph))
        with open(DOT_FILENAME) as dot_file:
            print("fake grapher, sorry", file=dot_file)
            
    @staticmethod
    def subgraph(graph, name):
        return graph.subgraph(name)
    
    def __init__(self, name):
        self.name = name

    def add_node(self, uid, label=None, shape=None, *args, **kwards):
        print("{}.add_node({}, {}, {})".format(self, uid, label, shape))
    
    def add_edge(self, uid1, uid2, style=None, hape=None, *args, **kwards):   
        print("{}.add_edge({} -> {}, {})".format(self, uid1, uid2, shape))
        
    def subgraph(self, name):
        print("subgraph({})".format(name))
        
        return to_string_graph("{}>>{}".format(self, name))
    
    def __str__(self):
        return "graph[{}]".format(self.name)

class pgv_graph:
    @staticmethod
    def new():
        return pgv.AGraph(directed=True, splines=False,
                          strict=False, compound=True)

    @staticmethod
    def draw(graph):
        graph.write(DOT_FILENAME) 
        #graph.draw(OUT_FILENAME, prog="dot")

    @staticmethod
    def subgraph(graph, name):
        return graph.add_subgraph(name=name)
        
class nx_graph:
    @staticmethod
    def new():
        return nx.MultiDiGraph(compound=True,
                               strict=False,
                               directed=True)
    
    @staticmethod
    def draw(G):
        pos = nx.spring_layout(G) # positions for all nodes
        edges = G.edges()

        with my_gdb.may_start_threads():
            # nodes
            nx.draw_networkx_nodes(G,pos,node_size=700)
            
            # edges
            nx.draw_networkx_edges(G,pos,edgelist=edges,
                                   width=6)
        
            # labels
            nx.draw_networkx_labels(G,pos,font_size=20,
                                    font_family='sans-serif')

        dot_file = DOT_FILENAME
        try:
            nx.write_dot(G, dot_file)
        except AttributeError as e: # module 'networkx' has no attribute 'write_dot'
            nx.drawing.nx_agraph.write_dot(G, dot_file)
        
    @staticmethod
    def subgraph(graph, name):
        return graph

graph_pkg = nx_graph
class cmd_graph_it (gdb.Command):
    self = None
    
    def __init__ (self):
        gdb.Command.__init__(self, "omp graph", gdb.COMMAND_OBSCURE)
        cmd_graph_it.self = self
        self.user_warned = False
        
    def invoke (self, arg, from_tty, do_open=False):
        if not could_initialize:
            log_user.critical("Graph command could not initialize correctly ...")
            return
        
        if "+init-only" in arg:
            log_user.info("OpenMP graph command initialized ? {}".format(could_initialize))
            return
        
        if not Thread.appli:
            log_user.critical("Graph root not constructed. Are you sure the application has started?")
            return

        if not self.user_warned:
            self.user_warned = True
            if not WITH_TASK_THREAD_FLOW:
                log_user.debug("Graph constructed without inter task thread flow.")
            if not WITH_WEAK_DEP:
                log_user.debug("Weak dependencies between tasks not constructed.")
            
        do_open = do_open or "+open" in arg
        do_show = "+show" in arg

        try:
            graph_pkg.draw(Thread.appli.graph)
        except Exception as e:
            import traceback, sys
            log.warning(traceback.format_exc())
            _type, value, tb = sys.exc_info()
            
            import pdb; pdb.post_mortem(tb)
        graph_pkg.draw(Thread.appli.graph)
        if do_show:
            gdb.execute("!cat {} ".format(DOT_FILENAME))
        
##########  ##########  ##########
##########  ##########  ##########
##########  ##########  ##########
##########  ##########  ##########

class Task:
    def __init__(self, multitask, uid, label="", shape=None):
        self.uid = uid
        self.multitask = multitask

        if shape is None: shape = "ellipse"
        
        self.multitask.graph.add_node(uid, label=label, shape=shape)
        
    def __str__(self):
        return self.uid

    def __repr__(self):
        return str(self)

    def happened_after(self, task, weak=False):
        if not WITH_WEAK_DEP and weak: return
        self.multitask.graph.add_edge(task.uid, self.uid,
                                      style="dotted" if weak else "filled")
        
    def happened_before(self, task, weak=False):
        if not WITH_WEAK_DEP and weak: return
        task.multitask.graph.add_edge(self.uid, task.uid,
                                      style="dotted" if weak else "filled")

    def set_property(self, key, value):
        self.multitask.graph.node[self.uid][key] = '"{}"'.format(value)

    def happened_after_property(self, to_task, key, value):
        self.multitask.graph.edge[self.uid][to_task.uid][0][key] = '"{}"'.format(value)
            
class MultiTask:
    uid_gen = defaultdict(lambda : 0)
    
    def __init__(self, creator_thread, key, label_type, top_graph=None):
        self.parent = my_gdb.SimpleClass()
        
        self.label_type = label_type
        self.key = "{}{}".format(key, MultiTask.uid_gen[key])
        MultiTask.uid_gen[key] += 1
        
        self.top_graph_init = top_graph is not None
        if self.top_graph_init:
            self.graph = top_graph
        else:
            self.parent.thread = creator_thread
            self.parent.task = creator_thread.task
            
            current_graph = creator_thread.task.multitask.graph
            self.graph = graph_pkg.subgraph(current_graph,
                                            "cluster_{}".format(self.key))
            
        self.task_cnt = 0
        self.internal_tasks = {}
        
    def internal_task(self, name=None, label=None,
                      outside=False, shape=None):
        uid = "task_{}_{}".format(self.key, self.task_cnt \
                                      if name is None else name)
                
        if uid in self.internal_tasks:
            return self.internal_tasks[uid]

        if outside:
            multitask = self.parent.task.multitask
            self.internal_tasks[uid] = multitask.internal_task(self.key+"_"+name,
                                                               label, False, shape)
            return self.internal_tasks[uid]
        
        if label is None:
            label = "{} Task #{}".format(self.label_type, self.task_cnt)
            
        self.task_cnt += 1

        new_task = self.internal_tasks[uid] = Task(self,
                                                   uid, label,
                                                   shape=shape)

        if self.top_graph_init:
            self.parent.task = new_task
            self.top_graph_init = False
            
        return new_task
    
    def __str__(self):
        return "Multitask {}".format(self.key)
    
    def __repr__(self):
        return str(self)
    
@my_gdb.Numbered
class Thread:
    COLORS = ("blue", "darkgreen", "purple", "orange")
    start = None
    appli = None
    main = None
    
    def __init__(self, task=None):
        self.color = Thread.COLORS[self.number % len(Thread.COLORS)]
        self.idle_cnt = 0
        self.task = task if task is not None else Thread.start
        
        self.moveToIdle(label=self.number) # create initial task
        
    def moveTo(self, new_task, from_start, label=None):        
        graph = self.task.multitask.graph if from_start \
            else new_task.multitask.graph
        attr = {"color": self.color}
        if label:
            attr["label"] = label
        graph.add_edge(self.task.uid, new_task.uid, **attr)
        
        self.task = new_task

    def moveToIdle(self, multitask=None, task=None,
                   from_start=True, label=None):
        if task is None:
            task = Task(self.task.multitask if multitask is None \
                            else multitask,
                        "{}__{}".format(self, self.idle_cnt),
                        shape="point")
            self.idle_cnt += 1

        self.moveTo(task, from_start, label=label)
            
    def __str__(self):
        return "thread_{}".format(self.number)
    
    def __repr__(self):
        return str(self)

def init_graph():
    top_graph = graph_pkg.new()
    Thread.appli = MultiTask(None, "app", "",
                             top_graph=top_graph)
    if not TASK_ONLY:
        Thread.main = Thread.appli.internal_task("main", label="",
                                                 shape="Mdiamond")
        Thread.start = Thread.appli.internal_task("start",
                                                  shape="point")

        ##########  ##########  ##########
        ##########  ##########  ##########
        ##########  ##########  ##########
        ##########  ##########  ##########
    
def pre_aspect(help_tracking, self, args):
    if help_tracking.is_init:
        self.graph = my_gdb.SimpleClass()
    
def graph_aspects(Tracks):
    from .. import representation

    @Tracks(representation.Dependence)
    class DependenceTracker:
        def __init__(this):
            this.declared_inouts = []
                
        def set_name(this):
            dep = this.self
            name = str(this.args.symb)
            
            for inout in dep.inouts:
                graph_dependency_property(this.self_to_this, inout,
                                          "varname", name)
                
        def set_task_inout_ready(this, after, ret):
            inout = ret
            if not inout:
                return
            graph_dependency_property(this.self_to_this, inout,
                                      "ready", True)
            
        def add_reader(this, after, ret):
            inout = ret
            if inout.writer:
                graph_declare_dependency(this.self, inout,
                                         this.self_to_this[inout.writer],
                                         this.self_to_this[this.args.task])
                this.declared_inouts.append(inout)

        def add_writer(this, after, ret):
            inout = ret
            for reader in inout.readers:
                graph_declare_dependency(this.self, inout,
                                         this.self_to_this[this.args.task],
                                         this.self_to_this[reader])

    @Tracks(representation.TaskJob)
    class TaskJobTracker:
        def __init__(this): 
            this.args.worker = this.meth_args
            try:
                this.args.masterTask = this.meth_kwargs['masterTask']
            except KeyError:
                this.args.masterTask = False
                
            if TASK_ONLY:
                if Thread.appli is None:
                    init_graph()
                
                if this.args.masterTask:
                    return
                
                this.task = Thread.appli.internal_task(
                    "ttask_{}".format(this.self.number),
                    label=("Task #{}".format(this.self.number)
                           if this.self.taskwait is not True
                           else "Taskwait"))
                
                return

            
            thread = this.self_to_this[this.args.worker].thread

            this.self.graph.task = repr(this.self)
            
            this.task = thread.task.multitask.internal_task(
                "ttask_{}".format(this.self.number),
                label=("Task #{}".format(this.self.number)
                       if this.self.taskwait is not True
                       else "Taskwait"),
                shape=(None if this.self.taskwait is not True
                       else "Mcircle"))
            
            this.task.happened_after(thread.task, weak=True)
            this.return_to = None
            this.thread = None
            
        def start(this):
            if TASK_ONLY: return
            
            thread = this.self_to_this[this.args.worker].thread
            this.return_to = thread.task
            
            if WITH_TASK_THREAD_FLOW:
                thread.moveTo(this.task, from_start=True)
            this.thread = thread
        
        def finish(this):
            if TASK_ONLY: return
            
            if WITH_TASK_THREAD_FLOW:
                this.thread.moveTo(this.return_to, from_start=False)

        def ready(this):
            pass

        def set_property(this):
            if this.self.masterTask:
                return

            this.task.set_property(this.args.name, this.args.value)
        
    if TASK_ONLY: return

    @Tracks(representation.Worker)
    class WorkerTracker:
        def __init__(this):
            if Thread.appli is None:
                init_graph()
            
            this.thread = Thread()
            
            if this.self.number == 1:
                this.thread.moveTo(Thread.main, from_start=True)
    
    @Tracks(representation.ParallelJob)
    class ParallelJobTracker:
        def __init__(this):
            this.args.num_workers, this.args.parent_worker = this.meth_args
            
            parent_thread = this.self_to_this[this.args.parent_worker].thread
            this.job = MultiTask(parent_thread, "par", "Par")
            this.first_start = True
            this.first_exit = True
            
        def start_working(this):
            entry_task = this.job.internal_task("entry_task")
            
            if this.first_start:
                this.job.parent.task.happened_before(entry_task)
                this.first_start = False

            thread = this.self_to_this[this.args.worker].thread
            thread.moveTo(entry_task, from_start=True)
                    
        def stop_working(this):
            thread = this.self_to_this[this.args.worker].thread

            exit_task = this.job.internal_task("exit_task", outside=True,
                                               label="main2", shape="Mdiamond")
            
            if this.first_exit:
                exit_task.happened_after(thread.task)    
                this.first_exit = False
                
            thread = this.self_to_this[this.args.worker].thread

            if thread is this.job.parent.thread:
                thread.moveTo(exit_task, from_start=False)
            
            thread.moveToIdle(exit_task.multitask, from_start=False)
            exit_task = this.job.internal_task("exit", outside=True,
                                               shape="point")
            thread.moveTo(exit_task, from_start=True)
            
    @Tracks(representation.SingleJob)
    class SingleTracker:
        def __init__(this): 
            this.args.worker, this.args.parallel_job = this.meth_args

            thread = this.self_to_this[this.args.worker].thread
            this.job = MultiTask(thread, "s", "Single")

            barrier = this.self_to_this[this.args.parallel_job.barrier].barrier
            this.task = this.job.internal_task(
                label="Single Task #{}".format(this.self.number))
            thread.task.happened_before(this.task)
            barrier.happened_after(this.task)
            
            this.parallel_job = this.self_to_this[this.args.parallel_job].job
            this.task.multitask = this.parallel_job
            
        def enter(this):
            thread = this.self_to_this[this.args.worker].thread
            if this.args.inside:
                thread.moveTo(this.task, from_start=True)
            else:
                thread.moveToIdle(this.job)

        def finished(this):
            pass

        def completed(this):
            pass
        
    @Tracks(representation.CriticalJob)
    class CriticalJobTracker:
        def __init__(this): 
            this.args.worker, this.args.parallel_job = this.meth_args
            thread = this.self_to_this[this.args.worker].thread
            
            this.job = MultiTask(thread, "crit", "Critical")
            this.parallel_job = this.self_to_this[this.args.parallel_job].job
            this.inside = None
        
        def try_enter(this):
            pass
        
        def entered(this):
            prev_inside = this.inside
            
            this.inside = this.job.internal_task()
            if prev_inside:
                this.inside.happened_after(prev_inside, weak=True)
            
            if this.job.task_cnt == 1:
                #add inbound link to graph
                pass
            
            thread = this.self_to_this[this.args.worker].thread
            thread.moveTo(this.inside, from_start=True)
        
        def left(this):
            exit_task = this.job.internal_task("exit_task", outside=True)

            thread = this.self_to_this[this.args.worker].thread
            thread.moveTo(exit_task, from_start=False)
            

    @Tracks(representation.Barrier)
    class BarrierTracker:
        def __init__(this): 
            this.args.parallel_job, this.args.worker = this.meth_args
            thread = this.self_to_this[this.args.worker].thread
            parallel_job = this.self_to_this[this.args.parallel_job].job
            this.barrier = parallel_job.internal_task(
                "barrier_{}".format(this.self.number),
                label="Barrier #{}".format(this.self.number),
                shape="rect")
            if this.self.single is None:
                this.barrier.happened_after(thread.task)
        def reach_barrier(this):
            thread = this.self_to_this[this.args.worker].thread
            thread.moveTo(this.barrier, from_start=False)
        
        def leave_barrier(this):
            parallel_job = this.self_to_this[this.self.parallel_job].job
            
            if "exit_task" not in this:
                this.exit_task = parallel_job.internal_task()
                this.exit_task.happened_after(this.barrier)
                
            thread = this.self_to_this[this.args.worker].thread
            thread.moveTo(this.exit_task, from_start=False)
        
    @Tracks(representation.SectionJob)
    class SectionJobTracker:
        def __init__(this): 
            this.args.parallel_job, this.args.worker, this.args.count = \
                this.meth_args
            thread = this.self_to_this[this.args.worker].thread
            
            this.job = MultiTask(thread, "sect", "Section")

            inner = this.job.internal_task("inner", shape="point")
            this.exit_task = this.job.internal_task("exit_task", outside=True)
            inner.happened_after(thread.task)
            this.exit_task.happened_after(inner)
            
        def work_on_section(this):
            thread = this.self_to_this[this.args.worker].thread

            if this.args.section_id != 0:
                new_task = this.job.internal_task()
                thread.moveTo(new_task, from_start=True)                
            else:
                thread.moveTo(this.exit_task, from_start=False)
        
        def ended(this):
            pass
        
        def completed(this):
            pass
                
def graph_declare_dependency(dep, inout, writer, reader):
    reader.task.happened_after(writer.task)
    
    _graph_link_property(writer, reader,
                        "ready", inout.ready)

    if dep.name:
        _graph_link_property(writer, reader, "varname", dep.name)

    if dep.is_taskwait_inbound is not None:
        _graph_link_property(writer, reader,
                            "taskwait",
                            "inbound" if dep.is_taskwait_inbound else "outbound")
        
def graph_dependency_property(self_to_this, inout, key, value):
    if not inout.writer:
        return
    
    writer = self_to_this[inout.writer]

    for reader in inout.readers:
        try:
            _graph_link_property(writer, self_to_this[reader], key, value)
        except KeyError:
            log.error("No dependecy registered between {} and {}".format(inout.writer, reader))
            import pdb;pdb.set_trace()
            pass
        
def _graph_link_property(writer, reader, key, value):
    writer.task.happened_after_property(reader.task, key, value)
