import os

import gdb

from mcgdb.toolbox import my_gdb

OmpGeneratedFrame = None

def filtered_frames():
    for fframe in gdb.frames.execute_frame_filters(gdb.newest_frame(), 0, -1):
        yield fframe


def fframe_to_class(fframe, cclass):
    while True:

        if isinstance(fframe, cclass):
            return fframe
        if not hasattr(fframe, "parent"):
            break
        fframe = fframe.parent
    return None

def fframe_to_string(fframe):
    if fframe.filename():
        if fframe.line():
            filename = " at {}:{}".format(fframe.filename(), fframe.line())
        else:
            filename = " in {}".format(fframe.filename())
    else:       
        filename = ""

    frame = fframe.inferior_frame()
    
    pc = "0x{0:08x}".format(fframe.address()) \
        if fframe.address() else ""

    fct = fframe.function() if not str(fframe.function()).isdigit() \
        else fframe.inferior_frame().name()
    
    args = "({})".format(", ".join(["{}={}".format(
                    arg.symbol().name,
                    arg.symbol().value(frame))
                                    for arg in fframe.frame_args()])) \
                                    if fframe.frame_args() else "" if fct and fct.startswith("#") else " ()"
        
    return "{}{}{}{}".format(
        pc,
        fct,
        args,
        filename)    


class TreeNode:
    def __init__(self, parent, siblings, descr, thread_info, frame):
        self.parent = parent
        self.siblings = siblings
        self.descr = descr
        self.thread_info = {thread_info}

        self.frame = frame
        
        self.children = {}
        if self.parent is not None:
            self.parent.children[descr] = self

        self.depth = 0
        while parent is not None:
            self.depth += 1
            parent = parent.parent

    def common_prefix(self):
        if not self.parent:
            return ""
        if len(self.parent.children.keys()) < 2:
            return ""
        
        prefix = os.path.commonprefix(self.parent.children.keys())

        if " at " in prefix:
            prefix = prefix.partition(" at ")[0]

        return prefix
    
    def __str__(self, idx=None):
        THREAD_SPACER_LENGTH = 45
        thread_info = "[Thread {}]".format(", ".join(map(str, self.thread_info))) \
            if not self.children \
            else " "+str(self.thread_info)+"\n"
        prefix = " " * 3 * self.depth if self.children else "-->" + " " * 2 * self.depth

        common = self.common_prefix()
        descr = self.descr.replace(common, "")

        if idx == 0 and common:
            prefix = prefix[:-1] + common + "\n" + prefix

        if common:
            prefix = "{}+".format(prefix[:-1])
        
        description = "{}{}".format(prefix, descr)

        if not self.children:
            spacer = " "*(max(2, THREAD_SPACER_LENGTH - len(description.rpartition("\n")[-1])))
            thread_info = spacer + thread_info
        
        return description + thread_info + \
            "\n".join([child.__str__(i) for i, child in enumerate(self.children.values())])
    
def substack(parent_node, from_job, from_thread):
    has_beginning = from_job is None

    current_parent = parent_node
    for fframe in reversed(list(filtered_frames())):
        fframe_gen = fframe_to_class(fframe, OmpGeneratedFrame)
        
        if not has_beginning and (fframe_gen is None or fframe_gen.job is not from_job):
            continue

        if has_beginning and fframe_gen is not None:
            for worker in sorted(fframe_gen.job.workers):
                with my_gdb.active_thread(worker.thread_key):
                    thread_info = int(gdb.selected_thread().num)
                    current_parent.thread_info.add(thread_info)
                    substack(current_parent, fframe_gen.job, worker.thread_key)
            break
        else:
            descr = fframe_to_string(fframe)
            thread_info = int(gdb.selected_thread().num)
            try:
                current_parent = current_parent.children[descr] # likely to fail
                current_parent.thread_info.add(thread_info)
            except KeyError:
                current_parent = TreeNode(
                    current_parent, None, descr, thread_info, fframe)
                has_beginning = True
        
def stack():
    root = TreeNode(None, {}, "<root>", "<>", None)
    with my_gdb.active_thread(gdb.selected_thread()):
        gdb.execute("thread 1", to_string=True)
        substack(root, None, gdb.selected_thread())

    print(root)
    
class cmd_omp_where (gdb.Command):
    
    def __init__ (self):
        gdb.Command.__init__(self, "omp where", gdb.COMMAND_OBSCURE)

    def invoke (self, args, from_tty):
        stack()

def activate():
    global OmpGeneratedFrame
    from ..capture import OmpGeneratedFrame
    cmd_omp_where()
