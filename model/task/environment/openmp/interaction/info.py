from __future__ import print_function
import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user.openmp.info")
from collections import defaultdict

import gdb

from mcgdb.toolbox import my_gdb

class cmd_omp_info_tasks_src (gdb.Command):
    
    def __init__ (self):
        gdb.Command.__init__(self, "info tasks by_src", gdb.COMMAND_OBSCURE)
        
    def invoke (self, arg, from_tty):
        with_src = "+src" in arg
        with_short = "+short" in arg
        with_deps = "+deps" in arg
        
        task_srcs = defaultdict(list)
        for task in representation.TaskJob.list_:
            task_srcs[task.properties.get('sources', None)].append(task)

        for src in sorted(task_srcs.keys()):
            print("{} ({} tasks)".format(
                    src if src is not None else "<No source-code line available>",
                  len(task_srcs[src])))
            
            task_ids = [str(task.number) for task in task_srcs[src]]
            
            if with_short:
                print("\t TaskJob #{}".format(" #".join(task_ids)))
            else:
                gdb.execute("info task {} -src {}".format(" ".join(task_ids), arg))
                
            # with last task from comprehension above
            if with_deps:
                print_dep(task, task.in_dependencies, is_in=True, short=True)
                print_dep(task, task.out_dependencies, is_in=False, short=True)
                
            if with_src:
                print("{sep}\n{src}\n{sep}".format(sep="--"*20, src=get_task_src(task)))
                
            print("")
            
class cmd_omp_info_tasks (gdb.Command):
    
    def __init__ (self):
        gdb.Command.__init__(self, "info tasks", gdb.COMMAND_OBSCURE, prefix=True)
        
    def invoke (self, arg, from_tty):
        args = gdb.string_to_argv(arg)
        filter_task = []
        filter_key = filter_val = None
        without_props = "-props" in arg
        with_sched = "+sched" in arg
        with_deps = "+deps" in arg
        without_nodeps = "-no-deps" in arg
        with_src = "+src" in arg
        without_src = "-src" in arg
        
        with_internal = "+internal" in arg
        with_none = "+none" in arg

        if without_src: with_src = False
        
        for arg in args:
            if "=" in arg:
                filter_key, filter_val = arg.split("=")
                break
            
            if arg.isdigit():
                filter_task.append(int(arg))
                
        for task in representation.TaskJob.list_:
            if filter_task and not task.number in filter_task:
                continue
            if without_nodeps and not task.in_dependencies+task.out_dependencies:
                    continue
            worker = task.properties.get("worker", None)
            if not worker:
                sign = "   "
            elif worker.thread_key == gdb.selected_thread():
                sign = "=> "
            else:
                sign = " * "
                
            print("{} {}".format(sign, task))
            for key, val in task.properties.items():
                if filter_key and filter_key not in key:
                    continue
                if filter_val and filter_val not in str(val):
                    continue
                if without_props and not (filter_val or filter_key):
                    continue
                if key.startswith("__") and not with_internal:
                    continue
                if val is None and not with_none:
                    continue
                if without_src and "sources" == key:
                    continue
                
                print("\t{}: {}".format(key, val))
                
            if with_deps:
                print_dep(task, task.in_dependencies, is_in=True)
                print_dep(task, task.inout_dependencies, is_in=True, is_out=True)
                print_dep(task, task.out_dependencies, is_out=True)
                    
            if with_sched and task.is_not_schedulable():
                print("\tNot schedulable.")
            if with_src:
                print("{sep}\n{src}\n{sep}".format(sep="--"*20, src=get_task_src(task)))
                
            if not without_props:
                print("")
            
        if representation.TaskJob.detect_lock():
            log.warn("Task graph is deadlocked.")
            
def get_task_src(task):
    try:
        sources = task.properties["sources"] # may throw a KeyError
        filename, _, lrange = sources.partition(":")
        lstart, lstop = lrange.split("-")
        return my_gdb.get_src(filename, lstart, lstop)
    except KeyError:
        return("\t\tSources not available for this task.")

def print_dep(task, deps, is_in=False, is_out=False, short=False):
    if not deps:
        return
    
    print("\t{} dependencies:".format("Input/output" if is_in and is_out else "Input" if is_in else "Output"))
    for dep in deps:
        inout = dep.get_task_inout(task, as_reader=is_in, as_writer=not is_in)

        try:
            # try task dependency name
            name = inout.reader_names[task] if is_in else inout.writer_name
        except KeyError:
            name = None

        if not name:
            # use dependency name
            name = dep.name if dep.name else ""
            
        WITH_ADDR = not bool(dep.name)
        
        if not inout:
            print("nop for {}".format(dep))
            continue

        print("\t\t#{uid} {name}{val}{addr}".format(
                uid=dep.number,
                name=name,
                val=" = {}".format(inout.val) if inout.ready and inout.val is not None else "",
                addr=" (@{})".format(dep.address) if WITH_ADDR else ""), end="")

        if short:
            print("") # finish the line
        else:
            if is_in:
                print(" from {} ({}ready)".format(
                        inout.writer if inout.writer else "no body.",
                        "" if inout.ready else "not "), end="")
                if not is_out:
                    print("") # finish the line
            if is_out:
                print(" to {}".format(
                        ", ".join(map(str, inout.readers)) if inout.readers else "nobody."))
    
class cmd_omp_info_workers (gdb.Command):
    """ Shows the position of the threads"""
    
    def __init__ (self):
        gdb.Command.__init__(self, "info workers", gdb.COMMAND_OBSCURE)
        
    def invoke (self, arg, from_tty):
        for worker in representation.Worker.list_:
            sign = ">" if worker.is_current() \
                else "#" if not worker.is_alive() \
                else " "

            post = "(dead)" if not worker.is_alive() \
                else "!" if worker.is_current() and gdb.parameter("scheduler-locking") == "on" \
                else ""
            if post: post = " " + post
            
            stack = " > ".join([str(j) for j in worker.job_stack])
            
            if worker.is_alive():
                gtid = int(worker.thread_key.num)
                str_gtid = "(tidx {})".format(gtid) if worker.number != gtid else ""
            else:
                str_gtid = ""
            print("{sign} {worker}: {stack}{post} {str_gtid}".format(**locals()))

class cmd_omp_info_barrier (gdb.Command):
    """ Shows the position of the threads"""
    
    def __init__ (self):
        gdb.Command.__init__(self, "info barrier", gdb.COMMAND_OBSCURE)
        
    def invoke (self, arg, from_tty):
        for barrier in representation.Barrier.list_:
            if barrier.open:
                continue

            print(barrier)
            print("---------")
            for depends in (barrier.sections, barrier.single):
                if depends:
                    print("  Created by {}".format(depends))

            if barrier.count == 0:
                print("  Open")
            elif barrier.count is False:
                print("  Not hit yet")
            else:
                print("  Workers blocked: {}".format(barrier.count))
                for worker, loc in barrier.loc.items():
                    print("      {} --> {}".format(worker, loc))            
                        
class cmd_omp_info_threads (gdb.Command):
    """ Shows GDB threads used in OpenMP"""
    
    def __init__ (self):
        gdb.Command.__init__(self, "omp info threads", gdb.COMMAND_OBSCURE)

    def invoke (self, args, from_tty):
        threads = gdb.execute("info threads {}".format(args), to_string=True)
        for line in threads.split("\n"):
            if "Thread" in line and not "Worker #" in line:
                continue
            print(line)
        
@my_gdb.internal
def activate():
    gdb.Command("omp info", gdb.COMMAND_OBSCURE, prefix=True)
    global representation
    from .. import representation
    cmd_omp_info_barrier()
    cmd_omp_info_workers()
    cmd_omp_info_tasks()
    cmd_omp_info_tasks_src()
    cmd_omp_info_threads()
