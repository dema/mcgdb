import gdb

current_worker = None

representation = None

def activate():
    global representation, current_worker
    from .. import representation
    
    current_worker = representation.Worker.get_current_worker

    cmd_omp_all_out()
    
class cmd_omp_all_out (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "omp all_out", gdb.COMMAND_OBSCURE)

    def invoke (self, args, from_tty):
        if not representation.RuntimeController.is_available():
            print("OpenMP runtime controller not available ...")
            return
        
        current_zone = current_worker().job_stack[-1]

        if not isinstance(current_zone, representation.SingleJob):
            print("Can only step all out of a SINGLE zone, sorry.")
            return

        worker = gdb.selected_thread()
        def done():
            worker.switch()
            print("[Out of the single zone.]")
            
        representation.rt_ctrl.set_barrier(representation.Zone.Barrier, done)
        
        gdb.execute("continue")
