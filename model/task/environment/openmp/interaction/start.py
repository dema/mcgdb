import gdb

import mcgdb.toolbox 
from mcgdb.toolbox import my_gdb
from mcgdb.toolbox import aspect
from mcgdb.interaction import push_stop_request

required_breakpoint_set = None

import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user.openmp.cli.start")

representation = None

OPM_ZONES = ("single", "parallel", "critical", "task", "sections", "barrier", "master")

current_worker = None

@my_gdb.internal
def initialize():
    global representation, current_worker, required_breakpoint_set, required_model
    from .. import representation
    from . import required_breakpoint_set
    current_worker = representation.Worker.get_current_worker

@my_gdb.internal
def activate():
    cmd_omp_start()
    
class cmd_omp_start (gdb.Command):
    
    def __init__ (self):
        gdb.Command.__init__(self, "omp start", gdb.COMMAND_OBSCURE)

    def invoke (self, args, from_tty):
        from .. import capture
        self.dont_repeat()

        if not required_breakpoint_set(["parallel", "omp_preload", "workers"]):
            return
        
        if not mcgdb.toolbox.required_model("OpenMP preloaded helper library."):
            log_user.critical("Cannot run this command, OpenMP preloaded helper library not found.")
            return
        
        if not representation.RuntimeController.is_available():
            print("OpenMP runtime controller not available ...")
            return
        
        try:
            is_ompss = capture.capture.IS_OMPSS # may throw AttributeError
        except AttributeError:
            is_ompss = False
        
        if is_ompss:
            self.start_ompss(args)
        else:
            self.start_omp(args)
        
    def start_ompss(self, args=""):
        with my_gdb.set_parameter("scheduler-locking", "on"):
            representation.Init.break_after_init = True
            representation.Init.after_init_cb = self.done_ompss
            gdb.execute("continue")
        
    def done_ompss(self):
        gdb.execute("set scheduler-locking off")
        print("[OMPSS started, task graph built.]")
        
    def start_omp(self, args=""):
        if not representation.RuntimeController.is_available():
            log_user.error("OpenMP runtime controller not available, starting freely ...")
            gdb.execute("start")
            if not representation.RuntimeController.is_available():
                log_user.warn("No OpenMP runtime controller loaded during startup, stopping here.")
                return
            else:
                log_user.info("OpenMP runtime controller found, starting OpenMP ...")
    
        if len(representation.Worker.list_) \
                and len(representation.Worker.list_[0].job_stack) \
                and not "--force" in args:
            print("OpenMP parallel execution already started.")
            return

        representation.rt_ctrl.set_barrier(representation.Zone.Parallel, self.done_omp)
        gdb.execute("continue")

    def done_omp(self):
        print("[OpenMP parallel zone started.]")
        
        gdb.execute("thread 1", to_string=True)
        
