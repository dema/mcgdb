#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#define __USE_GNU
#include <dlfcn.h>

struct ld_bindings_s {
  const char *name;
  void **real_address;
} ;

typedef int bool;

struct debugging_state_s {
  int initialized;
  int can_run;
};

struct trampoline_data {
  void *(*routine) (void *);
  void *arg;
};

extern volatile int mcgdb_can_pass_parallel;
extern volatile int mcgdb_can_pass_barrier;
extern volatile int mcgdb_can_pass_critical;
extern __thread volatile struct debugging_state_s thread_debug;
extern __thread volatile int gdb_thread_num;

void init_omp_preload(void);
void init_bindings(struct ld_bindings_s *bindings);
void dbg_crash_event(void);
void dbg_notify_event(void);
void mcgdb_thread_can_run(volatile int *can_pass);

void error(const char *format, ...);
