#include <stdint.h>

/*---------------------
 * identifiers
 *---------------------*/

typedef uint64_t ompt_thread_id_t;
#define ompt_thread_id_none ((ompt_thread_id_t) 0)     /* non-standard */

typedef uint64_t ompt_task_id_t;
#define ompt_task_id_none ((ompt_task_id_t) 0)         /* non-standard */

typedef uint64_t ompt_parallel_id_t;
#define ompt_parallel_id_none ((ompt_parallel_id_t) 0) /* non-standard */

typedef uint64_t ompt_wait_id_t;
#define ompt_wait_id_none ((ompt_wait_id_t) 0)         /* non-standard */


/*---------------------
 * ompt_frame_t
 *---------------------*/

typedef struct ompt_frame_s {
    void *exit_runtime_frame;    /* next frame is user code     */
    void *reenter_runtime_frame; /* previous frame is user code */
} ompt_frame_t;

/* initialization */
typedef void (*ompt_interface_fn_t)(void);

typedef ompt_interface_fn_t (*ompt_function_lookup_t)(
    const char *                      /* entry point to look up       */
);

/* threads */
typedef void (*ompt_thread_callback_t) (
    ompt_thread_id_t thread_id        /* ID of thread                 */
);

typedef enum {
    ompt_thread_initial = 1, // start the enumeration at 1
    ompt_thread_worker  = 2,
    ompt_thread_other   = 3
} ompt_thread_type_t;

typedef void (*ompt_thread_type_callback_t) (
    ompt_thread_type_t thread_type,   /* type of thread               */
    ompt_thread_id_t thread_id        /* ID of thread                 */
);

typedef void (*ompt_wait_callback_t) (
    ompt_wait_id_t wait_id            /* wait id                      */
);

/* parallel and workshares */
typedef void (*ompt_parallel_callback_t) (
    ompt_parallel_id_t parallel_id,    /* id of parallel region       */
    ompt_task_id_t task_id             /* id of task                  */
);

typedef void (*ompt_new_workshare_callback_t) (
    ompt_parallel_id_t parallel_id,   /* id of parallel region        */
    ompt_task_id_t parent_task_id,    /* id of parent task            */
    void *workshare_function          /* pointer to outlined function */
);

typedef void (*ompt_new_parallel_callback_t) (
    ompt_task_id_t parent_task_id,    /* id of parent task            */
    ompt_frame_t *parent_task_frame,  /* frame data of parent task    */
    ompt_parallel_id_t parallel_id,   /* id of parallel region        */
    uint32_t requested_team_size,     /* number of threads in team    */
    void *parallel_function           /* pointer to outlined function */
);

/* tasks */
typedef void (*ompt_task_callback_t) (
    ompt_task_id_t task_id            /* id of task                   */
);

typedef void (*ompt_task_switch_callback_t) (
    ompt_task_id_t suspended_task_id, /* tool data for suspended task */
    ompt_task_id_t resumed_task_id    /* tool data for resumed task   */
);

typedef void (*ompt_new_task_callback_t) (
    ompt_task_id_t parent_task_id,    /* id of parent task            */
    ompt_frame_t *parent_task_frame,  /* frame data for parent task   */
    ompt_task_id_t  new_task_id,      /* id of created task           */
    void *task_function               /* pointer to outlined function */
);

/* program */
typedef void (*ompt_control_callback_t) (
    uint64_t command,                 /* command of control call      */
    uint64_t modifier                 /* modifier of control call     */
);

typedef void (*ompt_callback_t)(void);
