#include <string.h>
#include <ompt.h>

#define DISABLE_OMPT_PRELOAD

#include "ompt_preload.h"

/* threads */
void thread_callback (
    ompt_thread_id_t thread_id        /* ID of thread                 */
) {
  printf("OMPT: thread 0x%lx\n", thread_id);
}

void wait_callback (
    ompt_wait_id_t wait_id            /* wait id                      */
){
  printf("OMPT: wait on 0x%lx\n", wait_id);
}

/* parallel and workshares */
void parallel_callback (
    ompt_parallel_id_t parallel_id,    /* id of parallel region       */
    ompt_task_id_t task_id             /* id of task                  */
){
  //printf("OMPT: task 0x%lx on parallel 0x%lx\n", task_id, parallel_id);
}

void new_parallel_callback (
    ompt_task_id_t parent_task_id,    /* id of parent task            */
    ompt_frame_t *parent_task_frame,  /* frame data of parent task    */
    ompt_parallel_id_t parallel_id,   /* id of parallel region        */
    uint32_t requested_team_size,     /* number of threads in team    */
    void *parallel_function           /* pointer to outlined function */
){
  printf("OMPT: new parallel 0x%lx, %p*%u, created by task 0x%lx\n",
    parallel_id, parallel_function, requested_team_size, parent_task_id);
}

/* tasks */
void task_callback (
    ompt_task_id_t task_id            /* id of task                   */
){
  printf("OMPT: task 0x%lx\n", task_id);
}

void new_task_callback (
    ompt_task_id_t parent_task_id,    /* id of parent task            */
    ompt_frame_t *parent_task_frame,  /* frame data for parent task   */
    ompt_task_id_t  new_task_id,      /* id of created task           */
    void *task_function               /* pointer to outlined function */
){
  printf("OMPT: new task 0x%lx on %p, created by task 0x%lx\n",
         new_task_id, task_function, parent_task_id);
}

void task_switch_callback (
    ompt_task_id_t suspended_task_id, /* tool data for suspended task */
    ompt_task_id_t resumed_task_id    /* tool data for resumed task   */
){
  printf("OMPT: task switch 0x%lx --> 0x%lx\n",
           suspended_task_id, resumed_task_id);
}

void thread_type_callback (
    ompt_thread_type_t thread_type,   /* type of thread               */
    ompt_thread_id_t thread_id        /* ID of thread                 */
){
  printf("OMPT: thread 0x%lx is %s\n", thread_id,
           (thread_type == 1 ? "initial" :
              (thread_type == 2 ? "worker" : "other")));
}


void new_workshare_callback (
    ompt_parallel_id_t parallel_id,   /* id of parallel region        */
    ompt_task_id_t parent_task_id,    /* id of parent task            */
    void *workshare_function          /* pointer to outlined function */
){
  printf("OMPT: new workshare %p, parallel id is  0x%lx, created by 0x%lx\n",
           workshare_function, parallel_id, parent_task_id);
}

void control_callback (
    uint64_t command,                 /* command of control call      */
    uint64_t modifier                 /* modifier of control call     */
){
  printf("OMPT: control: 0x%lx --> 0x%lx\n", command, modifier);
}

void generic_callback(void) {
  printf("OMPT: generic callback\n");
}


ompt_set_callback_t ompt_set_callback;

int ompt_initialize(ompt_function_lookup_t ompt_fn_lookup,
                    const char *version,
                    unsigned int ompt_version) {
printf("OMPT: hello OMPT %s\n", version);
#ifdef DISABLE_OMPT_PRELOAD
  return 0;
#endif
  
  void *cb = NULL;
  printf("OMPT: hello OMPT %s\n", version);
  ompt_set_callback = (ompt_set_callback_t) ompt_fn_lookup("ompt_set_callback");

#define set_callback(event_name, callback_type, event_id)               \
  if (!strcmp(#callback_type, "ompt_wait_callback_t")) {                \
    cb = wait_callback;                                                 \
  } else if (!strcmp(#callback_type, "ompt_parallel_callback_t")) {   \
    cb = parallel_callback;                                             \
  } else if (!strcmp(#callback_type, "ompt_new_parallel_callback_t")) { \
    cb = new_parallel_callback;                                         \
  } else if (!strcmp(#callback_type, "ompt_new_task_callback_t")) {      \
    cb = new_task_callback;                                             \
  } else if (!strcmp(#callback_type, "ompt_task_callback_t")) {          \
    cb = task_callback;                                                 \
  } else if (!strcmp(#callback_type, "ompt_task_switch_callback_t")) {          \
    cb = task_switch_callback;                                                 \
  } else if (!strcmp(#callback_type, "ompt_thread_type_callback_t")) {          \
    cb = thread_type_callback;                                                 \
  } else if (!strcmp(#callback_type, "ompt_thread_callback_t")) {          \
    cb = thread_callback;                                                 \
  } else if (!strcmp(#callback_type, "ompt_control_callback_t")) {          \
    cb = control_callback;                                                 \
  } else if (!strcmp(#callback_type, "ompt_new_workshare_callback_t")) {          \
    cb = new_workshare_callback;                                                 \
  } else if (!strcmp(#callback_type, "ompt_callback_t")) {          \
    cb = generic_callback;                                                 \
  } else {                                                              \
    printf("OMPT: unknown event type: " #event_name "--> " #callback_type "\n"); \
  }                                                                     \
  if (cb) {                                                             \
    printf("OMPT: register event "#event_name" \n");                         \
    ompt_set_callback(event_name, cb);                                  \
  } 

  
  FOREACH_OMPT_EVENT(set_callback);
#undef set_callback
  
  return 1;
}
