import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user.openmp.preload")
import traceback

import gdb

from mcgdb.toolbox import my_gdb

from ... import representation
from .. import OmpFunctionBreakpoint
from .. import current_worker

PRELOAD_SUFFIX = "_preload.c"

class ld_preload_pthread_create_trampo_Breakpoint(OmpFunctionBreakpoint):
    omp_set = "omp_preload"
    
    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "pthread_create_trampoline")
        
    def prepare_before(self):
        try:
            gdb.execute("set gdb_thread_num = {}".format(gdb.selected_thread().num))
        except gdb.MemoryError as e:
            log.warning("Cannot set gdb_thread_num ({})".format(e))

        return False, False, {}

class FrameFilter:
    def __init__(self):
        self.enabled = True
        self.priority = 600
        
    def filter(self, frames):
        for frame in frames:
            if PRELOAD_SUFFIX in str(frame.filename()):
                continue
            
            yield frame

class ld_preload_thread_can_run_Breakpoint(OmpFunctionBreakpoint):
    omp_set = "omp_preload"
    
    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "mcgdb_thread_can_run")

    def prepare_before(self):
        finished = representation.rt_ctrl.worker_at_barrier(current_worker())
        
        return finished, False, {}

CONTROL_FLAGS = {}

def set_barrier(self, zone):
    gdb.execute("\tset {} = 0".format(CONTROL_FLAGS[zone]))

def release_barrier(self, zone, workers, cb):
    gdb.execute("set {} = 1".format(CONTROL_FLAGS[zone]))
    
    def hook():
        with my_gdb.set_parameter("scheduler-locking", "on"):
            for worker in workers:
                worker.thread_key.switch()
                try:
                    TO_STRING=True # means silent
                    gdb.execute("finish", to_string=TO_STRING)
                    try:
                        gdb.newest_frame().read_var("target") # may throw an exception
                        gdb.execute("tbreak *target", to_string=TO_STRING)
                        gdb.execute("cont", to_string=TO_STRING)
                    except ValueError: # no target to tbreak
                        gdb.execute("finish", to_string=TO_STRING)

                except gdb.error as e:
                    log.warning("Could not finish barrier release ... ({})".format(e))
                    log.info(gdb.execute("where"))
        if not cb: return
        
        try: cb()
        except Exception as e:
            log.error(e)
            print(traceback.format_exc())
        
    my_gdb.set_prompt_hook(hook)
        
def activate():
    CONTROL_FLAGS.update({
            representation.Zone.Parallel : "mcgdb_can_pass_parallel",
            representation.Zone.Barrier  : "mcgdb_can_pass_barrier",
            representation.Zone.Critical : "mcgdb_can_pass_critical"
            })
    symbols_found = True
    try:
        for control_flags in CONTROL_FLAGS.values():
            if not gdb.lookup_symbol(control_flags)[0]:
                symbols_found = False
                break
    except gdb.error as e:
        if "No frame selected" not in str(e):
            log.warning("Exception in OMP_PRELOAD.py: {}".format(e))
        symbols_found = False
    finally:
        if not symbols_found:
            msg = "Cannot locate ldPreload symbols.", "OpenMP debugging functionnalities will be limited."
            length = max(map(len, msg))
            log_user.error("#"*(length+6))
            for line in msg:
                log_user.error("## {}{} ##".format(line, " "*(length - len(line))))
            log_user.error("#"*(length+6))
            return
    
    ld_preload_pthread_create_trampo_Breakpoint()
    ld_preload_thread_can_run_Breakpoint()
    
    representation.DebuggerBarrier.is_available = True
    representation.DebuggerBarrier.capture__release_barrier = release_barrier
    representation.DebuggerBarrier.capture__set_barrier = set_barrier
    
    gdb.frame_filters["OpenMP ldPreload"] = FrameFilter()

    try:
        key = "Pthread_preload"
        representation.TaskJob.preferred_blocked = key
        representation.TaskJob.blockers[key] = Blocker()
    except KeyError as e:
        log.warning(e)

class Blocker:
    BLOCKER_FCT = "__thread_blocker"
    BLOCKER_FCT_X86_OFFSET = 4

    restart_for_task = set()
    
    def __init__(self):
        thread_blocker = gdb.lookup_symbol(Blocker.BLOCKER_FCT)
        if not thread_blocker[0]:
            raise KeyError("Cannot lookup thread blocker function ...")

        if "86" in gdb.execute("show architecture", to_string=True):
            addr_name = str(thread_blocker[0].value().address + Blocker.BLOCKER_FCT_X86_OFFSET)
            addr = addr_name.partition(" ")[0]
        else:
            log.warn("Using default blocker offset")
            addr = Blocker.BLOCKER_FCT
            
        self.addr = addr
        self.blocked_task = {}

        self.must_stop = True
        self.lazy = True
        
    def block(self, task=None, active=None):
        if task and active:
            log.warning("Blocking an *active* task. Implementation deadlocks may appear unexpectedly.")
            worker = task.properties["worker"]
        else:
            worker = current_worker()

        if not worker:
            log.error("Cannot find current worker. Blocking current thread.")
            
        with my_gdb.active_thread(worker.thread_key if worker is not None else gdb.selected_thread()):
            here = str(gdb.newest_frame().read_register("pc")).partition(" ")[0]
            if task and here == self.addr:
                raise RuntimeError("Current thread is already blocked.")

            self.blocked_task[task] = (here, gdb.selected_thread())
            try:
                gdb.execute("set $pc = {}".format(Blocker.BLOCKER_FCT))
                gdb.execute("flushregs")

            except Exception as e:
                log.critical("Blocking of {} failed: {}".format(task, e))
                log.error(e)
                return False
        return True
    
    def unblock(self, task=None):
        try:
            good_pc, thread = self.blocked_task[task]
        except KeyError:
            log.warning("{} was not actively blocked, nothing to do on inferior.".format(task))
            return
            
        with my_gdb.active_thread(thread):
            here = str(gdb.newest_frame().read_register("pc")).partition(" ")[0]
            if here != self.addr:
                raise RuntimeError("Current thread is not blocked.")
            
            gdb.execute("set $pc = {}".format(good_pc))
        
        Blocker.restart_for_task.add(task)
        del self.blocked_task[task]
            
            
