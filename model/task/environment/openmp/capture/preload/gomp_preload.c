#include "omp_preload.h"

void (*real_GOMP_barrier) (void);
void (*real_GOMP_critical_start) (void);
void (*real_GOMP_critical_end) (void);
void (*real_GOMP_critical_name_start) (void **pptr);
void (*real_GOMP_critical_name_end) (void **pptr);
void (*real_GOMP_parallel_start) (void (*fn) (void *), void *data, unsigned num_threads);
void (*real_GOMP_parallel_end) (void);
void (*real_GOMP_parallel) (void (*fn) (void *), void *data, unsigned num_threads, unsigned int flags);
unsigned (*real_GOMP_sections_start) (unsigned count);
void (*real_GOMP_parallel_sections_start) (void (*fn) (void *), void *data,
                              unsigned num_threads, unsigned count);
void (*real_GOMP_sections_end_nowait) (void);

bool (*real_GOMP_single_start) (void);
void (*real_GOMP_task) (void (*fn) (void *), void *data, void (*cpyfn) (void *, void *),
           long arg_size, long arg_align, bool if_clause, unsigned flags,
           void **depend);

/*********************************************************************/

struct ld_bindings_s gomp_bindings[] = {
  {"GOMP_barrier", (void **) &real_GOMP_barrier},
  {"GOMP_parallel", (void **) &real_GOMP_parallel},
  {"GOMP_critical_end", (void **) &real_GOMP_critical_end},
  {NULL, NULL}
};

void init_gomp_preload(void) {
  static int inited = 0;
  
  if (inited) {
    return;
  }
  
  init_omp_preload();
  init_bindings(gomp_bindings);

  inited = 1;
}

/*********************************************************************/

void
GOMP_barrier (void) {
  init_gomp_preload();

  real_GOMP_barrier();
  mcgdb_thread_can_run(&mcgdb_can_pass_barrier);
}

void
GOMP_parallel_trampoline(void *arg) {
  struct trampoline_data *data = (struct trampoline_data *) arg;
  void *(*target)(void *) = data->routine;
  thread_debug.can_run = 1;
  thread_debug.initialized = 1;
  
  mcgdb_thread_can_run(&mcgdb_can_pass_parallel);

  target(data->arg);
}

void
GOMP_parallel (void *(*fn) (void *), void *arg, unsigned num_threads, unsigned int flags) {
  struct trampoline_data *data;
  
  init_gomp_preload();

  data = malloc(sizeof(*data));
  data->routine = fn;
  data->arg = arg;
  
  real_GOMP_parallel (GOMP_parallel_trampoline, data, num_threads, flags);
  //free(data);
  // memory leak here, if GOMP_parallel is blocking, use
  // normal variable, otherwise hook 'GOMP_parallel_end'.
}

void
GOMP_critical_end (void) {
  real_GOMP_critical_end ();
  mcgdb_thread_can_run(&mcgdb_can_pass_critical);
}

/*
void
GOMP_critical_start (void) {

}

void
GOMP_critical_name_start (void **pptr) {
}

void
GOMP_critical_name_end (void **pptr) {

}

void
GOMP_parallel_start (void (*fn) (void *), void *data, unsigned num_threads) { 

}

void
GOMP_parallel_end (void) { 
}


unsigned
GOMP_sections_start (unsigned count) {
}

void
GOMP_parallel_sections_start (void (*fn) (void *), void *data,
                              unsigned num_threads, unsigned count) {
}

void
GOMP_sections_end_nowait (void) {
}

bool
GOMP_single_start (void){
}

void
GOMP_task (void (*fn) (void *), void *data, void (*cpyfn) (void *, void *),
           long arg_size, long arg_align, bool if_clause, unsigned flags,
           void **depend) {

}

int
omp_get_thread_num (void) {

}*/
