#include <pthread.h>
#include "omp_preload.h"

int (*real_pthread_create)(pthread_t *thread, const pthread_attr_t *attr,
                      void *(*start_routine) (void *), void *arg);

struct ld_bindings_s pthread_bindings[] = {
  {"pthread_create", (void **) &real_pthread_create},
  {NULL, NULL}
};

void init_pthread_preload(void) {
  static int inited = 0;
  
  if (inited) {
    return;
  }
  
  init_omp_preload();
  init_bindings(pthread_bindings);

  inited = 1;
}

void *
pthread_create_trampoline(void *arg) {
  struct trampoline_data *data = (struct trampoline_data *) arg;
  void *ret;

  thread_debug.can_run = 1;
  thread_debug.initialized = 1;

  ret = data->routine(data->arg);

  free(data);

  return ret;
}

int
pthread_create(pthread_t *thread, const pthread_attr_t *attr,
               void *(*start_routine) (void *), void *arg) {
  struct trampoline_data *data;

  init_pthread_preload();
  
  data = malloc(sizeof(*data));
  data->routine = start_routine;
  data->arg = arg;
  
  return real_pthread_create(thread, attr, pthread_create_trampoline, data);
}

void __thread_blocker(void) {
  /* This thread/task is blocked by mcGDB.
   * Do not try to exit manually this function,
   * it would compromize your execution.  */
  while(1);
  *(void **) NULL = 0; // should not come here
}

