#include "omp_preload.h"

__thread volatile struct debugging_state_s thread_debug = {0, 0};
__thread volatile int gdb_thread_num = -1;

volatile int mcgdb_can_pass_parallel = 1;
volatile int mcgdb_can_pass_barrier = 1;
volatile int mcgdb_can_pass_critical = 1;

void mcgdb_thread_can_run(volatile int *can_pass) {
  while (!thread_debug.can_run);
  
  while (can_pass != NULL && ! *can_pass);
}

void init_bindings(struct ld_bindings_s *bindings) {
  int i = 0;
  while (bindings[i].name) {
    *bindings[i].real_address = dlsym(RTLD_NEXT, bindings[i].name);
    if (*bindings[i].real_address == NULL) {
      error("in `dlsym` of %s: %s\n", bindings[i].name, dlerror());
    }
    i++;
  }
}

//void init_ldPreload(void) __attribute__((constructor));

void init_omp_preload(void) {
  static int inited = 0;
  
  if (inited) {
    return;
  }
  
  //init_bindings(omp_bindings);

  inited = 1;
}

/*********************************************************************/

void dbg_crash_event(void) {}
void dbg_notify_event(void) {}

void error(const char *format, ...) {
  va_list args;

  va_start(args, format);

  printf("ERROR: ");
  vprintf(format, args);

  va_end(args);

  dbg_crash_event();

  exit(1);
}
