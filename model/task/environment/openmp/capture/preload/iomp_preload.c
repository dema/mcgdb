#include <string.h>

#include "ompt_preload.h"
#include "omp_preload.h"

#include "kmp_os.h"

/*
 * Force definition here, *must be in sync with what is compiled*.
 */ 
#ifndef OMPT_SUPPORT
#def OMPT_SUPPORT
#endif
/*!
 * The ident structure that describes a source location.
 */
typedef struct ident {
    kmp_int32 reserved_1;   /**<  might be used in Fortran; see above  */
    kmp_int32 flags;        /**<  also f.flags; KMP_IDENT_xxx flags; KMP_IDENT_KMPC identifies this union member  */
    kmp_int32 reserved_2;   /**<  not really used in Fortran any more; see above */
#if USE_ITT_BUILD
                            /*  but currently used for storing region-specific ITT */
                            /*  contextual information. */
#endif /* USE_ITT_BUILD */
    kmp_int32 reserved_3;   /**< source[4] in Fortran, do not use for C++  */
    char const *psource;    /**< String describing the source location.
                            The string is composed of semi-colon separated fields which describe the source file,
                            the function and a pair of line numbers that delimit the construct.
                             */
} ident_t;

/*!
@ingroup PARALLEL
The type for a microtask which gets passed to @ref __kmpc_fork_call().
The arguments to the outlined function are
@param global_tid the global thread identity of the thread executing the function.
@param bound_tid  the local identitiy of the thread executing the function
@param ... pointers to shared variables accessed by the function.
*/
typedef void (*kmpc_micro)              ( kmp_int32 * global_tid, kmp_int32 * bound_tid, ... );
typedef void (*kmpc_micro_bound)        ( kmp_int32 * bound_tid, kmp_int32 * bound_nth, ... );

typedef kmp_int32 kmp_critical_name[8];
typedef int     (*launch_t)( int gtid );
enum fork_context_e
{
    fork_context_gnu,                           /**< Called from GNU generated code, so must not invoke the microtask internally. */
    fork_context_intel,                         /**< Called from Intel generated code.  */
    fork_context_last
};
/*********************************************************************/

void
(*real__kmpc_barrier)(ident_t *loc, kmp_int32 global_tid);
int (*real__kmp_fork_call)(
    ident_t   * loc,
    int         gtid,
    enum fork_context_e  call_context, // Intel, GNU, ...
    kmp_int32   argc,
#if OMPT_SUPPORT
    void       *unwrapped_task,
#endif
    microtask_t microtask,
    launch_t    invoker,
#if (KMP_ARCH_X86_64 || KMP_ARCH_ARM || KMP_ARCH_AARCH64) && KMP_OS_LINUX
    va_list   * ap
#else
    va_list     ap
#endif
    );
void
(*real__kmpc_end_critical)(ident_t *loc, kmp_int32 global_tid, kmp_critical_name *crit);
/*********************************************************************/

struct ld_bindings_s iomp_bindings[] = {
  {"__kmpc_barrier", (void **) &real__kmpc_barrier},
  {"__kmp_fork_call", (void **) &real__kmp_fork_call},
  {"__kmpc_end_critical", (void **) &real__kmpc_end_critical},
  {NULL, NULL}
};

void init_iomp_preload(void) {
  static int inited = 0;
  
  if (inited) {
    return;
  }
  
  init_omp_preload();
  init_bindings(iomp_bindings);

  inited = 1;
}

/*********************************************************************/

void
__kmpc_barrier (ident_t *loc, kmp_int32 global_tid) {
  init_iomp_preload();

  real__kmpc_barrier(loc, global_tid);
  mcgdb_thread_can_run(&mcgdb_can_pass_barrier);
}

microtask_t global_microtask = NULL;
void kmpc_fork_call_trampoline (kmp_int32 * global_tid, kmp_int32 * bound_tid, ... ) {
  microtask_t target = global_microtask;
  
  thread_debug.can_run = 1;
  thread_debug.initialized = 1;
    
  if (global_microtask == NULL) {
    printf("IOMP_PRELOAD::WARNING: MICROTASK IS NULL\n");
    return;
  }
  
  mcgdb_thread_can_run(&mcgdb_can_pass_parallel);
  /* do not put anything here, mcgdb will `finish` mcgdb_thread_can_run
   * and `step` into global_microtask(). */

  // call global_microtask(global_tid, bound_tid, ...)

#if 1 || defined(__GCC__)
  void *arg = __builtin_apply_args();

  __builtin_apply((void*)target, arg, 100);

  return;
  
#elif defined(__i386__)
  // (impossible w/o assembly)

  //unwind this frame (copied from function prolog disassemly)
  asm volatile("mov    -0xb8(%rbp),%rdi\n\t"
               "mov    -0xa8(%rbp),%rsi\n\t"
               "mov    -0xa0(%rbp),%rdx\n\t"
               "mov    -0x98(%rbp),%rcx\n\t"
               "mov    -0x90(%rbp),%r8\n\t"
               "mov    -0x88(%rbp),%r9\n\t"
               "add     $0xc0,%rsp\n\t");
  
  //movq	$[global_microtask], -8(%%rsp)
  volatile register void ** rsp asm ("rsp");
  *(rsp-1) = target;
  
  asm volatile(
    "mov    %rbp,%rsp\n\t"
    "pop     %rbp\n\t"
    "mov     (%rax), %rax\n\t"
    "jmpq    *-16(%rsp)");
  //never reached
#else
  #error Cannot intercept variadic functions without gcc or x86
#endif

}

int
__kmp_fork_call(
    ident_t   * loc,
    int         gtid,
    enum fork_context_e  call_context, // Intel, GNU, ...
    kmp_int32   argc,
#if OMPT_SUPPORT
    void       *unwrapped_task,
#endif
    microtask_t microtask,
    launch_t    invoker,
#if (KMP_ARCH_X86_64 || KMP_ARCH_ARM || KMP_ARCH_AARCH64) && KMP_OS_LINUX
    va_list   * ap
#else
    va_list     ap
#endif
  ) {
  init_iomp_preload();

  global_microtask = microtask;
  
  return real__kmp_fork_call(loc,
                              gtid,
                              call_context,
                              argc,
#if OMPT_SUPPORT
                              unwrapped_task,
#endif
                              kmpc_fork_call_trampoline,
                              invoker,
                              ap);
}

void
__kmpc_end_critical(ident_t *loc, kmp_int32 global_tid, kmp_critical_name *crit) {
  real__kmpc_end_critical (loc, global_tid, crit);
  mcgdb_thread_can_run(&mcgdb_can_pass_critical);
}

