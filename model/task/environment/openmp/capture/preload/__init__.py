import os
import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

import gdb

import mcgdb

from mcgdb.toolbox import my_gdb

def get_here():
    assert "__init__.py" in __file__ # may end with py or pyc
    return __file__.rpartition("/")[0]

LD_PRELOAD_RPATH = "__binaries__/libmcgdb_omp.preload.so"
GDB_PRELOAD = "set env LD_PRELOAD="

def prepare():
    ld_preload = "{}/{}".format(get_here(), LD_PRELOAD_RPATH)
    if not os.path.exists(ld_preload):
        log_user.warning("Cannot find mcGDB OpenMP helper library ({}, in {})".format(LD_PRELOAD_RPATH, get_here()))
        log_user.info("Please preload it before starting the application:")
        log_user.info(GDB_PRELOAD+"...")
        return
    
    log_user.info("Preloading libmcgdb_omp.preload.so into the application.")
    log.info("Using LD_PRELOAD={}".format(ld_preload))
    my_gdb.extend_env("LD_PRELOAD", ld_preload)
    
def no_prepare():
    log_user.warn("OpenMP debugging requires libmcgdb_opm.preload.so to run correctly.")
    log_user.warn("Please run this command if you want to debug OpenMP applications.")
    log_user.warn(GDB_PRELOAD+LD_PRELOAD_RPATH)
    
def activate():
    def activate_preload(do_activate, include_self):
        mcgdb.toggle_activate_submodules(__name__)(do_activate=True)

    mcgdb.register_model("OpenMP preloaded helper library.", "libmcgdb_omp.preload.so", activate_preload, objfile=True)
    
from . import omp_preload
