import logging; log = logging.getLogger(__name__)
import os
import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb

from ... import representation
from .. import current_worker
from .. import OmpFunctionBreakpoint

AYU_LIBRARY_PATH = "/home/kevin/travail/git/temanejo-mcgdb/install/lib/"
AYU_LDPRELOAD = "libayudame.so"
NX_ARGS = "--instrumentation ayudame"

ayu_event = None

def on_ayu_loaded():
    global ayu_event
    ayu_event = ayu_eventBreakpoint()
    tca_initializeBreakpoint()

def on_ayu_tca_loaded():
    stepper_request_progress_taskBreakpoint()
    
def configure(args=None):
    global ayu_socket
    import ayudame
    import ayudame.ayu_socket
    ayu_socket = ayudame.ayu_socket

    global ayu_event
    
    if gdb.inferiors() and gdb.inferiors()[0].pid != 0:
        log.warning("Inferior already started, it's too late to preload Ayudame.")
        return
    
    if ayu_event is not None:
        log.warning("Temanejo proxy is already initialized.")
        return
        
    ayu_library_path = args
    if not ayu_library_path:
        ayu_library_path = AYU_LIBRARY_PATH

    if not os.path.exists(ayu_library_path):
        log.error("Ayudame library path not found ({}).".format(ayu_library_path))
        log.error("Please provide a valid path in parameter.")
        return

    preloaded = my_gdb.extend_env("LD_PRELOAD", AYU_LDPRELOAD)
    log.info("LD_PRELOAD set to '{}'.".format(preloaded))

    ld_library_path = my_gdb.extend_env("LD_LIBRARY_PATH", AYU_LIBRARY_PATH)
    log.info("LD_LIBRARY_PATH set to '{}'.".format(ld_library_path))

    nx_args = my_gdb.extend_env("NX_ARGS", NX_ARGS, join=" ")
    log.info("NX_ARGS set to '{}'.".format(nx_args))
        
    my_gdb.exec_on_sharedlibrary(on_ayu_loaded, "libayudame.so")
    my_gdb.exec_on_sharedlibrary(on_ayu_tca_loaded, "libtca-ayudame.so")
    
    return True

class ayu_eventBreakpoint(OmpFunctionBreakpoint):
    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "ayu_event")
        
        self.tasks = {}
        self.dependencies = {}
        self.id_to_struct = {}
        
    @staticmethod
    def ayu_id_to_task(ayu_id):
        for task in representation.TaskJob.list_:
            if task.properties.get("ayu_id", None) == ayu_id:
                return task

    def prepare_before(self):
        MAPPING = {
            ayu_socket.AYU_ADDTASK: ("add_task", self.add_task),
            ayu_socket.AYU_ADDDEPENDENCY: ("add_dependency", self.add_dependency),
            ayu_socket.AYU_SETPROPERTY: ("set_property", self.set_property),
            ayu_socket.AYU_FINISH: ("finish", self.finish)
            }
        
        data = {}

        evt_type = my_archi.first_arg(my_archi.INT)

        if evt_type == ayu_socket.AYU_REQUEST:
            log.warning("Event received from Ayudame is a request ... (type={})".format(evt_type))
            log.warning("Event ignored.")
            return
        
        event_data_type = gdb.lookup_type("ayu_event_data_t")
        event = my_archi.second_arg(event_data_type)

        if evt_type not in MAPPING.keys():
            log.warning("Unknown event type from Ayudame: {} ({}) {}".format(evt_type, int(evt_type), MAPPING))
            log.info(event)
            return

        struct_item, handler, = MAPPING[int(evt_type)]

        handler(event[struct_item])
        
        return False, False, data
        
    def add_task(self, event):
        tid = int(event["task_id"])
        assert tid not in self.tasks
        
        task_struct = {
            "id": tid,
            "label":  event["task_label"].string(),
            "scope": int(event["scope_id"]),
            "client": int(event["common"]["client_id"]),
            "properties": {}
            }
            
        self.tasks[tid] = task_struct
        self.id_to_struct[tid] = task_struct
        
    def add_dependency(self, event):
        did = int(event["dependency_id"])
            
        dep_struct = {
            "id": did,
            "from_id": int(event["from_id"]),
            "to_id": int(event["to_id"]),
            "label": event["dependency_label"].string(),
            "properties": {}
            }
        self.dependencies[did] = dep_struct
        self.id_to_struct[did] = dep_struct
        
    def set_property(self, event):
        oid =  int(event["property_owner_id"])
        
        key = event["key"].string()
        val = event["value"].string()
        
        assert oid in self.id_to_struct
        struct = self.id_to_struct[oid]
        
        struct["properties"][key] = val
        
        task = ayu_eventBreakpoint.ayu_id_to_task(oid)
        dep = self.dependencies.get(oid, None)
        loc_task = self.tasks.get(oid, None)
        
        if task is not None:
            prefix = "ayu_"
            
            if key in ("func_id", "thread"):
                prefix = "__{}".format(prefix)
                
            task.set_property("{}{}".format(prefix, key), val)
        elif dep is not None:
            dep["properties"][key] = val
        elif loc_task is not None:
            loc_task["properties"][key] = val
        else:
            log.warning("Cannot find task for {}".format(oid))
            
    def finish(self, event):
        pass

class tca_initializeBreakpoint(OmpFunctionBreakpoint):
    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "Client_event_handler_ompss::tca_initialize")
        
    def prepare_before(self):
        data = {}
        data["lookup_fct"] = fn = my_archi.second_arg()
        data["lookup_bpt"] = tca_lookup_Breakpoint(fn)
        
        return False, False, data
    
    def prepare_after(self, data):
        data["lookup_bpt"].enabled = False
        self.enabled = False

class tca_lookup_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func

    def __init__(self, fn):
        addr = str(fn).split("0x")[1]
        addr = "*0x"+str(addr).split(" ")[0]

        self.block = None
        self.unblock = None
        
        OmpFunctionBreakpoint.__init__(self, addr)
        
    def prepare_before(self):
        data = {}
        data["what"] = what = str(my_archi.first_arg(my_archi.CHAR_P).string())

        if what not in ("tca_request_block_task",
                        "tca_request_unblock_task"):
            return

        return False, True, data
        
    def prepare_after(self, data):
        assert not (self.block and self.unblock)
        
        ret = my_archi.return_value()
        addr = str(ret).split(" ")[0]
        if "_block" in data["what"]:            
            self.block = gdb.parse_and_eval("(int (*)(long int)) {}".format(addr)).dereference()
            
        elif "_unblock" in data["what"]:
            self.unblock = gdb.parse_and_eval("(int (*)(long int)) {}".format(addr)).dereference()

        if self.block and self.unblock:
            key = "Ayudame"
            representation.TaskJob.blockers[key] = Blocker(self.block, self.unblock)
            representation.TaskJob.preferred_blocked = key
            self.enabled = False
            
        return False

class stepper_request_progress_taskBreakpoint(OmpFunctionBreakpoint):
    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "stepper::stepper_request_progress_task")
        
    def prepare_before(self):
        if "@plt" in gdb.newest_frame().name():
            return
        
        data = {}
        data["tid"] = tid = int(my_archi.first_arg(my_archi.INT))

        task = [t for t in representation.TaskJob.list_ if t.properties.get("ayu_id", None) == tid]

        if not task: return
        task = task[0]

        is_blocked = task.properties.get("__blocked", None)
        if not (is_blocked or bool(is_blocked)):
            return

        # only blocked tasks reach this point
        
        data["old_ayu_state"] = task.properties.get("ayu_state", None)
        task.set_property("ayu_state", "blocked_by_ayudame")
        
        return False, True, data

    def prepare_after(self, data):
        # only blocked tasks reach this point
        task.set_property("ayu_state", data["old_ayu_state"])

class Blocker():
    def __init__(self, blocker, unblocker):
        self.block_fct = blocker
        self.unblock_fct = unblocker
        self.lazy = False
        
    def block(self, task, active):
        if active:
            log.warning("Ayudame task blocker cannot block active tasks, please select anothere one.")
            return False
        with my_gdb.set_parameter("scheduler-locking", "on"):
            self.block_fct(task.properties["ayu_id"])

            return True
        
    def unblock(self, task):
        with my_gdb.set_parameter("scheduler-locking", "on"):
            self.unblock_fct(task.properties["ayu_id"])
