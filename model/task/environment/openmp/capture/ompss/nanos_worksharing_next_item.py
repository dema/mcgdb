import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb
from mcgdb.model.task import representation as task_representation

from ... import representation
from .. import current_worker
from .. import OmpFunctionBreakpoint

class nanos_worksharing_next_item_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    
    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "nanos_worksharing_next_item")
        
    def prepare_before (self):
        data = {}
        data["info"] = my_archi.second_arg()

        worker = current_worker()
        parallel_job = representation.ParallelJob.get_current_parallel_job(worker)
        
        if parallel_job.section is None:
            parallel_job.start_section_zone(worker, 0)
        
        return (False, True, data)

    def prepare_after (self, data):
        worker = current_worker()
        parallel_job = representation.ParallelJob.get_current_parallel_job(worker)
        
        loop_t = gdb.lookup_type("nanos_ws_item_loop_t").pointer()
        loop_info = data["info"].cast(loop_t).dereference()

        if str(loop_info["execute"]) == "true":
            for idx in range(int(loop_info["lower"]), 1+int(loop_info["upper"])):
                parallel_job.section.work_on_section(worker, idx+1)
        else:
            parallel_job.section.work_on_section(worker, 0)
        if str(loop_info["last"]) == "true":
            parallel_job.section.completed()


def activate():
    nanos_worksharing_next_item_Breakpoint()
