import logging; log = logging.getLogger(__name__)

import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb

from ... import representation
from .. import current_worker

from .. import OmpFunctionBreakpoint, omp_fct_to_job, get_omp_zone

class nanos_Scheduler_workerLoop_Breakpoint(OmpFunctionBreakpoint):
    def __init__(self):
        # nanos::Scheduler::workerLoop
        OmpFunctionBreakpoint.__init__(self, "_ZN5nanos9Scheduler10workerLoopEv")
        
    def prepare_before(self):
        worker = representation.Worker(gdb.selected_thread())
        gdb.selected_thread().name = str(worker)
        return False, False, {}

    def prepare_after(self, data):
        representation.Worker.terminated(gdb.selected_thread())

class nanos_create_team_Breakpoint(OmpFunctionBreakpoint):
    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "nanos_create_team")
        
    def prepare_before(self):
        data = {}
        
        data["num_threads"] = nthreads = int(my_archi.third_arg(gdb.lookup_type("unsigned int").pointer()).dereference())
        data["parent_worker"] = current_worker()
        
        representation.rt_ctrl.set_max_threads(nthreads)
        
        data["job"] = representation.ParallelJob(data["num_threads"],
                                                 data["parent_worker"])
        return False, False, {}

class nanos_submit_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    
    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "nanos_submit")
        
    def prepare_before (self):
        data = {}
        task_uid_type = gdb.lookup_type("nanos_wd_t").pointer()
        task_uid = str(my_archi.first_arg(task_uid_type))

        if task_uid not in nanos_create_wd_compact_Breakpoint.uid_to_task_job:
            log.warning("Task {} not recognized".format(task_uid))
            return
        
        data["task"] = task = nanos_create_wd_compact_Breakpoint.uid_to_task_job[task_uid]
        
        data["num_accesses"] = int(my_archi.second_arg(my_archi.INT))

        try:
            dep_type = gdb.lookup_type("nanos_data_access_t")
        except gdb.error as e:
            dep_type = gdb.lookup_type("struct  nanos_data_access_internal_t")
            
        dependencies = my_archi.third_arg(dep_type.pointer())

        in_depends = []
        out_depends = []
        for depend in [dependencies[i] for i in range(data["num_accesses"])]:
            """{
             address = 0x7ffff4e60c98, 
             flags = {
               input = false, 
               output = true, 
               can_rename = false, 
               concurrent = false, 
               commutative = false
             }, 
             dimension_count = 1, 
             dimensions = 0x7ffff4e60c40, 
             offset = 0
            }"""

            address = str(depend["address"])
            
            if str(depend["flags"]["input"]) == 'true':
                in_depends.append(address)
            
            if str(depend["flags"]["output"]) == 'true':
                out_depends.append(address)

        task.set_dependencies(in_depends, out_depends)
        return False, False, data
    
class nanos_create_wd_compact_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    fct_to_task_job = {} # fct_id --> task
    uid_to_task_job = {} # wd ptr --> task
    thread_last_task = {} # gdb_thr --> task
    
    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "nanos_create_wd_compact")
        self.wd_to_id = gdb.parse_and_eval("nanos_get_wd_id")
        
    def prepare_before (self):
        data = {}

        data["wd_ptr"] = my_archi.first_arg(my_archi.VOID_PP)
        
        arg_ptr_type = gdb.lookup_type("struct nanos_const_wd_definition_1").pointer()
        arg_ptr = my_archi.second_arg().cast(arg_ptr_type)

        arg_type = gdb.lookup_type("nanos_smp_args_t").pointer()
        arg_val = arg_ptr["devices"][0]["arg"].cast(arg_type)
        data["fn"] = fct = arg_val["outline"]
        data["fct_name"] = str(arg_ptr["base"]["description"].string())

        worker = current_worker()

        # if the current worker is not associated with a parallel job,
        # but there is a parallel job available, then we're starting it
        is_parallel = representation.ParallelJob.get_current_parallel_job(worker) is None \
            and representation.ParallelJob.get_current_parallel_job() is not None
        
        stop_after = False
        if not is_parallel:
            data["task"] = representation.TaskJob(worker)
            nanos_create_wd_compact_Breakpoint.fct_to_task_job[str(fct)] = data["task"]

            data["task"].set_property("ayu_description", data["fct_name"])
            data["task_id_ptr"] = my_archi.first_arg(my_archi.VOID_PP)
            stop_after = True

            nanos_create_wd_compact_Breakpoint.thread_last_task[gdb.selected_thread()] = data["task"]
        try:
            wd_inner_fct_Breakpoint(fct, is_parallel)
        except mcgdb.capture.AlreadyInstalledException:
            pass
        
        return (False, stop_after, data)
    
    def prepare_after(self, data):
        assert "task" in data

        uid = str(data["task_id_ptr"].dereference())
        nanos_create_wd_compact_Breakpoint.uid_to_task_job[uid] = data["task"]
        data["task"].set_property("ayu_id", int(self.wd_to_id(data["wd_ptr"].dereference())))
        nanos_create_wd_compact_Breakpoint.thread_last_task[gdb.selected_thread()] = None
        
    def recognize(self, frame):
        self.prepare_before()
        
class wd_inner_fct_Breakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    
    def __init__(self, fct, is_parallel):
        addr = "*"+str(fct).split(" ")[0]
        mcgdb.capture.FunctionBreakpoint.__init__(self, addr)

        self.is_parallel = is_parallel
        self.fct = fct
        
        self.data = {}
        self.data["fct_name"] = self.fct_name = str(fct).split(" ")[1][1:-1]
        
    def prepare_before (self):
        data = {}
        # get current parallel zone and enter
        worker = current_worker()
    
        # worker is associated with the parallel job
        data["parallel_job"] = parallel_job = representation.ParallelJob.get_current_parallel_job(worker)

        is_parallel = False
        # if it not yet associated
        if parallel_job is None:
            # get the first parallel zone available
            data["parallel_job"] = parallel_job = representation.ParallelJob.get_current_parallel_job()
            
            if parallel_job:
                parallel_job.start_working(worker)
                is_parallel = True

        assert is_parallel == self.is_parallel
        
        if not is_parallel:
            # its a task
            assert str(self.fct) in nanos_create_wd_compact_Breakpoint.fct_to_task_job
            
            taskjob = nanos_create_wd_compact_Breakpoint.fct_to_task_job[str(self.fct)]
            
            taskjob.start(current_worker())
            
            data["taskjob"] = taskjob
            
        return (False, True, data)

    def prepare_after (self, data):
        if "taskjob" in data:
            data["taskjob"].finish()
            
            self.enabled = False
            self.deleted = True
            gdb.post_event(self.delete)
        else:
            # exit current parallel zone
            data["parallel_job"].stop_working(current_worker())
            if data["parallel_job"].count == 0:
                data["parallel_job"].completed()
        pass
        
def activate():
    nanos_create_team_Breakpoint()
    nanos_create_wd_compact_Breakpoint()
    nanos_submit_Breakpoint()
        
    my_gdb.exec_on_sharedlibrary(nanos_Scheduler_workerLoop_Breakpoint, "libnanox.so.1")
