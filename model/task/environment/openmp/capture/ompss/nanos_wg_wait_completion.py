import gdb

import mcgdb


from .. import OmpFunctionBreakpoint

from ... import representation
from .. import OmpFunctionBreakpoint

class nanos_wg_wait_completion_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    
    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "nanos_wg_wait_completion")

    def prepare_before(self):
        if not representation.Init.break_after_init:
            return
        
        if representation.Init.after_init_cb is not None:
            representation.Init.after_init_cb()

        representation.Init.break_after_init = False
        representation.Init.after_init_cb = None
        
        return True, False, {}
    
def activate():
    nanos_wg_wait_completion_Breakpoint()
