import logging; log = logging.getLogger(__name__)
import os

import gdb

import mcgdb

IS_OMPSS = True

from ... import representation
from ...representation import Worker

def activate():
    mcgdb.toggle_activate_submodules(__name__)(do_activate=True)
    worker = representation.Worker(gdb.selected_thread())
    gdb.selected_thread().name = str(worker)
    
# keep last to use objets above
from . import \
    nanos_create_wd_compact, \
    nanos_omp_barrier, \
    nanos_omp_single, \
    nanos_set_lock, \
    nanos_worksharing_next_item, \
    nanos_wg_wait_completion
