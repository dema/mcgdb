import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb
from mcgdb.model.task import representation as task_representation

from ... import representation
from .. import current_worker
from .. import OmpFunctionBreakpoint

class nanos_set_lock_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    
    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "nanos_set_lock")
        
    def prepare_before (self):
        data = {}
        data["lock"] = my_archi.first_arg()
        
        data["critical"] = critical = representation.CriticalJob.get_parallel_critical_zone(current_worker(), data["lock"])
        critical.try_enter(current_worker())

        return (False, True, data)

    def prepare_after(self, data):
        data["critical"] = critical = representation.CriticalJob.get_parallel_critical_zone(current_worker(), data["lock"], no_new=True)
        critical.entered(current_worker())
        
class nanos_unset_lock_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    
    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "nanos_unset_lock")

    def prepare_before (self):
        data = {}
        data["critical"] = critical = representation.CriticalJob.get_parallel_critical_zone(current_worker(), depth=-2, no_new=True)
        critical.left(current_worker())
        
        return False, False, data
    
def activate():
    nanos_set_lock_Breakpoint()
    nanos_unset_lock_Breakpoint()
    pass
    
