import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb
from mcgdb.model.task import representation as task_representation

from ... import representation
from .. import current_worker
from .. import OmpFunctionBreakpoint

class nanos_omp_single_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    
    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "nanos_omp_single")
        
    def prepare_before (self):
        data = {}
        return (False, True, data)

    def prepare_after (self, data):
        data["ret"] = my_archi.return_value(my_archi.INT)

        data["single"] = single = representation.SingleJob.get_parallel_single_zone(current_worker())
        single.enter(int(data["ret"]) == 1, current_worker())

def activate():
    nanos_omp_single_Breakpoint()
