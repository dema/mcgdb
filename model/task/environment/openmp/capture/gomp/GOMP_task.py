import logging; log = logging.getLogger(__name__)

import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb
from mcgdb.model.task import representation as task_representation

from ... import representation
from .. import current_worker
from .. import OmpFunctionBreakpoint, omp_fct_to_job
from .. import attach_recognizers
from .. import parse_omp_dependencies

class GOMP_task_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    omp_set = "task"
    
    task_ptr_to_task = {}
    current_per_thread = {}
    
    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "GOMP_task")
        
    def prepare_before (self):
        data = {}

        fn = my_archi.first_arg()
        
        addr = str(fn).split("0x")[1]
        addr = data["addr"]= "*0x"+str(addr).split(" ")[0]

        fct_name = data["fct_name"] = str(fn).split("0x")[1].split(" ")[1][1:-1]

        depend = gdb.parse_and_eval("depend")
        ### save fct details for gomp_init_task
        GOMP_task_Breakpoint.current_per_thread[gdb.selected_thread()] = addr, fct_name, depend
                
        return False, False, data

class gomp_init_task_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    omp_set = "task deps"
    
    def set_dependencies(self, task, depend):
        NDEPEND_IDX, NOUT_IDX, DEP_OFFSET = range(3)
        """
        depend[NOUT_IDX] = (size_t) ndepend;
        depend[NOUT_IDX] = (size_t) nout;
        // nin = ndepends-nout;
        depend[DEP_OFFSET+0] = &<out_1>;
        ...
        depend[DEP_OFFSET+nin] = &<out_$nout>;
        depend[DEP_OFFSET+nin+1] = &<in_1>;
        ...
        depend[DEP_OFFSET+ndepends] = &<in_$nin>;
        """
        
        size_t = gdb.lookup_type("size_t")
        ndepend = int(depend[NDEPEND_IDX].cast(size_t))
        nout = int(depend[NOUT_IDX].cast(size_t))
        
        #nin = ndepend - nout
        
        in_depends = []
        out_depends = []
        
        bucket = out_depends
        for i in range(ndepend):
            if i == nout:
                bucket = in_depends
                
            bucket.append(str(depend[DEP_OFFSET+i]))

        gdb.newest_frame().older().select()

        from .. import get_dependency_name
        in_name = list(map(get_dependency_name, in_depends))
        out_name = list(map(get_dependency_name, out_depends))
        deps = parse_omp_dependencies(task.properties['definition'],
                                      in_name, out_name)
        gdb.newest_frame().select()
        assert None not in deps["in"]+deps["inout"]+deps["out"]

        task.set_dependencies(deps["in"], deps["inout"], deps["out"])
        
    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "gomp_init_task")

    def prepare_before (self):
        data = {}
        
        # this function will be called within GOMP_task,
        # so we *know* which task we're talking about
        try:
            addr, fct_name, depend = GOMP_task_Breakpoint.current_per_thread[gdb.selected_thread()]
            del GOMP_task_Breakpoint.current_per_thread[gdb.selected_thread()]
        except KeyError:
            # ignore, exec doesn't come from GOMP_task
            return

        ### get OMP task structure
        where = gdb.newest_frame().function().name
        if where == self.location:
            task_ptr_val = my_archi.first_arg()
        else:
            assert where == 'GOMP_task'
            task_ptr_val = gdb.newest_frame().read_var("task")

        data["task_ptr"] = task_ptr = str(task_ptr_val)

        if str(task_ptr_val.dereference()['kind']) == 'GOMP_TASK_IMPLICIT':
            #log.warn("skip implicit task at {} ?".format(task_ptr))
            pass
        
        data["task"] = task = representation.TaskJob(current_worker())
            
        ### lookup task source lines 
        try:
            fct_symb = gdb.lookup_symbol(fct_name)[0]
            task.set_task_source_lines(*my_gdb.get_function_fname_and_lines(fct_symb))
        except KeyError:
            log.warning("Could not get task source lines for {}".format(task))
            

        ### set dependencies
        # depend is different from task_ptr_val["depend"]

        if my_gdb.addr2num(depend) != 0:
            self.set_dependencies(task, depend)
            
        ### set task outlined function breakpoint
        try:
            GOMP_task_function_Breakpoint(addr, fct_name)
        except mcgdb.capture.AlreadyInstalledException:
            # ignore, we only want it once
            pass
            
        #### save omp task --> repr.task mapping
        assert task_ptr_val not in GOMP_task_Breakpoint.task_ptr_to_task
        GOMP_task_Breakpoint.task_ptr_to_task[task_ptr] = task
        task.ready()

        task.properties["__fctname"] = fct_name
        return False, False, data
    
class GOMP_task_function_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    omp_set = "task"
    
    def __init__(self, addr, fct_name):
        OmpFunctionBreakpoint.__init__(self, addr)
        
        self.fct_name = fct_name
        
        from ..preload.omp_preload import Blocker
        self.restart_for_task = Blocker.restart_for_task

        assert self.fct_name not in omp_fct_to_job
        omp_fct_to_job[self.fct_name] = self.identify

    def prepare_before (self):
        data = {}
        
        taskjob = self.identify()
        if taskjob is None:
            log.error("Could not identify the task currently running ...")
            return True, False, data
        else:
            data["taskjob"] = taskjob

        if not taskjob.properties["__fctname"] == self.fct_name:
            self.identify()
            return True, False, None
        assert gdb.newest_frame().name() == self.fct_name
        assert taskjob.properties["__fctname"] == self.fct_name
            
        if taskjob in self.restart_for_task:
            self.restart_for_task.remove(taskjob)
            taskjob.restart()
            return False, False, data
        else:
            assert taskjob.completed is False
            taskjob.start(current_worker())
            return False, True, data
        
    def prepare_after (self, data):
        assert data["taskjob"].completed is current_worker()
        data["taskjob"].finish()

    def identify(self):
        task = None
        
        try:
            task = gdb.newest_frame().older().read_var("child_task")
            if task.is_optimized_out:
                task = None
        except ValueError:
            pass

        if task is None:
            try:
                #assert gdb.newest_frame().older().name() == "GOMP_taskwait"
                task_ptr = gdb.newest_frame().older().read_register("r14")
                if int(task_ptr) == 0:
                    raise ValueError("Task pointer shouldn't be NULL...")
            except ValueError as e:
                log.critical("Could not read task register: {}".format(e))
                log.error("Maybe it's not the right architecture ... ?")
                return
            
            task_type = gdb.lookup_type("struct gomp_task").pointer()

            task = task_ptr.cast(task_type)
            # make sure we have a meaningful value
            assert gdb.newest_frame().name() in str(task["fn"]) 

        if task is None or task.is_optimized_out:
            return
        
        try:
            return GOMP_task_Breakpoint.task_ptr_to_task[str(task)]
        except KeyError:
            # ignore, it's not a real task
            return
        
    def recognize(self, frame):
        _, _, data = self.prepare_before()
        from mcgdb.capture import FunctionFinishBreakpoint
        FunctionFinishBreakpoint(self, data, None)
    
class gomp_barrier_handle_tasks_attach:
    @staticmethod
    def recognize(frame):
        newer = frame.newer() 
        if not newer or not newer.function(): return
        
        task = representation.TaskJob(None)
        fn = newer.function().value()

        GOMP_task_function_Breakpoint(fn, task)
    
def activate():
    GOMP_task_Breakpoint()
    gomp_init_task_Breakpoint()
    attach_recognizers["gomp_barrier_handle_tasks"] = gomp_barrier_handle_tasks_attach
