import logging; log = logging.getLogger(__name__)
import os

import gdb

import mcgdb

from ... import representation
from ...representation import Worker

FRAME_RENAME = {
    "gomp_team_barrier_wait_end" : "#pragma omp barrier",
    "gomp_barrier_handle_tasks": "<gomp task dispatcher>",
    "gomp_init_task" : "<gomp task initializer>"
    }

class RenamedFrame(mcgdb.capture.FrameStripperDecorator):
    def function(self):
        name = str(self.parent.inferior_frame().name())
        return FRAME_RENAME[name]
    def frame_locals(self): return None
    def frame_args(self): return None
    def address(self):
        return None
    def line(self):
        name = str(self.parent.inferior_frame().name())
        if FRAME_RENAME[name] == "#pragma omp barrier":
            return self.parent.inferior_frame().older().older().find_sal().line
        else:
            return None
    def filename(self):
        name = str(self.parent.inferior_frame().name())
        if FRAME_RENAME[name] == "#pragma omp barrier":
            return self.parent.inferior_frame().older().older().find_sal().symtab.filename
        else:
            return None
    
class FrameFilter:
    def __init__(self):
        self.enabled = True
        self.priority = 450
        
    def filter(self, frames):
        for frame in frames:
            name = str(frame.inferior_frame().name())

            if name in FRAME_RENAME:
                yield RenamedFrame(frame)
            else:
                yield frame
                
def activate():
    mcgdb.toggle_activate_submodules(__name__)(do_activate=True)
    gdb.frame_filters["GOMP Subroutines"] = FrameFilter()
    
    mcgdb.register_model("OpenMP Pthread",
                         "libpthread.so",
                         mcgdb.toggle_activate_submodules(__name__+".gomp_thread_start"), objfile=True)
        
# keep last to use objets above
from . import \
    GOMP_parallel_start_end, \
    GOMP_barrier, \
    GOMP_single_start, \
    GOMP_task, \
    GOMP_taskwait, \
    GOMP_sections_next, \
    GOMP_critical_start_end, \
    omp_get_thread_num, \
    gomp_thread_start
#    syscall_write

