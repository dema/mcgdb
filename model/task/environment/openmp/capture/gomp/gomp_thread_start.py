import logging; log = logging.getLogger(__name__)

import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb

from ... import representation
from .. import OmpFunctionBreakpoint

ENTRY_POINTS = ("gomp_thread_start", "start_thread")

class gomp_thread_start_Breakpoint(OmpFunctionBreakpoint):
    omp_set = "workers"
    
    def __init__(self):
        for start in ENTRY_POINTS:
            try:
                found = gdb.parse_and_eval(start) # or lookup_global_symbol ... ?
            except gdb.error:
                found = False
            finally:
                if found:
                    break
        else:
            msg = "WARNING: Cannot initialize NEW WORKER breakpoint" + \
                "({})".format(", ".join(start)) + ". " + \
                "Libgomp/pthread symbols migh be missing."        
            raise LookupError(msg)

        OmpFunctionBreakpoint.__init__(self, start)

    def prepare_before(self):
        worker = representation.Worker(gdb.selected_thread())
        gdb.selected_thread().name = str(worker)
        return False, True, {}

    def prepare_after(self, data):
        representation.Worker.terminated(gdb.selected_thread())
        
class FrameFilter:
    def __init__(self):
        self.enabled = True
        self.priority = 500
        
    def filter(self, frames):
        for frame in frames:
            if str(frame.function()) in ENTRY_POINTS:
                return
            yield frame
    
first = True
inserted = False
def activate():
    global inserted

    if inserted: return

    gdb.frame_filters["GOMP thread start"] = FrameFilter()

    try:
        gomp_thread_start_Breakpoint()
        inserted = True    
    except LookupError as e:
        global first
        if first: first = False
        else:
            msg = str(e)
            log.warning(msg)
            mcgdb.interaction.push_stop_request(msg)
    else:
        w = representation.Worker(gdb.selected_thread())
        gdb.selected_thread().name = str(w)
