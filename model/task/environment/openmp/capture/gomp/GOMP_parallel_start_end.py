import logging; log = logging.getLogger(__name__)

import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb

from ... import representation
from .. import current_worker

from .. import OmpFunctionBreakpoint, omp_fct_to_job, get_omp_zone

class GOMP_parallel_start_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    inner_breakpoint = {}
    omp_set = "parallel"
    
    def __init__(self, section=False, parallel=False):
        if section:
            name = "GOMP_parallel_sections_start"
        elif parallel:
            name = "GOMP_parallel"
        else:
            name = "GOMP_parallel_start"
            
        OmpFunctionBreakpoint.__init__(self, name)
        self.name = name
        self.section = section
        
    def prepare_before (self):
        if representation.rt_ctrl.max_threads is None:
            DYNAMIC = False # currently crashes GDB when inferior is asleep ...
            if DYNAMIC:
                representation.rt_ctrl.set_max_threads(int(gdb.parse_and_eval("omp_get_max_threads")()))
            else:
                MAX_THREAD = 4
                log.warn("Thread number hardcoded to {}".format(MAX_THREAD))
                representation.rt_ctrl.set_max_threads(MAX_THREAD)
            
        data = {}
        data["fn"] = my_archi.first_arg()
        data["num_threads"] = int(my_archi.third_arg(my_archi.INT))
        data["parent_worker"] = current_worker()

        data["section_count"] = (int(my_archi.fourth_arg(my_archi.INT)) 
                                 if self.section else 0)
        
        zone = get_omp_zone()
        data["is_for"] = is_for = zone is not None and len(zone) >= 2 and zone[1] == "for"
        
        data["job"] = representation.ParallelJob(data["num_threads"],
                                                 data["parent_worker"],
                                                 section_count=data["section_count"],
                                                 is_for=data["is_for"])

        inner = GOMP_parallel_function_Breakpoint(data["fn"],
                                                  data["job"],
                                                  data["parent_worker"])
        
        GOMP_parallel_start_Breakpoint.inner_breakpoint[data["job"]] = inner
        
        data["job"].start_working(data["parent_worker"])
        
        return (False, False, data)

    def recognize(self, frame):
        self.prepare_before()
        
class GOMP_parallel_end_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    omp_set = "parallel"
    
    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "GOMP_parallel_end")
        
    def prepare_before (self):
        return (False, True, {})

    def prepare_after (self, data):
        zone = representation.ParallelJob.work_completed(current_worker())
        if not zone:
            # zone already completed
            return

        # main worker didn't do the inner function
        inner = GOMP_parallel_start_Breakpoint.inner_breakpoint[zone]
        inner.enabled = False
        inner.deleted = True
        gdb.post_event(inner.delete)
        
class GOMP_parallel_function_Breakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    omp_set = "parallel"
    
    def __init__(self, fn, job, creator):
        addr = "*"+str(fn).split(" ")[0]
        mcgdb.capture.FunctionBreakpoint.__init__(self, addr)

        self.data = {}
        self.data["fct_name"] = self.fct_name = str(fn).split(" ")[1][1:-1]
        self.data["job"] = self.job = job
        self.creator = creator

        omp_fct_to_job[self.fct_name] = job
        
    def prepare_before (self, early=False, first=True):
        if first:
            worker = current_worker()
        
            if not worker is self.creator:
                self.job.start_working(worker)
                
        return False, True, self.data

    def prepare_after (self, data):
        self.job.stop_working(current_worker())
        
        if self.job.count == 0:
            self.job.completed()
            del GOMP_parallel_start_Breakpoint.inner_breakpoint[self.job]
            self.enabled = False
            self.deleted = True
            
            gdb.post_event(self.delete)
            
    def recognize(self, frame):
        _, _, data = self.prepare_before()
        from mcgdb.capture import FunctionFinishBreakpoint
        FunctionFinishBreakpoint(self, data, None)
        
def activate():
    GOMP_parallel_start_Breakpoint()
    GOMP_parallel_start_Breakpoint(parallel=True)
    GOMP_parallel_end_Breakpoint()
    


