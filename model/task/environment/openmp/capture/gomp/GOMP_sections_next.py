import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb

from ... import representation
from .. import current_worker
from .. import OmpFunctionBreakpoint
from . import GOMP_parallel_start_end

class GOMP_sections_start_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    omp_set = "sections"
    
    def __init__(self):        
        OmpFunctionBreakpoint.__init__(self, "GOMP_sections_start")

    def prepare_before (self):
        data = {}

        data["section_count"] = int(my_archi.first_arg(my_archi.INT))
        worker = current_worker()

        data["parallel_job"] = parallel_job = representation.ParallelJob.get_current_parallel_job(worker)
        parallel_job.start_section_zone(worker, data["section_count"])
        
        return (False, True, data)

    def prepare_after (self, data):
        data["section_id"] = int(my_archi.return_value(my_archi.INT))
        worker = current_worker()

        parallel_job = representation.ParallelJob.get_current_parallel_job(worker)
        parallel_job.section.work_on_section(worker, data["section_id"])

class GOMP_sections_end_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    omp_set = "sections"
    
    def __init__(self, no_wait=False):        
        OmpFunctionBreakpoint.__init__(self,
                   "GOMP_sections_end{}".format("_nowait" if no_wait else ""))
        self.no_wait = no_wait
        
    def prepare_before (self):
        if self.no_wait:
            worker = current_worker()
        
            data["parallel_job"] = parallel_job = representation.ParallelJob.get_current_parallel_job(worker)
            parallel_job.section.ended(worker)

        #only go after if blocking call
        return (False, not self.no_wait, None)

    def prepare_after (self, data):
        worker = current_worker()

        parallel_job = representation.ParallelJob.get_current_parallel_job(worker)
        parallel_job.section.completed()
        
class GOMP_sections_next_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    omp_set = "sections"
    
    def __init__(self):        
        OmpFunctionBreakpoint.__init__(self, "GOMP_sections_next")

    def prepare_before (self):
        data = {}
        return (False, True, data)

    def prepare_after (self, data):
        data["section_id"] = int(my_archi.return_value(my_archi.INT))

        worker = current_worker()
        
        parallel_job = representation.ParallelJob.get_current_parallel_job(worker)
        if representation.is_attached and parallel_job.section is None:
            parallel_job.start_section_zone(worker)
            parallel_job.section.work_on_section(worker, -1)
        parallel_job.section.work_on_section(worker, data["section_id"])
        
def activate():
    GOMP_sections_start_Breakpoint()
    GOMP_sections_end_Breakpoint()
    GOMP_parallel_start_end.GOMP_parallel_start_Breakpoint(section=True)
    
    GOMP_sections_next_Breakpoint()
