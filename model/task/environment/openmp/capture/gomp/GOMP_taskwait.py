import logging; log = logging.getLogger(__name__)

import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb
from mcgdb.model.task import representation as task_representation


from ... import representation
from .. import OmpFunctionBreakpoint
from .. import current_worker

class GOMP_taskwait_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    omp_set = "task deps"
    
    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "GOMP_taskwait")
        
    def prepare_before (self):
        data = {}

        data["taskwait"] = representation.TaskJob.task_wait(current_worker())

        return False, True, data

    def prepare_after(self, data):
        representation.TaskJob.task_wait_done(data["taskwait"])
        
def activate():
    GOMP_taskwait_Breakpoint()
