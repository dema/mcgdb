import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb

from ... import representation
from .. import current_worker, OmpFunctionBreakpoint, get_omp_zone

handlers = {} # opm operation -> handler
class omp_get_thread_num_Breakpoint(OmpFunctionBreakpoint):
    omp_set = "master"
    
    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "omp_get_thread_num")

    def prepare_before(self):
        zone = get_omp_zone()
        handler = handlers.get(zone[0] if zone else None, None)

        return handler.prepare_before() if handler else None    

class omp_master_Handler():
    
    def prepare_before(self):
        if current_worker().number != 1:
            return None
        
        representation.TaskJob.master_zone()

        return False, False, {}

        
def activate():
    omp_get_thread_num_Breakpoint()
    handlers["master"] = omp_master_Handler()
