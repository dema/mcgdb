import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb

from ... import representation
from .. import current_worker

class WriteBreakpoint(gdb.Breakpoint):
    def __init__(self):
        gdb.Breakpoint.__init__(self, "write", internal=True)
        self.silent = True
        
    def stop(self):
        fd = int(my_archi.first_arg(my_archi.INT))
        
        if fd not in (0, 1, 2):
            return None
        
        buf = str(my_archi.second_arg(my_archi.CHAR_P).string())
        count = int(my_archi.third_arg(my_archi.INT))
        
        buf = buf[:count]

        worker = current_worker()
        worker.say("###")

        for s in buf.split("\n"):
            if not s: continue
            current_worker().say(s)
        worker.say("###")

        gdb.execute("return")
        # write len(string) into return register ... ?
        
        return False
    
def activate():
    WriteBreakpoint()
