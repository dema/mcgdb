import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb

from ... import representation
from .. import current_worker
from .. import OmpFunctionBreakpoint

def filename_lineno():
    try:
        sal = gdb.newest_frame().older().find_sal()
        if not sal: return

        return "{}:{}".format(sal.symtab.filename, sal.line)
    except Exception as e:
        return str(e)

class GOMP_barrier_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    omp_set = "barrier"
    
    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "GOMP_barrier")
        
    def prepare_before (self):
        data = {}
        data["barrier"] = representation.Barrier.get_parallel_barrier(current_worker(), reach=True)

        data["barrier"].reach_barrier(current_worker(), filename_lineno())
        
        return (False, True, data)

    def prepare_after (self, data):
        data["barrier"].leave_barrier(current_worker())

    def recognize(self, frame):
        self.prepare_before()

def activate():
    GOMP_barrier_Breakpoint()
