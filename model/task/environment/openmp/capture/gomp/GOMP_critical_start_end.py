import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb
from mcgdb.model.task import representation as task_representation

from ... import representation
from .. import current_worker
from .. import OmpFunctionBreakpoint

class GOMP_critical_start_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    omp_set = "critical"
    
    def __init__(self, named=False):
        OmpFunctionBreakpoint.__init__(self, "GOMP_critical_{}start".format("name_" if named else ""))
        self.named = named
        
    def prepare_before (self):
        data = {}
        data["pc"] = gdb.newest_frame().older().read_register("pc")
        
        data["critical"] = critical = representation.CriticalJob.get_parallel_critical_zone(current_worker(), data["pc"])
        critical.try_enter(current_worker())
        
        return (False, True, data)

    def prepare_after(self, data):
        data["critical"] = critical = representation.CriticalJob.get_parallel_critical_zone(current_worker(), data["pc"], no_new=True)
        critical.entered(current_worker())

    def recognize(self, frame):
        if not frame.find_sal().symtab.filename.endswith("critical.c"): return
        self.prepare_before()
        
class GOMP_critical_end_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    omp_set = "critical"
    
    def __init__(self, named=False):
        OmpFunctionBreakpoint.__init__(self, "GOMP_critical_{}end".format("name_" if named else ""))
        self.named = named
        
    def prepare_before (self):
        data = {}
        data["critical"] = critical = representation.CriticalJob.get_parallel_critical_zone(current_worker(), depth=-2, no_new=True)
        critical.left(current_worker())
        
        return False, False, data
    
def activate():
    GOMP_critical_start_Breakpoint()
    GOMP_critical_end_Breakpoint()

    GOMP_critical_start_Breakpoint(named=True)
    GOMP_critical_end_Breakpoint(named=True)
    
