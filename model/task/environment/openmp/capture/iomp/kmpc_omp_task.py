import logging; log = logging.getLogger(__name__)

import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb
from mcgdb.model.task import representation as task_representation

from ... import representation
from .. import current_worker
from .. import OmpFunctionBreakpoint
from .. import parse_omp_dependencies

HANDLE_TO_TASK = {}

class kmp_check_deps_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    omp_set = "task deps"
    
    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "__kmp_check_deps")

    def prepare_before (self):
        task_handle = str(my_archi.third_arg(my_archi.VOID_P))
        if task_handle == "0x0":
            return

        task = HANDLE_TO_TASK.get(task_handle, None)
        if not task_handle:
            return
        
        depends = {"in": [], "inout": [], "out": []}
        ndeps = int(my_archi.nth_arg(6, my_archi.INT))

        dep_list_type = gdb.lookup_type("kmp_depend_info").pointer()
        dep_list = my_archi.nth_arg(7, dep_list_type)

        for depend in [dep_list[i] for i in range(ndeps)]:
            """{
              base_addr = 140737336479156, 
              len = 4, 
              flags = {
                in = false, 
                out = true
              }
            }"""

            address = str(depend["base_addr"].cast(my_archi.VOID_P))

            direct = "{}{}".format(
                "in" if str(depend["flags"]["in"]) == 'true' else "",
                "out" if str(depend["flags"]["out"]) == 'true' else ""
                )
            depends[direct].append(address)

        from .. import get_dependency_name

        
        depends_name = {direct: map(get_dependency_name, deps)
                        for direct, deps in depends.items()}
        
        deps = parse_omp_dependencies(task.properties['definition'],
                                      depends_name["in"],
                                      depends_name["out"],
                                      depends_name["inout"])
        
        
        task.set_dependencies(depends["in"], depends["inout"], depends["out"])

        task.ready()
        
        return False, False, {}
    
class kmpc_omp_task_alloc_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    omp_set = "task"
    
    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "__kmpc_omp_task_alloc")
        
    def prepare_before (self):
        data = {}

        fn = data["fn"] = my_archi.nth_arg(6)

        data["task"] = task = representation.TaskJob(current_worker())
        try:
            kmpc_task_function_Breakpoint(data["fn"])
        except mcgdb.capture.AlreadyInstalledException:
            # ignore, we only want it once
            pass

        try:
            fct_addr = "0x"+str(fn).split("0x")[1].split(" ")[0]
            
            fct_symb = gdb.block_for_pc(int(fct_addr, 16)).function

            task.set_task_source_lines(*my_gdb.get_function_fname_and_lines(fct_symb))
        except KeyError:
            log.warning("Could not get task source lines for {}".format(task))
        
        return False, True, data

    def prepare_after(self, data):
        handle = str(my_archi.return_value(my_archi.VOID_P))

        assert handle not in HANDLE_TO_TASK or HANDLE_TO_TASK[handle].completed is True

        HANDLE_TO_TASK[handle] = data["task"]
        
        data["task"].ready()
        
class kmpc_task_function_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    omp_set = "task"
    
    def __init__(self, fn):
        addr = str(fn).split("0x")[1]
        addr = "*0x"+str(addr).split(" ")[0]

        self.fct_name = str(fn).split("0x")[1].split(" ")[1][1:-1]

        OmpFunctionBreakpoint.__init__(self, addr)
                
    def prepare_before (self):
        data = {}
        gdb.newest_frame().older().select()
        
        handle = str(my_archi.second_arg(my_archi.VOID_P))
        data["taskjob"] = task = HANDLE_TO_TASK[handle]
        task.start(current_worker())
        
        return False, True, data

    def prepare_after (self, data):
        data["taskjob"].finish()

    def recognize(self, frame):
        _, _, data = self.prepare_before()
        from mcgdb.capture import FunctionFinishBreakpoint
        FunctionFinishBreakpoint(self, data, None)
        
def activate():
    kmpc_omp_task_alloc_Breakpoint()
    kmp_check_deps_Breakpoint()
