import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb
from mcgdb.model.task import representation as task_representation

from ... import representation
from .. import current_worker
from .. import OmpFunctionBreakpoint

class kmpc_critical_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    omp_set = "critical"

    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "__kmpc_critical")
        
    def prepare_before (self):
        data = {}
        data["pc"] = gdb.newest_frame().older().read_register("pc")
        
        data["critical"] = critical = representation.CriticalJob.get_parallel_critical_zone(current_worker(), data["pc"])
        critical.try_enter(current_worker())
        
        return (False, True, data)

    def prepare_after(self, data):
        data["critical"] = critical = representation.CriticalJob.get_parallel_critical_zone(current_worker(), data["pc"], no_new=True)
        critical.entered(current_worker())

    def recognize(self, frame):
        self.prepare_before()
        
class kmpc_end_critical_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    omp_set = "critical"
    
    def __init__(self, named=False):
        OmpFunctionBreakpoint.__init__(self, "__kmpc_end_critical")
        self.named = named
        
    def prepare_before (self):
        data = {}
        data["critical"] = critical = representation.CriticalJob.get_parallel_critical_zone(current_worker(), depth=-2, no_new=True)
        critical.left(current_worker())
        
        return False, False, data
    
def activate():
    kmpc_critical_Breakpoint()
    kmpc_end_critical_Breakpoint()

    
