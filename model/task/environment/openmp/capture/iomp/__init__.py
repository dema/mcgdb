import logging; log = logging.getLogger(__name__)

import gdb

import mcgdb

from ... import representation
from ...representation import Worker
  
def activate():
    mcgdb.toggle_activate_submodules(__name__)(do_activate=True)

    main_worker = representation.Worker(gdb.selected_thread())
    gdb.selected_thread().name = str(main_worker)

    #log.critical("Activate OMPT task tracking")
    #gdb.execute("set ompt_status += ompt_status_track")
    
# keep last to use objets above
from . import            \
     kmpc_barrier,       \
     kmpc_single,        \
     kmpc_critical,      \
     kmpc_omp_task,      \
     kmpc_for_static,    \
     kmpc_omp_taskwait,  \
     kmpc_fork_call,     \
     kmpc_launch_worker, \
     kmpc_master

#log.warning("Many OpenMP feature disabled (hardcoded)")

from . import  kmpc_for_static

    
from ..preload import omp_preload
