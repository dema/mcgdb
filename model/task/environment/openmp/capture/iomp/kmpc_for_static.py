import gdb

import logging; log = logging.getLogger(__name__)

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb

from ... import representation
from .. import current_worker
from .. import OmpFunctionBreakpoint


def ptr_to_int(ptr):
    return int(ptr.dereference())

class kmpc_for_static_init_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    omp_set = "loop static"
    
    def __init__(self, _type="4u"):
        OmpFunctionBreakpoint.__init__(self, "__kmpc_for_static_init_{}".format(_type))

    def prepare_before (self):
        data = {}
        #my_gdb.addr2num(my_archi.nth_arg(5, my_archi.VOID_P)) # should access loc->psource
        data["loc"] = my_gdb.addr2num(gdb.newest_frame().older().read_register("pc"))
        data["plower"] = my_archi.nth_arg(5, my_archi.INT_P)
        data["pupper"] = my_archi.nth_arg(6, my_archi.INT_P)
        data["incr"] = my_archi.nth_arg(8, my_archi.INT)
        
        data["loop"] = representation.ForLoopJob.get_loop_at_loc(data["loc"],
                                                                 ptr_to_int(data["plower"]),
                                                                 ptr_to_int(data["pupper"]),
                                                                 data["incr"])
        return False, True, data

    def prepare_after(self, data):
        worker = current_worker()
        data["loop"].start_work_on(worker,
                                   ptr_to_int(data["plower"]),
                                   ptr_to_int(data["pupper"]))
        
class kmpc_for_static_fini_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    omp_set = "loop static"
    
    def __init__(self,):
        OmpFunctionBreakpoint.__init__(self, "__kmpc_for_static_fini")

    def prepare_before (self):
        data = {}
        data["worker"] = worker = current_worker()
        
        data["loop"], data["iteration"] = loop, iteration \
            = representation.ForLoopJob.get_current_loop_of(worker)
        
        if not loop:
            log.warn("spurious end")
            return
        
        loop.stop_work_of(worker)
        
        stop_after = loop.break_after_next
        loop.break_after_next = False

        if stop_after:
            data["old-scheduler-locking"] = gdb.parameter("scheduler-locking")
            gdb.execute("set scheduler-locking on")
            
        return False, stop_after, data
        
    def prepare_after(self, data):
        gdb.execute("set scheduler-locking {}".format(data["old-scheduler-locking"]))
        log.warn("Stopped in {} after loop {} iteration {}-{}.".format(
                data["worker"],
                data["loop"],
                data["iteration"][0],
                data["iteration"][1]))
        
        if data["loop"].done:
            log.info("Loop {} is completed.".format(data["loop"]))
            
        return True

        
def activate():
    kmpc_for_static_init_Breakpoint(_type="4u")
    kmpc_for_static_init_Breakpoint(_type="4")
    kmpc_for_static_fini_Breakpoint()
