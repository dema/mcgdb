import logging; log = logging.getLogger(__name__)

import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb

from ... import representation
from .. import OmpFunctionBreakpoint

class kmpc_launch_worker_Breakpoint(OmpFunctionBreakpoint):
    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "__kmp_launch_worker")

    def prepare_before(self):
        data = {}
        data["worker"] = worker = representation.Worker(gdb.selected_thread())
        gdb.selected_thread().name = str(worker)
        return False, True, data

    def prepare_after(self, data):
        data["worker"].terminated()

class kmpc_launch_monitor_Breakpoint(OmpFunctionBreakpoint):
    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "__kmp_launch_monitor")

    def prepare_before(self):
        gdb.selected_thread().name = "Helper thread"
        return False, False, {}
        
def activate():
    kmpc_launch_worker_Breakpoint()
    kmpc_launch_monitor_Breakpoint()
