import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb
from mcgdb.model.task import representation as task_representation

from ... import representation
from .. import current_worker
from .. import OmpFunctionBreakpoint

class kmpc_master_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    omp_set = "master"
    
    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "__kmpc_master")
        
    def prepare_before (self):
        data = {}
        return (False, True, data)

    def prepare_after (self, data):
        data["ret"] = my_archi.return_value(my_archi.INT)
        if int(data["ret"]) == 1:
            representation.TaskJob.master_zone()

def activate():
    kmpc_master_Breakpoint()
