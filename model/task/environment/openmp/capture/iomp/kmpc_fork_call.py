import logging; log = logging.getLogger(__name__)

import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb

from ... import representation
from .. import current_worker
from .. import OmpFunctionBreakpoint

class kmpc_fork_call_Breakpoint(OmpFunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func
    omp_set = "parallel"
    
    inner_breakpoint = {}
    
    def __init__(self):
        OmpFunctionBreakpoint.__init__(self, "__kmpc_fork_call")
        
    def prepare_before (self):
        if representation.rt_ctrl.max_threads is None:
            representation.rt_ctrl.set_max_threads(int(gdb.parse_and_eval("omp_get_max_threads")()))
            
        data = {}
        data["fn"] = my_archi.third_arg()
        data["job"] = representation.ParallelJob(0, current_worker())

        inner = kmpc_parallel_function_Breakpoint(data["fn"],
                                                  data["job"],
                                                  current_worker())
        
        kmpc_fork_call_Breakpoint.inner_breakpoint[data["job"]] = inner

        data["job"].start_working(current_worker())
        
        return (False, True, data)

    def prepare_after (self, data):
        zone = representation.ParallelJob.work_completed(current_worker())
        if not zone:
            # zone already completed
            return
        
        inner = kmpc_fork_call_Breakpoint.inner_breakpoint[zone]
        inner.enabled = False
        inner.deleted = True
        gdb.post_event(inner.delete)

    def recognize(self, frame):
        self.prepare_before()
        
class kmpc_parallel_function_Breakpoint(mcgdb.capture.FunctionBreakpoint):
    func_type = mcgdb.capture.FunctionTypes.conf_func

    def __init__(self, fn, job, creator):
        addr = "*"+str(fn).split(" ")[0]

        mcgdb.capture.FunctionBreakpoint.__init__(self, addr)
        
        self.data = {}
        self.data["fct_name"] = self.fct_name = str(fn).split(" ")[1][1:-1]
        self.data["job"] = self.job = job
        self.creator = creator
        
    def prepare_before (self, early=False, first=True):
        if first:
            worker = current_worker()

            if not worker is self.creator:
                self.job.start_working(worker)

        return (False, True, self.data)

    def prepare_after (self, data):
        self.job.stop_working(current_worker())

        if self.job.count == 0:
            del kmpc_fork_call_Breakpoint.inner_breakpoint[self.job]
            self.enabled = False
            self.deleted = True
            gdb.post_event(self.delete)
        
    def recognize(self, frame):
        _, _, data = self.prepare_before()
        from mcgdb.capture import FunctionFinishBreakpoint
        FunctionFinishBreakpoint(self, data, None)
        
def activate():
    kmpc_fork_call_Breakpoint()
    


