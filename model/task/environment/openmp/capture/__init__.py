import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

from collections import defaultdict
import pyparsing

import gdb

import mcgdb
from .. import representation
from ..representation import Worker
from mcgdb.toolbox import my_gdb

capture = None

def activate_capture(load_gomp=False, load_iomp=False, load_ompss=False):
    def activate(do_activate=True, include_self=True):
        global capture
        if load_gomp:
            from . import gomp as capture
            try:
                from gomp import gomp_thread_start
            except:
                from .gomp import gomp_thread_start
            
        if load_iomp:
            from . import iomp as capture
            
        if load_ompss:
            from . import ompss as capture
        
        mcgdb.toggle_activate_submodules(__name__)(do_activate, include_self)
    return activate

OMP_FCT_PREFIX = ("GOMP_", "__kmp")

def initialize():
    mcgdb.register_model("GCC OpenMP", "libgomp.so", activate_capture(load_gomp=True), objfile=True)
    mcgdb.register_model("Intel OpenMP", "libomp.so", activate_capture(load_iomp=True), objfile=True)
    mcgdb.register_model("Intel OpenMP", "libiomp5.so", activate_capture(load_iomp=True), objfile=True)
    mcgdb.register_model("OMPss OpenMP", "libnanox-c.so.1", activate_capture(load_ompss=True), objfile=True)
    
def check_prepare(x):
    try:
        gdb.parse_and_eval("omp_get_thread_num")
        preload.prepare()
    except gdb.error:
        preload.no_prepare()

    gdb.events.new_objfile.disconnect(check_prepare)
    
def get_dependency_name(address):
    
    if isinstance(address, str):
        address = address.split()[0]
        address = int(address, 16)

    def lookup_variable():
        frame = gdb.newest_frame()

        while frame:
            fname = frame.name()
            
            if any([fname.startswith(prefix) for prefix in OMP_FCT_PREFIX]):
                frame = frame.older()
                continue
                
            var = my_gdb.address_to_variable(address, frame)
            if var:
                symb, offset = var
                return symb, offset, frame
            frame = frame.older()

    try:
        var = lookup_variable()
        if var:
            return var

        log.info("Should check the actual parent ...")
        with my_gdb.active_thread(my_gdb.main_thread()):
            return lookup_variable()
        
    except gdb.error as e:
        log.warn("Cannot get name for address {}: {}".format(hex(address), e))
        pass
    
omp_fct_to_job = {}

class OmpFramePragma(mcgdb.capture.FrameStripperDecorator):
    def function(self):
        fct = self.parent.inferior_frame().name()
        PREFIX = "GOMP_"
        if not fct.startswith(PREFIX):
            return fct

        return "#pragma omp {}".format(fct[len(PREFIX):])

def omp_has_bpt_set_enabled(sets):
    enabled = {}
    for setname in sets:
        try:
            enabled[setname] = OmpFunctionBreakpoint.breakpoint_sets_status[setname]
        except KeyError:
            enabled[setname] = None
    return enabled

first_run = True
def omp_bpt_set_default_enabled(name):
    global first_run
    if representation.model_centric_debugging:
        return True
    
    if first_run:
        log_user.critical("OpenMP model-centric breakpoints disabled by default.")
        log_user.critical("Consider running `omp breakpoint_sets enable all`.")
        first_run = False

    if name in ("worker", "base"): return True
    
    return False

class OmpFunctionBreakpoint(mcgdb.capture.FunctionBreakpoint):
    breakpoint_sets = defaultdict(list)
    breakpoint_sets_status = {}
    
    decorator = OmpFramePragma
    omp_set = "base"
    
    def __init__(self, spec):
        try:
            symb = gdb.lookup_symbol(spec)[0]
        except gdb.error as e:
            if not "No frame selected" in str(e):
                raise e
            
            symb = None
        
        if symb and str(symb.symtab) == "gomp_preload.c":
            spec = "gomp_preload.c:"+spec
        if symb and str(symb.symtab) == "iomp_preload.c":
            spec = "iomp_preload.c:"+spec
            
        mcgdb.capture.FunctionBreakpoint.__init__(self, spec)

        setname = self.__class__.omp_set # may be None
            
        OmpFunctionBreakpoint.breakpoint_sets[setname].append(self)
        try:
            set_enabled = OmpFunctionBreakpoint.breakpoint_sets_status[setname]
        except KeyError as e: # first item in the set
            set_enabled = omp_bpt_set_default_enabled(setname)
            OmpFunctionBreakpoint.breakpoint_sets_status[setname] = set_enabled
            
        self.enabled = set_enabled
            
class OmpGeneratedFrame(mcgdb.capture.FrameStripperDecorator):
    def __init__(self, parent):
        mcgdb.capture.FrameStripperDecorator.__init__(self, parent)

        fct = self.parent.inferior_frame().name()
        self.src_fct, _, self.uid = fct.split(".")
        
        try:
            job = omp_fct_to_job[fct] # may throw KeyError
            if hasattr(job, '__call__'):
                job = job() # may return None
        except KeyError:
            job = None

        self.job = job
        
    def function(self):
        if not self.job:
            # could not identify the job ...
            return "omp_subfunction<{}> of {}".format(self.uid, self.src_fct)
        
        
        if isinstance(self.job, representation.TaskJob):
            return "task #{}".format(self.job.number)
        else:
            classname = str(self.job.__class__.__name__)
            assert "Job" in classname
            name = classname.partition("Job")[0].lower()
            
            return "#{} zone #{} of {}".format(name, self.job.number, self.src_fct)
    
    def frame_locals(self): return self.parent.frame_locals()
    def frame_args(self): return self.parent.frame_args()
    def address(self): return self.parent.address()
    def line(self): return self.parent.line()
    def filename(self): return self.parent.filename()

def current_worker():
    return Worker.key_to_value(gdb.selected_thread())

def worker_is_alive(worker):
    return worker.thread_key and worker.thread_key.is_valid()

class FrameFilter:
    def __init__(self):
        self.enabled = True
        self.priority = 500
        
    def filter(self, frames):
        for frame in frames:
            if "._omp_" in str(frame.inferior_frame().name()):
                yield OmpGeneratedFrame(frame)
            else:
                yield frame

def get_omp_zone():
    try:
        older_frame = gdb.selected_frame().older()
        if older_frame.type() != gdb.NORMAL_FRAME:
            return
        
        sal = older_frame.find_sal()
        line = gdb.execute("l {file}:{line},{line}".format(
                file=sal.symtab.filename, line=sal.line), to_string=True).strip()
        line = line.partition("#pragma omp ")[-1]

        return line.split(" ")
    except Exception as e:
        log.info("Could not get OMP zone: {}".format(e))
        return None

attach_recognizers = {}
def attach():
    # detect workers
    known_threads = [worker.thread_key for worker in representation.Worker.list_]
    #assert len(known_threads) == 1

    all_threads = sorted(gdb.selected_inferior().threads(), key=lambda t:t.num)
    
    for thread in all_threads:
        if thread in known_threads: continue
        worker = representation.Worker(thread)
        gdb.selected_thread().name = str(worker)


    def all_frames():
        frames = []
        frame = gdb.newest_frame()
        while frame is not None:
            frames.insert(0, frame)
            frame = frame.older()
        return frames

    def has_recognizer(name):
        for where in (attach_recognizers, mcgdb.capture.FunctionBreakpoint.breakpointed):
            recognizer = where.get(name, None)
            if recognizer: return recognizer
        
    def lookup(frame):
        name = None

        try:
            # we need a function name here
            if frame.function() is None: return

            # check function name
            name = str(frame.function())
            if has_recognizer(name): return

            # check 'filename:function_name'
            # this may return the wrong file if frame filters are messed up...
            #name = "{}:{}".format(frame.function().symtab.filename.rpartition("/")[-1], name)
            name = "{}:{}".format(frame.find_sal().symtab.filename.rpartition("/")[-1], name)
            if has_recognizer(name): return

            # check pointer to function address
            try:
                name = "*"+ str(frame.function().value().address).split(" ")[0]
            except: pass
            if has_recognizer(name): return
        finally:
            # name is set to good key, or garbage if missing
            recognizer = has_recognizer(name)
            recognizer = getattr(recognizer, "recognize") if hasattr(recognizer, "recognize") \
                else None

            return recognizer
    
    for thread in all_threads:
        thread.switch()
        for frame in all_frames():
            recognizer = lookup(frame)
            if not recognizer: continue
            frame.select()
            recognizer(frame)
    all_threads[0].switch()

class Array:
    NO_LEN = True
    if NO_LEN: log.warn("Array length disabled (hardcoded)")
    
    resolvers = set()

    @staticmethod
    def matching_dependency(array):
        try:
            int(array, 16) # check if it's an address
            name = None            
        except (ValueError, TypeError):
            name = array.name.partition("[")[0]

        for an_array in Array.resolvers:
            an_array_name = an_array.name.partition("[")[0]
            if name != an_array_name:
                log.debug("{} != {}".format(name, an_array_name))
                continue
            
            if an_array.array.supersets(array):
                if array.range:
                    rng = list(range(array.range[0], array.range[1]+1))
                    for _, start, _, length in array.levels:
                        if not (start in rng and start+length in rng):
                            log.error("Found matching array but out of bound: {}{} not in {}".format(array.name, "".join([l for l in array.levels_to_str()]), array.range))
                return an_array
    
    def __init__(self, name):
        self.name = name
        self.real_name = None
        
        self.levels = []
        self.line_length = None
        self.start = None
        self.value = None
        self.range = None
        
    def add_level(self, beg_name, beg, ln_name, ln):
        if Array.NO_LEN:
            ln = 1
            
        self.levels.append([beg_name, beg, ln_name, ln])
        assert len(self.levels) <= 2
        
    def done(self):
        assert self.line_length is None

        if self.real_name is not None:
            symb, frame = self.real_name
            self.value = symb.value(frame)
        else:
            self.value = gdb.selected_frame().read_var(self.name)
            
        if self.value.is_optimized_out:
            log.warn("Cannot parse dependencies for array {}: symbol optimized out".format(self.name))
            return
        
        self.start = my_gdb.addr2num(self.value.address)
        typ = my_gdb.primitive_type(self.value.type)
        
        self.el_size = int(typ.sizeof)

        if self.levels:
            try:
                self.range = self.value.type.range()
            except RuntimeError: # This type does not have a range.
                pass
        
        if len(self.levels) == 2:
            def set_line_length():
                dist_str =  "&{name}[1][0] - &{name}[0][0]".format(name=self.name)
                self.line_length = int(gdb.parse_and_eval(dist_str))
            try:
                set_line_length()
            except gdb.error as e: # Attempt to take address of value not located in memory
                thr = gdb.selected_thread()
                frm = gdb.selected_frame()
                try:
                    gdb.execute("thread 1", to_string=True)
                    frame = my_gdb.oldest_frame()
                    while frame:
                        try:
                            frame.read_var(self.name)
                            frame.select()
                            set_line_length()
                            
                            return
                        except ValueError:
                            continue
                    log.error("Cannot get line length for {}: {}".format(self.name, e))
                finally:
                    gdb.execute("thread {}".format(thr.num), to_string=True)
                    frm.select()
        
    def supersets(self, array):
        if len(self.levels) >= 2 and self.line_length is None:
            log.warn("Array {} optimized out :( ({})".format(self.name, self))
            return

        try:
            addr = int(array, 16) # TypeError or array is a scalar address
        
            dist = (addr - self.start) / self.el_size

            pos = int(dist/self.line_length), int(dist % self.line_length)

            start = [l[1] for l in self.levels]
            length = [l[-1] for l in self.levels]

            # check if start < addr <= start+len
            return all([s <= x < s+l for s, x, l in zip(start, pos, length)])
        except TypeError:
            # array is an array

            start = [l[1] for l in self.levels]
            length = [l[-1] for l in self.levels]

            other_start = [l[1] for l in array.levels]
            other_length = [l[-1] for l in array.levels]

            class mat:
                def __init__(self, dims):
                    self.dims = dims

                def belongs(self, point):
                    for (start, length), pos in zip(self.dims, point):
                        if start <= pos < start+length:
                            continue
                        return False
                    return True

            # that's the best I can do this evening, sorry ...
            n_levels = len(self.levels)
            if n_levels == 1:
                x1, y1 = list(zip(other_start, [s+l for (s, l) in zip(other_start, other_length)]))[0]
                return mat(zip(start, length)).belongs((x1, y1))
            elif n_levels == 2:
                (x1, y1), (x2, y2) = zip(other_start, [s+l for (s, l) in zip(other_start, other_length)])
                return all(map(mat(zip(start, length)).belongs, [(x1, y1), (x1, y2), (x2, y1), (x2, y2)]))
            else:
                msg = "Array with {}dims not supported yet ...".format(n_levels)
                log.critical()
                assert not msg
                
    def levels_to_str(self, no_name=False):
        for (beg_name, beg, ln_name, ln) in self.levels:
            if no_name:
                yield ("[{}]".format(beg) if Array.NO_LEN
                       else "[{}:{}]".format(beg, ln))
                continue
            
            if str(beg) != beg_name:
                beg_name = "{}={}".format(beg_name, beg)
                
            if str(ln) != ln_name:
                ln_name = "{}={}".format(ln_name, ln)

            
            yield ("[{}]".format(beg_name) if Array.NO_LEN
                   else "[{}:{}]".format(beg_name, ln_name))

    @property
    def fullname(self):
        return self.name + "".join(self.levels_to_str())
    
    def __str__(self):            
        #return self.name + "[...]" * len(self.levels)
        return self.name + "".join(self.levels_to_str(no_name=True))
        
def lookup_array_range(dep, names):
    try:
        if "[" in dep:
            parsed = pyparsing.nestedExpr('[',']').parseString("[{}]".format(dep))[0]

            array = Array(name=parsed[0])

            if None in names:
                log.debug("Unexpected content in name lookup array: {}".format(names))
                names = [n for n in names if n is not None]
            
            matching_names = [(symb, frame) for symb, offset, frame in names
                              if symb.name.startswith(array.name+".")] # gomp specific
            assert not matching_names or len(matching_names) == 1
            if matching_names:
                array.real_name = matching_names[0]
                
            for sub in parsed[1:]:
                sub = sub[0]
                beg_name, _, ln_name = sub.partition(":")
                
                if not ln_name: ln_name = "1"

                beg, ln = map(int, map(gdb.parse_and_eval, (beg_name, ln_name)))
                
                array.add_level(beg_name, beg,
                                ln_name, ln)
            array.done()
            return array
    
        else:
            return str(gdb.parse_and_eval(dep).address)
    except KeyError as e:
        pass
    except gdb.error as e:
        log.warn("Cannot parse dependency '{}': {}".format(dep, e))
        pass
    
def parse_omp_dependencies(line, in_name, out_name, inout_name=None):
    dependencies = defaultdict(list)

    line = line.replace("untied", "")
    
    parsed_line = pyparsing.nestedExpr('(',')').parseString("({})".format(line))[0]

    names = {"in": in_name,
             "out": out_name,
             "inout": inout_name if inout_name is not None else in_name+out_name}
    
    for fct, vals in zip(parsed_line[0::2], parsed_line[1::2]):
        if fct != "depend":
            if fct not in ("private", "firstprivate", "shared"):
                log.warn("skip task attribute {} ({})".format(fct, vals))
            continue
        direct, _, deps = vals.pop(0).partition(":")
        direct = direct.strip()
        
        deps = (deps + "".join(vals)).split(",")
        
        # deps: list of dependencies
        # direct: in/inout/out
        
        for a_dep in deps:
            dependencies[direct].append(lookup_array_range(a_dep, names[direct]))
            
    return dependencies    

def inferior_call(event):
    x = gdb.execute("p/a {}".format(event.address), to_string=True)
    log.warn("Inferior call: {}".format(x[:-1].split("= ")[1]))

# def exited(event):
#     log.warn("EXITED")
    
def activate():
    representation._worker_is_alive = worker_is_alive
    representation._get_current_worker = current_worker
    representation._attach = attach
    representation._get_dependency_name = get_dependency_name
    representation._array_matching_dependency = Array.matching_dependency
    mcgdb.toggle_activate_submodules(__name__)(do_activate=True)
    gdb.frame_filters["OpenMP Subroutines"] = FrameFilter()

    # gdb.events.inferior_call.connect(inferior_call)
    # gdb.events.exited.connect(exited)
    
from . import preload

