#include <omp.h>
#include <stdio.h>

int main() {
  printf("@Base: Let's attack the death start !\n");
  printf("@Base: Releash all the starfighters.\n");
#pragma omp parallel
  {
    int id = omp_get_thread_num() + 1;

    printf("@%d On the way to the combat zone.\n", id);
#pragma omp single
    {
      printf("@%d I opened a breach in the star,\n", id);
      printf("@%d but we can only enter one by one.\n", id);
    }

#pragma omp critical
    {
      printf("@%d I'm entering the breach\n", id);
    }

    /****/
    
    printf("\n@%d Someone needs to cover us!\n", id);
    
#pragma omp single
    {
      printf("@%d I give the orders\n", id);

#pragma omp task
      {
        printf("@%d I cover your backs!\n", id);
      }
#pragma omp task
      {
        printf("@%d I cover your backs as well!\n", id);
      }
    }

#pragma omp sections
    {
#pragma omp section
      {
        printf("@%d I attack the energy field!\n", id);
      }
#pragma omp section
      {
        printf("@%d I attack the generator!\n", id);
      }
#pragma omp section
      {
        printf("@%d I follow the rabits!\n", id);
      }
    }

#pragma omp barrier
    
#pragma omp master
    {
      printf("@%d Now I attack the inner core...\n", id);
      printf("@%d ... and destroy it !\n", id);
    }
#pragma omp barrier
    
    printf("@%d We're done!!!\n", id);
  }
  printf("@Base: Everybody's back to safety, great job!\n");
}
