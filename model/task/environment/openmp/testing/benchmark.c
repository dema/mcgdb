#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>

void do_something(int it);

/***********/
// modified by debugging, do not change to macro.
int repeat = 2;

int it_count = 50;
int us_sleep_time = 0;

int it_total = 0;
volatile int something;
void (*what_to_do)(int it) = NULL;
/***********/

void finish_data_ready(void) {}

void finish(struct timeval x, struct timeval y) {
  
  //static float it_total;
  static float ms_busy_time, us_busy_once;
  static float ms_total_sleep;
  static float ms_time;
  {
    static double x_us, y_us;

    x_us = (double) x.tv_sec*1000000 + (double) x.tv_usec;
    y_us = (double) y.tv_sec*1000000 + (double) y.tv_usec;

    //it_total = repeat * it_count;
    
    ms_time = (y_us - x_us)/1000;
    ms_total_sleep = us_sleep_time*(it_total/1000.0);
    ms_busy_time = ms_time - ms_total_sleep;
    us_busy_once = ms_busy_time * 1000 / it_total;
  }

  finish_data_ready();
  
  printf("Repeat: %d; Loop: %d; usleep %dus (one) %1.fms (total)\n",
         repeat, it_count, us_sleep_time, ms_total_sleep);
  printf("------------------\n");
  printf("Total time: %1.fms\n", ms_time);
  printf("Busy time : %1.fms\n", ms_busy_time);
  printf("Busy once : %1.fus\n", us_busy_once);
}

void do_something(int it) {
  something = it;
#pragma omp atomic
  it_total++;
  if (us_sleep_time)
    usleep(us_sleep_time);
}

void do_nothing(int it) {
  something = it;
}

void do_single(int it) {
  #pragma omp single
    {
      do_nothing(it);
    }
}

void do_critical(int it) {
  #pragma omp critical
    {
      do_nothing(it);
    }
}

void do_sections(int it) {
  #pragma omp sections
    {
      #pragma omp section
      {
        do_nothing(it);
      }
      #pragma omp section
      {
        do_nothing(it+1);
      }
      #pragma omp section
      {
        do_nothing(it+2);
      }
    }
}

void do_barrier(int it) {
  #pragma omp barrier
  do_nothing(it);
}

void do_master(int it) {
  #pragma omp master
  {
    do_nothing(it);
  }
}

void do_single_task(int it) {
  #pragma omp single
    {
      #pragma omp task
      {
        //printf("Task going %d/%d.\n", it, it_count);
        do_something(it);
      }
    }
}

void benchmark(void (*action)(int)) {
  #pragma omp parallel
  {
    int i;
    for (i = 0; i < it_count; ++i) {
      do_something(i);
      action(i);
    }
  }
}

void run_once(void (*what_to_do)(int)) {
    struct timeval before , after;
    int i;
    
    benchmark(what_to_do); // warm-up
    printf("Warmup done.\n");
    
    it_total = 0;
    gettimeofday(&before , NULL);
    
    for (i = 0; i < repeat; ++i) {
      benchmark(what_to_do);
    }

    gettimeofday(&after , NULL);

    finish(before, after);
    
    return;
}


int main(int argc, char** argv) {
  if (what_to_do == NULL) {
    void (*WHAT_TO_RUN[])(int) = {do_critical, do_master, do_sections, do_barrier, do_single};
    char *NAMES[] =              {"Critical",  "Master", "Sections",   "Barrier",  "Single"};
    
    int NB_RUNS = sizeof(WHAT_TO_RUN)/sizeof(WHAT_TO_RUN[0]);
    
    for (int i = 0; i < NB_RUNS; i++) {
      printf("\n\n---- <%s> ----\n", NAMES[i]);
      run_once(WHAT_TO_RUN[i]);
      printf("---- </%s> ----\n", NAMES[i]);
    }
  } else {
    run_once(what_to_do);
  }
}
