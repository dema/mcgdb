from math import floor, ceil

import logging
log = logging.getLogger(__name__)

NO_PRINT = True
DEFAULT_TERM_WIDTH = 80

half_floor = lambda f : int(floor(float(f)/2))
half_ceil = lambda f : int(ceil(float(f)/2))

term_width = None

def terminal_size():
    try:
        import fcntl, termios, struct # standard lib packages
        h, w, hp, wp = struct.unpack('HHHH',
                                     fcntl.ioctl(0, termios.TIOCGWINSZ,
                                                 struct.pack('HHHH', 0, 0, 0, 0)))
        return w, h
    except Exception as e:
        log.warning("Cannot retrieve terminal width, defaulting to {}.".format(DEFAULT_TERM_WIDTH))
        return DEFAULT_TERM_WIDTH

def initialize():
    
    #if NO_PRINT: return
    
    global term_width
    term_width = terminal_size()[0]
    

def say(what=None, who=None, count=None):
    if NO_PRINT:
        return
    what = str(what) if what else ""
    if count is None:
        if not NO_PRINT:
            what = " <{}> ".format(what) if what else ""
            
            to_fill = term_width - len(what)
        
            print ("-"*half_floor(to_fill) + what + "-"*half_ceil(to_fill))
    else:
        what = what.strip()
        what, _, next_print = what.partition("\n")
        if next_print:
            next_print = "\n"+next_print
            
        spc_per_thr = int(floor(float(term_width) / count) - 1)
        
        empty_block = " "*spc_per_thr + "|"
        
        before_blocks = empty_block * (who - 1)

        what_free_size = spc_per_thr - len(what)

        while what_free_size < 0:
            last_spc = what.rindex(" ") if " " in what else spc_per_thr
            if last_spc > spc_per_thr: last_spc = spc_per_thr
            
            next_print = what[last_spc:] + next_print
            what = what[:last_spc]
            what_free_size = spc_per_thr - len(what)
            
        what_block = half_floor(what_free_size)*" " + what + half_ceil(what_free_size)*" " + "|"

        after_block = empty_block * (count - who)

        if not NO_PRINT:
            print("|{}{}{}".format(before_blocks, what_block, after_block))
        
        if next_print:
            say(next_print, who, count)
