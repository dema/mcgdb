import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

import gdb

import mcgdb
from . import representation, toolbox, interaction, capture

def activate():
    gdb.execute("set print thread-events off")
    capture.preload.prepare()
    gdb.execute("mcgdb autodetect_models")
    
def initialize():
    log_user.debug("OpenMP environments registered")

    mcgdb.register_model("OpenMP", "omp", mcgdb.toggle_activate_submodules(__name__), binname=True)
