/* Original from Andi Drebes, University of Manchester.  */

#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <numa.h>
#define __USE_GNU
#include <dlfcn.h>
  
int alloc_system = 1;

void* alloc_ptr = NULL;
size_t alloc_free = (2UL << 32UL);
size_t alloc_offs = 0;

size_t alloc_alignment = 0;
int alloc_interleaving = 0;
int alloc_hugepages = 0;
int alloc_touch = 0;
int alloc_verbose = 0;
int alloc_print_config = 0;

int alloc_params_set_from_dbg = 0;

static void *(*real_malloc)(size_t sz) = NULL;


static void print_alloc_params(void);
static void set_alloc_params_from_env(void);
void* malloc(size_t sz);

void preload_init_malloc (void) __attribute__((constructor));

void preload_init_malloc (void) {
  if (real_malloc != NULL) {
    return; // already initialized
  }
  
  real_malloc = dlsym(RTLD_NEXT, "malloc");
  if (NULL == real_malloc) {
    fprintf(stderr, "Error in `dlsym`: %s\n", dlerror());
  }

  if (!alloc_params_set_from_dbg) {
    set_alloc_params_from_env();
  }
  if (1 || alloc_print_config) {
    print_alloc_params();
  }
}

static void print_alloc_params(void) {
  printf("Allocator configuration:\n");
  if (alloc_system) {
    printf("  Use the system malloc\n");
  } else {
    printf("  Alignment: %zu\n", alloc_alignment);
    printf("  Interleaving: %s\n", alloc_interleaving ? "YES" : "NO");
    printf("  Use huge pages: %s\n", alloc_hugepages ? "YES" : "NO");
    printf("  Touch blocks: %s\n", alloc_touch ? "YES" : "NO");
  }
}

static void set_alloc_params_from_env(void) {
  const char* tmp;

  if ((tmp = getenv("ALLOC_SYSTEM")))
    alloc_system = atoi(tmp);
  
  if ((tmp = getenv("ALLOC_ALIGNMENT")))
    alloc_alignment = atoi(tmp);

  if ((tmp = getenv("ALLOC_INTERLEAVING")))
    alloc_interleaving = atoi(tmp);

  if ((tmp = getenv("ALLOC_INTERLEAVED")))
    alloc_interleaving = atoi(tmp);

  if ((tmp = getenv("ALLOC_HUGEPAGES")))
    alloc_hugepages = atoi(tmp);

  if ((tmp = getenv("ALLOC_TOUCH")))
    alloc_touch = atoi(tmp);

  if ((tmp = getenv("ALLOC_VERBOSE")))
    alloc_verbose = atoi(tmp);

  if((tmp = getenv("ALLOC_PRINTCONFIG")))
    alloc_print_config = atoi(tmp);
}

static size_t mem_allocated = 0;
static size_t nb_calls = 0;

void *malloc(size_t sz) {
  if (real_malloc == NULL) {
    preload_init_malloc();
  }
  if (alloc_system) {
    return real_malloc(sz);
  }

  mem_allocated += sz;
  nb_calls += 1;
  
  size_t padding = 0;
  void* ret;

  if (alloc_alignment) {
    if ((((long)alloc_ptr) + alloc_offs) % alloc_alignment) {
      padding = alloc_alignment - ((((long)alloc_ptr) + alloc_offs) % alloc_alignment);
    }
  }

  if(!alloc_ptr) {
    if (alloc_interleaving) {
      alloc_ptr = numa_alloc_interleaved(alloc_free);
      
    } else {
      alloc_ptr = real_malloc(alloc_free);
    }
    
    if (alloc_hugepages) {
      if (madvise(alloc_ptr, alloc_free, MADV_HUGEPAGE)) {
	fprintf(stderr, "madvise failed\n");
	exit(1);
      }
    }
  }

  if (alloc_free < sz + padding) {
    fprintf(stderr, "Not enough space\n");
    exit(1);
  }

  ret = (void*)(((long) alloc_ptr)+alloc_offs+padding);
  alloc_free -= sz + padding;
  alloc_offs += sz + padding;

  if (alloc_touch) {
    ((char*)ret)[0] = 0;
  }
  
  if (alloc_verbose) {
    printf("Returning %p (sz = %zu, alignment = %zu, padding = %zu, ret %% alignment = %zu)\n",
           ret, sz, alloc_alignment, padding, (size_t)(((long)alloc_ptr) % alloc_alignment));
  }
  
  return ret;
}
