#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <numaif.h>
#include <string.h>

void usage(void)
{
	fprintf(stderr, "usage: movepage pid to-node addr len\n");
	exit(1);
}

int main(int argc, char *argv[]) {

  if (argc < 4 || argc > 5) {
    usage();
  }

  int pid = atoi(argv[1]);
  int to_node_start, to_node_end, node_inc;
  char *to_node_arg = argv[2];

  char *is_node_range = strchr(to_node_arg, '-');
  if (is_node_range == NULL) {
    // dest node is a node id
    to_node_start = atoi(to_node_arg);
    to_node_end = to_node_start;
    node_inc = 0;
  } else {
    // dest node is a range of node ids
    is_node_range[0] = '\0'; // end first half here
    is_node_range++; // second half starts after the -

    to_node_start = atoi(to_node_arg);
    
    char *has_node_inc = strchr(is_node_range, '@');
    if (has_node_inc != NULL) {
      has_node_inc[0] = '\0';
      has_node_inc++;

      node_inc = atoi(has_node_inc);
    } else {
      node_inc = 1;
    }
    
    to_node_end = atoi(is_node_range);
  }

  void *addr = (void *) strtol(argv[3], NULL, 0);
  unsigned long len = argc > 4 ? strtol(argv[4], NULL, 0) : 1; // default len: only one page
  void *addr_end = addr+len;
  int ret;
  
  long int PAGESIZE = sysconf(_SC_PAGESIZE) /*(long int) getpagesize()*/;
  
  void *page = (void *)((long int)addr & ~(PAGESIZE-1));
  unsigned long nb_pages = (addr_end - page) / PAGESIZE + 1;
  printf("PID %d: move %p .. %p to nodes [%d:%d:%d] (%lu pages)\n", pid, addr, addr_end, to_node_start, to_node_end, node_inc, nb_pages);
  
  void **pages = malloc(sizeof(*pages) * nb_pages);
  int *nodes = malloc(sizeof(*pages) * nb_pages);
  int *status = malloc(sizeof(*status) * nb_pages);
  
  unsigned long i = 0;
  int current_node = to_node_start;

  while (page < addr_end) {
    if (i >= nb_pages) {
      fprintf(stderr, "error in page splitting ...\n");
      exit(2);
    }
  
    pages[i] = page;
    nodes[i] = current_node;
    
    if (to_node_start != to_node_end) {
      current_node += node_inc;
      if (current_node > to_node_end) {
        current_node = to_node_start;
      }
    }
    
    page += PAGESIZE;
    i += 1;
  }
      
  ret = move_pages(pid, nb_pages, pages, nodes, status, MPOL_MF_MOVE);
  
  if (ret) {
    perror("move_pages error");
  }

  int *on_nodes = malloc(sizeof(int) * nb_pages);

  ret = move_pages(pid, nb_pages, pages, NULL, on_nodes, 0);
  if (ret) {
    perror("move_pages check error");
  }

  unsigned long err = 0;
  unsigned long unmapped = 0;
#define UNMAPPED_PAGE -2
  for (size_t i = 0; i < nb_pages - 1; i++) {
    if (on_nodes[i] != nodes[i] && on_nodes[i] != UNMAPPED_PAGE) {
      fprintf(stderr, "Page %zu (%p) should be on node %d, but is on node %d\n", i, pages[i], nodes[i], on_nodes[i]);
      if (!ret) {
        ret = -1;
      }
      err++;
    } else if (on_nodes[i] == UNMAPPED_PAGE) {
      unmapped++;
    }
  }
  
  if (err > 0) {
    fprintf(stderr, "move_page failed to move %lu over %lu pages...\n", err, nb_pages);
  }

  if (unmapped > 0) {
    fprintf(stderr, "move_page failed to move %lu over %lu pages because they were unmapped...\n", unmapped, nb_pages);
  }
  
  return -ret;
}

