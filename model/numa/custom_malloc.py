import os
import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

import gdb

import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb

THIS_FILE_PATH = os.path.realpath(__file__.rpartition("/")[0])+"/"

CUSTOM_MALLOC_LOCATION = "__binaries__/libmcgdb_malloc.preload.so"
GDB_PRELOAD = "set env LD_PRELOAD="

class cmd_numa_malloc_preload(gdb.Command):
    def __init__ (self):
        gdb.Command.__init__ (self, "numa custom_malloc preload", gdb.COMMAND_NONE)
        
    def invoke (self, args, from_tty):
        path = THIS_FILE_PATH+CUSTOM_MALLOC_LOCATION
        if not os.path.exists(path):
            log.error("Custom malloc library doesn't exist at {} ...".format(path))
            return
        
        gdb.execute(GDB_PRELOAD+path)
        log.warn("Custom malloc library will be preloaded *on the next starts*.")


class cmd_numa_malloc_start(gdb.Command):
    def __init__ (self):
        gdb.Command.__init__ (self, "numa custom_malloc start", gdb.COMMAND_NONE)

    def invoke (self, args, from_tty):
        gdb.execute("set var alloc_system = 0")
        gdb.execute("set var nb_calls = 0")
        gdb.execute("set var mem_allocated = 0")

        OPT={"align": "alloc_alignment",
             "inter": "alloc_interleaving",
             "huge": "alloc_hugepages",
             "touch": "alloc_touch",
             "verbose": "alloc_verbose"}
        
        if "help" in args:
            log_user.info("Options are: {}".format(", ".join(OPT.keys())))
            return
        
        for opt_name, var_name in OPT.items():
            pref, found, rst = args.partition(opt_name)
            if not found: continue
            
            val = int("0"+rst.split(" ")[0][1:]) if rst.startswith("=") else 1

            gdb.execute("set var {} = {}".format(OPT[opt_name], val))
                
        
        log_user.info("Custom malloc enabled")
        gdb.parse_and_eval("print_alloc_params")()
        
class cmd_numa_malloc_stop(gdb.Command):
    def __init__ (self):
        gdb.Command.__init__ (self, "numa custom_malloc stop", gdb.COMMAND_NONE)
        
    def invoke (self, args, from_tty):
        gdb.execute("set var alloc_system = 1")
        log_user.info("Custom malloc disabled")
        gdb.parse_and_eval("print_alloc_params")()
        log_user.info("Memory allocated: {}b".format(gdb.parse_and_eval("mem_allocated")))
        log_user.info("Number of calls to malloc: {}".format(gdb.parse_and_eval("nb_calls")))

        gdb.execute("set var nb_calls = 0")
        gdb.execute("set var mem_allocated = 0")
        
def initialize():
    gdb.Command("numa custom_malloc", gdb.COMMAND_NONE, prefix=True)
    cmd_numa_malloc_preload()
    cmd_numa_malloc_start()
    cmd_numa_malloc_stop()
