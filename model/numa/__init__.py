import gdb
import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb

import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user.numa")


from . import core_node, mapped, move


class cmd_numa(gdb.Command):
    def __init__(self, limited=False):
        gdb.Command.__init__ (self, "numa", gdb.COMMAND_NONE, prefix=True)
        self.numa_loaded = None
        
    def invoke (self, args, from_tty):        
        if self.numa_loaded is not None:
            log_user.info("Numa module already loaded.")
            return

        on_activate()
        self.numa_loaded = True
        log_user.info("Numa module successfully loaded.")
        
        if args:
            gdb.execute("numa {}".format(args))

            
def on_activate():
    core_node.on_activate()
    move.on_activate()
    mapped.on_activate()
    
    
def initialize():
    cmd_numa()


