import gdb
import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb

import os
import subprocess

from collections import defaultdict
import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user.numa")

try:
    import shutil
except ImportError:
    shutil = None

from . import move
    
THIS_FILE_PATH = os.path.realpath(__file__.rpartition("/")[0])+"/"
    
PAGEMAP = "pagemap"
THIS_FILE_PATH = os.path.realpath(__file__.rpartition("/")[0])+"/"
IS_MAPPED = THIS_FILE_PATH + "__binaries__/is_mapped"

pagemap_bin = None

class cmd_numa_pagemap(gdb.Command):
    def __init__ (self):
        gdb.Command.__init__ (self, "numa pagemap", gdb.COMMAND_NONE)
        global pagemap_bin

        has_pagepage = False
        try:
            has_pagepage = shutil.which(PAGEMAP) is not None  # > py3.3
        except AttributeError: # py2: shutil doesn't have which
            pass

        if not has_pagepage:
            try:
                has_pagepage = "/{}".format(PAGEMAP) in subprocess.check_output("which {}".format(PAGEMAP), shell=True, stderr=subprocess.STDOUT)
            except subprocess.CalledProcessError: # which returned false
                pass
                
        if not has_pagepage:
            pagemap_path = os.path.join(THIS_FILE_PATH, PAGEMAP)
            
            if not os.path.isfile(pagemap_path):
                log_user.error("Cannot find `{}` on PATH nor in `{}`".format(PAGEMAP, PAGEMAP_PATH))
                log_user.error("pagemap command won't certainly work.")
            else:
                pagemap_bin = pagemap_path
                
    def invoke (self, args, from_tty):
        print_raw = False
        if "-raw" in args:
            print_raw = True
            args = args.replace("-raw", "")
            
        try:
            addr = gdb.parse_and_eval(args)
        except gdb.error as e:
            log_user.error("Cannot evaluate '{}': {}".format(args, e))
            return
        
        cmd = "{} -n {} {}".format(pagemap_bin, gdb.selected_inferior().pid, hex(my_gdb.addr2num(addr)))
        res = subprocess.check_output(cmd, shell=True)
        try:
            pm_addr, _, node = res[:-1].split(" ")
            if print_raw:
                print(node[1:])
            else:
                log_user.info("Address 0x{} is located on node {}".format(pm_addr, node))
        except ValueError: # couldn't split correctly
            if not res[:-1]:
                if print_raw:
                    print(-2)
                else:
                    log_user.warn("Pagemap chose not to answer ... (page not mapped yet?)")
            else:
                if print_raw:
                    print(-1)
                else:
                    log_user.warn("Unexpected response from pagemap: '{}'".format(res[:-1]))

class cmd_numa_is_3d_matrix_mapped(gdb.Command):
    def __init__ (self):
        gdb.Command.__init__ (self, "numa is_3d_matrix_mapped", gdb.COMMAND_NONE)
        
        self.pages_mapped = None
        self.pages_not_mapped = None
        self.mapping = None
        
    def do_check_mapping(self, current_start, current_end, errors=None):
        length = current_end-current_start
        import math
        nb_pages = math.ceil(length / move.PAGESIZE)
        is_mapped_str = gdb.execute("numa is_mapped {} {}".format(hex(current_start), length), to_string=True)
        print(is_mapped_str)

        for line in is_mapped_str.split("\n"):
            if "is mapped on node" in line:
                self.pages_mapped += 1
                node = line.rpartition(" ")[-1]
            elif "is not mapped" in line:
                self.pages_not_mapped += 1
                node = None
            else:
                continue
            
            self.mapping[node] += 1
            
    def invoke (self, args, from_tty):
        #numa is_3d_matrix_mapped r x y z sizeof(double)
        cls = move.cmd_numa_move_3d_matrix
        try:
            mat_name, mat, x, y, z, elt_size = cls.parse_and_eval_common_params(*args.split(" "))
        except Exception as e:
            log_user.error("Expected parameters: matrix_name x_size y_size z_size elt_size (mat[x][y][z]): {}".format(e))
            return

        self.mapping = defaultdict(int)
        self.pages_mapped = 0
        self.pages_not_mapped = 0
        
        cls.do_invoke(mat_name, mat, x, y, z, elt_size, self.do_check_mapping)
        import pdb;pdb.set_trace()
        pct_mapped = float(self.pages_mapped) / (self.pages_mapped+self.pages_not_mapped)
        log_user.info("{:.2f}% of the pages are mapped".format(pct_mapped*100))
        for key in sorted(self.mapping.keys()):
            val = self.mapping[key]
            if key is None:
                msg = "Not mapped: {}".format(val)
            else:
                pct_on_node = val / self.pages_mapped
                msg = "Node {}: {} ({:.2f}%)".format(key, val, pct_on_node*100)
            
            log_user.info(msg)

        
class cmd_numa_is_mapped(gdb.Command):
    def __init__ (self):
        gdb.Command.__init__ (self, "numa is_mapped", gdb.COMMAND_NONE)
        
    def invoke (self, args, from_tty):
        print_raw = False
        
        if "-raw" in args:
            print_raw = True
            args = args.replace("-raw", "")
            
        try:
            if " " in args:
                addr, length = args.split(" ")
                length = my_gdb.addr2num(gdb.parse_and_eval(length))
            else:
                addr = args
                length = 1
            addr = my_gdb.addr2num(gdb.parse_and_eval(addr))
        except gdb.error as e:
            log_user.error("Cannot evaluate '{}': {}".format(args, e))
            return

        cmd = "{} {} {}".format(IS_MAPPED, gdb.selected_inferior().pid,
                                hex(addr), hex(length))
        try:
            res = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
            if print_raw:
                res = (res.replace("Page ", "")
                          .replace("is mapped on node ", "=> N")
                          .replace("is not mapped ", "=>"))
            print(res[:-1])
                
        except subprocess.CalledProcessError as e:
            log.error("{} returned error code {}.".format(cmd, e.returncode))
            error = str(e.output)[:-1]
            log.warn(error)
        

def on_activate():
    cmd_numa_pagemap()
    cmd_numa_is_mapped()
    
    cmd_numa_is_3d_matrix_mapped()
    
