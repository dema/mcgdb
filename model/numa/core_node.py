import gdb
import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb

import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user.numa")


class cmd_numa_current_core(gdb.Command):
    def __init__ (self):
        gdb.Command.__init__ (self, "numa current_core", gdb.COMMAND_NONE)

    def invoke (self, args, from_tty):
        gdb.execute("numa current_node {} -core".format(args))

        
class cmd_numa_node_cores(gdb.Command):
    def __init__ (self):
        gdb.Command.__init__ (self, "numa node_cores", gdb.COMMAND_NONE)

    def invoke (self, args, from_tty):
        try:
            target_node = int(args)
        except Exception as e:
            log_user.warn("Expected a node id as parameter ({})".format(e))
            return

        log_user.info("Core on node {}:".format(target_node))
        for cpu, node in cmd_numa_current_node.singleton.cpu_to_node.items():
            if node == target_node:
                log_user.info("{}".format(cpu))

                
class cmd_numa_core_node(gdb.Command):
    def __init__ (self):
        gdb.Command.__init__ (self, "numa core_node", gdb.COMMAND_NONE)

    def invoke (self, args, from_tty):
        try:
            target_core = int(args)
        except Exception as e:
            log_user.warn("Expected a core id as parameter ({})".format(e))
            return
        node = cmd_numa_current_node.singleton.cpu_to_node.get(target_core, "<not found>")
        log_user.info("Core {} is on node {}.".format(target_core, node))

        
class cmd_numa_current_node(gdb.Command):
    singleton = None
    
    def __init__ (self):
        gdb.Command.__init__ (self, "numa current_node", gdb.COMMAND_NONE)
        self.cpu_to_node = {}
        cmd_numa_current_node.singleton = self
        
        with open("/proc/cpuinfo") as cpuinfo_f:
            last_cpu = None
            for line in cpuinfo_f.readlines():
                key, _, val = line[:-1].partition(":")
                if key.strip() == "processor":
                    last_cpu = int(val.strip())
                elif key.strip() == "physical id":
                    assert last_cpu is not None
                    self.cpu_to_node[last_cpu] = int(val.strip())
                    last_cpu = None
                else:
                    pass

        log_user.info("numa: found {} CPUs over {} processors.".format(len(self.cpu_to_node), len(set(self.cpu_to_node.values()))))
        
    def invoke (self, args, from_tty):
        if not gdb.selected_thread():
            log_user.warn("No thread currently selected ...")
            return

        ptid = gdb.selected_thread().ptid
        with open("/proc/{}/task/{}/status".format(ptid[0], ptid[1])) as task_status_f:
            for line in task_status_f.readlines():
                key, _, val = line[:-1].partition(":")
                if key.strip() == "Cpus_allowed_list":
                    break
            else:
                if "-raw" in args:
                    print(-1)
                else:
                    log_user.warn("Couldn't find Cpus_allowed_list ... is you Linux up-to-date?")
                    return
                
        allowed_cpus = val.strip()
        if "-" in allowed_cpus:
            if "-raw" in args:
                # not handled yet ...
                print(-1)
            else:
                log_user.warn("Thread #{} is bound to CPUs {}.".format(int(gdb.selected_thread().num), allowed_cpus))
        else:
            if "-core" in args:
                if "-raw" in args:
                    print(allowed_cpus)
                else:
                    log_user.warn("Thread #{} is bound to node cpu {}.".format(int(gdb.selected_thread().num), allowed_cpus))
            else:
               bound_to_node = self.cpu_to_node[int(allowed_cpus)]
               if "-raw" in args:
                   print(bound_to_node)
               else:
                   log_user.warn("Thread #{} is bound to node N{}, cpu {}.".format(int(gdb.selected_thread().num), bound_to_node, allowed_cpus))

class cmd_numa_current_node_by_call(gdb.Command):
    def __init__ (self):
        gdb.Command.__init__ (self, "numa current_node_by_call", gdb.COMMAND_NONE)
        
    def invoke (self, args, from_tty):
        for symb in ("numa_node_of_cpu", "sched_getcpu"):
            if not gdb.lookup_symbol(symb):
                log_user.error("Symbol '{}' not found, cannot get current node.".format(symb))
                return
        
        with my_gdb.set_parameter("scheduler-locking", "on"):
            node = gdb.execute("p numa_node_of_cpu(sched_getcpu())", to_string=True)
        node = int(node.partition("= ")[-1])

        if "-raw" in args:
            print(node)
        else:
            log_user.info("Current node is N{}".format(node))

def on_activate():
    cmd_numa_current_node()
    cmd_numa_current_core()
    
    cmd_numa_current_node_by_call()
    cmd_numa_node_cores()
    cmd_numa_core_node()
