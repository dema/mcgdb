#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <numaif.h>
#include <string.h>

void usage(void)
{
	fprintf(stderr, "usage: is_mapped pid addr [len]\n");
	exit(1);
}

int main(int argc, char *argv[]) {

  if (argc < 3 || argc > 5) {
    usage();
  }

  int pid = atoi(argv[1]);
  void *addr = (void *) strtol(argv[2], NULL, 0);
  
  unsigned long len = argc > 3 ? strtol(argv[3], NULL, 0) : 1; // default len: only one page
  void *addr_end = addr+len;
  int ret;
  //printf("PID %d: is mapped addr %p .. %p?\n", pid, addr, addr_end);
  
  long int PAGESIZE = sysconf(_SC_PAGESIZE) /*(long int) getpagesize()*/;
  
  void *page = (void *)((long int)addr & ~(PAGESIZE-1));
  unsigned long nb_pages = (addr_end - page) / PAGESIZE + 1;
  printf("PID %d: is mapped page %p .. %p (%lu pages)?\n", pid, page, page + ((nb_pages+1)*PAGESIZE)-1, nb_pages);
  
  void **pages = malloc(sizeof(*pages) * nb_pages);
  int *status = malloc(sizeof(*status) * nb_pages);
  int *on_nodes = malloc(sizeof(int) * nb_pages);

  
  for(unsigned long i = 0; page < addr_end; i++) {
    if (i >= nb_pages) {
      fprintf(stderr, "error in page splitting ...\n");
      exit(2);
    }
  
    pages[i] = page;
    page += PAGESIZE;
  }

  ret = move_pages(pid, nb_pages, pages, NULL, on_nodes, 0);
  if (ret) {
    perror("move_pages check error");
  }

#define UNMAPPED_PAGE -2
  for (size_t i = 0; i < nb_pages; i++) {
    if (on_nodes[i] == UNMAPPED_PAGE) {
      printf("Page %p is not mapped\n", pages[i]);
    } else {
      printf("Page %p is mapped on node %d\n", pages[i], on_nodes[i]);
    }
  }
  
  return -ret;
}

