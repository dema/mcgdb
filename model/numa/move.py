import gdb
import mcgdb
from mcgdb.toolbox.target import my_archi
from mcgdb.toolbox import my_gdb

import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user.numa")

import os
import subprocess


PAGESIZE = 4096
THIS_FILE_PATH = os.path.realpath(__file__.rpartition("/")[0])+"/"
MOVE_PAGE = THIS_FILE_PATH + "__binaries__/move_page"

class cmd_numa_movepage(gdb.Command):
    def __init__ (self):
        gdb.Command.__init__ (self, "numa move_page", gdb.COMMAND_NONE)
        
    def invoke (self, args, from_tty):
        addr, length, dest_node = args.split(" ")

        length = int(length)
        dest_node = int(dest_node)

        try:
            addr = my_gdb.addr2num(gdb.parse_and_eval(addr))
        except gdb.error as e:
            log_user.error("Cannot evaluate '{}': {}".format(addr, e))
            return
        
        cmd = " ".join(map(str, [MOVE_PAGE, gdb.selected_inferior().pid, dest_node, hex(addr), length]))

        try:
            log_user.warn("Process {} ({}b) to node {}".format(hex(addr), length, dest_node))
            res = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
            log_user.info(str(res)[:-1])
        except subprocess.CalledProcessError as e:
            log_user.error("{} returned error code {}.".format(cmd, e.returncode))
            error = str(e.output)[:-1]
            log_user.warn(error)

            
class cmd_numa_move_3d_matrix(gdb.Command):
    def __init__ (self):
        gdb.Command.__init__ (self, "numa move_3D_matrix", gdb.COMMAND_NONE)
        
    def invoke (self, args, from_tty):
        #numa move_3D_matrix r x y z sizeof(double) dest
        mat_name, x, y, z, elt_size, dest_node = args.split(" ")
        try:
            argl = args.split(" ")
            dest_node = argl.pop()
            mat_name, mat, x, y, z, elt_size = self.__class__.parse_and_eval_common_params(*argl)
            dest_node = int(dest_node)
        except Exception as e:
            log_user.error("Expected parameters: matrix_name x_size y_size z_size elt_size (mat[x][y][z]): {}".format(e))
            return
        
        def do_move_page(current_start, current_end, errors=None):
            if errors is None:
                errors = []
            assert current_start is not None and current_end is not None
            
            length = current_end - current_start
            cmd = " ".join(map(str, [MOVE_PAGE, gdb.selected_inferior().pid, dest_node, hex(int(current_start)), length]))
            nb_pages = int(length / PAGESIZE) + 1
        
            try:
                log_user.warn("{} # {} pages".format(cmd, nb_pages))
                res = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
                log_user.info(str(res)[:-1])
            except subprocess.CalledProcessError as e:
                log.error("{} returned error code {}.".format(cmd, e.returncode))
                errors.append(str(e.output)[:-1])
                log.warn(errors[-1])
            return errors
        
        self.__class__.do_invoke(mat_name, mat, x, y, z, elt_size, do_move_page)
        
    @staticmethod
    def parse_and_eval_common_params(mat_name, x, y, z, elt_size):
        mat = gdb.parse_and_eval(mat_name)
        x = int(gdb.parse_and_eval(x))
        y = int(gdb.parse_and_eval(y))
        z = int(gdb.parse_and_eval(z))
        elt_size = int(gdb.parse_and_eval(elt_size))

        return mat_name, mat, x, y, z, elt_size

    @staticmethod
    def do_invoke(mat_name, mat, x, y, z, elt_size, do_on_pages):
        z_length = z*elt_size

        current_start = None
        current_end = None
        errors = []

        row_x = None
        col_y = None
        
        for row_x in range(x):
            log_user.info("Process {}[{}]".format(mat_name, row_x))
            for col_y in range(y):
                
                start_addr = my_gdb.addr2num(mat[row_x][col_y][0].address)

                page_start = start_addr & ~(PAGESIZE-1)
                page_end = ((page_start + z_length) & ~(PAGESIZE-1)) + PAGESIZE-1
                
                if current_start is None:
                    current_start = page_start
                    current_end = page_end
                    
                if page_start > current_end:
                    
                    log_user.warn("Process until {}[{}][{}]".format(mat_name, row_x, col_y))

                    # need to flush
                    do_on_pages(current_start, current_end, errors)

                    current_start = page_start
                    current_end = page_end
                else:
                    assert page_end >= current_end
                    current_end = page_end

        log_user.warn("Process until the end {}[{}][{}]".format(mat_name, row_x, col_y))
        do_on_pages(current_start, current_end, errors)

        if errors:
            log_user.warn("{} errors happend during the page processing:".format(len(errors)))
            for err in errors:
                log_user.info(err)
                log_user.info("---")

                

class cmd_numa_move_3d_matrix_spread(gdb.Command):
    pages_move = 0
    
    def __init__ (self):
        gdb.Command.__init__ (self, "numa move_3D_matrix_spread", gdb.COMMAND_NONE)
        
    def invoke (self, args, from_tty):
        try:
            mat_name, x, y, z, elt_size = args.split(" ")
            mat = gdb.parse_and_eval(mat_name)
            x = int(gdb.parse_and_eval(x))
            y = int(gdb.parse_and_eval(y))
            z = int(gdb.parse_and_eval(z))
            elt_size = int(gdb.parse_and_eval(elt_size))
        except Exception as e:
            log_user.error("Expected parameters: matrice_name x_size y_size z_size elt_size # (mat[x][y][z]): {}".format(e))
            import pdb;pdb.set_trace()

            return
        
        z_length = z*elt_size

        current_start = None
        current_end = None
        current_start_idx = [0, 0]
        
        errors = []
        
        def do_move_page(dest_node):
            assert current_start is not None and current_end is not None
            
            length = current_end - current_start
            cmd = " ".join(map(str, [MOVE_PAGE, gdb.selected_inferior().pid, dest_node, hex(int(current_start)), length]))
            nb_pages = int(length / PAGESIZE) + 1
            
            cmd_numa_move_3d_matrix_spread.pages_move += nb_pages
            try:
                res = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
                log_user.warn(str(res)[:-1])
            except subprocess.CalledProcessError as e:
                log.error("{} returned error code {}.".format(cmd, e.returncode))
                errors.append(str(e.output)[:-1])
                log.warn(errors[-1])

        cpu_to_node = cmd_numa_current_node.singleton.cpu_to_node
        NB_NODE = max(cpu_to_node.values()) + 1
        CORE_PER_NODE = len([i for i in cpu_to_node.values() if i == 0])
        NB_CORE = NB_NODE * CORE_PER_NODE

        import math # x = 514
        chunk_size = math.ceil(float(x) / NB_CORE) # 3
        SECOND_HALF = x % NB_CORE # 130
        current_core = 0
        current_core_load = 0
        
        prev_node = None
        for row_x in range(x):
            dest_node = cpu_to_node[current_core]
            
            if prev_node is None:
                prev_node = dest_node
                
            current_core_load += 1
            if current_core_load >= chunk_size:
                current_core += 1
                current_core %= NB_CORE
                current_core_load = 0

                if current_core == SECOND_HALF:
                    chunk_size = math.ceil((x - row_x) / (NB_CORE - current_core))

            if dest_node != prev_node and prev_node is not None:
                log_user.info("")
                log_user.info("Node break")
                log_user.info("Move {}[{}][{}] -- {}[{}][{}] to {}".format(mat_name, current_start_idx[0], current_start_idx[1],
                                                                                        mat_name, row_x-1, y-1, dest_node))
                log_user.info("")

                do_move_page(prev_node)
                prev_node = dest_node
            
                current_start = None
                
            #log_user.info("Process {}[{}] to {}".format(mat_name, row_x, dest_node))
            
            for col_y in range(y):
                start_addr = my_gdb.addr2num(mat[row_x][col_y][0].address)

                page_start = start_addr & ~(PAGESIZE-1)
                page_end = ((page_start + z_length) & ~(PAGESIZE-1)) + PAGESIZE-1
                
                if current_start is None:
                    current_start_idx = row_x, col_y
                    
                    current_start = page_start
                    current_end = page_end
                    
                if False and page_start > current_end+1:
                    target_x = row_x
                    target_y = col_y
                    if target_y == 0:
                        target_x -=1
                        target_y = y - 1
                    else:
                        target_y -= 1
                        
                    log_user.info("Move {}[{}][{}] -- {}[{}][{}] to {} (page break)".format(mat_name, current_start_idx[0], current_start_idx[1],
                                                                                            mat_name, target_x, target_y, dest_node))

                    # need to flush
                    do_move_page(dest_node)

                    current_start_idx = row_x, col_y
                    current_start = page_start
                    current_end = page_end
                else:
                    assert page_end >= current_end
                    current_end = page_end

        log_user.info("Move {}[{}][{}] -- {}[{}][{}] to {} (end-of-matrix)".format(mat_name, current_start_idx[0], current_start_idx[1],
                                                                               mat_name, row_x, col_y, dest_node))
        do_move_page(dest_node)
        
        if errors:
            log_user.warn("{} errors happend during the page moves:".format(len(errors)))
            for err in errors:
                log_user.info(err)
                log_user.info("---")
            import pdb;pdb.set_trace()
            pass

class cmd_numa_pages_moved(gdb.Command):
    def __init__ (self):
        gdb.Command.__init__ (self, "numa pages_moved", gdb.COMMAND_NONE)
        
    def invoke (self, args, from_tty):
        pages = cmd_numa_move_3d_matrix_spread.pages_move
        log_user.info("{} page moved, or {}".format(pages, size_fmt(pages*PAGESIZE)))


def size_fmt(num, suffix='B'): # from SO, could be improved ...
    for unit in ['','Ki','Mi','Gi','Ti','Pi','Ei','Zi']:
        if abs(num) < 1024.0:
            return "%.0f%s %s" % (num, unit, suffix)
        num /= 1024.0
    return "%.0f%s%s" % (num, 'Yi', suffix)
            
class cmd_numa_spread_pages(gdb.Command):
    def __init__ (self):
        gdb.Command.__init__ (self, "numa spread_pages", gdb.COMMAND_NONE)
        
    def invoke (self, args, from_tty):
        if gdb.selected_inferior().pid == 0:
            log_user.error("No PID for this inferior. Is it running?")
            return

        node_inc = int(args) if args else 1
        
        with open("/proc/{}/maps".format(gdb.selected_inferior().pid)) as fmap:
            # search for "00601000-00622000 rw-p 00000000 00:00 0  [heap]"
            for line in fmap.readlines():
                if "heap" in line: break
                
        start, stop = line.split(" ")[0].split("-")
        start, stop = int(start, 16), int(stop, 16)
        length = stop - start
        size = size_fmt(length)
        
        log_user.info("Process heap goes from {} to {} (={})".format(hex(start), hex(stop), size))

        l = sorted(list(cmd_numa_current_node.singleton.cpu_to_node.values()))
        dest_node = "{}-{}@{}".format(l[0], l[-1], node_inc)
        cmd = " ".join(map(str, [MOVE_PAGE, gdb.selected_inferior().pid, dest_node, hex(int(start)), length]))
        nb_pages = int(length / PAGESIZE) + 1
        
        try:
            log.warn("{} # {} pages".format(cmd, nb_pages))
            res = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as e:
            log.error("{} returned error code {}.".format(cmd, e.returncode))
            error = str(e.output)[:-1]
            log.warn(error)
        
def on_activate():
    cmd_numa_movepage()
    cmd_numa_move_3d_matrix()
    cmd_numa_move_3d_matrix_spread()
    cmd_numa_spread_pages()
    cmd_numa_pages_moved()
