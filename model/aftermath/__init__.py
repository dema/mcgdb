import gdb

import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

import mcgdb
from mcgdb.toolbox import my_gdb

def initialize():
    
    am_command()
    am_dump_trace_command()
    
class am_dump_trace_command (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "aftermath dump_trace", gdb.COMMAND_OBSCURE)
        
    def invoke (self, arg, from_tty):
        if not arg:
            arg = "trace.ost"
            
        with my_gdb.set_parameter("detach-on-fork", "off"):
            with my_gdb.set_parameter("scheduler-locking", "on"):
                with my_gdb.active_thread(my_gdb.main_thread()):
                    log_user.info("Forking to a new process ...")
                    #gdb.execute("p clone(main, malloc(512)-512, 17)") # 17 = SIGCHLD
                    gdb.execute("p fork()") 
                    
                    tmp_inf = gdb.inferiors()[-1]
                    my_gdb.switch_to_inferior(tmp_inf)
                    gdb.execute("finish")

                    log_user.info("Dumping OpenMP trace to {} ...".format(arg))
                
                    gdb.execute('call am_trace_dump(&kmpt_trace, "{}")'.format(arg))
                
                    gdb.execute("kill")
                my_gdb.remove_inferior(tmp_inf)
        
        log_user.info("Done.")

class am_command (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "aftermath", gdb.COMMAND_OBSCURE, prefix=True)
