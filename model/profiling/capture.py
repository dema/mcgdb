import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

import gdb

import mcgdb
from mcgdb.toolbox import my_gdb

exec_at_main = []
exec_at_exit = []
exec_on_exited = []

class FunctionProfile_Breakpoint(mcgdb.capture.FunctionBreakpoint):
    decorator = None
    
    def __init__(self, fct, profile):
        mcgdb.capture.FunctionBreakpoint.__init__(self, fct)
        self.profile = profile
        
    def prepare_before (self):
        self.profile.start()
            
        return False, True, {}
    
    def prepare_after (self, data):
        self.profile.stop()

class HitCounter_Breakpoint(mcgdb.capture.FunctionBreakpoint):
    decorator = None
    
    def __init__(self, loc):
        mcgdb.capture.FunctionBreakpoint.__init__(self, loc)
        self.infos = []
        
    def prepare_before (self):
        for info in self.infos:
            info.hit()

    def register(self, info):
        self.enabled = True 

        self.infos.append(info)
        
    def unregister(self, info):
        self.infos.remove(info)
        if not self.infos:
            self.enabled = False
            
class ZoneProfile_Breakpoint(mcgdb.capture.FunctionBreakpoint):
    decorator = None
    
    def __init__(self, loc, profile, is_start):
        mcgdb.capture.FunctionBreakpoint.__init__(self, loc)
        
        self.profile = profile
        self.is_start = is_start
        
    def prepare_before (self):
        if self.is_start:
            self.profile.start()
        else:
            self.profile.stop()
            
        return False, False, {}

class Exit_Breakpoint(mcgdb.capture.FunctionBreakpoint):
    decorator = None
    
    def __init__(self):
        mcgdb.capture.FunctionBreakpoint.__init__(self, "exit")

    def prepare_before (self):
        if exec_at_exit:
            log_user.info("On exit")
            for do in exec_at_exit:
                do()

        return False, False, {}

class Main_Breakpoint(mcgdb.capture.FunctionBreakpoint):
    decorator = None
    
    def __init__(self):
        mcgdb.capture.FunctionBreakpoint.__init__(self, "main", temporary=True)

    def prepare_before (self):
        if exec_at_main:
            log_user.info("On main")
            for do in exec_at_main:
                do()
        try:  
            Exit_Breakpoint()
        except mcgdb.capture.AlreadyInstalledException:
            pass

def on_exited(evt):
    # stop perf here
    if exec_on_exited:
        log_user.info("On exited")
        for do in exec_on_exited:
            do()

def initialize():
    my_gdb.exec_on_sharedlibrary(Main_Breakpoint, "*")
    gdb.events.exited.connect(on_exited)

