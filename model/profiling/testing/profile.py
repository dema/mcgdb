import types, re, os

from gdb_tester import *

import mcgdb

def check_profiling_consistency(gdb, prof_id, expected):
    for key, val in expected.items():
        gdb.execute("profile summary {} {}".format(prof_id, key), re.compile("{}:\s+{}".format(key, val)))
    
################
################

class test_environment:
    def __init__(self, gdb):
        self.gdb = gdb
        
    def __enter__(self):
        self.gdb.start(CSource("profile.c"), init_hook=mcgdb.testing.gdb__init_mcgdb)
    
    def __exit__(self, type, value, traceback):
        self.gdb.reset(hard=True)

def test(gdb, mod=None):
    if mod is None:
        mod = globals()
        
    for entry in sorted(mod.keys()):
        if not entry.startswith("test__"):
            continue
        if not isinstance(mod[entry], types.FunctionType):
            continue

        test_fct = mod[entry]

        with test_environment(gdb):
            test_fct(gdb)        
    
    gdb.quit()
