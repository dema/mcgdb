from gdb_tester import *

import mcgdb

from . import profile

################
### FUNCTION ###
################

def test__profile_fct_sequential(gdb, silent=False):
    if not silent:
        gdb.set_title("Function: sequential")
    
    gdb.execute_many(["start"])
    gdb.execute("set do_what = 3")
    
    gdb.execute("profile function do_sequencial", "set profiling breakpoint")
    gdb.execute("profile config 1 first-last only")
    
    gdb.execute("cont")

    profile.check_profiling_consistency(gdb, "1", {"printf hits": 1})
    
def __profile_fct_nesting(gdb, config, subtitle, nb_printf=1):
    gdb.set_title("Function: nesting "+subtitle)
    
    gdb.execute_many(["start"])
    gdb.execute("set do_what = 1")
    
    gdb.execute("profile function nesting", "set profiling breakpoint")

    config()
    
    gdb.execute("cont")
    
    profile.check_profiling_consistency(gdb, "1", {"printf hits": nb_printf})

def test(gdb):
    profile.test(gdb, globals())
