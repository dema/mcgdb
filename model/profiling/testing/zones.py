from gdb_tester import *

import mcgdb

from . import profile

#############
### ZONES ###
#############
    
def test__profile_zone(gdb):
    gdb.set_title("Zone")
    
    gdb.execute_many(["start"])
    gdb.execute("profile zone {}-{}".format(gdb.get_location("location: AFTER NESTING"),
                                            gdb.get_location("location: BEFORE RETURN"),
                                            line_only=True), "set profiling zone on")
    gdb.execute("profile config 1 first-last only")
    gdb.execute_many(["cont"])
    
    profile.check_profiling_consistency(gdb, "1", {"printf hits": 31})


def test(gdb):
    profile.test(gdb, globals())
