import types, re, os

from gdb_tester import *

import mcgdb

from . import profile, function

#############
### Modes ###
#############

def test__profile_mode(gdb):
    gdb.execute("profile function nesting", "set profiling breakpoint")

    def config_soust():
        gdb.execute("profile config 1 soustractive only")

    with test_environment(gdb):
        function.__profile_fct_nesting(gdb, config_soust, "soustractive")

    def config_per_fct():
        gdb.execute("profile config 1 per-function only")

    with test_environment(gdb):
        function.__profile_fct_nesting(gdb, config_per_fct, "per-function")

    def config_first_last():
        gdb.execute("profile config 1 first-last only")

    with test_environment(gdb):
        function.__profile_fct_nesting(gdb, config_first_last, "first-last", nb_printf=6)
