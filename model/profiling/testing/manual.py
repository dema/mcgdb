from gdb_tester import *

import mcgdb

from . import profile

##############
### MANUAL ###
##############
    
def test__profile_manual_auto_start_stop(gdb):
    gdb.set_title("Manual: auto start and stop")
    
    gdb.execute("profile manual start", "start manual profiling")
    
    gdb.execute("run")
    
    profile.check_profiling_consistency(gdb, "1", {"printf hits": 37})

def test__profile_manual_normal(gdb):
    gdb.set_title("Manual: normal")
    
    gdb.execute_many(["start"])

    gdb.execute("profile manual start", "start manual profiling")

    gdb.execute("tbreak {}".format(gdb.get_location("location: BEFORE RETURN")))

    gdb.execute("continue")

    gdb.execute("profile manual stop", "stop manual profiling")
    
    profile.check_profiling_consistency(gdb, "1", {"printf hits": 37})

def test(gdb):
    profile.test(gdb, globals())
