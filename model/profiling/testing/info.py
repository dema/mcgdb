import re

from gdb_tester import *


import mcgdb

def test(gdb):
    gdb.start(CSource("profile.c"), init_hook=mcgdb.testing.gdb__init_mcgdb)
    from mcgdb.model.profiling.info import perf
    gdb.execute("info counters", perf.perf_info_ldpreload.__doc__)
    gdb.execute("counters disable 1")
    gdb.execute("info counters", "# disabled")
    gdb.execute("counters enable 1")
    gdb.test("disabled" not in gdb.execute("info counters")[1],
             "counters enable 1")

