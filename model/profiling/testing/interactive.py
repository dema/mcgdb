from gdb_tester import *

import mcgdb

from . import profile

def test__interactive__function(gdb):
    gdb.set_title("Interactive function call")
    gdb.execute("start")
    
    out = gdb.execute("profile manual interactive\n5\nnesting(nesting_depth)\nend")[1]
    gdb.test("start manual profiling" in out, "start manual profiling")
    gdb.test("stop manual profiling" in out, "stop manual profiling")
    gdb.test("manual profile" in out, "has profile summary")

    out = gdb.execute("profile manual interactive -redo")[1]
    gdb.test("manual profile" in out, "has profile summary")
    gdb.test("=============" in out, "has profile summary")

def test__interactive__app(gdb):
    gdb.set_title("Interactive whole app")
    gdb.execute("start")

    out = gdb.execute("profile manual interactive -app\n3")[1]
    gdb.test("manual profile" in out, "has profile summary")
    gdb.test("=============" in out, "has profile summary")
    gdb.test("Switching to thread" in out, "switched to main thread")
    
def test(gdb):
    profile.test(gdb, globals())
