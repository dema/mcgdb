from gdb_tester import *

import mcgdb

from . import profile, function

###############
###  GRAPH  ###
###############

def test__profile_graph_seq(gdb):
    gdb.set_title("Graph")
    
    function.test__profile_fct_sequential(gdb, silent=True)
    
    gdb.execute("profile graph", "Done")
    
    gdb.execute("profile graph plot first 1")

    for what in ["sequential"]:
        if os.path.exists(what+".html"):
            gdb.test_success(what)
            os.remove(what+".html")
        else:
            gdb.test_fail(what)

def test(gdb):
    profile.test(gdb, globals())
