#ifndef NO_OMP
  #include <omp.h>
#endif
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>

__thread int tls;

/*****************************/
/*   controlled by debugger  */
/*****************************/
int do_what = 0; // 0: do all, 1: do nesting, 2: do concurrent, 3: do sequencial

int nesting_depth = 5;
int sequencial_count = 5;

int do_concurrent_nesting = 1;
/*****************************/

void do_sequencial(int idx) {
  int i;
  for (i = 1; i <= idx+1; i++) {
    printf("seq%s", (i == idx+1) ? "\n" : " ");
  }
  for (i = 0; i < idx; i++) {
    // do something intensive 
  }
}
void nesting(int depth) {
  printf("nesting #%d\n", depth); // printf used for dbg counting
  
  if (depth > 0) {
    nesting(depth -1);
  } else {
#ifndef NO_OMP
  #pragma omp barrier
#endif
  }
}

void do_concurrent(void) {
#ifndef NO_OMP
  tls = omp_get_thread_num();
  printf("tlse #%d\n",tls);
  nesting(tls);
#endif
    
  printf("concurrent #%d\n",
#ifdef NO_OMP
         -1
         
#else
         omp_get_thread_num()
#endif  
         ); // printf used for dbg counting
#ifndef NO_OMP
#pragma omp barrier // enforce concurrency
#endif
}

void concurrent(int do_nesting) {
#ifndef NO_OMP
#pragma omp parallel
#endif
  {
    if (do_what) {
      nesting(nesting_depth);
    } else {
      do_concurrent();
    }   
  }
}

// mcgdb a.out -ex start -ex "set do_what = 1" -ex "profile function nesting" -ex "profile summary on_exit"
// mcgdb a.out -ex start -ex "set do_what = 3" -ex "profile function do_sequencial" -ex "profile summary on_exit"
int main(int argc, char** argv) {
  if (do_what == 0 ||  do_what == 1) {
    printf("Do nesting\n");
    printf("==========\n");
    nesting(nesting_depth);
  }
  /* location: AFTER NESTING */
  if (do_what == 0) {
    printf("\n");
  }
  if (do_what == 0 || do_what == 2) {
    printf("Do concurrent\n");
    printf("=============\n");
    concurrent(do_concurrent_nesting);
  }
  if (do_what == 0) {
    printf("\n");
  }
  if (do_what == 0 || do_what == 3) {
    int i;
    
    printf("Do sequencial\n");
    printf("=============\n");
    for (i = 0; i < sequencial_count; i++) {
      do_sequencial(i);
    }
    for (i = sequencial_count-1; i >= 0; i--) {
      do_sequencial(i);
    }
  }
  /* location: BEFORE RETURN */
}
