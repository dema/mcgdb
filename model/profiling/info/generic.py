from collections import OrderedDict, defaultdict

import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

import gdb

class generic_info():
    "generic template"
    name = "generic template"
    
    def __init__(self):
        self.__results = OrderedDict(("name", value))
        
    def start(self):
        pass
    
    def stop(self, paused=False):
        pass

    def to_log(self, ongoing=False):
        return self.__results

    @property
    def results(self):
        return self.__results
    
__COUNTERS__ = [generic_info]
