import subprocess
import signal
import time
from collections import defaultdict, OrderedDict

import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

import gdb

from ..interaction import counters

DISABLE_STARTUP_SYNC = False
MAX_WAIT = 5 # in 1/10 s
HAS_PERF_SIGNAL = False # need to test version

if DISABLE_STARTUP_SYNC:
    log.warn("No startup sync between perf and mcgdb")

class perf_info_standalone():
    "perf stat counters (standalone)"
    name = "perf standalone"
    UNITS = {"task-clock": "msec"}
    
    def __init__(self):
        self.perf = None
        self.reset()
        self.rcvd = None
        
    def start(self):
        assert self.perf is None

        self.rcvd = False
        
        if DISABLE_STARTUP_SYNC:
            self.rcvd = True
        else:
            def handler(signum, frame):
                self.rcvd = True
            
            import signal, time, os
            old = signal.signal(signal.SIGUSR1, handler)

        command = ['perf', 'stat', '-x,',
                   '-e', counters.param_perf_counters.get()
                   ]
        
        if HAS_PERF_SIGNAL:
             # not working with old versions of perf
            command += ['--pre', "kill -USR1 {}".format(os.getpid())]
        command += [  #'-p', str(gdb.selected_inferior().pid),
            '-p', str(gdb.selected_thread().ptid[1]),
            'sleep', '999999999' # otherwise pref stat --pre returns immediatly
                      ]
        self.perf = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.res = "running"
        assert self.perf.pid > 1

        if not DISABLE_STARTUP_SYNC:
            cnt = MAX_WAIT
            for cnt in range(MAX_WAIT):
                if self.rcvd: break
                time.sleep(0.1)

            signal.signal(signal.SIGUSR1, old)
        
            if not self.rcvd and HAS_PERF_SIGNAL:
                #log.error("Waited to long for perf ...")
                pass
            
    def stop(self):
        assert not self.perf.returncode
        sleep_pid = subprocess.Popen(["pgrep", "-P", str(self.perf.pid)],
                                     stdout=subprocess.PIPE).stdout.read().decode('ascii')
        subprocess.Popen(["kill", "-INT", sleep_pid[:-1]],
                         stdout=subprocess.PIPE).communicate()

        stdout, stderr = self.perf.communicate()
        
        assert self.perf.returncode is not None
        self.perf = None
        
        self.res = stderr.decode("ascii")

        results = self.parse()
        for k, v in results.items():
            if v is None: continue
            try:
                self.results[k] += v
            except TypeError:
                log.error("Cannot add '{}' to '{}'.".format(v, self.results[k]))
                
                self.results[k] = "{}/{}".format(self.results[k], v)
            
    def reset(self):
        self.results = defaultdict(int)
        
        if self.perf and self.perf.returncode:
            self.perf.kill()
            assert self.perf.returncode

        self.res = None

    def to_log(self, ongoing=False):
        if ongoing:
            assert False
            
        if not self.results:
            return False

        info = OrderedDict()
        for k, val in self.results.items():
            try:
                details = perf_info_standalone.UNITS[k]
            except KeyError:
                details = ""
            
            info[k] = val, details
        return info
    
    def parse(self):
        lines = self.res.split("\n")
        
        results = OrderedDict()
        for line in lines:
            if not line: continue
            if line == "sleep: Interrupt": continue
            if "sleep" in line:
                log.critical("is this line really expected? {}".format(line))
            try:
                line_split = line.split(",")
                if len(line_split) == 2:
                    # old perf versions
                    val, desc = line_split
                else:
                    # new versions
                    val, _1, desc, _2, pct = line_split

                if val == '<not counted>':
                    line = None
                elif "." in val:
                    val = float(val)
                elif val.isdigit():
                    val = int(val)
                else:
                    pass
      
            except Exception as e:
                if "Error" in line or 'Fatal' in line:
                    desc = "PERF ERROR"
                    val = line
                else:
                    import pdb;pdb.set_trace()
                    desc = line
                    val = str(e) #, ""
                
            results[desc] = val
            
        return results

__COUNTERS__ = [perf_info_standalone]
