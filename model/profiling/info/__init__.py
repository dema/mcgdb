import importlib

import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

from mcgdb.toolbox import my_gdb

# will be added to global scope
info_counters = "perf_standalone", "omp", "numa", "proc" #"counter_id" #"proc", "hit", "counter", "perf", "gprof", "memleaks"

info_counter_classes = None # will be populated in initialize()
disabled_counters = set()

@my_gdb.Numbered
class Info_set(list):
    def __init__(self, arg):
        list.__init__(self, arg)
        
    def __repr__(self):
        return "InfoSet[{}]".format(self.number)
    
    def start(self):
        for info in self:
            info.start()

    def stop(self):
        for info in self:
            info.stop()

    def get(self, key):
        values = []
        for info in self:
            try:
                values.append(info.results[key])
            except KeyError: pass
        return values
            
def new_infoset():
    counters = []
    for info_counter_class in info_counter_classes:
        try:
            if info_counter_class.__name__ == "numa_location_info":
                #counters.append(info_counter_class("&r[$omp_loop_start()][0][0]"))
                #counters.append(info_counter_class("&u[$omp_loop_start()][0][0]"))
                pass
            else:
                counters.append(info_counter_class())
        except RuntimeError as e:
            log.info("Couldn't instantiate counter class '{}'".format(info_counter_class.__name__))
            log.info(e)
        
    return Info_set(counters)

def initialize():
    global info_counter_classes
    info_counter_classes = []
    for info_counter in info_counters:
        try:
            info_counter_mod = importlib.import_module(".{}".format(info_counter), __name__)
            info_counter_classes.extend(info_counter_mod.__COUNTERS__)
            globals()[info_counter] = info_counter_mod
        except Exception as e:
            log.critical("Could not load counter '{}': {}".format(info_counter, e))
            
class Unit():
    def __init__(self, unit):
        self.unit = unit
            
class UnitFloat(float, Unit):
    def __new__(self, value, unit):
        return float.__new__(self, value)
    
    def __init__(self, value, unit):
        float.__init__(value)
        Unit.__init__(self, unit)

class UnitInt(int, Unit):
    def __new__(self, value, unit):
        return int.__new__(self, value)
    
    def __init__(self, value, unit):
        int.__init__(value)
        Unit.__init__(self, unit)
