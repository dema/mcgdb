from collections import OrderedDict, defaultdict

import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

import gdb

class counter_info():
    "Simple counters"
    name = "Simple counters"
    
    def __init__(self):
        self.reset()
        self.pid = 0
        
    def start(self):
        self.hits += 1
        self.depth += 1
        self.pid = gdb.selected_inferior().pid
    
    def stop(self, paused=False):
        self.max_depth = max(self.depth, self.max_depth)
        self.depth -= 1

    def to_log(self, ongoing=False):
        return OrderedDict((
            ("pid", self.pid),
            ("hits", self.hits),
            ("max depth", self.max_depth)))
        
    def reset(self):
        self.hits = 0
        self.depth = 0
        self.max_depth = 0

    @property
    def results(self):
        return {k: getattr(self, k) for k in ("pid", "hits", "max_depth")}

__COUNTERS__ = [counter_info]
