from collections import OrderedDict, defaultdict

import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")


import subprocess, signal
import signal, time, os, fcntl

import gdb
from . import UnitFloat
from .. import capture

MCGDB_PATH = "/home/kevin/travail/Python/mcgdb"
PREF_LD_PRELOAD = MCGDB_PATH+ "/model/profiling/__binaries__/libmcgdb_perf_stat.preload.so"

try: # Python 3 standard library.
    ProcessLookupError
except NameError: # No such class in Python 2.
    ProcessLookupError = NotImplemented

class perf_process():
    UNITS = {"task-clock": "msec"}
    
    self = None
    counters_at_death = None
    @staticmethod
    def launch():
        if perf_process.self is not None:
            log.warn("perf is already running.")
            return perf_process.self

        return perf_process()
    
    def __init__(self):
        assert perf_process.self is None
        perf_process.self = self
        
        self.perf = None
        self.rcvd = None
        self.has_run = False

        def at_cont(_not_used):
            log.warn("HAZ RUN")
            self.has_run = True
            gdb.events.cont.disconnect(at_cont)
            
        gdb.events.cont.connect(at_cont)
        
        self.do_launch()
        
    def do_launch(self):
        assert self.perf is None
        
        self.rcvd = False
        def handler(signum, frame):
            self.rcvd = True

        old = signal.signal(signal.SIGUSR1, handler)

        pid = gdb.selected_inferior().pid
        log.error("Attach perf to process {}".format(pid))
        command = ['perf', 'stat', '-x;',
                   #'--pre', "kill -USR1 {}".format(os.getpid()),
                   '-e', profile.interaction.counters.param_perf_counters.get(),
                   '-p', str(pid),
                   #'sleep', '999999999' # otherwise pref stat --pre returns immediatly
                   ]
        
        nm_res = os.popen("nm -a {}".format(os.popen("which {}".format(command[0])).read().strip())).readlines()
        perf_addr = [l for l in nm_res if "process_interval" in l][0].partition(" ")[0]
        self.perf = subprocess.Popen(command,
                                     env={"LD_PRELOAD": PREF_LD_PRELOAD,
                                          "PROCESS_INTERVAL_ADDR": perf_addr},
                                     stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        self.res = "running"

        assert self.perf.pid > 1

        def wait_for_start():
            MAX_WAIT = 1
            cnt = MAX_WAIT
            for cnt in range(MAX_WAIT):
                if self.rcvd: break
                time.sleep(0.1)

            if not self.rcvd:
                #log.error("Waited to long for perf ...")
                pass
                
        wait_for_start()
        signal.signal(signal.SIGUSR1, old)
        
        def make_non_blocking(file_):
            fd = file_.fileno()
            flag = fcntl.fcntl(fd, fcntl.F_GETFD)
            fcntl.fcntl(fd, fcntl.F_SETFL, flag | os.O_NONBLOCK)
            
        make_non_blocking(self.perf.stderr)
        
    def get_counters(self):
        if self.perf is None:
            return perf_process.counters_at_death
        
        assert not self.perf.returncode
        try:
            self.perf.send_signal(signal.SIGUSR2)
        except ProcessLookupError:
            return None # already dead
        except Exception as e:
            log.warn("Could not get perf counters info ... {}".format(e))
            import pdb;pdb.set_trace()
            raise e
        
        content = ""
        while not content.endswith("8<--\n"):
            read = self.perf.stderr.read()
            if not read:
                continue

            content += read.decode("ascii")
        
        return content.split("\n")[:-2] # remove last line of 8<-- and last \n
    
    @staticmethod
    def kill():
        if not perf_process.self:
            return
        
        perf_process.self.do_kill()
        perf_process.self = None
        
    def do_kill(self):
        if self.has_run:
            perf_process.counters_at_death = self.get_counters()
            
        try:
            self.perf.send_signal(signal.SIGTERM)
            self.perf.wait()
        except ProcessLookupError:
            pass # already dead
            
        self.perf = None
        
class perf_info_ldpreload():
    "perf stat counters"
    name = "perf stat"
    
    def __init__(self):
        self.reset()
        
    def start(self):
        assert perf_process.self is not None
        assert self.start_res is None and not self.running

        # save current perf value
        self.start_res = perf_process.self.get_counters()
        
        self.running = True
        
    def stop(self):        
        assert self.start_res and self.running
        
        self.accumulate_results(self.__results)
        
        self.reset(complete=False)
        self.running = False

    @property
    def results(self):
        results = self.__results
        
        if self.running:
            results = results.copy()
            self.accumulate_results(results)
        
        return results
    
    def accumulate_results(self, results):
        assert self.start_res

        if not perf_process.self:
            assert perf_process.counters_at_death is not None
            current_res = perf_process.counters_at_death
        else:
            current_res = perf_process.self.get_counters()
            
        slice_results = self.slice_counters(self.start_res, current_res)
        self.add_slice_to_results(slice_results, results)
        
    def add_slice_to_results(self, slice_results, results):
        for k, v in slice_results.items():
            if v is None: continue
            try:
                results[k] += v
            except TypeError:
                results[k] = "{}/{}".format(results[k], v)
                
    def reset(self, complete=True):
        self.running = False
        
        self.start_res = None
        
        if not complete:
            return
        
        self.__results = defaultdict(int)

    def to_log(self, ongoing=False):
        results = self.__results
        if ongoing and self.running:
            results = results.copy()
            self.accumulate_results(results)

        info = OrderedDict()
        for k, val in results.items():
            try: val = UnitFloat(val, perf_process.UNITS[k])
            except KeyError: pass # no unit
            
            info[k] = val
        
        return info
    
    def parse(self, raw_res):
        print(raw_res)
        results = OrderedDict()
        for line in raw_res:
            if not line: continue
            if line == "sleep: Interrupt": continue
            if "sleep" in line:
                log.critical("is this line really expected? {}".format(line))
            try:
                """ The fields are in this order:
                - optional usec time stamp in fractions of second (with -I xxx)
                - optional CPU, core, or socket identifier
                - optional number of logical CPUs aggregated
                - counter value
                - unit of the counter value or empty
                - event name
                - run time of counter
                - percentage of measurement time the counter was running
                - optional variance if multiple values are collected with -r
                - optional metric value
                - optional unit of metric
                """
                
                '2205047402   ,     ,instructions:u,708631957          ,  83.85    ,1.21    ,insn per cycle'
                'counter value, unit,event name    ,run time of counter, percentage,variance,metric,unit'
                
                line_split = line.split(";")
                val, desc, pct = line_split[0], line_split[2], line_split[3]
                details = line_split[-1]
                
                if val == '<not counted>':
                    line = None
                elif "." in val:
                    val = float(val)
                elif val.isdigit():
                    val = int(val)
                else:
                    pass

            except Exception as e:
                val = line
                line = str(e) #, ""
                
            try:
                if not desc and details:
                    desc = details
                results[desc] = val
            except KeyError:
                import pdb;pdb.set_trace()
                pass
            
        return results

    def slice_counters(self, start_res, stop_res):
        assert start_res is not None
        assert stop_res is not None

        start = self.parse(start_res)
        stop = self.parse(stop_res)

        assert len(set(start) & set(stop)) == len(start)

        results = OrderedDict()
        for key in start.keys():
            try:
                if start[key] is stop[key] is None:
                    results[key] = None
                    continue
                if start[key] is None:
                    start[key] = 0
                if "not counted" in str(start[key]):
                    start[key] = 0
                    
                results[key] = stop[key] - start[key]
                if results[key] < 0: import pdb;pdb.set_trace()
            except TypeError: # cannot substract ...
                results[key] = ("{} -- {}".format(start[key], stop[key])
                                if start[key] != stop[key] else start[key])
        
        return results
    
def initialize():
    #capture.exec_at_main.append(perf_process.launch)
    #capture.exec_at_exit.append(perf_process.kill)
    #capture.exec_on_exited.append(perf_process.kill)
    pass

__COUNTERS__ = [] # perf_info_ldpreload
