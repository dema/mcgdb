from collections import OrderedDict, defaultdict

import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

import gdb
from mcgdb.toolbox import aspect

loop = [0, 0]

class omp_loop_len_info():
    "omp loop len counter"
    name = "omp loop len counter"
    
    def __init__(self):
        self.__results = {"omp_loop_start": None,
                          "omp_loop_len": None}
        
    def start(self):
        pass
    
    def stop(self, paused=False):
        self.__results["omp_loop_start"] = loop[1]
        self.__results["omp_loop_len"] = loop[1] - loop[0] + 1
        
    def to_log(self, ongoing=False):
        return self.__results

    @property
    def results(self):
        return self.__results
    
__COUNTERS__ = [omp_loop_len_info]

def loop_aspects(Tracks):
    from mcgdb.model.task.environment.openmp import representation
    
    @Tracks(representation.ForLoopJob)
    class ForLoopJob:
        def __init__(this):
            pass
                
        def start_work_on(this):
            loop[:] = this.args.lower, this.args.upper
        
aspect.register("loop-len-counter", loop_aspects)
