from collections import OrderedDict, defaultdict

import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

import gdb

class numa_node_info():
    "numa node"
    name = "numa node"
    
    def __init__(self):
        self.__results = {"numa node": None}
        gdb.execute("numa", to_string=True) # init, just in case
        
    def start(self):
        good = True
        try:
            node = gdb.execute("numa current_node -raw", to_string=True)
            self.__results["numa node"] = int(node)
            
        except Exception as e:
            self.__results["numa node"] = str(e)
            good = False
            
        try:
            core = gdb.execute("numa current_core -raw", to_string=True)
            self.__results["numa core"] = int(core)
        except Exception as e:
            self.__results["numa core"] = str(e)
            good = False

        if good:
            self.__results["numa node core"] = int(node) % 8
        
    def stop(self, paused=False):
        pass

    def to_log(self, ongoing=False):
        return self.__results

    @property
    def results(self):
        return self.__results

class numa_location_info():
    def __init__(self, var):
        self.var = var
        self.name = "{} location".format(var)
        self.__results = {self.name: None}
        
        gdb.execute("numa", to_string=True) # init, just in case
        
        
    def start(self):
        loc = gdb.execute("numa pagemap -raw {}".format(self.var), to_string=True)
        self.__results[self.name] = int(loc[:-1])

    def stop(self, paused=False):
        pass
    
    def to_log(self, ongoing=False):
        return self.__results


__COUNTERS__ = [numa_node_info, numa_location_info]
