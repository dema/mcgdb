from collections import OrderedDict, defaultdict

import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

import gdb
import mcgdb
from mcgdb.toolbox.target import my_archi
from ..info import UnitInt

class Alloc_Breakpoint(mcgdb.capture.FunctionBreakpoint):
    FUNCS = ["malloc"]
    
    decorator = None
    
    def __init__(self, fct):
        mcgdb.capture.FunctionBreakpoint.__init__(self, fct)
        
    def prepare_before (self):
        data = {}
        data["size"] = int(my_archi.first_arg(my_archi.UINT))
        
        return False, True, data
    
    def prepare_after (self, data):
        ptr = str(my_archi.return_value(my_archi.VOID_P))
        
        for counter in counters:
            counter.alloc(ptr, data["size"])
            
        return False
    
class Free_Breakpoint(mcgdb.capture.FunctionBreakpoint):
    FUNCS = ["free"]
    
    decorator = None
    
    def __init__(self, fct):
        mcgdb.capture.FunctionBreakpoint.__init__(self, fct)
        
    def prepare_before (self):
        ptr = str(my_archi.first_arg(my_archi.VOID_P))
        
        for counter in counters:
            counter.free(ptr)
        
        return False, False, {}

breakpoints = []
def handle_breakpoints(do_insert):
    if not breakpoints:
        assert do_insert
        
        def instantiate(clazz):
            for fct in clazz.FUNCS:
                breakpoints.append(clazz(fct))
            
        instantiate(Alloc_Breakpoint)
        instantiate(Free_Breakpoint)
    else:
        for bpt in breakpoints:
            bpt.enabled = do_insert

counters = []
def setup_breakpoints(counter, do_install):
    assert not (do_install and counter in counters)

    if do_install and not counters:
        handle_breakpoints(do_insert=True)
    
    (counters.append if do_install else counters.remove)(counter)

    if not do_install and not counters:
        handle_breakpoints(do_insert=False)
        

class memleak_info():
    "Memory leaks"
    name = "Memory leaks"
    
    def __init__(self):
        self.freed_size = 0
        self.free_from_outside = 0
        self.allocated_size = 0
        self.allocated = {}
    
    def start(self):
        setup_breakpoints(self, do_install=True)
    
    def stop(self, paused=False):
        setup_breakpoints(self, do_install=False)

    def to_log(self, ongoing=False):
        return self.results

    @property
    def results(self):
        return {"total size allocated": UnitInt(self.allocated_size, "b"),
                "leaked in profile": UnitInt(sum(self.allocated.values()), "b"),
                "free on unknown pointer": self.free_from_outside
                }

    def alloc(self, ptr, size):
        assert ptr not in self.allocated
        self.allocated[ptr] = size
        
        self.allocated_size += size
        
    def free(self, ptr):
        try:
            del self.allocated[ptr]
        except KeyError:
            # was not allocated in section
            self.free_from_outside += 1
        
    
__COUNTERS__ = [memleak_info]
