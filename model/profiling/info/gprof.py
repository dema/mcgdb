from collections import OrderedDict, defaultdict

import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

import gdb
import mcgdb
from mcgdb.toolbox import my_gdb

from .. import capture
from mcgdb.toolbox.target import my_archi

low, high = None, None
write_gmon = None
monstartup = None
mcleanup = None
preinited = None

def preinit_gprof():
    global preinited
    
    try:
        preinited = bool(gdb.parse_and_eval("__monstartup"))
    except gdb.error: pass
    
    if preinited:
        monstartup_Breakpoint()
    else:
        log.warn("Cannot init GPROF counter (could not find gprof '__monstartup' symbol)")
        
def init_gprof():
    try:
        prod = gdb.selected_frame().find_sal().symtab.producer
        log.warn("Profile flag found in symtab producer ? {}".format("-p" in prod))
    except Exception as e:
        log.warn("Cannot check producer flags ({})".format(e))

    try:
        global monstartup, mcleanup
        monstartup = gdb.parse_and_eval("__monstartup")
        mcleanup = gdb.parse_and_eval("_mcleanup")
        
        log.warn("gprof counter enabled!")
    except Exception as e:
        log.warn("Cannot enable GPROF counters: symbols not found.")
        inited = False
        
class gprof_info():
    "gprof profiling info"

    name = "gprof counter"

    def __init__(self):
        if not preinited:
            raise RuntimeError("Module not initialized.")
        self.running = False
        self.results = {"gprof":"rien pour le moment"}
        
    def start(self):
        assert not self.running
        
        self.running = True
        monstartup(low, high)
        
    def stop(self):
        assert self.running
        
        self.running = False
        mcleanup()
        monstartup(low, high)
        
    def to_log(self, ongoing=False):
        return self.results

class monstartup_Breakpoint(mcgdb.capture.FunctionBreakpoint):
    decorator = None
    
    def __init__(self):
        mcgdb.capture.FunctionBreakpoint.__init__(self, "__monstartup")
        
    def prepare_before (self):
        global low, high
        try:
            low, high = int(my_archi.first_arg(my_archi.ULONG)), int(my_archi.second_arg(my_archi.ULONG))
            log.warn("PC: {} --> {}".format(hex(low), hex(high)))
        except Exception as e:
            log.error("GPROF: Could not retrieve low/high pc values: {}".format(e))
            low, high = None, None
            
        return False, False, {}
        
def initialize():
    my_gdb.exec_on_sharedlibrary(preinit_gprof, "*")
    capture.exec_at_main.append(init_gprof)

__COUNTERS__ = [gprof_info]
