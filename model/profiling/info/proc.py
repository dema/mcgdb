from collections import OrderedDict, defaultdict

import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

import gdb
from . import UnitFloat


class proc_info():
    UNITS = {}
    
    def __init__(self):
        self.reset()
        
    def reset(self):
        self.start_values = {}
        self.results = {}
        
    def start(self):
        status = self.parse()

        assert not self.start_values
        for key, val in self.results.items():
            self.start_values[key] = status[key]
            
    def stop(self):
        self.parse_and_update(self.results)
        self.start_values.clear()
        
    def parse_and_update(self, values):
        status = self.parse()
        
        for key, prev_val in values.items():
            values[key] = prev_val
            try:
                values[key] += int(status[key]) - int(self.start_values[key])
            except KeyError: # key not in self.start_values
                pass
            
    def parse(self):
        status_values = {}
        with open(self.filename.format(gdb.selected_inferior().pid)) as status:
            for line in status.readlines():
                key, val = line.split(":")
                status_values[key] = val.strip()
                
        return status_values

    def to_log(self, ongoing=False):
        IGNORE_IGNORED = True
        results = self.results

        info = OrderedDict()
        
        if ongoing:
            results = results.copy()
            self.parse_and_update(results)
            
        ignored = []
        for k, v in results.items():
            if not v and not IGNORE_IGNORED:
                ignored.append(k)
                continue
            
            try: v = UnitFloat(v, self.__class__.UNITS[k])
            except KeyError: pass # no unit
                 
            info[k] = v
            
        if not IGNORE_IGNORED and ignored:
            info["ignored"] = ", ".join(ignored)

        return info
    
class proc_status_info(proc_info):
    "Information from /proc/$PID/status"
    name = "/proc/$PID/status"
    
    def __init__(self):
        proc_info.__init__(self)
        
        self.results = {"voluntary_ctxt_switches": 0,
                       "nonvoluntary_ctxt_switches": 0}

        self.filename = "/proc/{}/status"

class proc_io_info(proc_info):
    "Information from /proc/$PID/io"
    name = "/proc/$PID/io"
    
    def __init__(self):
        proc_info.__init__(self)
        
        self.results = {"rchar": 0, "wchar": 0,
                       "syscr": 0, "syscw": 0,
                       "read_bytes": 0, "write_bytes": 0,
                       "cancelled_write_bytes": 0}

        self.filename = "/proc/{}/io"
        
class proc_stat_info(proc_info):
    "Information from /proc/$PID/stat"
    name = "/proc/$PID/stat"
    
    UNITS = {"utime":"msec", "stime":"msec"}
    
    def __init__(self):
        proc_info.__init__(self)
        self.filename = "/proc/{}/stat"
        self.results = {"utime":0, "stime":0, "maj_flt":0, "cmaj_flt":0,"min_flt":0,"cmin_flt":0,}
        
    def parse(self):
        KEYS = ["pid", "tcomm", "state", "ppid", "pgid", "sid",
                "tty_nr", "tty_pgrp", "flags",
                "min_flt", "cmin_flt", "maj_flt", "cmaj_flt",
                "utime", "stime", "cutime", "cstime",
                "priority", "nice", "num_threads", "it_real_value", "start_time",
                "vsize", "rss", "rsslim",
                "start_code", "end_code", "start_stack", "esp", "eip",
                "pending", "blocked", "sigign", "sigcatch",
                "wchan", "zero1", "zero2", "exit_signal", "cpu", "rt_priority", "policy"]
        
        status_values = {}
        with open(self.filename.format(gdb.selected_inferior().pid)) as status_f:
            line = status_f.readline()
        status = {k:v for k, v in zip(KEYS, line.split())}
        
        return status

__COUNTERS__ = [proc_stat_info] #, proc_io_info, proc_status_info]
