from collections import OrderedDict, defaultdict

import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

import gdb

cpt = 1

class counter_id_info():
    "counter id"
    name = "counter id"
    
    def __init__(self):
        self.__results = {"cid": None}
        
    def start(self):
        global cpt
        self.__results["cid"] = cpt
        cpt += 1
        
    def stop(self, paused=False):
        pass

    def to_log(self, ongoing=False):
        return self.__results

    @property
    def results(self):
        return self.__results
    
__COUNTERS__ = [counter_id_info]
