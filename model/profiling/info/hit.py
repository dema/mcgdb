from collections import OrderedDict, defaultdict

import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

from .. import capture

class hit_info():
    "Breakpoint hit counter"
    name = "Hit counter"
    
    breakpoints = {}
    
    def __init__(self, function="printf"):
        try:
            self.breakpoint = hit_info.breakpoints[function]
        except KeyError:
            self.breakpoint = \
                hit_info.breakpoints[function] = capture.HitCounter_Breakpoint(function)
            
        self.function = function
        self.hit_cnt = 0
        
    def start(self):
        self.breakpoint.register(self)

    def stop(self):
        self.breakpoint.unregister(self)

    def hit(self):
        self.hit_cnt += 1

    def to_log(self, ongoing=False):
        return self.results

    @property
    def results(self):
        return {"{} hits".format(self.function): self.hit_cnt}


__COUNTERS__ = [hit_info]
