#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <sys/types.h>
#include <unistd.h>
#define __USE_GNU
#include <dlfcn.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include<ctype.h>

/* better way in pseudo C:
 * #define PROCESS_INTERVAL ascii_to_long($PROCESS_INTERVAL_ADDR)
 * and
 * PROCESS_INTERVAL_ADDR=0x$(nm -a /usr/bin/perf | grep process_interval | cut -d" " -f1)
 */

#define PROCESS_INTERVAL_KEY "PROCESS_INTERVAL_ADDR"
char *atohx(char *dst, const char *src);
void (*process_interval)(void) = NULL;

void my_handler(int signum) {
    if (signum != SIGUSR2) {
      return;
    }
    
    process_interval();
    fprintf(stderr, "8<--8<--8<--8<--8<--8<--\n");
}

void init(void) __attribute__((constructor));

void init(void){
  char *process_interval_str = (void *) getenv(PROCESS_INTERVAL_KEY);
  if (!process_interval_str) {
    printf("PERF_PRELOAD ERROR: process_interval env variable (%s) not set.\n", PROCESS_INTERVAL_KEY);
    return;
  }
  //printf("PERF_PRELOAD DEBUG: parsing '%s'\n", process_interval_str);
  if (!atohx((char *) &process_interval, process_interval_str)) {
    printf("PERF_PRELOAD ERROR: process_interval address lookup failed ('%s').\n", process_interval_str);
    return;
  }
  
  printf("PERF_PRELOAD INFO: pid is %d\n", getpid());
  
  //printf("PERF_PRELOAD DEBUG: process_interval is at  %p\n", process_interval);
  signal(SIGUSR2, my_handler);
}


char *atohx(char *dst, const char *src) {   
    char *ret = dst;
    int pos = strlen(src);
    
    if (pos % 2 != 0) {
      printf("src length must be multiple of 2\n");
      *ret = 0;
      return NULL;
    }

    if ((pos*2) < sizeof(void *)*4) {
      printf("src is not long enough\n");
      *ret = 0;
      return NULL;
    }

    pos -= 2;
    if (src[0] == '0' && src[1] == 'x') {
      src += 2;
      pos -= 2;
    }
    
    for (int lsb, msb; pos >= 0; pos -= 2) {   
        msb = tolower(src[pos]);
        lsb = tolower(src[pos + 1]);
        
        msb -= isdigit(msb) ? 0x30 : 0x57;
        lsb -= isdigit(lsb) ? 0x30 : 0x57;
        if((msb < 0x0 || msb > 0xf) || (lsb < 0x0 || lsb > 0xf)) {
            *ret = 0;
            return NULL;
        }
        *dst++ = (char)(lsb | (msb << 4));
    }
    //*dst = 0;
    return ret;
}
