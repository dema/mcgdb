import logging; log = logging.getLogger("mcgdb.log.user")

import gdb

from mcgdb.toolbox import my_gdb

REGS = "rax","rbx","rcx","rdx","rsi","rdi","rbp","rsp","r8","r9","r10","r11","r12","r13","r14","r15","rip","eflags","cs","ss","ds","es","fs","gs"#x"pc", "rbp", "rsp", "r12", "r13", "r14", "r15"
UP_REGS = "rip", "rbp"
UP_UP_REGS = "rip", "rbp"

def save_thread_registers():
    reg_values = list(map(my_gdb.addr2num, map(gdb.newest_frame().read_register, REGS)))
    
    up_reg_values = list(map(my_gdb.addr2num, map(gdb.newest_frame().older().read_register, UP_REGS))) \
                         if gdb.newest_frame().older() else None
    
    
    up_up_reg_values = list(map(my_gdb.addr2num, map(gdb.newest_frame().older().older().read_register, UP_UP_REGS))) \
        if up_reg_values and  gdb.newest_frame().older().older() else None

    log.error("Thread registers saved.")
    
    return reg_values, up_reg_values, up_up_reg_values
    
def restore_thread_registers(regs):
    reg_values, up_regs, up_up_regs = regs
    
    gdb.execute("frame 0", to_string=True)
    for reg, val in zip(REGS, reg_values):
        gdb.execute("set ${} = {}".format(reg, val), to_string=True)
        #if reg == "rip": print("rip: 0x{}".format(hex(val)))
    gdb.execute("flushregs")
    log.error("Thread 1 registers restored.")
    if not up_regs: return
            
    for reg, val in zip(UP_REGS, up_regs):
        gdb.execute("frame 1", to_string=True)
        gdb.execute("set ${} = {}".format(reg, val), to_string=True)

        #if reg == "rip": print("rip: 0x{}".format(hex(val)))
    gdb.execute("flushregs")
    log.error("Thread 2 registers restored.")
    if not up_up_regs: return
            
    for reg, val in zip(UP_UP_REGS, up_up_regs):
        gdb.execute("frame 2", to_string=True)
        gdb.execute("set ${} = {}".format(reg, val), to_string=True)
        #if reg == "rip": print("rip: 0x{}".format(hex(val)))
        
    log.error("Thread 3 registers restored.")
