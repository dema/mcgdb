from collections import defaultdict

import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

import gdb

import mcgdb
import mcgdb.interaction
from mcgdb.toolbox import my_gdb
from . import capture, info, interaction, checkpoint

class Profile_info():
    def __init__(self, profile):
        self.firstlast = profile.__class__.instanciate_info()
        
        self.per_fct = []
        self.per_fct_stack = [] # current stack of profiles
        
        self.soustractive = []
        self.soustractive_stack = []
        
        self.inside = 0
        self.has_started = False

    # def results(self, firstlast=False, per_fct=False, soustractive=False):
    #     assert sum([firstlast, per_fct, soustractive]) == 1
        
    #     if firstlast:
    #         res = {}
    #         for info in self.firstlast:
    #             res.update(info.results)
    #         return res
        
    #     if per_fct or soustractive:
    #         res = defaultdict(list)
            
    #         infosets = self.per_fct if per_fct else self.soustractive
    #         for infoset in infosets:
    #             for info in infoset:
    #                 for k,v in info.results.items():
    #                     res[k].append(v)
    #         return res
            
@my_gdb.Numbered
class Profile():
    profiles = []
    breakpoints = []
    
    @classmethod
    def instanciate_info(clazz):
        return info.new_infoset()
    
    @property
    def thread_key(self):
        return gdb.selected_thread() if self.is_thread_specific else "appli"
        
    @property
    def per_thread(self):
        return self.per_thead_infos[self.thread_key]

    @property
    def thread_specific(self):
        return self.is_thread_specific
        
    @thread_specific.setter
    def thread_specific(self, value):
        assert not self.per_thead_infos # not populated yet

        self.is_thread_specific = value

    def shift_profile(self):
        assert not self.running
        
        self.per_thead_infos = defaultdict(lambda : Profile_info(self))
        self.all_per_thead_infos.append(self.per_thead_infos)
    
    def __init__(self):
        self.running = False
        
        self.is_thread_specific = False

        self.per_thead_infos = None
        self.all_per_thead_infos = []
        self.shift_profile()

        self.do_soustractive = True
        self.do_per_function = True
        self.do_first_last = True

        self.cumulative = False
        
        Profile.profiles.append(self)
        
    def start(self):
        self.running = True
        self.per_thread.inside += 1
        self.per_thread.has_started = True
        
        def firstlast():
            if self.per_thread.inside > 1: 
                return
        
            self.per_thread.firstlast.start()

        def per_fct():
            info_set = self.__class__.instanciate_info()
            
            self.per_thread.per_fct.append(info_set)
            self.per_thread.per_fct_stack.append(info_set)
            
            info_set.start()
            
        def soustractive():
            info_set = self.__class__.instanciate_info()
            
            self.per_thread.soustractive.append(info_set)
            self.per_thread.soustractive_stack.append(info_set)
            
            try:
                # stop the parent profile
                self.per_thread.soustractive_stack[-2].stop()
            except IndexError: # if any
                pass
            info_set.start()
            
        if self.do_first_last:
            firstlast()
        if self.do_per_function:
            per_fct()
        if self.do_soustractive:
            soustractive()
            
    def stop(self, complete_stop=False):
        self.running = False
        self.per_thread.inside -= 1
                
        def firstlast():
            if self.per_thread.inside >= 1: 
                return

            infoset = self.per_thread.firstlast
            infoset.stop()
            

            def need_to_stop():  
                for cpt, cmpt, val in Profile.breakpoints:
                    for res in infoset.get(cpt):
                        if any([cmpt == "==" and res == val,
                               cmpt == ">" and res > val,
                               cmpt == "<" and res < val]): return cpt, cmpt, val
                        
            stop = need_to_stop()
            if stop:
                log_user.warn("Profiling breakpoint: Found {}".format(" ".join(map(str,stop))))
                mcgdb.interaction.push_stop_request()
                
        def per_fct():
            info_set = self.per_thread.per_fct_stack.pop()
            
            info_set.stop()

        def soustractive():
            info_set = self.per_thread.soustractive_stack.pop()
            info_set.stop()
            
            try:
                if not complete_stop:
                    # restart the parent profile
                    self.per_thread.soustractive_stack[-1].start()
            except IndexError: # ... if any
                pass

        if self.do_first_last:
            firstlast()
        if self.do_per_function:
            per_fct()
        if self.do_soustractive:
            soustractive()
            
        if self.per_thread.inside == 0 and not self.cumulative:
            self.shift_profile()
            
class FunctionProfile(Profile):
    def __init__(self, args):
        Profile.__init__(self)
        self.bpt = capture.FunctionProfile_Breakpoint(args, self)

    def __str__(self):
        return "function profile[{}]".format(self.bpt.location)
    
class ManualProfile(Profile):
    def __init__(self):
        Profile.__init__(self)

    def __str__(self):
        return "manual profile"

    def start(self):
        if gdb.selected_inferior().pid == 0:
            # inferior not started yet
            capture.exec_at_main.append(lambda : Profile.start(self))
        else:
            Profile.start(self)

class ZoneProfile(Profile):
    def __init__(self, args, repeat=0):
        Profile.__init__(self)
        
        self.filename, _, lrange = args.rpartition(":")
        self.fname = self.filename
        self.cpt = None
        self.repeat = repeat
        
        assert lrange
        
        if not self.filename:
            self.filename = gdb.selected_frame().function().symtab.filename # raise exception if fails
            self.fname = self.filename.rpartition("/")[-1]
            
            assert self.filename
            
        self.lstart, self.lstop = lrange.split("-")
        
        capture.ZoneProfile_Breakpoint("{}:{}".format(self.filename, self.lstart), self, is_start=True)
        capture.ZoneProfile_Breakpoint("{}:{}".format(self.filename, self.lstop), self, is_start=False)


    def start(self):
        if self.repeat:
            log.critical("checkpoint")
            self.cpt = checkpoint.save_thread_registers()
            self.repeat -= 1

        Profile.start(self)
        
    def stop(self, complete_stop=False):
        if self.repeat:
            self.repeat -= 1
            log.critical("restart {}".format(self.repeat))
            checkpoint.restore_thread_registers(self.cpt)
            return 

        Profile.stop(self, complete_stop)

        if self.cpt:
            self.cpt = None
            log.critical("benchmark done")
            mcgdb.interaction.push_stop_request("benchmark done")
            
    def __str__(self):
        return "zone[{}:{}-{}]".format(self.fname, self.lstart, self.lstop)

def check_all_stop():
    first = True
    
    for prof in Profile.profiles:
        if not prof.running:
            continue
        
        if first:
            log_user.warn("Stopping profiles at exit ...")
            first = False
            
        log_user.warn("{} ...".format(prof))
        prof.stop(complete_stop=True)
        
def initialize():
    capture.exec_at_exit.append(check_all_stop)
