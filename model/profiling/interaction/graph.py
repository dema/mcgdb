from collections import defaultdict, OrderedDict

import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

import gdb
profile = None # resolved in initialize()


def sequential(record, name):
    join = " ; " if record and isinstance(record[0], str) else " "

    values = join.join(map(str, record))
    
    print('{} | {}'.format(name, values))
    
    
class cmd_profile_graph_plot (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "profile graph plot", gdb.COMMAND_OBSCURE)

    @staticmethod
    def details_to_graph(prof, profile_info, key, ongoing=False):

        def print_a_set(info_set):
            if key is None:
                keys = set()
            for info in info_set:
                results = info.to_log(ongoing)

                if not results:
                    continue

                if key is None:
                    keys.update(results.keys())
                    continue
                
                if key not in results:
                    continue
                
                if isinstance(results[key], str):
                    return results[key]
                try:
                    return results[key][0]
                except TypeError:
                    return results[key]
                
            if key is None:
                return keys
            
            log.info("Key '{}' not found ...".format(key))
            
        def print_sets(info_sets):
            return [print_a_set(info_set) for info_set in info_sets]
            
        return print_a_set(profile_info.firstlast)
        
    def invoke (self, args, from_tty):
        no_run_warn = " -no-run-warn" in args
        args = args.replace(" -no-run-warn", "")
        
        prof_id, _, key = args.partition(" ")
        prof_type = "first last"
        
        if prof_id.isdigit():
            prof_id = int(prof_id)
        elif prof_id == "all":
            pass
        else:
            log_user.error("Expected <profile id|all> <key> arguments.")
            return

        
        found = False
        to_all_graph = []
        for prof in profile.Profile.profiles:
            if prof_id not in ("all", prof.numbers[profile.Profile]):
                continue

            found = True
            
            if prof.running:
                if not no_run_warn:
                    log_user.warn("Skipping {} #{}, it's running".format(prof, prof.numbers[profile.Profile]))
                continue
            
            thread_key = prof.thread_key
            
            to_graph = defaultdict(list)
            for per_thread_dict in prof.all_per_thead_infos:
                per_thread = per_thread_dict[thread_key]

                to_graph["first last"].append(self.details_to_graph(prof, per_thread, key))

            if prof_id == "all":
                if to_graph[prof_type][0] is None:
                    pass # what to do here ??

                to_all_graph.append(to_graph[prof_type][0])
                continue
            
            if not to_graph:
                log.warn("Nothing to plot ...")
                
            break

        if prof_id == "all":
            sequential(to_all_graph, key)
        
        if not found:
            log_user.error("No profile with id={}".format(prof_id))
            
class cmd_profile_graph_plot_all (cmd_profile_graph_plot):
    def __init__ (self):
        gdb.Command.__init__(self, "profile graph plot-all", gdb.COMMAND_OBSCURE)
        
    def invoke (self, args, from_tty):
        prof_id, _, key = args.partition(" ")

        if prof_id.isdigit():
            prof_id = int(prof_id)
        elif prof_id == "all":
            pass

        keys = gdb.execute("profile graph keys {} -raw".format(args), to_string=True)
        for key in sorted(keys[:-1].split(",")):
            gdb.execute("profile graph plot {} {} -no-run-warn".format(args, key))

class cmd_profile_graph_keys(gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "profile graph keys", gdb.COMMAND_OBSCURE)
        
    def invoke (self, args, from_tty):
        prof_id, _, key = args.partition(" ")
        
        if prof_id.isdigit():
            prof_id = int(prof_id)
        elif prof_id == "all":
            pass
        elif not prof_id:
            prof_id = "all"
        else:
            log.error("Expected profile id or 'all' in parameters.")
            return
        
        raw = "-raw" in args
        if raw or prof_id == "all":
            key_set = set()
        
        for prof in profile.Profile.profiles:
            if prof_id not in ("all", prof.numbers[profile.Profile]):
                continue

            thread_key = prof.thread_key

            per_thread = prof.all_per_thead_infos[0][thread_key]

            keys = cmd_profile_graph_plot.details_to_graph(prof, per_thread, None)
            if not raw and not prof_id == "all":
                log_user.info("Profile id={}".format(prof.numbers[profile.Profile]))
                log_user.warn(", ".join(keys))
            else:
                map(key_set.add, keys)
        if raw:
            print(",".join(key_set))
        elif prof_id == "all":
            log_user.info("Keys for all the profiles:")
            log_user.warn(", ".join(key_set))
        
class cmd_profile_graph (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "profile graph", gdb.COMMAND_OBSCURE, prefix=True)
        self.loaded = False
        
    def invoke (self, args, from_tty):
        if self.loaded:
            if not args:
                log_user.warn("Already loaded ...")
            else:
                log_user.error("Invalid arguments: No subcommand called. ({})".format(args))
            return
        self.loaded = True
        
        on_load()

        if args:
            gdb.execute("profile graph {}".format(args))
            
def on_load():
    plot = cmd_profile_graph_plot()

    cmd_profile_graph_plot_all()
    cmd_profile_graph_keys()
    
def initialize():
    global profile
    from .. import profile
    
    cmd_profile_graph()
