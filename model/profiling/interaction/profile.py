from collections import OrderedDict
import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

import gdb
profile = None # resolved in initialize()

class cmd_profile_fct (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "profile function", gdb.COMMAND_OBSCURE)
        
    def invoke (self, args, from_tty):
        log_user.info("set profiling breakpoint on '{}'".format(args))
        
        try:
            profile.FunctionProfile(args)
        except Exception as e:
            log_user.error("{}: {}".format(e.__class__.__name__, e))

        
class cmd_profile_zone (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "profile zone", gdb.COMMAND_FILES)
        
    def invoke (self, args, from_tty):
        log_user.info("set profiling zone on '{}'".format(args))
        profile.ZoneProfile(args)

class cmd_profile_benchmark (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "profile benchmark", gdb.COMMAND_FILES)
        
    def invoke (self, args, from_tty):
        repeat, _, args = args.partition(" ")
        repeat = int(repeat)
        log_user.info("set benchmark zone on '{}' * {}".format(args, repeat))
        profile.ZoneProfile(args, repeat)

def initialize():
    global profile
    from .. import profile
    
    cmd_profile_fct()
    cmd_profile_zone()
