from collections import OrderedDict, defaultdict
import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

import gdb
profile = None # resolved in initialize()

from . import manage
from ..info import Unit

class cmd_profile_summ_on_exit (gdb.Command):
    connected = False

    @staticmethod
    def on_exit(evt):
        gdb.execute("profile info -full")
        cmd_profile_summ.connected = False
        gdb.events.exited.disconnect(cmd_profile_summ_on_exit.on_exit)
        
    def __init__ (self):
        gdb.Command.__init__(self, "profile summary on_exit", gdb.COMMAND_OBSCURE)
        
    def invoke (self, args, from_tty):
        if "off" in args:
             cmd_profile_summ_on_exit.connected = False
             try:
                 gdb.events.exited.disconnect(cmd_profile_summ_on_exit.on_exit)
             except Exception:
                 pass
             return
        if cmd_profile_summ_on_exit.connected:
            log_user.warn("Profile summary on exit already set.")
            return
        
        cmd_profile_summ_on_exit.connected = True
        gdb.events.exited.connect(cmd_profile_summ_on_exit.on_exit)
        log_user.warn("Profile summary on exit registered.")
    
class cmd_profile_summ_ongoing (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "profile summary ongoing", gdb.COMMAND_OBSCURE)
                
    def invoke (self, args, from_tty):
        try:
            prof_id = int(args)
        except Exception:
            prof_id = None
            
        for prof in profile.Profile.profiles:
            if prof_id is not None and prof_id != prof.numbers[profile.Profile]:
                continue

            if not prof.running:
                log_user.warn("Profile #{} is not running.".format(prof.numbers[profile.Profile]))

            prof.details_to_log(ongoing=True, firstlast=True, per_fct=True, soustractive=True)

class cmd_profile_info (gdb.Command):
    def __init__ (self, what="profile info"):
        gdb.Command.__init__(self, what, gdb.COMMAND_OBSCURE)
        
    def invoke (self, args, from_tty):
        do_stash = "-stash" in args
        do_config = "-config" in args
        do_full = "-full" in args
        
        try:
            prof_id, _, rest = args.partition(" ")
            prof_id = int(prof_id)
        except Exception:
            prof_id = None

        lst = manage.stash if do_stash else profile.Profile.profiles
        if do_stash:
            log_user.warn("*************")
            log_user.warn("*** STASH ***")
            log_user.warn("*************")
       
        for prof in lst:
            if prof_id is not None and prof.numbers[profile.Profile] != prof_id:
                continue

            log_user.warn("#{} {}".format(prof.numbers[profile.Profile], prof))
            #log_user.info("\t{} hits".format(len(prof.all_per_thead_infos)))

            if do_config:
                log_user.info("")
                log_user.info("\tPer-thread: {}".format(prof.thread_specific))
                log_user.info("")
                log_user.info("\tPer-function: {}".format(prof.do_per_function))
                log_user.info("\tSoustractive: {}".format(prof.do_soustractive))
                log_user.info("\tFirst last  : {}".format(prof.do_first_last))
                log_user.info("")
                
            if prof_id is None and not do_full:
                continue

            if not do_config and prof.thread_specific:
                log_user.warn("*** Per thread profile ***")
            
            cumul_len = len([profile_info for profile_info in prof.all_per_thead_infos if profile_info[prof.thread_key].has_started])
            
            for i, profile_info in enumerate(prof.all_per_thead_infos):
                thread_profile_info = profile_info[prof.thread_key]
                
                if not thread_profile_info.has_started:
                    continue
                
                if cumul_len != 1:
                    log_user.warn("*******")
                    log_user.warn("** {} **".format(i+1))
                    log_user.warn("*******")
                else:
                    #log_user.warn("Single iteration:")
                    pass
                self.details_to_log(prof, thread_profile_info, rest,
                                    firstlast=True, per_fct=True, soustractive=True)
                
    @staticmethod
    def details_to_log(prof, profile_info, args,
                       ongoing=False,
                       firstlast=False, per_fct=False, soustractive=False):
        log_user.info(prof)
        log_user.info(len(str(prof)) * "=")

        def print_sets(info_sets):
            for info_set in info_sets:
                print_a_set(info_set)
                log_user.info("***")
                
        def print_a_set(info_set):
            MAX_VALUE_LEN = 20
            
            res = OrderedDict()
            for info in info_set:
                results = info.to_log(ongoing)
                if results is False:
                    continue
                res.update(results)
                
            if not res:
                log_user.info("EMPTY")
                return

            max_len = max(len(k) for k in res)
            for k, v in res.items():
                if args and not args in k:
                    continue
                
                value, details = v if isinstance(v, tuple) else (v, "")

                if isinstance(value, float):
                    str_val = "{:.3f}".format(value)
                elif isinstance(value, int):
                    str_val = commify(value)
                else:
                    str_val = str(value)

                padding_value = (MAX_VALUE_LEN - len(str_val.partition(".")[0])) * " "
                    
                if isinstance(value, Unit):
                    str_val += " " + value.unit
                    
                if details:
                    str_val += " ({})".format(details)
                    
                padding_label = (max_len - len(k)) * " "
                log_user.info("".join([k, ": ", padding_label, padding_value, str_val]))
                
        if prof.do_first_last and firstlast:
            log_user.info("first start last stop")
            log_user.info("---------------------")

            print_a_set(profile_info.firstlast)
            
            if per_fct or soustractive:
                log_user.info("")

        if prof.do_per_function and per_fct:
            log_user.info("per function")
            log_user.info("------------")

            print_sets(profile_info.per_fct)
            
            if soustractive:
                log_user.info("")
                
        if prof.do_soustractive and soustractive:
            log_user.info("soustractive")
            log_user.info("------------")

            print_sets(profile_info.soustractive)
                
        if firstlast or per_fct or soustractive:
            log_user.info("---")

class InfoEntrySummary():
    def __init__(self, key):
        self.key = key
        self.values = {}

        self.Operators = [self.min, self.max, self.avg]
        
    def treat(self, value, prof):
        for op in self.Operators:
            op(value, prof)

    def min(self, value, prof):
        if isinstance(value, str): return
        try:
            mini, min_prf = self.values["min"]
            if value > mini: return
        except KeyError: pass # first pass
        
        self.values["min"] = value, prof
        
    def max(self, value, prof):
        if isinstance(value, str): return
        try:
            maxi, max_prf = self.values["max"]
            if value < maxi: return
        except KeyError: pass # first pass
        
        self.values["max"] = value, prof
        
    def avg(self, value, prof):
        if isinstance(value, str): return
        
        try:
            (prev, _what_), it = self.values["avg"]
            it += 1
            # iterative mean http://www.heikohoffmann.de/htmlthesis/node134.html
            new = prev + float(value) - prev*1/it 
        except KeyError:  # first pass
            new = value
            it = 0
        except Exception as e:
            import pdb;pdb.set_trace()
            return
        self.values["avg"] = new, it
            
class cmd_profile_summ (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "profile summary", gdb.COMMAND_OBSCURE, prefix=True)
                
    def invoke (self, args, from_tty):
        summ = {}

        def add_infoset(prof, infoset):
            for info in infoset:
                for k, v in info.to_log().items():
                    if k not in summ:
                        summ[k] = InfoEntrySummary(k)
                    
                    summ[k].treat(v, prof)
                    
        def print_infoset_summary():
            if not summ:
                print("No profile to print ...")
                return
            
            max_len = max(len(k) for k in summ.keys())
            for key, summary in summ.items():
                def str_val(k, v):
                    val = v[0]
                    if isinstance(val, float):
                        s = "{:.2f}".format(val)
                        s = s.partition(".00")[0]
                        if len(s) > 5:
                            s = s.partition(".")[0]
                    else:
                        s = str(val)

                    if isinstance(val, Unit):
                        s += " " + val.unit
                        
                    if k in ("min", "max"):
                        s += " #{}".format(v[1].numbers[profile.Profile])
                    return "{}: {}".format(k, s)
                
                kkey = key
                values = list(summary.values.values())
                if values and all([x[0] == values[0][0] for x in values]):
                    print("{}{} {} {}".format(kkey,
                                           " "*(max_len-len(kkey)),
                                           list(summary.values.values())[0][0],
                                           "/".join(summary.values.keys())))
                else:
                    for k, v in summary.values.items():
                        val = str_val(k, v)
                        print("{}{} {}".format(kkey,
                                               " "*(max_len-len(kkey)),
                                               val))
                        kkey = ""
                print("{}----".format("-"*(max_len-len(kkey))))
                
        for prof in profile.Profile.profiles:
            for i, profile_info in enumerate(prof.all_per_thead_infos):
                thread_profile_info = profile_info[prof.thread_key]
                
                if not thread_profile_info.has_started:
                    continue

                add_infoset(prof, thread_profile_info.firstlast)
        
        print_infoset_summary()
        
def commify(n):
    if n is None: return None
    
    sepdec = ","
    
    n = str(n)
    dollars, cents = n.split(sepdec) if sepdec in n else n, None

    r = []
    for i, c in enumerate(reversed(str(dollars))):
        if i and (not (i % 3)):
            r.insert(0, ",") # mon_thousands_sep
        r.insert(0, c)
    out = ''.join(r)
    if cents:
        out += '.' + cents # mon_decimal_point
    return out


def initialize():
    global profile
    from .. import profile
    
    cmd_profile_info()
    cmd_profile_info("info profiles")
    
    cmd_profile_summ()
    cmd_profile_summ_on_exit()
    cmd_profile_summ_ongoing()
