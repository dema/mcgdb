import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

import gdb
from mcgdb.toolbox import my_gdb
profile = None # resolved in initialize()

class cmd_profile_break (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "profile break", gdb.COMMAND_OBSCURE, prefix=True)

    def invoke (self, args, from_tty):
        if args == "clear":
            profile.Profile.breakpoints[:] = []
            
            log_user.info("Profiling breakpoints cleared")
            return
        elif args.startswith("del"):
            log_user.warn("Not implemented yet ...")
            return
        elif not args:
            for i, (cpt, cmpt, val) in enumerate(profile.Profile.breakpoints):
                print("#{} {} {} {}".format(i, cpt, cmpt, val))
            return
        
class cmd_profile_break_if (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "profile break if", gdb.COMMAND_OBSCURE)
        
    def invoke (self, args, from_tty):
        try:
            cpt, cmpt, val = args.split(" ")
            val = float(val) if "." in val else int(val)
            assert cmpt in ("==", "<", ">")
        except ValueError:
            log.warn("Argument format invalid. Expected '<cpt name> <comparator> <value>'")
            return
        
        profile.Profile.breakpoints.append((cpt, cmpt, val))
        
        log_user.info("Profiling breakpoint set on {}.".format(args))
        
@my_gdb.internal
def initialize():
    global profile
    from .. import profile
    
    cmd_profile_break()
    cmd_profile_break_if()
