import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

import gdb

from mcgdb.toolbox import my_gdb
from .. import info

profile = None # resolved in initialize()

def initialize():
    global profile
    from .. import profile

class cmd_info_counters(gdb.Command):
    profile = None
    
    def __init__ (self):
        gdb.Command.__init__(self, "info counters", gdb.COMMAND_OBSCURE)
        
    def invoke (self, args, from_tty):
        spacer_name = max(len(clazz.name) for clazz in info.info_counter_classes) + 1
        spacer_descr = max(len(clazz.__doc__) for clazz in info.info_counter_classes) + 1
        
        for i, clazz in enumerate(info.info_counter_classes):
            disabled = ((spacer_descr-len(clazz.__doc__))*" "+
                " # disabled") if clazz in info.disabled_counters else ""
            
            log_user.info("#{} {}:{}{}{}".format(i, clazz.name,
                                                 " "*(spacer_name-len(clazz.name)),
                                                 clazz.__doc__,
                                                 disabled))

class cmd_counters_disable(gdb.Command):
    profile = None
    
    def __init__ (self, do_disable):
        gdb.Command.__init__(self, "counters {}".format("disable" if do_disable else "enable"),
                             gdb.COMMAND_OBSCURE)
        self.do_disable = do_disable
        
    def invoke (self, args, from_tty):
        cid = int(args)

        clazz = info.info_counter_classes[cid]
        
        if self.do_disable:
            info.disabled_counters.add(clazz)
        else:
            info.disabled_counters.remove(clazz)

def initialize():
    gdb.Command("counters", gdb.COMMAND_OBSCURE, prefix=True)
    cmd_info_counters()
    cmd_counters_disable(do_disable=True)
    cmd_counters_disable(do_disable=False)
