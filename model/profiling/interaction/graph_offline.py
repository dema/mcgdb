from collections import defaultdict, OrderedDict
import time, shutil

import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

import gdb
    
def initialize():
    global profile
    from .. import profile
    try:
        gdb.execute("profile ")
    except gdb.error:
        gdb.Command("profile graph", gdb.COMMAND_OBSCURE, prefix=True)
    
    cmd_profile_offline_graph()

class  cmd_profile_offline_graph(gdb.Command):
    """ Line format:
    line name | <values>* [End-of-line modifiers]

End-of-line modifiers:
    y2: put on secondary axis
    y#: hide this dataset (use it for sorting ?)
    < : sort dataset, and use order for following datasets
    @n: pick only values == n, and apply to the following datasets
    / : divide dataset values with next dataset
    """
    def __init__ (self):
        gdb.Command.__init__(self, "profile graph offline", gdb.COMMAND_OBSCURE, prefix=True)
        
    def invoke (self, args, from_tty):
        import pygal

        import fileinput

        chart = pygal.Line()
        
        names = []
        do_div = False
        do_add = False
        do_pick = False
        do_sort = False
        do_label = False

        finput = fileinput.input()
        try:
            for line in finput:
                line = line[:-1]

                if not line:
                    break

                if line.startswith("|| "):
                    if do_label:
                        log_user.warn("Labels already let, ignoring this line.")
                        continue
                    do_label = True
                    line.replace("|| ", "")
                    chart.x_labels = line.split(";")
                    continue
                
                name, found, values = line.partition(" | ")
                if not found:
                    log_user.warn("Invalid format. Expected '<name> | <values>*'")
                    continue

                hidden = False
                secondary = False
                try:
                    if " y2" in values:
                        secondary = True
                        values = values.replace(" y2", "")
                    if " y#" in values:
                        hidden = True
                        values = values.replace(" y#", "")
                    if " @" in values:
                        values, _, do_pick = values.partition(" @")
                        do_pick = [float(do_pick), None, name]
                    
                    if " <" in values:
                        if names:
                            log_user.warn("Sorting indicator must be on the first line only. Ignoring this one.")
                        else:
                            do_sort = True
                            values = values.replace(" <", "")
                    
                    if " /" in values:
                        do_div = True
                        values = values.replace(" /", "")

                    if " +" in values:
                        do_add = True
                        values = values.replace(" +", "")
                        
                    cast = float if "." in values else int
                    values = list(map(cast, values.strip().split(" ")))
                    if not values:
                        raise ValueError("Values list is empty ...")
                except Exception as e:
                    log_user.warn("Parsing failed, the line above will be ignored ({})".format(e))
                    continue
  
                ##########
            
                if do_sort is True:
                    do_sort = values
                    values = sorted(do_sort)
                    name += " (sorted)"
                
                elif do_sort:
                    values = [y for (x,y) in sorted(zip(do_sort, values))]
                
                if do_pick and do_pick[1] is None:
                    do_pick[1] = values
                    continue

                ########
            
                elif do_pick:
                    value_to_pick, pick_values, pick_name = do_pick
                
                    name = "{} (picked from {} = {})".format(name, pick_name, value_to_pick)
                    values = [v for p, v in zip(pick_values, values) if p == value_to_pick]
                
                    log_user.info("Plotting {} | {}".format(name, values))

                ########
                if do_add is True:
                    do_add = name, values
                    continue
                elif do_add:
                    name = "{}+{}".format(do_add[0], name)
                    values = [a+b for a, b in zip(do_add[1], values)]
                    do_add = False
                    log_user.info("Plotting {} | {}".format(name, values))
                    
                if do_div is True:
                    do_div = name, values
                    continue
            
                elif do_div:
                    name = "{}/{}".format(do_div[0], name)
                    values = [a/b for a, b in zip(do_div[1], values)]
                    do_div = False
                    log_user.info("Plotting {} | {}".format(name, values))

                if not hidden:
                    chart.add(name, values, secondary=secondary)
                names.append(name)
        except KeyboardInterrupt:
            log_user.warn("Keyboard interruption received, aborting.")
        finally:
            try: finput.close()
            except Exception: pass # already closed, doesn't matter

        title = ", ".join(names)

        DEFAULT = '/tmp/chart', ".png"
        if args:
            target = args
        else:
            target = "".join((DEFAULT[0], time.strftime("-%Y%m%d-%H%M%S"), DEFAULT[1]))

        chart.title = title
        log_user.info("Rendering chart plot of {} into {}".format(title, target))
        chart.render_to_png(target)
        
        if not args:
            shutil.copy(target, "".join(DEFAULT))
