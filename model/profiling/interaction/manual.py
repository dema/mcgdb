import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

import gdb

from mcgdb.toolbox import my_gdb
    
profile = None # resolved in initialize()

class cmd_profile_manual_startstop (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "profile manual start_stop", gdb.COMMAND_OBSCURE)
        
    def invoke (self, args, from_tty):
        op = "start" if cmd_profile_manual_start.profile is None else "stop"
        gdb.execute("profile manual {}".format(op))
        
class cmd_profile_manual_start (gdb.Command):
    profile = None
    
    def __init__ (self):
        gdb.Command.__init__(self, "profile manual start", gdb.COMMAND_OBSCURE)
        
    def invoke (self, args, from_tty):
        if cmd_profile_manual_start.profile is not None:
            log.error("Manual profiling already ongoing ...")
            return
        
        log_user.info("start manual profiling ...")
        
        prof = cmd_profile_manual_start.profile = profile.ManualProfile()
        gdb.execute("profile configure {} first-last only".format(prof.numbers[profile.Profile]))
        prof.start()
        log_user.info("manual profiling started.")

class cmd_profile_manual_stop (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "profile manual stop", gdb.COMMAND_OBSCURE)
        
    def invoke (self, args, from_tty):
        if cmd_profile_manual_start.profile is None:
            log.error("No manual profiling ongoing ...")
            return

        silent = "-q" in args or "-quiet" in args
        log_user.info("stop manual profiling")

        prof = cmd_profile_manual_start.profile
        prof.stop()

        if not silent:
            gdb.execute("info profile {}".format(prof.numbers[profile.Profile]))
        cmd_profile_manual_start.profile = None

class cmd_profile_manual_interactive (gdb.Command):
    old_run =  {"code": None,
                "cnt": 1,
                "flags": None,
                "whole app": None}
    
    def __init__ (self):
        gdb.Command.__init__(self, "profile manual interactive", gdb.COMMAND_OBSCURE)
        
    def invoke (self, args, from_tty):
        if gdb.selected_inferior().pid == 0:
            log.error("The program must be running for the interactive profiling command to work.")
            return
        
        if cmd_profile_manual_start.profile is not None:
            log.error("Manual profiling already ongoing, please stop it first.")
            return

        whole_app = "-app" in args
        quiet = "-q" in args or "-quiet" in args

        flags = None
        if "-flags=" in args:
            flags = args.partition("-flags=")[-1]
            if flags.startswith('"'):
                flags = flags[1:].partition('"')[0]
            else:
                flags = flags.split(" ")[0]

            repeat_flags = flags.split(",")
            if len(repeat_flags) > 1:
                for repeat_flag in repeat_flags:
                    new_args = args.replace(flags, repeat_flag)
                    log_user.error("")
                    log_user.error("Profiling with flags={}".format(repeat_flag))
                    self.invoke(new_args, from_tty)
                    log_user.error("")
                return
                    
                
        if "-file" in args:
            use_file = args.partition("-file")[-1]
            if use_file.startswith('='): use_file = use_file[1:]
            if use_file.startswith(' '): use_file = use_file[1:]
            if use_file.startswith('"'):
                use_file = use_file[1:].partition('"')[0]
            else:
                use_file = use_file.split(" ")[0]
        else:
            use_file = None
                
        if whole_app:
            log.warn("Switching to thread 1, main frame.")
            gdb.execute("thread 1", to_string=True)
                
            old_frame = gdb.selected_frame()
            main_frame = my_gdb.oldest_frame()
            main_frame.select()
            
        if whole_app is not self.old_run["whole app"]:
            self.old_run["code"] = None

        code = it = None
        if "-redo" in args:
            use_file = self.old_run["use_file"]
            code = self.old_run["code"]
            it = self.old_run["cnt"]
            if not flags:
                flags = self.old_run["flags"]
            
        if not use_file and (not code or not it):
            it = input("How many iterations? ({}) ".format(self.old_run["cnt"]))
            try:
                if not it:
                    it = self.old_run["cnt"]
                else:
                    it = int(it)
                    assert it > 0
            except Exception as e:
                log.error("Invalid iteration counter '{}': {}".format(it, e))
                return

            if whole_app:
                args = ", ".join([e.name for e in main_frame.block() if e.is_argument])
                code = "{}({});".format(main_frame.function().name, args)
            else:
                print("Code to benchmak: (, )")
                print("- finish with 'end' or two blank lines")
                print("- use `i` as iteration counter")
                if self.old_run["code"]:
                    print("- let the line empty to reuse previous code")
                    
                code = ""
                while True:
                    read = input("> ")

                    if not code and not read:
                        code = self.old_run["code"]
                        break

                    # break after two blank lines
                    if not read:
                        read = input("> ")
                        if not read: break
                    
                    if read == "end": break
                    code += read + "\n"

            if not code.endswith(";"): code += ";"
                    
        self.old_run["code"] = code
        self.old_run["cnt"] = it
        self.old_run["flags"] = flags
        self.old_run["whole app"] = whole_app
        self.old_run["use_file"] = use_file
        
        gdb.execute("profile manual start")

        if not use_file:
            to_compile = """compile
int {i};
for ({i} = 0; i < {it}; {i}++) {{
  {code}
}} """.format(i="i", it=it, code=code)
        else:
            to_compile = "compile file {}".format(use_file)
            
        failed = False
        try:
            new_flags = gdb.parameter("compile-args")
            if flags:
                new_flags = flags + " " + new_flags
            with my_gdb.set_parameter("compile-args", new_flags):
                gdb.execute(to_compile, to_string=True)
        except Exception as e:
            log.error("Compilation failed ... {}".format(e))
            try:
                with my_gdb.set_parameter("compile-args", new_flags):
                    gdb.execute(to_compile, to_string=False)
            except Exception: pass
            self.old_run["code"] = None
            failed = True
            
        finally:
            gdb.execute("profile manual stop {}".format("-quiet" if failed else ""),
                        to_string=failed)
            
def initialize():
    global profile
    from .. import profile

    gdb.Command("profile manual", gdb.COMMAND_OBSCURE, prefix=True)
    cmd_profile_manual_startstop()
    cmd_profile_manual_start()
    cmd_profile_manual_stop()
    cmd_profile_manual_interactive()
