import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

import gdb

from . import graph, manual, info, manage, profile, summary, break_if
from . import counters, graph_offline

def initialize():    
    gdb.Command("profile", gdb.COMMAND_OBSCURE, prefix=True)

    
    
