import subprocess

import gdb

import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

DEFAULT = "instructions,cycles,task-clock"

def perf_list():
    command = ['perf', 'list']
    perf = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = perf.communicate()
    perf.wait()
    
    assert perf.returncode is not None
    
    try:
        stdout = stdout.decode("ascii")
    except Exception:
        pass
    
    return stdout

def test_perf_counters(counters):
    command = ['perf', 'stat', '-x,',
               '-e', counters,
               'sleep', '0.001']
    
    perf = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = perf.communicate()
    perf.wait()
    assert perf.returncode is not None
    
    try:
        stderr = stderr.decode("ascii")
    except Exception:
        pass
    
    return perf.returncode == 0, stderr

class param_perf_counters(gdb.Parameter):
    """See `perf list` for more details about the expected values."""
    
    __self = None

    @classmethod
    def get(clazz):
        try:
            return clazz.__self.value
        except ValueError:
            # not initialized
            return DEFAULT

    def __init__ (self):
        self.show_doc = \
        self.set_doc = """
Set of counters used for profiling with `perf stat -e ...` :

{}
    """.format(perf_list())
        
        gdb.Parameter.__init__(self, "profile-perf-counters",
                               gdb.COMMAND_OBSCURE,
                               gdb.PARAM_STRING)
        self.value = DEFAULT
        assert param_perf_counters.__self is None
        param_perf_counters.__self = self
        
        self.prev = DEFAULT
        
    def get_set_string(self):
        ok, stderr = test_perf_counters(self.value)
        if ok:
            self.prev = self.value
            return "perf profiling counters set to '{}'".format(self.value)
        else:
            bad_val = self.value
            self.value = self.prev
            
            log_user.error("Could not set perf profiling counters to '{}'.".format(bad_val))
            log_user.warning(stderr)
            
            return "perf profiling counters unchanged ('{}')".format(self.prev)
        
    def get_show_string (self, svalue):
        return "perf profiling counters are '{}'".format(svalue)

def initialize():
    param_perf_counters()
