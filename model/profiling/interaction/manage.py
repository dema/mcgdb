from collections import OrderedDict
import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")

import gdb
profile = None # resolved in initialize()

stash = []
            
class cmd_profile_delete (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "profile delete", gdb.COMMAND_OBSCURE)

    def invoke (self, args, from_tty):
        do_stash = "-stash" in args
        do_unstash = "-unstash" in args

        assert not (do_unstash and do_stash)
        
        if args == "all":
            prof_id = -1
        else:
            try:
                prof_id = int(args)
            except ValueError:
                log.error("Cannot parse profile id in '{}'".format(args))
                return
        lst = stash if do_unstash else profile.Profile.profiles
        
        for prof in list(lst):
            if prof_id != -1 and prof.numbers[profile.Profile] != prof_id:
                continue
            
            lst.remove(prof)
            if do_stash:
                cmd_profile_delete.stash.append(prof)

            action = "stashed" if do_stash else "unstashed" if do_unstash else "removed"
            log_user.info("Profile #{} '{}' {}.".format(prof.numbers[profile.Profile], prof, action))

class cmd_profile_reset (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "profile reset", gdb.COMMAND_OBSCURE)

    def invoke (self, args=None, from_tty=None):
        for profile in self.profiles:
            profile.reset()
            
class cmd_profile_configure (gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "profile configure", gdb.COMMAND_OBSCURE)

    def invoke (self, args, from_tty):
        prof_id, _, conf = args.partition(" ")
        try:
            prof_id = int(prof_id)
        except ValueError:
            log_user.error("Cannot parse profile id ({}), it must be a number.".format(prof_id))
            return
            
        conf, _, conf_val = conf.partition(" ")
        
        for prof in list(profile.Profile.profiles):
            if prof.numbers[profile.Profile] != prof_id:
                continue

            if prof.per_thead_infos: # already populated
                log_user.error("Profile #{} already started, ".format(prof_id)+
                                "cannot configure it anymore.")
                return
            
            if conf == "per-thread":
                log_user.info("Setting profile #{} per thread.".format(prof.numbers[profile.Profile]))
                prof.thread_specific = True

                break

            HOW_PROFILE = {"soustractive": "do_soustractive",
                           "first-last": "do_first_last",
                           "per-function": "do_per_function"}
            
            if conf in HOW_PROFILE:
                try:
                    VALID_VALUES = {"only": None,
                                    "on": True,
                                    "off": False}
                    val = VALID_VALUES[conf_val.lower()]
                except KeyError:
                    log_user.errror("Configuration value '{}' not allowed ({})".format(conf_val, ",".join(VALID_VALUES)))
                    return

                
                if val is None: # set CONF to True, rest to False
                    for opt, attr in HOW_PROFILE.items():
                        setattr(prof, attr, True if conf == opt else False)
                else: # set CONF to *val
                    setattr(prof, HOW_PROFILE[conf], val)
                    
                break
            
            log_user.warn("Option '{}' not recognized.".format(conf))
            break
        else:
            log_user.warn("Could not find a profile with id={}".format(prof_id))

def initialize():
    global profile
    from .. import profile
    
    cmd_profile_delete()
    
    cmd_profile_configure()
    cmd_profile_reset()
