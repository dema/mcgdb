import os, sys
import logging
import logging.config
from collections import OrderedDict

log = logging.getLogger(__name__)

MOD_LOGFORMAT = "| %(name)s | %(log_color)s%(message)s%(reset)s"
USR_LOGFORMAT = "| %(log_color)s%(message)s%(reset)s"

class GdbOutputFileWrapper(object):
    wrapped = None
    
    def __init__(self, wrapped):
        GdbOutputFileWrapper.wrapped = wrapped
        
    def __getattribute__(self, name):
        if name == "wrapped":
            return GdbOutputFileWrapper.wrapped
        if name == "closed":
            return False
        else:
            return self.wrapped.__getattribute__(name)

try:
    # avoid bug with colorlog atexit trying to access sys.stdout.closed
    # which doesn't exist in gdb.GdbOutputFile
    
    sys.stdout = GdbOutputFileWrapper(sys.stdout) 

    import colorlog
except ImportError as e:
    colorlog = None # warn later, when logging is configured 

def log_fmt(mod=False, usr=False):
    assert usr ^ mod # xor
    
    fmt = MOD_LOGFORMAT if mod else USR_LOGFORMAT

    if colorlog is None:
        fmt = fmt.replace("%(log_color)s", "")\
                 .replace("%(reset)s", "")
        fmt = "%(levelname)-8s" + fmt
        return {'format' : fmt}
    else:
        return {
            '()' : "colorlog.ColoredFormatter",
            'fmt' : fmt
            }
    
LOG_CONFIG = {
    'version': 1,
    'formatters': {
        'mod' : log_fmt(mod=True),
        'usr' : log_fmt(usr=True)
        },
    'handlers': {
        'mod_stdout': {
            'class': 'logging.StreamHandler',
            'formatter': 'mod',
            'stream': 'ext://sys.stdout',
            'level': "__MCGDB_LOG_MOD_OUT_LEVEL"
            },
        "mod_file": {
            "class": "logging.handlers.RotatingFileHandler",
            "formatter": "mod",
            "maxBytes": 10485760,
            "backupCount": 1,
            "encoding": "utf8",
            
            "filename": "__MCGDB_LOG_MOD_FILE_NAME",
            "level":    "__MCGDB_LOG_MOD_FILE_LEVEL"
            },
        
        'usr_stdout': {
            'class': 'logging.StreamHandler',
            'formatter': 'usr',
            'stream': 'ext://sys.stdout',
            
            'level': "__MCGDB_LOG_USR_OUT_LEVEL"
            },
        "usr_file": {
            "class": "logging.handlers.RotatingFileHandler",
            "formatter": "usr",
            "maxBytes": 10485760,
            "backupCount": 1,
            "encoding": "utf8",
            
            "filename": "__MCGDB_LOG_USR_FILE_NAME",
            "level":    "__MCGDB_LOG_USR_FILE_LEVEL"
            },
        },
    
    'loggers': {
        '' : {'level': 'DEBUG',
              'handlers': ['mod_stdout', "mod_file"]},
        
        'mcgdb': {'level': 'DEBUG'},
        
        'mcgdb.log': {'level': 'DEBUG',
                      'handlers': ['mod_stdout', "mod_file"],
                      'propagate': False},
        
        'mcgdb.log.user': {'level': 'DEBUG',
                           'handlers': ['usr_stdout', 'usr_file', 'mod_file'],
                           'propagate': False},
        }
    }

DEFAULT_ENV = { # can be changed through environment variables
    # mcgdb.log.*
    "MCGDB_LOG_MOD_OUT_LEVEL": "WARN",
    "MCGDB_LOG_MOD_FILE_NAME": "/tmp/mcgdb.log",
    "MCGDB_LOG_MOD_FILE_LEVEL": "DEBUG",

    # mcgdb.log.user.*
    "MCGDB_LOG_USR_OUT_LEVEL": "INFO",
    "MCGDB_LOG_USR_FILE_NAME": "/tmp/mcgdb_user.log",
    "MCGDB_LOG_USR_FILE_LEVEL": "DEBUG"
    }


changed_properties = OrderedDict()
def env_to_dictconfig(env):
    PREFIX = "__"
    PREFIX_LEN = len(PREFIX)
    
    mcgdb_env = dict(DEFAULT_ENV)
    mcgdb_env.update(env)

    def replace(path, where):
        for key, value in where.items():
            if isinstance(value, dict):
                replace("{}.{}".format(path, key), value)
            elif str(value).startswith("__"):
                new_val = mcgdb_env[value[PREFIX_LEN:]]
                where[key] = new_val
                changed_properties["{}.{}".format(path[1:], key)] = new_val
    
    replace("", LOG_CONFIG)

def setup_log():
    env_to_dictconfig(os.environ)
    logging.config.dictConfig(LOG_CONFIG)

def info_log():
    if not colorlog:
        log.warn("Colorlog python (pip) package not installed, defaulting to basic logs.")

    key_len = max(map(len, changed_properties.keys()))
    for key, value in changed_properties.items():
        log.info("Log config: {0: <{1}} --> {2}".format(key, key_len, value))

    log.debug("*********************************\n")

    log_user = logging.getLogger("mcgdb.log.user")
    log_user.debug("*********************************\n")
    
    log_user.error("Full logs saved in {} and {}".format(
            LOG_CONFIG["handlers"]["mod_file"]["filename"],
            LOG_CONFIG["handlers"]["usr_file"]["filename"]))
