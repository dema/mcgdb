import gdb
from mcgdb.toolbox import my_gdb
import logging; log = logging.getLogger("mcgdb.log.user")

REGS = "rax","rbx","rcx","rdx","rsi","rdi","rbp","rsp","r8","r9","r10","r11","r12","r13","r14","r15","rip","eflags","cs","ss","ds","es","fs","gs"#x"pc", "rbp", "rsp", "r12", "r13", "r14", "r15"
UP_REGS = "rip", "rbp"
UP_UP_REGS = "rip", "rbp"

saved_registers = []
def log_thread_registers():
    for thread in gdb.selected_inferior().threads():
        with my_gdb.active_thread(thread):
            ptid = thread.ptid

            reg_values = map(long, map(gdb.newest_frame().read_register, REGS))
            up_reg_values = map(long, map(gdb.newest_frame().older().read_register, UP_REGS))
            try:
                up_up_reg_values = map(long, map(gdb.newest_frame().older().older().read_register, UP_UP_REGS))
            except AttributeError:
                log.error("no up up frame")
                up_up_reg_values = []
                
            log.info("thread {}{}".format(thread.num, " (main thread)" if ptid[0] == ptid[1] else ""))

            log.info(", ".join(["{} = {}".format(reg, hex(int(val))) \
                                 for reg, val in zip(REGS, reg_values)]))
            log.info(", ".join(["{} = {}".format(reg, hex(int(val))) \
                                 for reg, val in zip(UP_REGS, up_reg_values)]))
            log.info(", ".join(["{} = {}".format(reg, hex(int(val))) \
                                 for reg, val in zip(UP_UP_REGS, up_up_reg_values)]))
            
def save_thread_registers():
    log.error("Saving thread registers.")
    for thread in gdb.selected_inferior().threads():
        with my_gdb.active_thread(thread):
            ptid = thread.ptid
            #if ptid[0] == ptid[1]: continue
            reg_values = map(long, map(gdb.newest_frame().read_register, REGS))
            
            up_reg_values = map(long, map(gdb.newest_frame().older().read_register, UP_REGS)) \
                if gdb.newest_frame().older() else None
            
                
            up_up_reg_values = map(long, map(gdb.newest_frame().older().older().read_register, UP_UP_REGS)) \
                if up_reg_values and  gdb.newest_frame().older().older() else None
                    
            saved_registers.append((reg_values, up_reg_values, up_up_reg_values))
    assert len(saved_registers) == len(gdb.selected_inferior().threads()) 
    
def restore_thread_registers():
    log.error("Restoring thread registers.")
    assert len(saved_registers)  == len(gdb.selected_inferior().threads())
    for thread in gdb.selected_inferior().threads():
        with my_gdb.active_thread(thread):
            ptid = thread.ptid
            #if ptid[0] == ptid[1]: continue
            reg_values, up_regs, up_up_regs = saved_registers.pop(0)
            gdb.execute("frame 0")
            for reg, val in zip(REGS, reg_values):
                gdb.execute("set ${} = {}".format(reg, val))
                if reg == "rip": print("rip: 0x{}".format(hex(val)))
            gdb.execute("flushregs")

            if not up_regs: continue
            
            for reg, val in zip(UP_REGS, up_regs):
                gdb.execute("frame 1")
                gdb.execute("set ${} = {}".format(reg, val))
                if reg == "rip": print("rip: 0x{}".format(hex(val)))
            gdb.execute("flushregs")

            if not up_up_regs: continue
            
            for reg, val in zip(UP_UP_REGS, up_up_regs):
                gdb.execute("frame 2")
                gdb.execute("set ${} = {}".format(reg, val))
                if reg == "rip": print("rip: 0x{}".format(hex(val)))
    log.error("Thread registers restored.")
            
class Checkpoint(gdb.Breakpoint):
    def __init__(self):
        gdb.Breakpoint.__init__(self, "checkpoint", internal=True)
        self.cpt = 0
        self.silent = True
        
    def stop(self):
        self.cpt += 1
        if self.cpt == len(gdb.selected_inferior().threads()):
            log.error("Stopped at checkpoint.")
            self.cpt = 0
            return True
        else:
            return False

class Restart(gdb.Breakpoint):
    def __init__(self):
        gdb.Breakpoint.__init__(self, "restart", internal=True)
        self.cpt = 0
        self.silent = True
        
    def stop(self):
        self.cpt += 1
        if self.cpt == len(gdb.selected_inferior().threads()):
            log.error("Stopped at restart.")
            self.cpt = 0
            return True
        else:
            log.error("HIT restart with {}.".format(self.cpt))
            return False
        
class Forked(gdb.Breakpoint):
    def __init__(self):
        gdb.Breakpoint.__init__(self, "forked", internal=True)
        self.silent = True
        
    def stop(self):
        return True
    
class go_cmd(gdb.Command):
    def __init__ (self):
        gdb.Command.__init__(self, "go", gdb.COMMAND_OBSCURE)
        
    def invoke (self, args, from_tty):
        if "dbg" in args:
            import pdb;pdb.set_trace()
            
        gdb.execute("set detach-on-fork off")
        gdb.execute("start")
        gdb.execute("set debugger_attached = 1")
        gdb.execute("cont")

        if "check" in args:
            return
        save_thread_registers()
        
        if "saved_regs" in args:
            log_thread_registers()

        gdb.execute("thread 1")
        gdb.execute("set first_pass_all_threads_arrived = 1")
        gdb.execute("set scheduler-locking on")
        gdb.execute("set nb_threads = {}".format(len(gdb.selected_inferior().threads())))
        gdb.execute("finish")
        gdb.execute("set scheduler-locking off")
        gdb.execute("inferior 1")
        log.error("CHECKPOINTED")
        
        if "fork" in args:
            return
        
        gdb.execute("cont")
        
        log.error("RESTART")
        gdb.execute("kill")
        gdb.execute("inferior 2")
        #gdb.execute("remove-inferiors 1")
        gdb.execute("set var never_restarted = 0")
        gdb.execute("cont")
        log.error("THREADED")
        if "thread" in args:
            return
        
        restore_thread_registers()
        
        if "reload_regs" in args:
            log_thread_registers()


Checkpoint()
Restart()
Forked()
go_cmd()
