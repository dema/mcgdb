import inspect
import sys
import logging; log = logging.getLogger(__name__)
import traceback

PY3 = sys.version_info >= (3, 0)

from mcgdb.toolbox import python_utils

tracked = []
class Tracker(object):
    def __new__(cls, *args, **kwargs):
        obj = object.__new__(cls)
        
        if cls not in tracked:
            tracked.append(cls)
            
            orig_init = cls.__init__
            def new_init (self, *args, **kws):
                do_tracking(orig_init, self, args, kws,
                            is_init=True, before=True)
                orig_init(self, *args, **kws)

                do_tracking(orig_init, self, args, kws,
                            is_init=True, after=True)

            cls.__init__ = new_init
            
        return obj
    
    def __getattribute__(self, name):
        attr = object.__getattribute__(self, name)
    
        if not hasattr(attr, '__call__') or name is "__class__":
            return attr
        
        if not self.__class__.__module__.startswith("mcgdb"):
            return attr
        
        def newfunc(*args, **kwargs):
            do_tracking(attr, self, args, kwargs, before=True)
            ret = attr(*args, **kwargs)
            do_tracking(attr, self, args, kwargs, after=True, ret=ret)
            return ret
        
        return newfunc

tracked_classes = []
def Tracked(clazz, manual=False):
    if not manual:
        # arrived here through the decorator
        orig_init = clazz.__init__
        tracked_classes.append(clazz)
    
        def new_init (self, *args, **kws):
            if self.__class__ not in tracked_classes:
                Tracked(self.__class__, manual=True)
                
            orig_init(self, *args, **kws)
        clazz.__init__ = new_init
        
    def track(meth):
        def new_meth (self, *args, **kws):
            do_tracking(meth, self, args, kws)
            return meth(self, self, *args, **kws)
            
        return new_meth
        
    for name, meth in inspect.getmembers(clazz, predicate=inspect.ismethod):
        #if name is "__init__": continue
        # ignore @classmethod methods
        if meth.__self__ is clazz: continue
        
        setattr(clazz, name, track(meth))
        
    return clazz

def logger(func):
    def inner(*args, **kwargs): #1
        print ("Arguments were: %s, %s" % (args, kwargs))
        return func(*args, **kwargs) #2
    return inner


class HelpTracking:
    meth_to_argnames = {}
    
    def __init__(self, self_to_this, meth, meth_self, meth_args, meth_kwargs,
                 is_init, is_before, is_after, ret, trackers):
        self.trackers = trackers
        self.self_to_this = self_to_this
        self.meth = meth
        self.meth_self = meth_self
        self.meth_args = meth_args
        self.meth_kwargs = meth_kwargs
        self.is_init = is_init
        assert not (is_before and is_after)
        self.is_before = is_before
        self.is_after = is_after
        self.ret = ret
        
    def get_args(self):
        # we can't access meth_args name for inits,
        # so assignation has to be done manually
        args = python_utils.SimpleClass()
        args.update(self.meth_kwargs)
        
        if not self.is_init:
            try:
                meth_arg_names = HelpTracking.meth_to_argnames[self.meth.__func__]
            except KeyError:
                log.info("Lookup arguments of {}".format(self.meth.__func__))
                if PY3:
                    meth_arg_names = inspect.getargspec(self.meth).args
                else:
                    meth_arg_names = self.meth.im_func.func_code.co_varnames
                    
                HelpTracking.meth_to_argnames[self.meth.__func__] = meth_arg_names

                
            args.update(dict(zip(meth_arg_names,
                                 (self.meth_self,)+self.meth_args)))
                    
        return args
    
    def route(self):
        if self.is_init:
            func_name = "__init__"
        else:
            func_name = (self.meth.__name__ if PY3 
                         else self.meth.im_func.func_name)

        meth_class = (self.meth_self.__class__ if PY3 
                      else self.meth.im_class)
        if meth_class not in self.trackers: return
        tracker = self.trackers[meth_class]
        
        if not hasattr(tracker, func_name): return

        tracking_meth = getattr(tracker, func_name)

        if PY3:
            arg_names = inspect.getargspec(tracking_meth).args
        else:
            tracking_meth = tracking_meth.im_func
            arg_names = tracking_meth.func_code.co_varnames

        has_before = "before" in arg_names
        has_after = "after" in arg_names
        has_ret = "ret" in arg_names
        
        if self.is_before and not has_before:
            return
        if has_after and self.is_before:
            return
        
        if self.is_init:
            if has_before and not self.is_before:
                this = self.self_to_this[self.meth_self]
            else:
                # storage associated with each entity tracked
                this = self.self_to_this[self.meth_self] = python_utils.SimpleClass()
                this.self_to_this = self.self_to_this    
        else:
            try:
                this = self.self_to_this[self.meth_self]
            except KeyError:
                # `this` was not yet created, skip this call.
                return
            
        this.args = self.get_args()
        this.self = self.meth_self
        this.meth_args = self.meth_args
        this.meth_kwargs = self.meth_kwargs
        
        args = [this]
        kwargs = {}
        
        if self.is_before:
            kwargs["before"] = True

        if self.is_after and has_after:
            kwargs["after"] = True

            if has_ret:
                kwargs["ret"] = self.ret
                
        try:
            tracking_meth(this, **kwargs)
        except Exception as e:
            name = tracking_meth.__qualname__ if PY3 else \
                "{}.{}".format(tracking_meth.__module__, tracking_meth.func_name)
            log.warning("Tracking method '%s' failed: %s", name, e)
            log.warning(traceback.format_exc())
            _type, value, tb = sys.exc_info()
            
            import pdb; pdb.post_mortem(tb)

registered_trackers = {}
def register(name, aspect_classes, pre_aspect=None):
    aspect_trackers = {}
    def Tracks(what):
        def internal(clazz):
            aspect_trackers[what] = clazz
            return clazz
        return internal

    aspect_classes(Tracks)
    
    registered_trackers[name] = (pre_aspect, aspect_trackers, {})
    
def do_tracking(meth, meth_self, meth_args, meth_kwargs,
                is_init=False, before=False, after=False, ret=None):
    for name, (pre_aspect, aspect_trackers, self_to_this) \
            in registered_trackers.items():
        help_tracking = HelpTracking(self_to_this, meth, meth_self,
                                     meth_args, meth_kwargs, is_init, before, after,
                                     ret, aspect_trackers)
        
        args = help_tracking.get_args()
        self = help_tracking.meth_self

        if pre_aspect is not None:
            pre_aspect(help_tracking, self, args)
        
        help_tracking.route()
        
