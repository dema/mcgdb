from . import get_target

def access(path, mode):
    return get_target(access_pkg).access(path, mode)

def exists(path):
    return get_target(access_pkg).exists(path)

def listdir(path):
    return get_target(access_pkg).listdir(path)

def readlink(path):
    return get_target(access_pkg).readlink(path)

class path:
    def __init__(self):
        pass
    @staticmethod
    def exists(path):
        return get_target(access_pkg).path.exists(path)

#problem with this one
F_OK =  1 #get_target(access_pkg).F_OK
class Popen():
    def __init__(self, args):
        self.proc = get_target(access_pkg).Popen(args)
    
    def communicate(self):
        return self.proc.communicate()
        
    def __getattr__(self, name):
        if name in ["returncode", "pid"]:
            return getattr(self.proc, name)
        else:
            raise AttributeError("Can't access attribute '%s' of subprocess" %
                                 name)
            
class Fopen:
    def __init__(self, path, mode):
        self.file = get_target(access_pkg).Fopen(path, mode)
        
    def readlines(self):
        return self.file.readlines()
    
def do_attach(pid):
    get_target(access_pkg).do_attach(pid)

def initialize():
    import mcgdb.toolbox.target.access as access_pkg
