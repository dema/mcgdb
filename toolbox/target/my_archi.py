import gdb

# set during initialization phase
archi = None
get_target = None

VOID = None
VOID_P = None
VOID_PP = None
CHAR = None
CHAR_P = None
CHAR_PP = None
INT = None
INT_P = None
INT_108A = None
ULONG = None
UINT = None

def initialize_types():
    """
    Initialize types global variables with common types.
    """
    global VOID, VOID_P, VOID_PP
    global CHAR, CHAR_P, CHAR_PP
    global INT, INT_P, INT_108A
    global ULONG, UINT
    
    VOID = gdb.lookup_type("void")
    VOID_P = VOID.pointer()
    VOID_PP = VOID_P.pointer()
    
    CHAR = gdb.lookup_type("char")
    CHAR_P = CHAR.pointer()
    CHAR_PP = CHAR_P.pointer()
    
    INT = gdb.lookup_type("int")
    INT_P = INT.pointer()
    INT_108A = INT.array(108)

    UINT = gdb.lookup_type("unsigned int")
    ULONG = gdb.lookup_type("unsigned long")
    
def first_arg(ttype=None):
    return nth_arg(1, ttype)

def second_arg(ttype=None):
    return nth_arg(2, ttype)

def third_arg(ttype=None):
    return nth_arg(3, ttype)

def fourth_arg(ttype=None):
    return nth_arg(4, ttype)

def nth_arg(n, ttype=None):
    try:
        frame = gdb.selected_frame()
        arg = [e for e in frame.block() if e.is_argument][n-1] # hello
        ret = gdb.selected_frame().read_var(arg)
    except IndexError:
        ret = get_target(archi).nth_arg(n, ttype)
    except RuntimeError: #Cannot locate block for frame.
        ret = get_target(archi).nth_arg(n, ttype)
        
    if ret is None:
        raise IndexError("Architecture '%s' could't return %dth argument" %
                         (get_target(archi).__name__), n)
    
    return ret

def return_value(ttype=None):
    return get_target(archi).return_value(ttype)

def read_value(where, ttype=None):
    """
    Reads the memory location `where` with type `ttype`.

    :param where: location to read
    :param ttype: C type of `where`. Default: void *
    """
    ttype = VOID_P if ttype is None else ttype

    frame = gdb.selected_frame()
    if where.startswith("$"):
        return frame.read_register(where[1:]).cast(ttype)
    else:
        to_parse = "(%s) %s" % (str(ttype), where)
        return gdb.parse_and_eval(to_parse)

def load_types_on_first_new_objfile(evt):
    gdb.events.new_objfile.disconnect(load_types_on_first_new_objfile)
    initialize_types()
    
def initialize():
    global get_target
    global archi
    
    from . import archi
    from . import get_target

    gdb.events.new_objfile.connect(load_types_on_first_new_objfile)
    
