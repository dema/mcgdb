my_access = None

def init(my_gdb_pkg):
    global my_access
    from .. import my_access as access_pkg
    my_access = access_pkg

def is_alive(pid):
    return my_access.access("/proc/%d" % pid, my_access.F_OK)