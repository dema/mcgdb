from . import get_target, system

def is_alive(pid):
    return get_target(system).is_alive(pid)
