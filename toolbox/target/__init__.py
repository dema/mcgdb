import pkgutil 
import sys 
import logging; log = logging.getLogger(__name__)
log_user = logging.getLogger("mcgdb.log.user")
import gdb

from . import access, archi, system

from .. import python_utils

# package type --> implementation
current_impls = {}
# package type --> (list of implementations)
impls_list = {}

@python_utils.internal
def initialize():
    """
    Initializes the target stack (access, archi and system).
    Look into the three submodules, and initialize them.
    """
    from mcgdb.toolbox import my_gdb
    
    for ttype in [access, archi, system]:
        prefix = ttype.__name__ + "."
        type_name = ttype.__name__.split(".")[-1]
        impls_list[ttype] = {}
        for importer, impl_name, ispkg in pkgutil.iter_modules(ttype.__path__, prefix):
            try:
                impl = sys.modules[impl_name]
                impl_name = impl_name.split(".")[-1]
                log_user.debug("Found target %s.%s", type_name, impl_name)
                
                impl.init(my_gdb)
                impls_list[ttype][impl_name] = impl
            except KeyError as ke:
                pass
            except Exception as e:
                log.warn("Couldn't load target %s (%s)", impl_name, e)
        
        current_impls[ttype] = ttype.prefered
    
def get_target(ttype):
    """
    Returns the relevant implementation for the target type asked.
    :param ttype: archi, system or access submodule.
    :returns: the relevant implementation of the asked type.
    """
    
    if ttype == archi:
        str_archi = gdb.execute("show architecture", to_string=True) 
        if current_impls[archi].recognize(str_archi):
            return current_impls[archi]
        else:
            for impl in impls_list[ttype].values():
                if impl.recognize(str_archi):
                    current_impls[archi] = impl
                    return impl
            else:
                log.exception("No architecture found for '%s'", 
                                     str_archi)
            
    return current_impls[ttype]
