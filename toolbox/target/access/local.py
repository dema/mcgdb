import gdb
import os
import subprocess 

def init(my_gdb_pkg):
    pass

def access(path, mode):
    return os.access(path, mode)

def exists(path):
    return os.exists(path)

def listdir(path):
    return os.listdir(path)

def readlink(path):
    return os.readlink(path)

class path:
    def __init__(self):
        pass
    
    @staticmethod
    def exists(path):
        return os.path.exists(path)

F_OK = os.F_OK
class Popen():
    def __init__(self, args):
        self.proc = subprocess.Popen(args, stdout=subprocess.PIPE,
                                     stderr=subprocess.PIPE)
    
    def communicate(self):
        return self.proc.communicate()
        
    def __getattr__(self, name):
        if name in ["returncode", "pid"]:
            return getattr(self.proc, name)
        else:
            raise AttributeError("Can't access attribute '%s' of subprocess" %
                                 name)
            
class Fopen:
    def __init__(self, path, mode):
        self.file = open(path, mode)
        
    def readlines(self):
        return self.file.readlines()
    
def do_attach(pid):
    gdb.execute ("attach %s" % pid, to_string=True)
