import gdb

pushy = None

os = None
subprocess = None
F_OK = None

def init(my_gdb_pkg):
    global push
    import pushy # https://github.com/pushyrpc/pushy
    conn = pushy.connect("ssh:localhost")

    global os, subprocess, F_OK
    os = conn.modules.os
    subprocess = conn.modules.subprocess
    
    F_OK = os.F_OK

###

def access(path, mode):
    return os.access(path, mode)

def exists(path):
    return os.exists(path)

def listdir(path):
    return os.listdir(path)

def readlink(path):
    return os.readlink(path)

class path:
    def __init__(self):
        pass
    
    @staticmethod
    def exists(path):
        return os.path.exists(path)

class Popen():
    def __init__(self, args):
        self.proc = subprocess.Popen(args, stdout=subprocess.PIPE, 
                                     stderr=subprocess.PIPE)
    
    def communicate(self):
        return self.proc.communicate()
        
    def __getattr__(self, name):
        if name in ["returncode", "pid"]:
            return getattr(self.proc, name)
        else:
            raise AttributeError("Can't access attribute '%s' of subprocess" % 
                                 name)
            
class Fopen:
    def __init__(self, path, mode):
        self.file = open(path, mode)
        
    def readlines(self):
        return self.file.readlines()
    
def do_attach(pid):
    gdb.execute ("attach %s" % pid, to_string=True)
