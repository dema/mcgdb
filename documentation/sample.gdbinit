## To load mcgdb environment:

python 

### mcgdb Python package must be loadable:
sys.path.insert(0, "/path/to/mcgdb/parent/directory")
###

try:
    import mcgdb
    mcgdb.initialize_by_name()
except Exception as e:
    import traceback
    print ("Couldn't load Model-Centric Debugging: %s" % e)
    traceback.print_exc()
###

end

## almost mandatory:

set height 0
set width 0

## for conveniance:

set breakpoint pending on
set print pretty
set confirm off

# for debugging

set python print-stack full


