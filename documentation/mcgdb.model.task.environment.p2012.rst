P2012 package
=============

.. toctree::
   :maxdepth: 1

   mcgdb.model.task.environment.p2012.capture
   mcgdb.model.task.environment.p2012.interaction
   mcgdb.model.task.environment.p2012.toolbox

Environments
------------

.. toctree::
   :maxdepth: 1

   mcgdb.model.task.environment.p2012.npm
   mcgdb.model.task.environment.p2012.pedf
