archi package
==================================

i386 module
--------------------------------------

.. automodule:: mcgdb.toolbox.target.archi.i386
    :members:
    :undoc-members:
    :show-inheritance:

x86_64 module
----------------------------------------

.. automodule:: mcgdb.toolbox.target.archi.x86_64
    :members:
    :undoc-members:
    :show-inheritance:
