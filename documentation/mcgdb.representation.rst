Representation package
============================

Module contents
---------------

.. automodule:: mcgdb.representation
    :members:
    :undoc-members:
    :show-inheritance:

