Example: `mcGDB` support from scratch
=====================================

The easiest way to document `mcGDB` extension methodology might be
with an detailed illustration. So in this document, we'll see, step by
step, how to build an `mcGDB` extension for our own programming
environment.

Task-Based Programming Environment and Application
--------------------------------------------------

In :download:`example/task-model.h` you'll find the public API of a
simple programming environment based on concurrent tasks
(implementation is in :download:`example/task-model.c`):

.. include:: example/task-model.h
   :code: c
   :start-line: 7


And :download:`example/appli.c` contains an application running on top
of this environment:


.. include:: example/appli.c
   :code: c
   :start-line: 1

Fairly easy and straightforward, everything is sequential,
:c:func:`task_run` does barely nothing and body functions are actually
executed inside :c:func:`task_get_result`.

`mcGDB` Debugging Support
-------------------------

`mcGDB` debugging support is implemented inside
:download:`example/task_model_debugging.py`.

Initialization
..............

To load you Python module, you need to these two commands (or similar)
in GDB:

.. code-block:: python
   :linenos:
         
   python sys.path.append(".")
   python import task_model_debugging

Line 1 tells Python where to look for your packages, and line 2
actually loads it.

For the example, I put these lines into :download:`example/gdbinit`,
and run GDB with `gdb -ex "source ./gdbinit"` (for security reasons,
GDB may not want to load `.gdbinit` from an untrusted directory).

Capture Module
..............

Let's look at the function :c:data:`struct task_s *
task_new(task_body_f body);` capture breakpoint: :py:class:`~task_model_debugging.TaskNewBreakpoint`:

.. include:: example/task_model_debugging.py
  :code: python
  :start-after: ## <Documentation capture example>
  :end-before: ## </Documentation capture example>

In :py:meth:`~task_model_debugging.TaskNewBreakpoint.prepare_before`,
we capture the first parameter, corresponding to the body function of
the task. We also set a dynamic breakpoint, at the address of task
body function.

In :py:meth:`~task_model_debugging.TaskNewBreakpoint.prepare_after`,
we capture the return parameter, corresponding to the task handler.
The call to :py:meth:`~task_model_debugging.debug_capture` remains (on
purpose) from the early stage of the development: it was used to
ensure that the breakpoint was correctly triggered, and the parameters
successfully captured.

Lastly, we pass these parameters to the **representation** module, by
creating a new :py:class:`~task_model_debugging.Task` instance.

All the other capture breakpoints are based on the same template:

.. include:: example/task_model_debugging.py
  :code: python
  :start-after: ## <Documentation capture 2nd example>
  :end-before: ## </Documentation capture 2nd example>

Representation Module
.....................

The **representation** module consists here of a single class
definition, :py:class:`~task_model_debugging.Task`:

.. include:: example/task_model_debugging.py
  :code: python
  :start-after: ## <Documentation representation example>
  :end-before: ## </Documentation representation example>

There is a more-or-less one-to-one mapping between the **capture** breakpoints
and methods of class :py:class:`~task_model_debugging.Task`:

* :py:class:`~task_model_debugging.TaskBodyExecutionBreakpoint` calls
  :py:meth:`~task_model_debugging.Task.start_execution` and :py:meth:`~task_model_debugging.Task.finish_execution`,
* :py:class:`~task_model_debugging.TaskRunBreakpoint` calls
  :py:meth:`~task_model_debugging.Task.run`
* :py:class:`~task_model_debugging.TaskDestroyedBreakpoint` calls
  :py:meth:`~task_model_debugging.Task.destroy`
* ...

.. Note:: 

   The code in this class is independent of the actual implementation
   of the supportive environment, another environment based on the
   same model could reuse this class.

Interaction Module
..................

The last consist in implementing the user **interaction** module. In
this example that's fairly simple and limited. 

First, for listing the tasks of the application, we browse the
:py:attr:`task_model_debugging.Task.tasks` list and display the
different parameters of the task instances:

.. include:: example/task_model_debugging.py
  :code: python
  :start-after: ## <Documentation interaction example>
  :end-before: ## </Documentation interaction example>

The second example offers catchpoints on the task body execution. It
parses the parameters to select specific tasks, or all of them, and
then activates the catchpoints in :py:attr:`mcgdb.toolbox.catchable`.

.. include:: example/task_model_debugging.py
  :code: python
  :start-after: ## <Documentation interaction 2nd example>
  :end-before: ## </Documentation interaction 2nd example>

The second part of this mechanism is implemented in
:py:meth:`task_model_debugging.Task.start_execution`, when a stop
request is registered
(:py:meth:`mcgdb.capture.FunctionBreakpoint.push_stop_request`) when a
task with a catchpoint starts its execution:

.. include:: example/task_model_debugging.py
  :code: python
  :start-after: ## <Documentation representation start_exec example>
  :end-before: ## </Documentation representation start_exec example>

Python Code for Debugging Support
---------------------------------

Module task_model_debugging
...........................

.. automodule:: task_model_debugging
    :members:
    :undoc-members:
    :show-inheritance:

Module `mcgdb_lite`
...................

.. automodule:: mcgdb_lite
    :members:
    :undoc-members:
    :show-inheritance:
