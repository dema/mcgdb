NPM package
===========

.. toctree::
   :maxdepth: 2

   mcgdb.model.task.environment.p2012.npm.capture
   mcgdb.model.task.environment.p2012.npm.representation
   mcgdb.model.task.environment.p2012.npm.toolbox
   mcgdb.model.task.environment.p2012.npm.user
