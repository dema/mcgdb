#include <stdlib.h>

struct task_s;
struct task_future_int_s;

typedef int (*task_body_f)(void *);

struct task_s *
task_new(task_body_f body);

void
task_set_dependency(struct task_s *src, struct task_s *dst);

void
task_run(struct task_s *task);

int
task_get_future(struct task_future_int_s *future);

int
task_get_result(struct task_s *task);

void
task_destroy(struct task_s *task);
