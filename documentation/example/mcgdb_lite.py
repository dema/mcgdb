import gdb

class capture:
    class FunctionBreakpoint(gdb.Breakpoint):
        breakpointed = {}
        stop_requests = []
        
        def __init__ (self, spec):
            gdb.Breakpoint.__init__ (self, spec, internal=True)
            self.silent = True

        def stop (self):
            ret = self.prepare_before()
            if ret is None:
                #spurious stop
                return False

            (fct_stop, fct_finish, fct_data) = ret
                
            if fct_finish:
                capture.FunctionFinishBreakpoint(self, fct_data)

            while capture.FunctionBreakpoint.stop_requests:
                print(capture.FunctionBreakpoint.stop_requests.pop())
                fct_stop = True
                
            return fct_stop

        def prepare_before(self):
            return None

        def prepare_after(self, data):
            return False

        @classmethod
        def push_stop_request(clazz, msg):
            clazz.stop_requests.append(msg)
            
    class FunctionFinishBreakpoint (gdb.Breakpoint):
        def __init__ (self, parent, fct_data):
            gdb.Breakpoint.__init__(self, "*%s" % gdb.newest_frame().older().pc(), internal=True)
            #gdb.FinishBreakpoint.__init__(self, "*%s" % gdb.newest_frame(), internal=True)
            self.silent = True
            self.parent = parent
            self.fct_data = fct_data
            
        def stop(self):
            self.enabled = False
            
            fct_stop = self.parent.prepare_after(self.fct_data)
            
            gdb.post_event(self.delete)
            
            return fct_stop

        def out_of_scope(self):
            pass

        
class toolbox:
    class Catchable:
        catch = {}
        ALL_ENTITIES = "<all entities>"
        
        @staticmethod
        def register(name):
            toolbox.Catchable.catch[name] = set()

        @staticmethod
        def activateRemove(name, entity, do_add):
            if do_add:
                toolbox.Catchable.activate(name, entity)
            else:
                toolbox.Catchable.remove(name, entity)
        
        @staticmethod
        def activate(name, entity):
            lst = toolbox.Catchable.catch[name]
            if entity is None or entity is toolbox.Catchable.ALL_ENTITIES:
                entity = toolbox.Catchable.ALL_ENTITIES
                while len(lst):
                    lst.pop()
            lst.add(entity)
            
        @staticmethod
        def remove(name, entity):
            lst = toolbox.Catchable.catch[name]
            if entity is None:
                while len(lst) != 0:
                    lst.pop()
            else:
                toolbox.Catchable.catch[name].remove(entity)

        @staticmethod
        def is_set(name, entity=None):
            return (toolbox.Catchable.ALL_ENTITIES in toolbox.Catchable.catch[name]
                    or entity in toolbox.Catchable.catch[name])

    catchable = Catchable()
