#include <stdio.h>
#include "task-model.h"

int produce(void *not_used) {
  return 4; // chosen by fair dice roll.
            // guaranteed to be random
            /* http://xkcd.com/221/ */
}

int consume(void *data) {
  int result = 0;
  struct task_future_int_s *future_input = (struct task_future_int_s *) data;
  
  /* prepare computation */
  result -= 6;

  /* consume input */
  result += task_get_future(future_input) * 12;

  return result;;
}

int main(void) {
  struct task_s *prod = task_new(produce);
  struct task_s *cons = task_new(consume);

  task_set_dependency(NULL, prod);
  task_set_dependency(prod, cons);
  
  task_run(prod);
  task_run(cons);

  printf("Async result: %d\n", task_get_result(cons));
  
  task_destroy(prod);
  task_destroy(cons);
}
