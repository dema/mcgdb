OpenCL package
==============

.. toctree::

    mcgdb.model.gpu.environment.opencl.capture

Representation
~~~~~~~~~~~~~~

.. automodule:: mcgdb.model.gpu.environment.opencl.representation
    :members:
    :undoc-members:
    :show-inheritance:

User module
~~~~~~~~~~~

.. automodule:: mcgdb.model.gpu.user
    :members:
    :undoc-members:
    :show-inheritance:
