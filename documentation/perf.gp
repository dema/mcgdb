set terminal postscript eps enhanced color "Helvetica" 50
set output "results.eps"

# set key width -5 

set size 6.0,5.0
set multiplot 

set label "(mc)GDB BREAKPOINT COSTS" at screen 0.6,3 center front
set label "(in useconds)" at screen 0.6,2.9 center front

set key left box
set grid xtics ytics mxtics mytics lc rgb 'gray70' lt 1, lc rgb 'grey90' lt 1
unset xtics
set key outside

set origin 0.0,2.0
set size 3.3,3.0
set title "Native GDB and Python"
set key lmargin
plot for [col=1:7] 'perf.mcgdb.testing.native_gdb.csv' using 0:col with lines title columnheader lw 3, \
     for [col=1:2] 'perf.mcgdb.testing.mc_gdb.csv' using 0:col with lines title columnheader lw 3
     

set origin 2,3.5
set size 4,1.5
set title "OpenMP"
unset key 
set label "nominal time" at screen 4.7,4 center front
plot for [col=1:6] 'perf.mcgdb.model.task.environment.openmp.testing.no_instrumentation.csv' \
     using 0:col with lines title columnheader lw 3


set origin 3.1,1.8
set size 2.9,1.7
set label "mcGDB time (+BP hit count)" at screen 4.7,2.6 center front
unset title
set key below
plot for [col=1:6] 'perf.mcgdb.model.task.environment.openmp.testing.benchmark.csv' \
     using 0:col with lines title columnheader lw 3

set origin 0.0,0.0
set size 6.0,2.0

set title "All together"
plot for [col=1:7] 'perf.mcgdb.testing.native_gdb.csv' using 0:col with lines title columnheader lw 3, \
     for [col=1:2] 'perf.mcgdb.testing.mc_gdb.csv' using 0:col with lines title columnheader lw 3, \
     for [col=1:6] 'perf.mcgdb.model.task.environment.openmp.testing.benchmark.csv' \
         using 0:col with lines title columnheader lw 3