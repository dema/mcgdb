OpenMP package
==============

Capture Module
--------------

.. automodule:: mcgdb.model.task.environment.openmp.capture.gomp
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: mcgdb.model.task.environment.openmp.capture.iomp
    :members:
    :undoc-members:
    :show-inheritance:


Representation Module
---------------------

.. automodule:: mcgdb.model.task.environment.openmp.representation
    :members:
    :undoc-members:
    :show-inheritance:

Interaction Module
------------------

.. automodule:: mcgdb.model.task.environment.openmp.interaction
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: mcgdb.model.task.environment.openmp.interaction.step
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: mcgdb.model.task.environment.openmp.interaction.sequence
    :members:
    :undoc-members:
    :show-inheritance:

Toolbox Module
--------------

.. automodule:: mcgdb.model.task.environment.openmp.toolbox
    :members:
    :undoc-members:
    :show-inheritance:
