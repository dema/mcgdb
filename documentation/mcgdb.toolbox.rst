Toolbox package
=====================

Catchpointing
-------------

.. autoclass:: mcgdb.toolbox.Catchable
    :members:
    :undoc-members:
    :show-inheritance:

.. autoattribute:: mcgdb.toolbox.catchable

Target subpackage
-----------------

.. toctree::
   :maxdepth: 2

   mcgdb.toolbox.target

My gdb module
-------------

.. automodule:: mcgdb.toolbox.my_gdb
    :members:
    :undoc-members:
    :show-inheritance:

Paje module
-----------

.. automodule:: mcgdb.toolbox.paje
    :members:
    :undoc-members:
    :show-inheritance:

Python utils module
-------------------

.. automodule:: mcgdb.toolbox.python_utils
    :members:
    :undoc-members:
    :show-inheritance:
