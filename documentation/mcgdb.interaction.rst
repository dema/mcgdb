Interaction package
===================

NB: Argument parsing show be written with the help of gdb.string_to_argv
(https://sourceware.org/gdb/current/onlinedocs/gdb/Commands-In-Python.html#Commands-In-Python)

Module contents
---------------

.. automodule:: mcgdb.interaction
    :members:
    :undoc-members:
    :show-inheritance:

Messages module
---------------

.. automodule:: mcgdb.interaction.messages
    :members:
    :undoc-members:
    :show-inheritance:

My gdb module
-------------

.. automodule:: mcgdb.interaction.my_gdb
    :members:
    :undoc-members:
    :show-inheritance:

Target module
-------------

.. automodule:: mcgdb.interaction.target
    :members:
    :undoc-members:
    :show-inheritance:

Miscellaneous
-------------

Linux socket utils module
~~~~~~~~~~~~~~~~~~~~~~~~~

.. automodule:: mcgdb.interaction.linux_socket_utils
    :members:
    :undoc-members:
    :show-inheritance:
