Python Programming Interface
============================

Generic Toolset
---------------

.. toctree::
   :maxdepth: 15

   mcgdb
   mcgdb.capture
   mcgdb.interaction
   mcgdb.representation
   mcgdb.toolbox
   
Model-Specific Toolsets
-----------------------

Task-Based Models
~~~~~~~~~~~~~~~~~

.. toctree::
   :maxdepth: 15

   mcgdb.model.task
   mcgdb.model.task.interaction
   
P2012 Task-Based Environments
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 15
   
   mcgdb.model.task.environment.p2012.npm
   mcgdb.model.task.environment.p2012.pedf

GPU-Based Models
~~~~~~~~~~~~~~~~

.. toctree::
   :maxdepth: 15

   mcgdb.model.gpu

GPU-Based Environments
^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 15

   mcgdb.model.gpu.environment.opencl
   mcgdb.model.gpu.environment.cuda
