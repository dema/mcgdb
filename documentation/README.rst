Installation
============

Load mcGDB from GDB
-------------------

Put in `.gdbinit`:

.. code-block:: python

    python 
    sys.path.append("/path/to/Python")
    try:
      import mcgdb
      #mcgdb.initialize()
      mcgdb.initialize_by_name()
    except Exception as e:
      import traceback
      print ("Couldn't load Model-Centric Debugging: %s" % e)
      traceback.print_exc()
    end

Put in your `$PATH`:

    ln -s gdb mcgdb
    ln -s mcgdb mcgdb-omp

Test and benchmark mcGDB
------------------------

With `sys.path` correctly configured, run:

.. code-block:: python

    import mcgdb.testing
    mcgdb.testing.run_tests()
    mcgdb.testing.run_benchmark()

or from command-line:

.. code-block:: bash

    python3 -c 'import mcgdb.testing; mcgdb.testing.run_tests(); mcgdb.testing.run_benchmark()'

Generate mcGDB documentation
----------------------------

.. code-block:: bash

    cd /path/to/mcgdb
    cd documentation
    make html
    # or
    make -f /path/to/mcgdb/documentation/Makefile html

Requirements
------------

- Logging:
    + Colorlog https://pypi.python.org/pypi/colorlog (recommended)
- GDB internal thread safety:
    + Pysigset https://pypi.python.org/pypi/pysigset/ (recommended)
- Task/OpenMP
    + Enum34 https://pypi.python.org/pypi/enum34 (Python2 only)
    + Graph (one package--not currently in use)
        - Networkx https://pypi.python.org/pypi/networkx/ (optional)
        - PyGraphViz https://pypi.python.org/pypi/pygraphviz (optional)
    + Sequence Diagram
        - Seqdiag (mcGDB version)
- GPU/OpenCL
    + pyparsing https://pypi.python.org/pypi/pyparsing (optional)
- Toolbox/Target
    + Access/ssh
        - Pushy https://pypi.python.org/pypi/pushy
- Documentation
    + Rendering
        + Sphinx https://pypi.python.org/pypi/Sphinx
        + Sphinx RTD theme https://pypi.python.org/pypi/sphinx_rtd_theme (optional)

Convenience with GDB/mcGDB
--------------------------

Add these lines to your `.gdbinit`:

.. code-block:: bash

   ## almost mandatory:

   set height 0
   set width 0

   ## for conveniance:

   set breakpoint pending on
   set print pretty
   set confirm off

   # for debugging

   set python print-stack full

Generate mcGDB archive
----------------------

.. code-block:: bash
    
   git archive --format=tar.gz -o /path/to/mcgdb.tgz --prefix=$PREFIX/mcgdb/ master 
