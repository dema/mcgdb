Low-level Tracing
-----------------

Event and parameter tracing
~~~~~~~~~~~~~~~~~~~~~~~~~~~

As part of the SocTrace project, we implemented an initial support of
low-level (i.e. raw) tracing of the model events required for building
the model-centric representation of the application.

In practice, this means that, for all the events (i.e. breakpoints)
caught my `mcGDB`, we log the parameters we had to read to update our
internal representation.

All these information are machine and environment-implementation
dependent, hence the tracing is done from the **capture** modules,
based on a simple convention:

.. code-block:: python

    class clCreateBufferBP(mcgdb.capture.FunctionBreakpoint):
        [...]  

        def prepare_before (self):
           data = {}
           data["mode"] = my_archi.nth_arg(2, my_archi.INT)
           data["retcode_p"] = my_archi.nth_arg(5, my_archi.INT_P)
           [...]

           return (False, True, data)

       def prepare_after(self, data):
           data["ret"] =  my_archi.return_value(my_archi.VOID_P)
           [...]

in :py:meth:`mcgdb.capture.FunctionBreakpoint.prepare_before`, the
third parameter returned by the function, corresponding to the data
transmitted to the
:py:meth:`~mcgdb.capture.FunctionBreakpoint.prepare_after`, has to be
a `dict` object. If this is the case, then the breakpoint classname
and the content of this dictionary will be sent to the tracing
module. Likewise, method
:py:meth:`~mcgdb.capture.FunctionBreakpoint.prepare_after` can extend
the content of the dictionary with new values, and the function finish
event and parameters will be passed to the tracing module.

Tracing Implementation
~~~~~~~~~~~~~~~~~~~~~~

The trace generation is implemented in `mcGDB`
:py:mod:`mcgdb.toolbox.paje` module. This module exports two public
methods, :py:meth:`~mcgdb.toolbox.paje.before` and
:py:meth:`~mcgdb.toolbox.paje.after`, that are hooked into
:py:meth:`mcgdb.capture.FunctionBreakpoint.stop` and
:py:meth:`mcgdb.capture.FunctionFinishBreakpoint.stop`, respectively.

Module :py:mod:`mcgdb.toolbox.paje`, as the name suggests, generated
traces in the Paje format, thanks to `libpoti` shared library. 

.. seealso::

   * Paje format
   * libpoti library
   * mcgdb.toolbox.paje.LIBPOTI path configuration

Tracing Command-line Interface
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In GDB, run `paje_print [filename]` to dump the current content of the
tracer.

.. seealso::

   * Paje CLI implementation :py:class:`mcgdb.toolbox.paje.cmd_printPaje`

.. note::
   
   This support is still experimental. High-level tracing can also be
   interesting, for instance for graphical execution
   visualization. See for instance
   :py:class:`mcgdb.model.gpu.representation.Event` and
   :py:class:`mcgdb.model.gpu.interaction.cmd_PrintFlow`. (The trace
   reader, an Eclipse plugin, is still ST internal.)
