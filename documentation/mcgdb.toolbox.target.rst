Target package
==============

My archi module
---------------

.. automodule:: mcgdb.toolbox.target.my_archi
    :members:
    :undoc-members:
    :show-inheritance:

My access module
----------------

.. automodule:: mcgdb.toolbox.target.my_access
    :members:
    :undoc-members:
    :show-inheritance:

My system module
----------------

.. automodule:: mcgdb.toolbox.target.my_system
    :members:
    :undoc-members:
    :show-inheritance:

Target implementations
----------------------

.. toctree::

    mcgdb.toolbox.target.access
    mcgdb.toolbox.target.archi
    mcgdb.toolbox.target.system

Module contents
---------------

.. automodule:: mcgdb.toolbox.target
    :members:
    :undoc-members:
    :show-inheritance:

Miscellaneous
-------------

System utils module
~~~~~~~~~~~~~~~~~~~

.. automodule:: mcgdb.toolbox.target.system_utils
    :members:
    :undoc-members:
    :show-inheritance:
