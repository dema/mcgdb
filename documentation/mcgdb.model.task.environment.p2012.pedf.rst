PEDF package
============

Capture module
--------------

.. toctree::
   :maxdepth: 2

   mcgdb.model.task.environment.p2012.pedf.capture

Representation module
---------------------
        
.. automodule:: mcgdb.model.task.environment.p2012.pedf.representation
    :members:
    :undoc-members:
    :show-inheritance:
