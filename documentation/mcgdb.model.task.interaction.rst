Interaction package
===================


Components module
-----------------

.. automodule:: mcgdb.model.task.interaction.components
    :members:
    :undoc-members:
    :show-inheritance:

Messages module
---------------

.. automodule:: mcgdb.model.task.interaction.messages
    :members:
    :undoc-members:
    :show-inheritance:

Simu module
-----------

.. automodule:: mcgdb.model.task.interaction.simu
    :members:
    :undoc-members:
    :show-inheritance:

Simu gepop cli module
---------------------

.. automodule:: mcgdb.model.task.interaction.simu.gepop_cli
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: mcgdb.model.task.interaction
    :members:
    :undoc-members:
    :show-inheritance:
