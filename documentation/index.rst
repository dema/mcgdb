A Programming Model-Centric Debugger: `mcGDB`
=============================================

`mcGDB` is a GDB+Python implementation of `Programming-Model Centric Debugging`_,
from Kevin Pouget's PhD thesis work.

Last Developments
-----------------

.. toctree::
   :maxdepth: 2

   openmp

.. toctree::
   :maxdepth: 3

   perf_debugging

Modular Architecture
--------------------

`mcGDB` has a modular architecture, that can be extended to support
new programming models and environments:

- mcgdb_ module provides a set of generic tools_, abstractions_, and
  interaction_ mechanisms that can (and should) be reused to implement
  model-specific sub-modules.
- `mcgdb.model` holds the model-specific submodules. Currently, we
  provide:

  * mcgdb.model.task.environments.openmp: our current target (still
    under development):

    - `OpenMP`_ programming environment

  * mcgdb.model.gpu_: module for kernel-based GPU programming, with
    two environment support:

    - OpenCL_
    - Cuda_

  * mcgdb.model.task_: module for programming models based on
    interconnected tasks, with two `P2012 environments`_
    **(outdated)**:
    
    - `PEDF and Dataflow`_ programming
    - `NPM and Component`_ programming


- `Performance evaluation`_ of mcGDB.

.. _Programming-Model Centric Debugging: http://tel.archives-ouvertes.fr/tel-01010061/en/
.. _mcgdb: mcgdb.generic.html
.. _tools: mcgdb.toolbox.html
.. _abstractions: mcgdb.representation.html
.. _interaction: mcgdb.interaction.html

.. _mcgdb.model.gpu: mcgdb.model.gpu.html
.. _mcgdb.model.task: mcgdb.model.task.html

.. _OpenCL: mcgdb.model.gpu.environment.opencl.html
.. _Cuda: mcgdb.model.gpu.environment.cuda.html

.. _P2012 environments: mcgdb.model.task.environment.p2012.html
.. _PEDF and Dataflow: mcgdb.model.task.environment.p2012.pedf.html
.. _NPM and Component: mcgdb.model.task.environment.p2012.npm.html
.. _OpenMP: mcgdb.model.task.environment.openmp.html
.. _Performance evaluation: perf.html


Content
-------

This documentation is organized as follows:

.. toctree::

   README

.. toctree::

   extending

.. toctree::
   
   openmp

.. toctree::
   :maxdepth: 3

   api

.. toctree::
   
   perf

.. toctree::
   :maxdepth: 3

   traces

.. toctree::
   :maxdepth: 3

   example

Documentation generated on |today| from |release|.
