Task package
============

.. toctree::
    :maxdepth: 3

    mcgdb.model.task.interaction
    mcgdb.model.task.environment

Module contents
---------------

.. automodule:: mcgdb.model.task
    :members:
    :undoc-members:
    :show-inheritance:

Representation
~~~~~~~~~~~~~~

.. automodule:: mcgdb.model.task.representation
    :members:
    :undoc-members:
    :show-inheritance:

Toolbox
~~~~~~~

.. automodule:: mcgdb.model.task.toolbox.tasks
    :members:
    :undoc-members:
    :show-inheritance:

