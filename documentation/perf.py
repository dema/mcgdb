#! /usr/bin/python3

import os, sys        

CURRENT_DIR = os.path.dirname(os.path.realpath(__file__))

sys.path.append(CURRENT_DIR.replace("/mcgdb/documentation", ""))

import mcgdb

import gdb_tester

testing.GDB_Instance.CSV_REPEAT = 10
DO_RUN = True

class replace:
    def __init__(self, name):
        self.file = open(name, "w")
        
    def __enter__(self):
        self.file, sys.stdout = sys.stdout, self.file
        pass

    def __exit__(self, type, value, traceback):
        self.file, sys.stdout = sys.stdout, self.file
        self.file.close()
        pass

        
PACKAGES = ["mcgdb.testing.mc_gdb",
    "mcgdb.testing.native_gdb",
    "mcgdb.model.task.environment.openmp.testing.benchmark",
    "mcgdb.model.task.environment.openmp.testing.no_instrumentation"
    ]

for pkg in PACKAGES:
    print("Running {} ...".format(pkg))

    filename = "perf.{}.csv".format(pkg)
    if DO_RUN:
        with replace(filename):
            mcgdb.run_benchmark(pkg_prefix=pkg)
            
    os.system("sed -i 's/ 0us/ /g' '{}'".format(filename))

print("Plotting the figure ...")
os.system("gnuplot perf.gp")
print("Done.")
