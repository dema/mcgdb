Representation package
======================


Module contents
---------------

.. automodule:: mcgdb.model.task.environment.p2012.npm.representation
    :members:
    :undoc-members:
    :show-inheritance:

DMA link module
---------------

.. automodule:: mcgdb.model.task.environment.p2012.npm.representation.dma_link
    :members:
    :undoc-members:
    :show-inheritance:

FIFO link module
----------------

.. automodule:: mcgdb.model.task.environment.p2012.npm.representation.fifo_links
    :members:
    :undoc-members:
    :show-inheritance:

