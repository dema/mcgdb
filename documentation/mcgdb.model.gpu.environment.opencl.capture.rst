Capture package
===============

clCreateBuffer module
----------------------------------------------------------------

.. automodule:: mcgdb.model.gpu.environment.opencl.capture.clCreateBuffer
    :members:
    :undoc-members:
    :show-inheritance:

clCreateKernel module
----------------------------------------------------------------

.. automodule:: mcgdb.model.gpu.environment.opencl.capture.clCreateKernel
    :members:
    :undoc-members:
    :show-inheritance:

clEnqueueNDRangeKernel module
------------------------------------------------------------------------

.. automodule:: mcgdb.model.gpu.environment.opencl.capture.clEnqueueNDRangeKernel
    :members:
    :undoc-members:
    :show-inheritance:

clEnqueueReadWriteBuffer module
--------------------------------------------------------------------------

.. automodule:: mcgdb.model.gpu.environment.opencl.capture.clEnqueueReadWriteBuffer
    :members:
    :undoc-members:
    :show-inheritance:

clFinish module
----------------------------------------------------------

.. automodule:: mcgdb.model.gpu.environment.opencl.capture.clFinish
    :members:
    :undoc-members:
    :show-inheritance:

clReleaseKernel module
-----------------------------------------------------------------

.. automodule:: mcgdb.model.gpu.environment.opencl.capture.clReleaseKernel
    :members:
    :undoc-members:
    :show-inheritance:

clReleaseMemObject module
--------------------------------------------------------------------

.. automodule:: mcgdb.model.gpu.environment.opencl.capture.clReleaseMemObject
    :members:
    :undoc-members:
    :show-inheritance:

clSetKernelArg module
----------------------------------------------------------------

.. automodule:: mcgdb.model.gpu.environment.opencl.capture.clSetKernelArg
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: mcgdb.model.gpu.environment.opencl.capture
    :members:
    :undoc-members:
    :show-inheritance:
