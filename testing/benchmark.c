#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>

/***********/
// modified by debugging, do not change to macro.
int repeat = 2;

int it_count = 1000;
int us_sleep_time = 100;

/***********/

void finish_data_ready(void) {}

void finish(struct timeval x, struct timeval y) {
  
  static float it_total;
  static float ms_busy_time, us_busy_once;
  static float ms_total_sleep;
  static float ms_time;
  {
    static double x_us, y_us;

    x_us = (double) x.tv_sec*1000000 + (double) x.tv_usec;
    y_us = (double) y.tv_sec*1000000 + (double) y.tv_usec;

    it_total = repeat * it_count;
    
    ms_time = (y_us - x_us)/1000;
    ms_total_sleep = us_sleep_time*(it_total/1000.0);
    ms_busy_time = ms_time - ms_total_sleep;
    us_busy_once = ms_busy_time * 1000 / it_total;
  }

  finish_data_ready();
  
  printf("Repeat: %d; Loop: %d; usleep %dus (one) %1.fms (total)\n",
         repeat, it_count, us_sleep_time, ms_total_sleep);
  printf("------------------\n");
  printf("Total time: %1.fms\n", ms_time);
  printf("Busy time : %1.fms\n", ms_busy_time);
  printf("Busy once : %1.fus\n", us_busy_once);
}

void action(int it) {
  usleep(us_sleep_time);
}

void benchmark(void) {
  static int i;
  
  for (i = 0; i < it_count; ++i) {
    action(i);
  }
}

int main(int argc, char** argv) {
    struct timeval before , after;
    int i;
    
    benchmark(); // warm-up
    
    gettimeofday(&before , NULL);
    
    for (i = 0; i < repeat; ++i) {
      benchmark();
    }

    gettimeofday(&after , NULL);

    finish(before, after);
    
    return EXIT_SUCCESS;
}
