from gdb_tester import *
import mcgdb

gdb = None

def run(what, run="run"):
    gdb.set_title(what)
    gdb.execute("break finish_data_ready")
    gdb.execute(run)
    gdb.execute("up")

    gdb.save_value("us_busy_once", "%1.f", "us")

def nominal_time():
    run("Nominal time")

def gdb_watchpoint():
    gdb.execute("break benchmark")
    gdb.execute("run")
    gdb.execute("print &i")
    gdb.execute("watch *$1")
    gdb.execute("""command
silent
continue
end""")
    gdb.execute("py [b for b in gdb.breakpoints() if b.location == 'benchmark'][0].delete()")
    
    run("HW Watchpoint command", run="continue")
    
def gdb_breakpoint():
    gdb.execute("break action")
    gdb.execute("""command
silent
continue
end""")
    run("Breakpoint command")

PARAMETERS = [
    'int(gdb.parse_and_eval("it"))',
    'int(gdb.newest_frame().older().read_var("i"))'
    ]
def gdb_py_breakpoint_parameter(params_choice):
    params = [par for i, par in enumerate(PARAMETERS) if params_choice & (1 << i)]
            
    def to_run():
        gdb.execute("""python
class TestBP(gdb.Breakpoint):
  def __init__(self):
    gdb.Breakpoint.__init__(self, "action")
    self.silent=True

  def stop(self):
    {params}
    return False

TestBP()
end
""".format(params="\n    ".join(params)))
        run("Py BP param {:02b}".format(params_choice)[2:])
        
    return to_run
        
def benchmark(_gdb, what=None, no_mcgdb=True):
    global gdb
    gdb = _gdb
    
    if what is None:
        what = [nominal_time, gdb_breakpoint, gdb_watchpoint] + \
                list(map(lambda x: gdb_py_breakpoint_parameter(x), (0b0, 0b1, 0b10, 0b11)))
    
    gdb.start(CSource("benchmark.c"), None if no_mcgdb else mcgdb.testing.gdb__init_mcgdb)
    for prepare_and_run in what:
        prepare_and_run()
        gdb.reset(hard=(not no_mcgdb))
        
    gdb.quit()
