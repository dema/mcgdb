#! /bin/bash

notice=$(cat NOTICE)
notice_py=$(echo $notice | sed 's+^+#  +')
notice_cpp=$(echo $notice | sed 's+^+//  '+)

apply() {
    f=$1
    notice=$2
    if grep "Copyright 2016" "$f" --quiet
    then
        echo "skip $f (already patched)"
        continue
    fi
    #echo $notice "$f" > "$f.new"
    #mv "$f.new" "$f"
    echo "License Header copied to $f"
}

find . -name '*.py' | while read f
do
    apply "$f" "$notice_py"
done

find . -name '*.cpp' -o -name '*.c' -o -name '*.h' | while read f
do
    if grep "LLVM Compiler Infrastructure" "$f" --quiet
    then
        echo "skip $f (llvm)"
        continue
    fi
    apply "$f" "$notice_py"
done
